/********************************************************************\
 * 
 *  Name:         felabview.cxx
 *  Created by:   K.Olchanski
 * 
 *  Contents:     ALPHA data collector for Labview data
 * 
 *  $Id$
 * 
 *  $Log$
 * 
 *  Revision 1.30  2007/06/11 20:22:38  alpha
 *  KO - fix indentation, no changesa
 * 
 *  Revision 1.29  2007/06/11 20:14:27  alpha
 *  unknown - add bank for APDT
 * 
 *  Revision 1.28  2007/06/08 22:27:59  alpha
 *  unknown - add data bank for ELEC2007
 * 
 *  Revision 1.27  2007/06/03 12:27:35  alpha
 *  KO - reindent code added by Richard
 *  KO - fix MAGNETEVENT handling (have to run elog through ssh)
 * 
 *  Revision 1.26  2007/05/15 17:57:44  alpha
 *  RH - Added banks for MAXIGAUGE, VIONICS TWICKENHAM, AMI136, RUSSIAN, THERMIST, CERNOX
 * 
 *  Revision 1.25  2007/04/13 16:21:24  alpha
 *  KO - in November 2006 persons unknown added banks LHVE and LEKI (incorrectly). Commit these changes.
 * 
 *  Revision 1.24  2006/11/02 14:44:09  alpha
 *  KO - add banks HVVD, HVVM, HVIM for the high voltage monitoring
 * 
 *  Revision 1.23  2006/10/31 14:22:30  alpha
 *  KO - fix case error in the magnet events logger
 * 
 *  Revision 1.22  2006/10/31 13:32:12  alpha
 *  KO - keep track of the run number in gRunNumber
 *  KO - add "magnetevent" sendinto into the elog
 *  JWS - LPROBES/LHPR data
 * 
 *  Revision 1.21  2006/10/25 16:20:18  alpha
 *  KO - added LFC0..LFC7
 *  KO - added checking of bank lengths
 * 
 *  Revision 1.20  2006/09/21 12:59:37  alpha
 *  RAH - Added Faraday Cup data handling, label 'FaradayCup', bank name LFC
 * 
 *  Revision 1.19  2006/09/10 13:45:01  alpha
 *  RAH - added data handling from AMI level sensor. Lable 'amitemp', bank LAMI
 * 
 *  Revision 1.18  2006/09/02 16:05:56  alpha
 *  RAH - Added BNL Magnet taps data handling, lable BNLTaps, bank BNLT
 * 
 *  Revision 1.17  2006/09/01 12:04:51  alpha
 *  RAH - Added data handling for AD equipment survey. Lable ADSurvey, bank name ADSV
 * 
 *  Revision 1.16  2006/08/13 20:32:58  alpha
 *  RAH - Added data handling for the OxfordITC-- Label is OxfordITC, bank name is LOXF
 * 
 *  Revision 1.15  2006/08/02 15:44:08  alpha
 *  RAH - Added labview handling for Beckhoff data. Label is BKFElectrode, bank name is LBKF
 * 
 *  Revision 1.14  2006/07/27 11:55:08  alpha
 *  RAH - Added Handling for BNL diode data. Label is 'BNLDiode' and bank name is 'BNLD'
 * 
 *  Revision 1.13  2006/07/25 15:48:21  alpha
 *  RAH - Custom now also changed the Factor and Offsets of the History graph aswell
 * 
 *  Revision 1.12  2006/07/25 13:37:00  alpha
 *  RAH - Added handling of labview data under the lable 'CustomLabviewSensors' that changes the history graph 'Custom' to show the requested sensors
 * 
 *  Revision 1.11  2006/07/19 15:33:43  alpha
 *  RAH - Added the filament and electron gun handling code
 * 1533 
 *  Revision 1.10  2006/07/17 18:16:09  alpha
 *  RAH- Added handling of Positron data using bank LPOS
 * 
 *  Revision 1.9  2006/07/17 13:09:39  alpha
 *  KO- do not create seperate log files for each labview data message- append them all into one file.
 * 
 *  Revision 1.8  2006/07/16 16:39:50  alpha
 *  RAH- Added code for the BNLE (BNL Magnet Environment) data
 * 
 *  Revision 1.7  2006/07/16 10:28:27  alpha
 *  RAH- Added the BNLMagnet with bank BNLM
 * 
 *  Revision 1.6  2006/07/11 18:17:58  alpha
 *  KO- add handler for " " AD data
 * 
 *  Revision 1.5  2006/07/10 13:00:54  alpha
 *  KO- debugged and tested with "vacuum" data from Alex.
 * 
 *  Revision 1.4  2006/07/07 19:43:19  alpha
 *  KO- fix problems with reading incomplete data from sockets, improve data parsing
 *  KO- this version is untested
 * 
 *  Revision 1.3  2006/06/19 05:22:30  alpha
 *  KO- use official event IDs
 *  KO- read labview port number from ODB
 * 
 *  Revision 1.2  2006/05/29 19:15:26  alpha
 *  Richard- all decoding of Vacuum data
 * 
 *  Revision 1.1  2006/05/25 05:53:42  alpha
 *  First commit
 * 
 * 
 * \********************************************************************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <assert.h>
#include <ctype.h>

#undef HAVE_ROOT
#undef USE_ROOT
#include "midas.h"
#include "evid.h"

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
  #endif
  
  /*-- Globals -------------------------------------------------------*/
  
  /* The frontend name (client name) as seen by other MIDAS clients   */
  char *frontend_name = "felabview";
  /* The frontend file name, don't change it */
  char *frontend_file_name = __FILE__;
  
  /* frontend_loop is called periodically if this variable is TRUE    */
  BOOL frontend_call_loop = FALSE;
  
  /* a frontend status page is displayed with this frequency in ms */
  INT display_period = 000;
  
  /* maximum event size produced by this frontend */
  INT max_event_size = 200*1024;
  
  /* maximum event size for fragmented events (EQ_FRAGMENTED) */
  INT max_event_size_frag = 1024*1024;
  
  /* buffer size to hold events */
  INT event_buffer_size = 500*1024;
  
  extern INT run_state;
  extern HNDLE hDB;
  
  /*-- Function declarations -----------------------------------------*/
  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop();
  
  INT read_event(char *pevent, INT off);
  int openListenSocket(int port);
  
  /*-- Bank definitions ----------------------------------------------*/
  
  /*-- Equipment list ------------------------------------------------*/
  
  EQUIPMENT equipment[] = {
    
    {"Labview",               /* equipment name */
    { EVID_LABVIEW, (1<<EVID_LABVIEW), /* event ID, trigger mask */
    "SYSTEM",               /* event buffer */
    EQ_PERIODIC,            /* equipment type */
    LAM_SOURCE(0, 0xFFFFFF),                      /* event source */
    "MIDAS",                /* format */
    TRUE,                   /* enabled */
    RO_ALWAYS|RO_ODB,       /* when to read this data */
    1,                      /* poll for this many ms */
    0,                      /* stop run after this event limit */
    0,                      /* number of sub events */
    1,                      /* whether to log history */
    "", "", "",}
    ,
    read_event,      /* readout routine */
    NULL, NULL,
    NULL,       /* bank list */
    }
    ,
    
    {""}
  };
  
  #ifdef __cplusplus
}
#endif
/********************************************************************\
 *	      Callback routines for system transitions
 * 
 *  These routines are called whenever a system transition like start/
 *  stop of a run occurs. The routines are called on the following
 *  occations:
 * 
 *  frontend_init:  When the frontend program is started. This routine
 *		  should initialize the hardware.
 * 
 *  frontend_exit:  When the frontend program is shut down. Can be used
 *		  to releas any locked resources like memory, commu-
 *		  nications ports etc.
 * 
 *  begin_of_run:   When a new run is started. Clear scalers, open
 *		  rungates, etc.
 * 
 *  end_of_run:     Called on a request to stop a run. Can send
 *		  end-of-run event and close run gates.
 * 
 *  pause_run:      When a run is paused. Should disable trigger events.
 * 
 *  resume_run:     When a run is resumed. Should enable trigger events.
 * \********************************************************************/

#include "utils.cxx"

static int gHaveRun = 0;
static int gRunNumber = 0;

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  int status;
  
  setbuf(stdout,NULL);
  setbuf(stderr,NULL);
  
  gRunNumber = odbReadInt("/runinfo/run number",0,0);
  printf("Run number %d\n", gRunNumber);
  
  status = openListenSocket(odbReadUint32("/equipment/labview/Settings/tcp_port",0,12021));
  if (status != FE_SUCCESS)
    return status;
  
  return FE_SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  gHaveRun = 0;
  
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  gHaveRun = 1;
  gRunNumber = run_number;
  printf("begin run %d\n",run_number);
  
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  static bool gInsideEndRun = false;
  
  if (gInsideEndRun)
  {
    printf("breaking recursive end_of_run()\n");
    return SUCCESS;
  }
  
  gInsideEndRun = true;
  
  gHaveRun = 0;
  gRunNumber = run_number;
  printf("end run %d\n",run_number);
  
  gInsideEndRun = false;
  
  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  gHaveRun = 0;
  gRunNumber = run_number;
  
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  gHaveRun = 1;
  gRunNumber = run_number;
  
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
   *    the frontend is idle or once between every event */
  printf("frontend_loop!\n");
  return SUCCESS;
}

/********************************************************************\
 * 
 *  Readout routines for different events
 * 
 * \********************************************************************/

#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

const int kMaxSockets = 100;
const int kBufSize = 100*1024;

struct LabviewSocket
{
  int fd;
  char buf[kBufSize];
  int  bufwptr;
  static int ofd;
  bool hasNewData;
  bool isOpen;
  struct sockaddr_in addrinfo;
  
  LabviewSocket(int socket) // ctor
  {
    fd = socket;
    bufwptr = 0;
    hasNewData = true;
    isOpen      = true;
    //ofd = 0;
    
    if (true && ofd <= 0)
    {
      time_t t = time(0);
      char fname[1024];
      sprintf(fname,"/data4/ebit/felabview/labview%d.dat",(int)t);
      ofd = open(fname,O_WRONLY|O_CREAT|O_LARGEFILE,0777);
      printf("Writing labview data to %s\n",fname);
    }
  }
  
  void closeSocket() // close socket
  {
    if (!isOpen)
      return;
    
    //printf("Socket %d closed\n",fd);
      
      if (fd > 0)
	close(fd);
      fd = 0;
      
      #if 0
      if (ofd > 0)
	close(ofd);
      ofd = 0;
      #endif
      
      isOpen = false;
      hasNewData = true;
  }
  
  ~LabviewSocket() // dtor
  {
    if (isOpen)
      closeSocket();
    //printf("Socket %d deleted\n",fd);
      bufwptr = 0;
      hasNewData = false;
  }
  
  int tryRead()
  {
    int avail = kBufSize - bufwptr;
    int rd = read(fd,buf+bufwptr,avail);
    
    //printf("read from socket %d yelds %d\n",fd,rd);
    
    if (rd == 0) // nothing to read, socket closed at the other end
      return 0;
    if (rd < 0) // error, close socket
      return -1;
    if (ofd > 0)
    {
      char hdr[1024];
      sprintf(hdr,"\n=== time: %d, socket: %d\n",(int)time(NULL),fd);
      write(ofd,hdr,strlen(hdr));
      write(ofd,buf+bufwptr,rd);
    }
    bufwptr += rd;
    socklen_t addrinfolen = sizeof(addrinfo);
    int status = getpeername(fd, (sockaddr*)(&addrinfo), &addrinfolen);
    if(status<0)
      printf("Getsockname error\n");
    buf[bufwptr] = 0;
    hasNewData = true;
    return rd;
  }
  
  void flush()
  {
    bufwptr = 0;
    hasNewData = false;
  }
};

int LabviewSocket::ofd = -1;

int gNumSockets = 0;
LabviewSocket* gSockets[kMaxSockets];

int gListenSocket = 0;
static int gPtr = 0;

int openListenSocket(int port)
{
  gListenSocket = socket(PF_INET,SOCK_STREAM,0);
  
  if (gListenSocket <= 0)
  {
    cm_msg(MERROR,"openListenSocket","Cannot create socket, errno %d (%s)",errno,strerror(errno));
    return FE_ERR_HW;
  }
  
  int on = 1;
  int status = setsockopt(gListenSocket,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));
  if (status != 0)
  {
    cm_msg(MERROR,"openListenSocket","Cannot setsockopt(gListenSocket,SOL_SOCKET,SO_REUSEADDR), errno %d (%s)",errno,strerror(errno));
    return FE_ERR_HW;
  }
  
  struct sockaddr_in xsockaddr;
  xsockaddr.sin_family = AF_INET;
  xsockaddr.sin_port = htons(port);
  xsockaddr.sin_addr.s_addr = INADDR_ANY;
  
  status = bind(gListenSocket,(sockaddr*)(&xsockaddr),sizeof(xsockaddr));
  if (status != 0)
  {
    cm_msg(MERROR,"openListenSocket","Cannot bind() to port %d, errno %d (%s)",port,errno,strerror(errno));
    return FE_ERR_HW;
  }
  
  status = listen(gListenSocket,10);
  if (status != 0)
  {
    cm_msg(MERROR,"openListenSocket","Cannot listen() to port %d, errno %d (%s)",port,errno,strerror(errno));
    return FE_ERR_HW;
  }
  
  cm_msg(MINFO,"OpenListenSocket","Listening for connections on TCP port %d",port);
  
  return FE_SUCCESS;
}

void tryAccept()
{
  struct sockaddr_in xsockaddr;
  socklen_t xsockaddrlen = sizeof(xsockaddr);
  int socket = accept(gListenSocket,(sockaddr*)(&xsockaddr),&xsockaddrlen);
  if (socket <= 0)
  {
    cm_msg(MERROR,"openListenSocket","Cannot accept() new connection, errno %d (%s)",errno,strerror(errno));
    return;
  }
  
  for (int i=0; i<gNumSockets; i++)
    if (gSockets[i] == NULL)
    {
      gSockets[i] = new LabviewSocket(socket);
      return;
    }
    
    gSockets[gNumSockets] = new LabviewSocket(socket);
  gNumSockets ++;
}

bool trySelect(double xtimeout = 0.0001)
{
  fd_set rset, wset, eset;
  struct timeval timeout;
  
  int maxsocket = 0;
  
  timeout.tv_sec  = (int)xtimeout;
  timeout.tv_usec = (int)((xtimeout - timeout.tv_sec)*1000000.0);
  
  //printf("timeout %d sec, %d usec\n", timeout.tv_sec, timeout.tv_usec);
  
  FD_ZERO(&rset);
  FD_ZERO(&wset);
  FD_ZERO(&eset);
  
  FD_SET(gListenSocket,&rset);
  FD_SET(gListenSocket,&eset);
  
  if (gListenSocket > maxsocket)
    maxsocket = gListenSocket;
  
  for (int i=0; i<gNumSockets; i++)
    if (gSockets[i] && gSockets[i]->isOpen)
    {
      FD_SET(gSockets[i]->fd,&rset);
      FD_SET(gSockets[i]->fd,&eset);
      if (gSockets[i]->fd > maxsocket)
	maxsocket = gSockets[i]->fd;
    }
    
    int ret = select(maxsocket+1,&rset,&wset,&eset,&timeout);
  //printf("try select: maxsocket %d, ret %d!\n",maxsocket,ret);
    if (ret < 0)
    {
      if (errno == EINTR) // Interrupted system call
	return false;
      
      cm_msg(MERROR,"trySelect","select() returned %d, errno %d (%s)",ret,errno,strerror(errno));
      return false;
    }
    
    if (FD_ISSET(gListenSocket,&rset))
      tryAccept();
    
    for (int i=0; i<gNumSockets; i++)
      if (gSockets[i])
	if (FD_ISSET(gSockets[i]->fd,&rset) || FD_ISSET(gSockets[i]->fd,&eset))
	{
	  int ret = gSockets[i]->tryRead();
	  if (ret <= 0)
	  {
	    //delete gSockets[i];
	    //gSockets[i] = 0;
	    gSockets[i]->closeSocket();
	    continue;
	  }
	}
	
	bool hasNewData = false;
	
	for (int i=0; i<gNumSockets; i++)
	  if (gSockets[i])
	    hasNewData |= gSockets[i]->hasNewData;
	  
	  //printf("try select: have new data: %d\n",hasNewData);
	
	return hasNewData;
}

/*-- Trigger event routines ----------------------------------------*/
extern "C" INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
 *  is available. If test equals TRUE, don't return. The test
 *  flag is used to time the polling */
{
  //printf("poll_event %d %d %d!\n",source,count,test);
  
  for (int i = 0; i < count; i++)
  {
    int lam = trySelect();
    printf("trySelect %d\n", lam);
    if (lam)
      if (!test)
	return lam;
  }
  return 0;
}

/*-- Interrupt configuration ---------------------------------------*/
extern "C" INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
  switch (cmd) {
    case CMD_INTERRUPT_ENABLE:
      break;
    case CMD_INTERRUPT_DISABLE:
      break;
    case CMD_INTERRUPT_ATTACH:
      break;
    case CMD_INTERRUPT_DETACH:
      break;
  }
  return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/

static const int kMaxBanks = 100;
static char gBankName[kMaxBanks][10];
static int  gBankSize[kMaxBanks];

static int checkBankSize(const char*name, int size, const char* str, double data[])
{
  for (int i=0; i<kMaxBanks; i++)
  {
    if (gBankSize[i] ==0) /* end of bank array */
    {
      printf("Bank %s, new size: %d, labview data [%s]\n", name, size, str);
      strlcpy(gBankName[i], name, 10);
      gBankSize[i] = size;
    }
    
    if (strcmp(name, gBankName[i]) == 0)
    {
      if (size != gBankSize[i])
      {
	printf("Bank %s, size changed from %d to %d, labview data [%s], maybe padding with zeroes\n", name, gBankSize[i], size, str);
	
	if (data)
	  for (int j=size; j<gBankSize[i]; j++)
	    data[j] = 0;
	  
	  return gBankSize[i];
      }
      
      return size;
    }
  }
  
  return size;
}

bool decodeData(char*pevent,char*buf,int bufLength, struct sockaddr_in addrinfo)
{
  //printf("decode data [%s] length %d\n",buf,bufLength);
  
  // do nothing with empty data packets
  if (bufLength < 1)
    return false;
  
  if(strncmp(buf,"QUERY",5) ==0) // Looks for a query for data
	
	// buf needs to be of format "QUERY NAME NN"
  {
    printf("QUERY ");
    buf += 6; //Skip over "QUERY "
    
    if(strlen(buf)<4)
    {
      printf("INVALID\n");
      return false;
    }
    
    //get the name of the bank
    char name[5];
    strncpy(name, buf, 4);
    name[4] = '\0';
    printf("for bank %s ",name);
    
    char odb_addr[100];
    strcpy(odb_addr,"/equipment/labview/Variables/");
    strcat(odb_addr,name);
    
    // Now get the index
    buf+=4; //Skip over the name
    int ind = atoi(buf); //bank index 
    printf("[%d] ", ind);
    
    //get the address of the remote computer
    printf("from %s:%d ", inet_ntoa(addrinfo.sin_addr), ntohs(addrinfo.sin_port));
    
    
    HNDLE hKey, hDB;
    KEY key;
    double kData;
    int kSize;
    
    cm_get_experiment_database(&hDB,NULL); //get a handle to the odb
    db_find_key(hDB,0,odb_addr,&hKey);
    
    if(hKey == 0)
    {
      printf("NOT FOUND\n");
      return false;
    }
    
    db_get_key(hDB, hKey, &key);
    
    if(ind>=key.num_values)
    {
      printf("INDEX OUT OF RANGE: key has %d values\n",key.num_values);
      return false;
    }
    
    kSize = sizeof(kData);
    db_get_data_index(hDB, hKey, &kData, &kSize, ind, TID_DOUBLE);
    
    printf(": %.9g\n",kData);
    
    sleep(50);
    
    // Send the data to the client
    
    int sendSocket = socket(AF_INET, SOCK_STREAM, 0);
    
    if(sendSocket<=0)
    {
      printf("COULD NOT OPEN OUTPUT SOCKET\n");
      return false;
    }
    
    struct timeval timeout;
    timeout.tv_usec =100000;
    
    setsockopt(sendSocket,SOL_SOCKET,SO_SNDTIMEO,&timeout,sizeof(timeout));
    
    int connecterror = connect(sendSocket, (sockaddr *)(&addrinfo), sizeof(addrinfo));
    
    if(connecterror<0)
    {
      printf("COULD NOT CONNECT TO CLIENT - ERROR %d\n",connecterror);
      return false;
    }
    //write(sendSocket,kData,sizeof(kData));
    
    FILE * str_out = fdopen(sendSocket, "w");
    fprintf(str_out,"%lf\n",kData);
    fflush(str_out);
    fclose(str_out);
    
    close(sendSocket);
    
    return false;
    
  }
  if (1)
  {
    char tag[256];
    strncpy(tag, buf, sizeof(tag));
    tag[sizeof(tag)-1] = 0;
    for (unsigned int i=0; i<sizeof(tag); i++)
      if (isspace(tag[i]))
      {
	tag[i] = 0;
	break;
      }
      printf("data from \'%s\'\n", tag);
  }
  
  //-------------------------------------------------------------
  printf("---%s---\n", buf);
  if (strncmp(buf,"V0AC",4) == 0)
  {
    char* s = buf;
    
    // skip the record name
    s += 4;
    
    /* init bank structure */
    bk_init32(pevent);
    
    // create the databank
    double* pdata;
    bk_create(pevent, "V0AC", TID_DOUBLE, &pdata);
    
    int i = 0;
    for (; i<100 && *s; i++)
    {
      while (isspace(*s))
	s++;
      
      // convert string to double
	pdata[i] = strtod(s,&s);
    }
    
    i = checkBankSize("V0AC", i, buf, pdata);
    //printf("..%i",i);
    //printf("..%s",buf);
    
    bk_close(pevent, pdata + i);
    return true;
  } 
  
  //-------------------------------------------------------------
  else if (strncmp(buf,"V0AV",4) == 0)
  {
    char* s = buf;
    
    // skip the record name
    s += 4;
    
    /* init bank structure */
    bk_init32(pevent);
    
    // create the databank
    double* pdata;
    bk_create(pevent, "V0AV", TID_DOUBLE, &pdata);
    
    int i = 0;
    for (; i<100 && *s; i++)
    {
      while (isspace(*s))
	s++;
      
      // convert string to double
	pdata[i] = strtod(s,&s);
    }
    
    i = checkBankSize("V0AV", i, buf, pdata);
    
    bk_close(pevent, pdata + i);
    return true;
  } 

  //-------------------------------------------------------------
  else if (strncmp(buf,"V0BF",4) == 0)
  {
    char* s = buf;
    
    // skip the record name
    s += 4;
    
    /* init bank structure */
    bk_init32(pevent);
    
    // create the databank
    double* pdata;
    bk_create(pevent, "V0BF", TID_DOUBLE, &pdata);
    
    int i = 0;
    for (; i<100 && *s; i++)
    {
      while (isspace(*s))
	s++;
      
      // convert string to double
	pdata[i] = strtod(s,&s);
    }
    
    i = checkBankSize("V0BF", i, buf, pdata);
    
    bk_close(pevent, pdata + i);
    return true;
  } 

  //-------------------------------------------------------------
  else if (strncmp(buf,"V0TS",4) == 0)
  {
    char* s = buf;
    
    // skip the record name
    s += 4;
    
    /* init bank structure */
    bk_init32(pevent);
    
    // create the databank
    double* pdata;
    bk_create(pevent, "V0TS", TID_DOUBLE, &pdata);
    
    int i = 0;
    for (; i<100 && *s; i++)
    {
      while (isspace(*s))
	s++;
      
      // convert string to double
	pdata[i] = strtod(s,&s);
    }
    
    i = checkBankSize("V0TS", i, buf, pdata);
    
    bk_close(pevent, pdata + i);
    return true;
  } 

  //-------------------------------------------------------------
  else if (strncmp(buf,"V0AL",4) == 0)
  {
    char* s = buf;
    
    // skip the record name
    s += 4;
    
    /* init bank structure */
    bk_init32(pevent);
    
    // create the databank
    double* pdata;
    bk_create(pevent, "V0AL", TID_DOUBLE, &pdata);
    
    int i = 0;
    for (; i<100 && *s; i++)
    {
      while (isspace(*s))
	s++;
      
      // convert string to double
	pdata[i] = strtod(s,&s);
    }
    
    i = checkBankSize("V0AL", i, buf, pdata);
    
    bk_close(pevent, pdata + i);
    return true;
  } 

  //-------------------------------------------------------------
  else if (strncmp(buf,"V0AT",4) == 0)
  {
    char* s = buf;
    
    // skip the record name
    s += 4;
    
    /* init bank structure */
    bk_init32(pevent);
    
    // create the databank
    double* pdata;
    bk_create(pevent, "V0AT", TID_DOUBLE, &pdata);
    
    int i = 0;
    for (; i<100 && *s; i++)
    {
      while (isspace(*s))
	s++;
      
      // convert string to double
	pdata[i] = strtod(s,&s);
    }
    
    i = checkBankSize("V0AT", i, buf, pdata);
    
    bk_close(pevent, pdata + i);
    return true;
  } 
  //-------------------------------------------------------------
  else if (strncmp(buf,"V0OT",4) == 0)
  {
    char* s = buf;
    
    // skip the record name
    s += 4;
    
    /* init bank structure */
    bk_init32(pevent);
    
    // create the databank
    double* pdata;
    bk_create(pevent, "V0OT", TID_DOUBLE, &pdata);
    
    int i = 0;
    for (; i<100 && *s; i++)
    {
      while (isspace(*s))
	s++;
      
      // convert string to double
	pdata[i] = strtod(s,&s);
    }
    
    i = checkBankSize("V0OT", i, buf, pdata);
    
    bk_close(pevent, pdata + i);
    return true;
  } 
  
  //-------------------------------------------------------------
  else
  {
    printf("unexpected data: [%s], length %d\n",buf,bufLength);
    
    /* data we do not know what to do with is sent to MIDAS verbatim */
    
    if (false)
    {
      /* init bank structure */
      bk_init32(pevent);
      char* pdata;
      bk_create(pevent, "LABV", TID_CHAR, &pdata);
      int len = bufLength;
      memcpy(pdata,buf,len);
      pdata += len;
      bk_close(pevent, pdata);
      return true;
    }
    
    return false;
  }
}

INT read_event(char *pevent, INT off)
{
  //printf("read event\n");
  
  equipment[0].last_called = 0;
  
  if (!trySelect(0.1))
    return 0;
  
  
  for (int i=0; i<=gNumSockets; i++)
  {
    if (gPtr >= gNumSockets)
      gPtr = 0;
    
    //printf("try ptr %d\n",gPtr);
      
      if (gSockets[gPtr] && gSockets[gPtr]->hasNewData)
      {
	// check that the buffered data has the record terminators
	
	char*end = strstr(gSockets[gPtr]->buf,"\n");
	if (!end)
	  end = strstr(gSockets[gPtr]->buf,"\r");
	
	if (gSockets[gPtr]->isOpen && !end)
	{
	  // no record terminators- incomplete
	  // data- wait for more data...
	  gSockets[gPtr]->hasNewData = false;
	  gPtr++;
	  continue;
	}
	
	if (!end)
	  end = gSockets[gPtr]->buf + gSockets[gPtr]->bufwptr;
	
	// zero-terminate the data buffer
	  end[0] = 0;
	  
	  // kill any preceeding \r and \n
	  if (end[-1] == '\n')
	    end[-1] = 0;
	  else if (end[-1] == '\r')
	    end[-1] = 0;
	  
	  // kill any following \r and \n
	    if (end[1] == '\n')
	      end++;
	    else if (end[1] == '\r')
	      end++;
	    
	    int endoffset = end-gSockets[gPtr]->buf;
	    int leftover = gSockets[gPtr]->bufwptr - endoffset - 1;
	    
	    //printf("socket %d, length %d, end %p, terminated at %d, leftover data %d\n",gPtr,gSockets[gPtr]->bufwptr,end,endoffset,leftover);
	    
	    bool doSendEvent = decodeData(pevent,gSockets[gPtr]->buf,endoffset, gSockets[gPtr]->addrinfo);
	    
	    if (leftover > 0)
	    {
	      // move any leftover data to the beginning of
	      // the data buffer using memmove() because
	      // source and destination do overlap
	      memmove(gSockets[gPtr]->buf,end+1,leftover);
	      gSockets[gPtr]->buf[leftover] = 0;
	      gSockets[gPtr]->bufwptr = leftover;
	      gSockets[gPtr]->hasNewData = true;
	      
	      //printf("leftover bytes %d: [%s]\n",gSockets[gPtr]->bufwptr,gSockets[gPtr]->buf);
	    }
	    else
	    {
	      leftover = 0;
	      gSockets[gPtr]->buf[leftover] = 0;
	      gSockets[gPtr]->bufwptr    = leftover;
	      gSockets[gPtr]->hasNewData = false;
	      
	      if (!gSockets[gPtr]->isOpen)
	      {
		delete gSockets[gPtr];
		gSockets[gPtr] = NULL;
	      }
	    }
	    
	    // 
	    gPtr++;
	    
	    //printf("send event: %d\n",doSendEvent);
	    
	    // send the data to MIDAS
	    if (doSendEvent)
	      return bk_size(pevent);
      }
      
      gPtr++;
  }
  
  return 0;
}

//end
