/*********************************************************************
  Name:         vmicvme.h
  Created by:   Pierre-Andre Amaudruz

  Contents:     VME interface for the VMIC VME board processor
                
  $Log: vmicvme.h,v $
*********************************************************************/

#include <stdio.h>
#include <string.h>
#include <vme/vme.h>
#include <vme/vme_api.h>

#ifndef  __VMICVME_H__
#define  __VMICVME_H__

#ifndef MIDAS_TYPE_DEFINED
typedef  unsigned long int   DWORD;
typedef  unsigned short int   WORD;
#endif

#ifndef  SUCCESS
#define  SUCCESS             (int) 1
#endif
#define  ERROR               (int) -1000

#define  VME_IOCTL_SLOT_GET  (DWORD) 0x7000
#define  VME_IOCTL_AMOD_SET  (DWORD) 0x7001
#define  VME_IOCTL_AMOD_GET  (DWORD) 0x7002
#define  VME_IOCTL_PTR_GET   (DWORD) 0x7003
#define  MAX_VME_SLOTS       (int) 16

#define  DMA_MODE            (int) 1
#define  DEFAULT_SRC_ADD     0x2000
#define  DEFAULT_AMOD        0x39
#define  DEFAULT_NBYTES      0x10000    /* 1MB */

typedef struct {
  vme_master_handle_t  wh;
  DWORD           am;
  DWORD        nbytes;
  DWORD          *ptr;
  DWORD          low;
  DWORD          high;
  int           valid;
} VMETABLE;

int mvme_init  (void); 
int mvme_exit  (void);
int mvme_read  (void *dst, DWORD src, DWORD nbytes);
int mvme_read2 (void *dst, DWORD src, DWORD nbytes, DWORD flags);
int mvme_write  (DWORD src, DWORD vme_addr, DWORD nbytes);
int mvme_write2(void *dst, DWORD src, DWORD nbytes, DWORD flags);
int mvme_mmap  (DWORD vme_addr, DWORD nbytes, DWORD am);
int mvme_unmap (DWORD src, DWORD nbytes);
int mvme_ioctl (DWORD req, DWORD *param);

#endif
