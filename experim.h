/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Fri Sep 28 18:57:58 2012

\********************************************************************/

#define EXP_PARAM_DEFINED

typedef struct {
  char      comment[128];
} EXP_PARAM;

#define EXP_PARAM_STR(_name) char *_name[] = {\
"[.]",\
"comment = STRING : [128] DAQ tests",\
"",\
NULL }

#define EXP_EDIT_DEFINED

typedef struct {
  BOOL      write_data;
} EXP_EDIT;

#define EXP_EDIT_STR(_name) char *_name[] = {\
"[.]",\
"Write Data = LINK : [19] /Logger/Write data",\
"number of scans = LINK : [53] /Equipment/TITAN_ACQ/ppg cycle/begin_scan/loop count",\
"",\
NULL }

#ifndef EXCL_CATHODE

#define CATHODE_PARAM_DEFINED

typedef struct {
  INT       offset;
  INT       charge_spectrum_threshold;
  INT       process_waveforms;
  INT       correlation_report_interval;
  float     charge_dispersion;
  INT       delta_t_energy_threshold_si;
  INT       delta_t_energy_threshold_ge;
} CATHODE_PARAM;

#define CATHODE_PARAM_STR(_name) char *_name[] = {\
"[.]",\
"offset = INT : 1",\
"Charge Spectrum Threshold = INT : 10",\
"process_waveforms = INT : 1",\
"correlation_report_interval = INT : 8000",\
"Charge Dispersion = FLOAT : 1",\
"Delta T Energy Threshold Si = INT : 1000",\
"Delta T Energy Threshold Ge = INT : 300",\
"",\
NULL }

#endif

#ifndef EXCL_SIS3302

#define SIS3302_PARAM_DEFINED

typedef struct {
  struct {
    INT       internal_trigger;
    INT       negative_input;
    INT       eventsize_threshold;
    INT       pretrigger_delay;
    INT       trigger_gate_length;
    INT       energy_gate_length;
    INT       rawbuf_start;
    INT       raw_samples;
    INT       energy_sample_length;
    INT       energy_sample_index1;
    INT       energy_sample_index2;
    INT       energy_sample_index3;
    struct {
      INT       threshold[8];
      INT       peakingtime[8];
      INT       gaptime[8];
      INT       decimation[8];
      INT       pulse_length;
      INT       internal_gate_length;
      INT       internal_delay;
    } triggersettings;
    struct {
      INT       peakingtime[4];
      INT       gaptime[4];
      INT       decimation[4];
      INT       tau[8];
    } energysampling;
    INT       dac[8];
  } sis3302g;
} SIS3302_PARAM;

#define SIS3302_PARAM_STR(_name) char *_name[] = {\
"[sis3302g]",\
"internal trigger = INT : 3",\
"negative input = INT : 0",\
"eventsize threshold = INT : 10",\
"pretrigger delay = INT : 0",\
"trigger Gate Length = INT : 448",\
"Energy Gate Length = INT : 630",\
"RawBuf Start = INT : 0",\
"Raw Samples = INT : 256",\
"Energy Sample Length = INT : 256",\
"Energy Sample Index1 = INT : 0",\
"Energy Sample index2 = INT : 0",\
"Energy Sample index3 = INT : 0",\
"",\
"[sis3302g/triggersettings]",\
"threshold = INT[8] :",\
"[0] 15",\
"[1] 40",\
"[2] 15",\
"[3] 55",\
"[4] 20",\
"[5] 45",\
"[6] 20",\
"[7] 0",\
"peakingtime = INT[8] :",\
"[0] 8",\
"[1] 8",\
"[2] 8",\
"[3] 8",\
"[4] 8",\
"[5] 8",\
"[6] 8",\
"[7] 0",\
"gaptime = INT[8] :",\
"[0] 16",\
"[1] 16",\
"[2] 16",\
"[3] 16",\
"[4] 16",\
"[5] 16",\
"[6] 16",\
"[7] 0",\
"decimation = INT[8] :",\
"[0] 2",\
"[1] 2",\
"[2] 2",\
"[3] 2",\
"[4] 2",\
"[5] 2",\
"[6] 2",\
"[7] 0",\
"pulse length = INT : 10",\
"internal gate length = INT : 2",\
"internal delay = INT : 0",\
"",\
"[sis3302g/energysampling]",\
"peakingtime = INT[4] :",\
"[0] 150",\
"[1] 150",\
"[2] 150",\
"[3] 150",\
"gaptime = INT[4] :",\
"[0] 250",\
"[1] 250",\
"[2] 250",\
"[3] 250",\
"decimation = INT[4] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"tau = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"",\
"[sis3302g]",\
"dac = INT[8] :",\
"[0] 50000",\
"[1] 50000",\
"[2] 50000",\
"[3] 50000",\
"[4] 50000",\
"[5] 50000",\
"[6] 50000",\
"[7] 50000",\
"",\
NULL }

#endif

#ifndef EXCL_SIS3302G

#define SIS3302G_PARAM_DEFINED

typedef struct {
  struct {
    INT       internal_trigger;
    INT       negative_input;
    INT       eventsize_threshold;
    INT       pretrigger_delay;
    INT       trigger_gate_length;
    INT       energy_gate_length;
    INT       rawbuf_start;
    INT       raw_samples;
    INT       energy_sample_length;
    INT       energy_sample_index1;
    INT       energy_sample_index2;
    INT       energy_sample_index3;
    struct {
      INT       threshold[8];
      INT       peakingtime[8];
      INT       gaptime[8];
      INT       decimation[8];
      INT       pulse_length;
      INT       internal_gate_length;
      INT       internal_delay;
    } triggersettings;
    struct {
      INT       peakingtime[4];
      INT       gaptime[4];
      INT       decimation[4];
      INT       tau[8];
    } energysampling;
    INT       dac[8];
  } sis3302g;
} SIS3302G_PARAM;

#define SIS3302G_PARAM_STR(_name) char *_name[] = {\
"[sis3302g]",\
"internal trigger = INT : 3",\
"negative input = INT : 1",\
"eventsize threshold = INT : 100",\
"pretrigger delay = INT : 0",\
"trigger Gate Length = INT : 448",\
"Energy Gate Length = INT : 630",\
"RawBuf Start = INT : 0",\
"Raw Samples = INT : 0",\
"Energy Sample Length = INT : 0",\
"Energy Sample Index1 = INT : 0",\
"Energy Sample index2 = INT : 0",\
"Energy Sample index3 = INT : 0",\
"",\
"[sis3302g/triggersettings]",\
"threshold = INT[8] :",\
"[0] 15",\
"[1] 40",\
"[2] 15",\
"[3] 55",\
"[4] 20",\
"[5] 45",\
"[6] 20",\
"[7] 0",\
"peakingtime = INT[8] :",\
"[0] 8",\
"[1] 8",\
"[2] 8",\
"[3] 8",\
"[4] 8",\
"[5] 8",\
"[6] 8",\
"[7] 0",\
"gaptime = INT[8] :",\
"[0] 16",\
"[1] 16",\
"[2] 16",\
"[3] 16",\
"[4] 16",\
"[5] 16",\
"[6] 16",\
"[7] 0",\
"decimation = INT[8] :",\
"[0] 2",\
"[1] 2",\
"[2] 2",\
"[3] 2",\
"[4] 2",\
"[5] 2",\
"[6] 2",\
"[7] 0",\
"pulse length = INT : 10",\
"internal gate length = INT : 2",\
"internal delay = INT : 0",\
"",\
"[sis3302g/energysampling]",\
"peakingtime = INT[4] :",\
"[0] 150",\
"[1] 150",\
"[2] 150",\
"[3] 150",\
"gaptime = INT[4] :",\
"[0] 250",\
"[1] 250",\
"[2] 250",\
"[3] 250",\
"decimation = INT[4] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"tau = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"",\
"[sis3302g]",\
"dac = INT[8] :",\
"[0] 50000",\
"[1] 50000",\
"[2] 50000",\
"[3] 50000",\
"[4] 50000",\
"[5] 50000",\
"[6] 50000",\
"[7] 50000",\
"",\
NULL }

#endif

#ifndef EXCL_TIGCOL

#define TIGCOL_SETTINGS_DEFINED

typedef struct {
  INT       command;
  BOOL      block_start_stop;
  INT       use_dma_transfer;
  INT       frontend_event_assembly;
  INT       debug_event_interval;
  INT       debug_event_channels;
  INT       debug_channel_offset;
  BOOL      frontend_quiet;
  INT       trigger_mode;
  INT       prescale_factor;
  BOOL      disable_waveforms;
  BOOL      zerosuppress_ge;
  BOOL      reinitialise;
  BOOL      hardware_gainmatch;
  struct {
    INT       collector_mask;
    INT       disabled_port_mask;
    struct {
      INT       channel_enable_mask;
      INT       channel_trigger_mask;
      INT       polarity_mask;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_0;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_1;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_8;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_9;
    } card_0;
  } master;
  struct {
    INT       collector_mask;
    INT       disabled_port_mask;
    struct {
      INT       channel_enable_mask;
      INT       channel_trigger_mask;
      INT       polarity_mask;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_0;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_1;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_2;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_3;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_4;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_5;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_6;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_7;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_8;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_9;
    } card_0;
  } slave1;
} TIGCOL_SETTINGS;

#define TIGCOL_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Command = INT : 0",\
"block start stop = BOOL : n",\
"use_dma_transfer = INT : 0",\
"frontend_event_assembly = INT : 1",\
"debug_event_interval = INT : 200",\
"debug event channels = INT : 10",\
"debug channel offset = INT : 60",\
"frontend_quiet = BOOL : n",\
"Trigger Mode = INT : 2",\
"Prescale Factor = INT : 1",\
"disable_waveforms = BOOL : y",\
"zerosuppress_ge = BOOL : y",\
"Reinitialise = BOOL : n",\
"Hardware Gainmatch = BOOL : n",\
"",\
"[Master]",\
"Collector Mask = INT : 3",\
"Disabled Port Mask = INT : 65471",\
"",\
"[Master/Card_0]",\
"Channel Enable Mask = INT : 771",\
"channel trigger mask = INT : 768",\
"Polarity mask = INT : 1",\
"",\
"[Master/Card_0/Chan_0]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[Master/Card_0/Chan_1]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[Master/Card_0/Chan_8]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[Master/Card_0/Chan_9]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1]",\
"Collector Mask = INT : 0",\
"Disabled Port Mask = INT : 65471",\
"",\
"[slave1/Card_0]",\
"Channel Enable Mask = INT : 512",\
"channel trigger mask = INT : 512",\
"Polarity mask = INT : 8",\
"",\
"[slave1/Card_0/Chan_0]",\
"Hit Threshold = INT : 110",\
"Trigger Threshold = INT : 110",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 64",\
"Sample window size = INT : 2",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 12328",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 40",\
"",\
"[slave1/Card_0/Chan_1]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 64",\
"Sample window size = INT : 256",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 5000",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 200",\
"",\
"[slave1/Card_0/chan_2]",\
"Hit Threshold = INT : 150",\
"Trigger Threshold = INT : 150",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 64",\
"Sample window size = INT : 2",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 15000",\
"Mode = INT : 12328",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 40",\
"",\
"[slave1/Card_0/chan_3]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 40000",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/chan_4]",\
"Hit Threshold = INT : 13",\
"Trigger Threshold = INT : 13",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 64",\
"Sample window size = INT : 156",\
"K_param = INT : 400",\
"L_param = INT : 480",\
"M_Param = INT : 4000",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 50",\
"",\
"[slave1/Card_0/chan_5]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/chan_6]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/chan_7]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/Chan_8]",\
"Hit Threshold = INT : 20",\
"Trigger Threshold = INT : 20",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 64",\
"Sample window size = INT : 256",\
"K_param = INT : 400",\
"L_param = INT : 480",\
"M_Param = INT : 4000",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 50",\
"",\
"[slave1/Card_0/Chan_9]",\
"Hit Threshold = INT : 110",\
"Trigger Threshold = INT : 110",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 64",\
"Sample window size = INT : 2",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 15000",\
"Mode = INT : 12328",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 40",\
"",\
NULL }

#define TIGCOL_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TIGCOL_COMMON;

#define TIGCOL_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 2",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 50",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxebit.triumf.ca",\
"Frontend name = STRING : [32] feEbit",\
"Frontend file name = STRING : [256] feebit.c",\
"",\
NULL }

#endif

#ifndef EXCL_TITAN_ACQ

#define TITAN_ACQ_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      char      experiment_name[32];
      char      ppgload_path[50];
      char      ppg_path[50];
      char      ppg_perl_path[50];
      float     daq_service_time__ms_;
      float     standard_pulse_width__ms_;
      float     awg_clock_width__ms_;
      float     tdcblock_width__ms_;
      float     ppg_clock__mhz_;
      DWORD     polarity;
      BOOL      external_trigger;
      BOOL      external_clock;
    } input;
    struct {
      char      compiled_file_time[32];
      DWORD     compiled_file_time__binary_;
      float     time_slice__ms_;
      float     ppg_nominal_frequency__mhz_;
      float     minimal_delay__ms_;
      float     ppg_freq_conversion_factor;
      BOOL      ppg_running;
      float     last_transition__ms_;
      BOOL      external_clock;
      BOOL      good_external_clock;
      struct {
        BOOL      external_clock_alarm;
        BOOL      ppg_status_alarm;
      } alarms;
      struct {
        INT       num_loops;
        float     start_loop[10];
        float     one_loop_duration[10];
        float     all_loops_end[10];
        char      loop_names[10][20];
      } loops;
    } output;
  } ppg;
} TITAN_ACQ_SETTINGS;

#define TITAN_ACQ_SETTINGS_STR(_name) char *_name[] = {\
"[ppg/input]",\
"Experiment name = STRING : [32] ebit",\
"PPGLOAD path = STRING : [50] /home/ebit/online/ppg/ppgload",\
"PPG path = STRING : [50] /home/ebit/online/ppg",\
"PPG perl path = STRING : [50] /home/ebit/online/ppg/perl",\
"DAQ service time (ms) = FLOAT : 0",\
"standard pulse width (ms) = FLOAT : 0.001",\
"AWG clock width (ms) = FLOAT : 0.001",\
"TDCBLOCK width (ms) = FLOAT : 0.001",\
"PPG clock (MHz) = FLOAT : 100",\
"Polarity = DWORD : 0",\
"external trigger = BOOL : n",\
"external clock = BOOL : n",\
"",\
"[ppg/output]",\
"compiled file time = STRING : [32] Wed Jul 25 11:08:41 2012",\
"compiled file time (binary) = DWORD : 1343239721",\
"Time Slice (ms) = FLOAT : 1e-05",\
"PPG nominal frequency (MHz) = FLOAT : 10",\
"Minimal Delay (ms) = FLOAT : 3e-05",\
"PPG freq conversion factor = FLOAT : 10",\
"PPG running = BOOL : n",\
"last transition (ms) = FLOAT : 0",\
"external clock = BOOL : n",\
"good external clock = BOOL : n",\
"",\
"[ppg/output/alarms]",\
"external clock alarm = BOOL : n",\
"ppg status alarm = BOOL : n",\
"",\
"[ppg/output/loops]",\
"num_loops = INT : 0",\
"start_loop = FLOAT[10] :",\
"[0] 5e-05",\
"[1] 0.0101",\
"[2] 8.01275",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"one_loop_duration = FLOAT[10] :",\
"[0] 295.0216",\
"[1] 7.0026",\
"[2] 10",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"all_loops_end = FLOAT[10] :",\
"[0] 29502.16",\
"[1] 7.01275",\
"[2] 18.0128",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"loop_names = STRING[10] :",\
"[20] SCAN",\
"[20] EXTR",\
"[20] SCLR",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"",\
NULL }

#define TITAN_ACQ_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TITAN_ACQ_COMMON;

#define TITAN_ACQ_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 10",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 377",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxebit.triumf.ca",\
"Frontend name = STRING : [32] feEbit_ppg",\
"Frontend file name = STRING : [256] feebit_ppg.c",\
"",\
NULL }

#define TITAN_ACQ_MODE_NAME_DEFINED

typedef struct {
} TITAN_ACQ_MODE_NAME;

#define TITAN_ACQ_MODE_NAME_STR(_name) char *_name[] = {\
"mode_name = STRING : [8] 1a",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1A_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans1;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    dummy;
  } skip1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    undummy;
  } unskip1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans3;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse10;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } time_ep11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_scan;
  struct {
    double    dummy;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
  } image;
  struct {
    char      names[32][64];
  } names;
} TITAN_ACQ_PPG_CYCLE_MODE_1A;

#define TITAN_ACQ_PPG_CYCLE_MODE_1A_STR(_name) char *_name[] = {\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 5e-05",\
"loop count = INT : 100000000",\
"",\
"[trans1]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH1",\
"",\
"[begin_extr]",\
"time offset (ms) = DOUBLE : 1",\
"loop count = INT : 10",\
"time reference = STRING : [20] _TTRANS1",\
"",\
"[pulse1]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 2",\
"ppg signal name = STRING : [16] CH2",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 3",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 4",\
"time reference = STRING : [20] _TEND_PULSE2",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TEND_PULSE3",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[skip1]",\
"dummy = DOUBLE : 1",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH6",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 3",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[unskip1]",\
"undummy = DOUBLE : 2",\
"",\
"[end_extr]",\
"time offset (ms) = DOUBLE : 0.0005",\
"time reference = STRING : [20] _TEND_PULSE4",\
"",\
"[trans2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH1",\
"",\
"[trans3]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH10",\
"",\
"[begin_sclr]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 1",\
"time reference = STRING : [20] ",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[end_sclr]",\
"time offset (ms) = DOUBLE : 10",\
"time reference = STRING : [20] _TBEGSCLR",\
"",\
"[trans4]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH10",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH11",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TSTART_PULSE10",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH13",\
"",\
"[time_ep11]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] _TTIME_EP11",\
"",\
"[skip]",\
"dummy = DOUBLE : 1",\
"",\
"[image]",\
"name = STRING : [32] ebit_1a_skip_trans.gif",\
"mode = STRING : [8] 1a",\
"",\
"[names]",\
"names = STRING[32] :",\
"[64] ISAC Beam Gate",\
"[64] TITAN Beam Gate1",\
"[64] Titan Beam Gate2",\
"[64] RFQ Extraction",\
"[64] Capture  1   ",\
"[64] Capture  2   ",\
"[64] Transfer  1  ",\
"[64] Transfer  2  ",\
"[64] Scaler  1  ",\
"[64] Scaler  2  ",\
"[64] Tig 10     ",\
"[64] Dump       ",\
"[64] Kicker  1   ",\
"[64] Kicker2",\
"[64] Channel15",\
"[64] Channel16",\
"[64] Channel17",\
"[64] Channel18",\
"[64] Channel19",\
"[64] Channel20",\
"[64] Channel21",\
"[64] Channel22",\
"[64] Channel23",\
"[64] Channel24",\
"[64] Channel25",\
"[64] Channel26",\
"[64] Channel27",\
"[64] Channel28",\
"[64] Channel29",\
"[64] Channel30",\
"[64] Channel31",\
"[64] Channel32",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1B_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans1;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_extr;
  struct {
    double    dummy;
  } skip1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans3;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse10;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    undummy;
  } unskip1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } time_ep11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_scan;
  struct {
    double    dummy;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
  } image;
  struct {
    char      names[32][64];
  } names;
} TITAN_ACQ_PPG_CYCLE_MODE_1B;

#define TITAN_ACQ_PPG_CYCLE_MODE_1B_STR(_name) char *_name[] = {\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 5e-05",\
"loop count = INT : 100000000",\
"",\
"[trans1]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH1",\
"",\
"[begin_extr]",\
"time offset (ms) = DOUBLE : 1",\
"loop count = INT : 2",\
"time reference = STRING : [20] _TTRANS1",\
"",\
"[pulse1]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 2",\
"ppg signal name = STRING : [16] CH2",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 3",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 4",\
"time reference = STRING : [20] _TEND_PULSE2",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TEND_PULSE3",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 2",\
"ppg signal name = STRING : [16] CH6",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 3",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[end_extr]",\
"time offset (ms) = DOUBLE : 0.0005",\
"time reference = STRING : [20] _TEND_PULSE6",\
"",\
"[skip1]",\
"dummy = DOUBLE : 1",\
"",\
"[trans2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH1",\
"",\
"[trans3]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH10",\
"",\
"[begin_sclr]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 20",\
"time reference = STRING : [20] ",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[end_sclr]",\
"time offset (ms) = DOUBLE : 10",\
"time reference = STRING : [20] _TBEGSCLR",\
"",\
"[trans4]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH10",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH11",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TSTART_PULSE10",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH13",\
"",\
"[unskip1]",\
"undummy = DOUBLE : 2",\
"",\
"[time_ep11]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"",\
"[skip]",\
"dummy = DOUBLE : 0",\
"",\
"[image]",\
"name = STRING : [32] ebit_1b_end_after_extr.gif",\
"mode = STRING : [8] 1b",\
"",\
"[names]",\
"names = STRING[32] :",\
"[64] ISAC Beam Gate",\
"[64] TITAN Beam Gate1",\
"[64] Titan Beam Gate2",\
"[64] RFQ Extraction",\
"[64] Capture  1   ",\
"[64] Capture  2   ",\
"[64] Transfer  1  ",\
"[64] Transfer  2  ",\
"[64] Scaler  1  ",\
"[64] Scaler  2  ",\
"[64] Tig 10     ",\
"[64] Dump       ",\
"[64] Kicker  1   ",\
"[64] Kicker2",\
"[64] Channel15",\
"[64] Channel16",\
"[64] Channel17",\
"[64] Channel18",\
"[64] Channel19",\
"[64] Channel20",\
"[64] Channel21",\
"[64] Channel22",\
"[64] Channel23",\
"[64] Channel24",\
"[64] Channel25",\
"[64] Channel26",\
"[64] Channel27",\
"[64] Channel28",\
"[64] Channel29",\
"[64] Channel30",\
"[64] Channel31",\
"[64] Channel32",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1C_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans1;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans3;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse10;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } time_ep11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_scan;
  struct {
    double    dummy;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
  } image;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } time_last;
  struct {
    char      names[32][64];
  } names;
} TITAN_ACQ_PPG_CYCLE_MODE_1C;

#define TITAN_ACQ_PPG_CYCLE_MODE_1C_STR(_name) char *_name[] = {\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 5e-05",\
"loop count = INT : 100",\
"",\
"[trans1]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH1",\
"",\
"[begin_extr]",\
"time offset (ms) = DOUBLE : 0.01",\
"loop count = INT : 1",\
"time reference = STRING : [20] _TTRANS1",\
"",\
"[pulse1]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 2",\
"ppg signal name = STRING : [16] CH2",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 2",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] _TEND_PULSE2",\
"pulse width (ms) = DOUBLE : 0.0001",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TEND_PULSE3",\
"pulse width (ms) = DOUBLE : 0.02",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TEND_PULSE3",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH6",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 0.078",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 0.0005",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[end_extr]",\
"time offset (ms) = DOUBLE : 0.0005",\
"time reference = STRING : [20] _TEND_PULSE5",\
"",\
"[trans2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH1",\
"",\
"[trans3]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TENDEXTR",\
"ppg signal name = STRING : [15] CH10",\
"",\
"[begin_sclr]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 1",\
"time reference = STRING : [20] _TTRANS3",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 0.0001",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 0.0001",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 0.0001",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 0.0001",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[end_sclr]",\
"time offset (ms) = DOUBLE : 10",\
"time reference = STRING : [20] _TBEGSCLR",\
"",\
"[trans4]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TENDSCLR",\
"ppg signal name = STRING : [15] CH10",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH11",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 250",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 0.5",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 0.0086",\
"time reference = STRING : [20] _TSTART_PULSE10",\
"pulse width (ms) = DOUBLE : 0.0002",\
"ppg signal name = STRING : [16] CH13",\
"",\
"[time_ep11]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"",\
"[trans5]",\
"time offset (ms) = DOUBLE : 0.5",\
"time reference = STRING : [20] _TENDEXTR",\
"ppg signal name = STRING : [15] CH14",\
"",\
"[trans6]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TENDSCLR",\
"ppg signal name = STRING : [15] CH14",\
"",\
"[trans7]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TENDEXTR",\
"ppg signal name = STRING : [15] CH15",\
"",\
"[trans8]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TENDSCLR",\
"ppg signal name = STRING : [15] CH15",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 27",\
"time reference = STRING : [20] _TTIME_EP11",\
"",\
"[skip]",\
"dummy = DOUBLE : 0",\
"",\
"[image]",\
"name = STRING : [32] ebit_1c_all.gif",\
"mode = STRING : [8] 1c",\
"",\
"[time_last]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"",\
"[names]",\
"names = STRING[32] :",\
"[64] ISAC Beam Gate",\
"[64] TITAN Beam Gate1",\
"[64] Titan Beam Gate2",\
"[64] RFQ Extraction",\
"[64] Capture  1   ",\
"[64] Capture  2   ",\
"[64] Transfer  1  ",\
"[64] Transfer  2  ",\
"[64] Scaler  1  ",\
"[64] Scaler  2  ",\
"[64] Tig 10     ",\
"[64] Dump       ",\
"[64] Kicker  1   ",\
"[64] Kicker2",\
"[64] Channel15",\
"[64] Channel16",\
"[64] Channel17",\
"[64] Channel18",\
"[64] Channel19",\
"[64] Channel20",\
"[64] Channel21",\
"[64] Channel22",\
"[64] Channel23",\
"[64] Channel24",\
"[64] Channel25",\
"[64] Channel26",\
"[64] Channel27",\
"[64] Channel28",\
"[64] Channel29",\
"[64] Channel30",\
"[64] Channel31",\
"[64] Channel32",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1D_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
    char      time_reference[20];
  } pulse9;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
    char      time_reference[20];
  } pulse10;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
    char      time_reference[20];
  } pulse11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans1;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans3;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } time_ep11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } delay_eoc;
  struct {
    double    dummy;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
  } image;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } delay1;
  struct {
    char      names[32][64];
  } names;
} TITAN_ACQ_PPG_CYCLE_MODE_1D;

#define TITAN_ACQ_PPG_CYCLE_MODE_1D_STR(_name) char *_name[] = {\
"[pulse9]",\
"time offset (ms) = DOUBLE : 0",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH11",\
"time reference = STRING : [20] T0",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 0.001",\
"pulse width (ms) = DOUBLE : 0.5",\
"ppg signal name = STRING : [16] CH12",\
"time reference = STRING : [20] T0",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 0.001",\
"pulse width (ms) = DOUBLE : 0.5",\
"ppg signal name = STRING : [16] CH13",\
"time reference = STRING : [20] T0",\
"",\
"[trans1]",\
"time offset (ms) = DOUBLE : 3",\
"time reference = STRING : [20] T0",\
"ppg signal name = STRING : [15] CH1",\
"",\
"[begin_extr]",\
"time offset (ms) = DOUBLE : 0.01",\
"loop count = INT : 1",\
"time reference = STRING : [20] _TTRANS1",\
"",\
"[pulse1]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] CH2",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TEND_PULSE2",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TEND_PULSE3",\
"pulse width (ms) = DOUBLE : 0.0805",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 0.0001",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 0.015",\
"ppg signal name = STRING : [16] CH6",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 0.015",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[end_extr]",\
"time offset (ms) = DOUBLE : 0.0005",\
"time reference = STRING : [20] _TEND_PULSE4",\
"",\
"[trans2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH1",\
"",\
"[trans3]",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH10",\
"",\
"[begin_sclr]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 1",\
"time reference = STRING : [20] ",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[end_sclr]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] _TBEGSCLR",\
"",\
"[trans4]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH10",\
"",\
"[time_ep11]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"",\
"[trans5]",\
"time offset (ms) = DOUBLE : 0.5",\
"time reference = STRING : [20] _TENDEXTR",\
"ppg signal name = STRING : [15] CH14",\
"",\
"[trans6]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TENDSCLR",\
"ppg signal name = STRING : [15] CH14",\
"",\
"[trans7]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TENDEXTR",\
"ppg signal name = STRING : [15] CH15",\
"",\
"[trans8]",\
"time offset (ms) = DOUBLE : 0.5",\
"time reference = STRING : [20] _TENDSCLR",\
"ppg signal name = STRING : [15] CH15",\
"",\
"[delay_eoc]",\
"time offset (ms) = DOUBLE : 1.5",\
"time reference = STRING : [20] _TENDSCLR",\
"",\
"[skip]",\
"dummy = DOUBLE : 0",\
"",\
"[image]",\
"name = STRING : [32] ebit_1d_ext_trig.gif",\
"mode = STRING : [8] 1d",\
"",\
"[delay1]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TTIME_EP11",\
"",\
"[names]",\
"names = STRING[32] :",\
"[64] ISAC Beam Gate",\
"[64] TITAN Beam Gate1",\
"[64] Titan Beam Gate2",\
"[64] RFQ Extraction",\
"[64] Capture  1   ",\
"[64] Capture  2   ",\
"[64] Transfer  1  ",\
"[64] Transfer  2  ",\
"[64] Scaler  1  ",\
"[64] Scaler  2  ",\
"[64] Tig 10     ",\
"[64] Dump       ",\
"[64] Kicker  1   ",\
"[64] Kicker2",\
"[64] Channel15",\
"[64] Channel16",\
"[64] Channel17",\
"[64] Channel18",\
"[64] Channel19",\
"[64] Channel20",\
"[64] Channel21",\
"[64] Channel22",\
"[64] Channel23",\
"[64] Channel24",\
"[64] Channel25",\
"[64] Channel26",\
"[64] Channel27",\
"[64] Channel28",\
"[64] Channel29",\
"[64] Channel30",\
"[64] Channel31",\
"[64] Channel32",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1E_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse10;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse12;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse13;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse14;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse15;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse16;
  struct {
    INT       channel;
    INT       dummy;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
    INT       num_channels_to_display;
    INT       max_active_channel;
  } image;
  struct {
    char      names[32][64];
  } names;
  INT       bitpat_to_set_on_stop;
} TITAN_ACQ_PPG_CYCLE_MODE_1E;

#define TITAN_ACQ_PPG_CYCLE_MODE_1E_STR(_name) char *_name[] = {\
"[pulse1]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH1",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH2",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 10",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0.075",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0.075",\
"ppg signal name = STRING : [16] CH6",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0.075",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 1.091",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0.4",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 5",\
"ppg signal name = STRING : [16] CH10",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 5",\
"ppg signal name = STRING : [16] CH11",\
"",\
"[pulse12]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[pulse13]",\
"time offset (ms) = DOUBLE : 0.0902",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0.4008",\
"ppg signal name = STRING : [16] CH13",\
"",\
"[pulse14]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0.011",\
"ppg signal name = STRING : [16] CH14",\
"",\
"[pulse15]",\
"time offset (ms) = DOUBLE : 4",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH15",\
"",\
"[pulse16]",\
"time offset (ms) = DOUBLE : 50",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH16",\
"",\
"[skip]",\
"channel = INT : 11",\
"dummy = INT : 11",\
"",\
"[image]",\
"name = STRING : [32] none",\
"mode = STRING : [8] 1e",\
"num channels to display = INT : 16",\
"max active channel = INT : 14",\
"",\
"[names]",\
"names = STRING[32] :",\
"[64] ISAC Beam Gate",\
"[64] RFQ Extraction trigger",\
"[64] flexible 1 (2nd RFQ ET)",\
"[64] flexible 2 (3rd RFQ ET)",\
"[64] XY CB4 (SCI TOF gate)",\
"[64] Quadrupole Triplet [temp trapping]",\
"[64] Sikler Lens",\
"[64] flexible 3 (Camera trigger)",\
"[64] trapping (Coll kick)",\
"[64] flexible 4 (2nd trapping)",\
"[64] flexible 5  (3rd trapping)",\
"[64] AFG trigger",\
"[64] EBIT extraction (Gun kick)",\
"[64] BNG (HCI TOF Gate)",\
"[64] flexible 6 (open)",\
"[64] flexible 7 (open)",\
"[64] not assigned",\
"[64] Channel18",\
"[64] Channel19",\
"[64] Channel20",\
"[64] Channel21",\
"[64] Channel22",\
"[64] Channel23",\
"[64] Channel24",\
"[64] Channel25",\
"[64] Channel26",\
"[64] Channel27",\
"[64] Channel28",\
"[64] Channel29",\
"[64] Channel30",\
"[64] Channel31",\
"[64] Channel32",\
"",\
"[.]",\
"bitpat to set on stop = INT : 0",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1F_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse10;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse12;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse13;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse14;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse15;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse16;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_scan;
  struct {
    INT       channel;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
    INT       num_channels_to_display;
    INT       max_active_channel;
  } image;
  struct {
    char      names[32][64];
  } names;
} TITAN_ACQ_PPG_CYCLE_MODE_1F;

#define TITAN_ACQ_PPG_CYCLE_MODE_1F_STR(_name) char *_name[] = {\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0.05",\
"loop count = INT : 1000",\
"",\
"[pulse1]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 93",\
"ppg signal name = STRING : [16] CH1",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 10",\
"ppg signal name = STRING : [16] CH2",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 30",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 5",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 2",\
"ppg signal name = STRING : [16] CH6",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 3",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 4",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 5",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 37",\
"ppg signal name = STRING : [16] CH10",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 6",\
"ppg signal name = STRING : [16] CH11",\
"",\
"[pulse12]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 5",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[pulse13]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 5",\
"ppg signal name = STRING : [16] CH13",\
"",\
"[pulse14]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH14",\
"",\
"[pulse15]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH15",\
"",\
"[pulse16]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH16",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 100",\
"time reference = STRING : [20] _TBEGSCAN",\
"",\
"[skip]",\
"channel = INT : 11",\
"",\
"[image]",\
"name = STRING : [32] none",\
"mode = STRING : [8] 1f",\
"num channels to display = INT : 16",\
"max active channel = INT : 16",\
"",\
"[names]",\
"names = STRING[32] :",\
"[64] Channel1",\
"[64] Channel2",\
"[64] Channel3",\
"[64] Channel4",\
"[64] Channel5",\
"[64] Channel6",\
"[64] Channel7",\
"[64] Channel8",\
"[64] Channel9",\
"[64] Channel10",\
"[64] Channel11",\
"[64] Channel12",\
"[64] Channel13",\
"[64] Channel14",\
"[64] Channel15",\
"[64] Channel16",\
"[64] Channel17",\
"[64] Channel18",\
"[64] Channel19",\
"[64] Channel20",\
"[64] Channel21",\
"[64] Channel22",\
"[64] Channel23",\
"[64] Channel24",\
"[64] Channel25",\
"[64] Channel26",\
"[64] Channel27",\
"[64] Channel28",\
"[64] Channel29",\
"[64] Channel30",\
"[64] Channel31",\
"[64] Channel32",\
"",\
NULL }

//#define TITAN_ACQ_TUNES_DEFINED

  //typedef struct {
//  struct {
//    char      default[39];
//  } _e;
//  char      last_tune[32];
//} TITAN_ACQ_TUNES;

#define TITAN_ACQ_TUNES_STR(_name) char *_name[] = {\
"[1e]",\
"default = STRING : [39] /home/ebit/online/tunes/1e_Default.odb",\
"",\
"[.]",\
"last tune = STRING : [32] ",\
"",\
NULL }

#define TITAN_ACQ_POL_STRING_DEFINED

typedef struct {
} TITAN_ACQ_POL_STRING;

#define TITAN_ACQ_POL_STRING_STR(_name) char *_name[] = {\
"pol_string = STRING : [18] +/-  PPG Chan",\
NULL }

#endif

#ifndef EXCL_POLL_PPG

#define POLL_PPG_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} POLL_PPG_COMMON;

#define POLL_PPG_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 1",\
"Period = INT : 500",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxebit.triumf.ca",\
"Frontend name = STRING : [32] feEbit_ppg",\
"Frontend file name = STRING : [256] feebit_ppg.c",\
"",\
NULL }

#endif

#ifndef EXCL_SCALER

#define SCALER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} SCALER_COMMON;

#define SCALER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 377",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxebit.triumf.ca",\
"Frontend name = STRING : [32] feEbit_ppg",\
"Frontend file name = STRING : [256] feebit_ppg.c",\
"",\
NULL }

#endif

#ifndef EXCL_TRIGGER

#define TRIGGER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TRIGGER_COMMON;

#define TRIGGER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 2",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 50",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxebit.triumf.ca",\
"Frontend name = STRING : [32] feSis",\
"Frontend file name = STRING : [256] fesis.c",\
"",\
NULL }

#define TRIGGER_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      INT       internal_trigger;
      INT       negative_input;
      INT       eventsize_threshold;
      INT       pretrigger_delay;
      INT       trigger_gate_length;
      INT       energy_gate_length;
      INT       rawbuf_start;
      INT       raw_samples;
      INT       energy_sample_length;
      INT       energy_sample_index1;
      INT       energy_sample_index2;
      INT       energy_sample_index3;
      struct {
        INT       threshold[8];
        INT       peakingtime[8];
        INT       gaptime[8];
        INT       decimation[8];
        INT       pulse_length;
        INT       internal_gate_length;
        INT       internal_delay;
      } triggersettings;
      struct {
        INT       peakingtime[4];
        INT       gaptime[4];
        INT       decimation[4];
        INT       tau[8];
      } energysampling;
      INT       dac[8];
    } sis3302g;
  } sis3302g;
} TRIGGER_SETTINGS;

#define TRIGGER_SETTINGS_STR(_name) char *_name[] = {\
"[sis3302g/sis3302g]",\
"internal trigger = INT : 255",\
"negative input = INT : 255",\
"eventsize threshold = INT : 448",\
"pretrigger delay = INT : 0",\
"trigger Gate Length = INT : 0",\
"Energy Gate Length = INT : 0",\
"RawBuf Start = INT : 0",\
"Raw Samples = INT : 0",\
"Energy Sample Length = INT : 0",\
"Energy Sample Index1 = INT : 0",\
"Energy Sample index2 = INT : 0",\
"Energy Sample index3 = INT : 0",\
"",\
"[sis3302g/sis3302g/triggersettings]",\
"threshold = INT[8] :",\
"[0] 15",\
"[1] 40",\
"[2] 15",\
"[3] 55",\
"[4] 20",\
"[5] 45",\
"[6] 20",\
"[7] 0",\
"peakingtime = INT[8] :",\
"[0] 8",\
"[1] 8",\
"[2] 8",\
"[3] 8",\
"[4] 8",\
"[5] 8",\
"[6] 8",\
"[7] 0",\
"gaptime = INT[8] :",\
"[0] 16",\
"[1] 16",\
"[2] 16",\
"[3] 16",\
"[4] 16",\
"[5] 16",\
"[6] 16",\
"[7] 0",\
"decimation = INT[8] :",\
"[0] 1",\
"[1] 1",\
"[2] 1",\
"[3] 1",\
"[4] 1",\
"[5] 1",\
"[6] 1",\
"[7] 0",\
"pulse length = INT : 10",\
"internal gate length = INT : 2",\
"internal delay = INT : 0",\
"",\
"[sis3302g/sis3302g/energysampling]",\
"peakingtime = INT[4] :",\
"[0] 150",\
"[1] 150",\
"[2] 150",\
"[3] 150",\
"gaptime = INT[4] :",\
"[0] 250",\
"[1] 250",\
"[2] 250",\
"[3] 250",\
"decimation = INT[4] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"tau = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"",\
"[sis3302g/sis3302g]",\
"dac = INT[8] :",\
"[0] 50000",\
"[1] 50000",\
"[2] 50000",\
"[3] 50000",\
"[4] 50000",\
"[5] 50000",\
"[6] 50000",\
"[7] 50000",\
"",\
NULL }

#endif

#ifndef EXCL_PERIODIC

#define PERIODIC_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} PERIODIC_COMMON;

#define PERIODIC_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 3",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 33",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 511",\
"Period = INT : 1000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxebit.triumf.ca",\
"Frontend name = STRING : [32] FeTigCol",\
"Frontend file name = STRING : [256] fetigcol.c",\
"",\
NULL }

#endif

#ifndef EXCL_BEAMLINE

#define BEAMLINE_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} BEAMLINE_COMMON;

#define BEAMLINE_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 11",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 16",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 121",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 10",\
"Frontend host = STRING : [32] titan04.triumf.ca",\
"Frontend name = STRING : [32] scEpics",\
"Frontend file name = STRING : [256] scepics.c",\
"",\
NULL }

#define BEAMLINE_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      char      channel_name[34][32];
    } beamline;
  } devices;
  char      names[34][32];
  float     update_threshold_measured[34];
} BEAMLINE_SETTINGS;

#define BEAMLINE_SETTINGS_STR(_name) char *_name[] = {\
"[Devices/Beamline]",\
"Channel name = STRING[34] :",\
"[32] TITAN:SYS:TC1",\
"[32] TITAN:SYS:TC1",\
"[32] CCS2ISAC:BL2ACURRENT",\
"[32] TLN2:LED1:RDVOL",\
"[32] TLN2:LED2:RDVOL",\
"[32] TLN2:LED3:RDVOL",\
"[32] TLN2:LED4:RDVOL",\
"[32] TLN2:LED5:RDVOL",\
"[32] TLN2:LED6:RDVOL",\
"[32] TLN2:LED7:RDVOL",\
"[32] TLN2:PRESSD",\
"[32] TLN2:PRESSM",\
"[32] TLN2:LEDX:RDVOL",\
"[32] TLN2:FCYC:STATDRV",\
"[32] TLN2:FCYC:STATFRC",\
"[32] TLN2:NEXTFILLTIME.DESC",\
"[32] TLN2:LED8:RDVOL",\
"[32] Channel17",\
"[32] Channel18",\
"[32] Channel19",\
"[32] EBIT:IG1:RDVAC",\
"[32] EBIT:IG2:RDVAC",\
"[32] IBRMS1:RMS71:CALIB.VAL",\
"[32] Channel23",\
"[32] Channel24",\
"[32] Channel25",\
"[32] Channel26",\
"[32] Channel27",\
"[32] Channel28",\
"[32] Channel29",\
"[32] Channel30",\
"[32] Channel31",\
"[32] Channel32",\
"[32] Channel33",\
"",\
"[.]",\
"Names = STRING[34] :",\
"[32] General%RFQDC",\
"[32] General%Temperature1",\
"[32] General%ProtonCurrent",\
"[32] LN2%LED1",\
"[32] LN2%LED2",\
"[32] LN2%LED3",\
"[32] LN2%LED4",\
"[32] LN2%LED5",\
"[32] LN2%LED6",\
"[32] LN2%LED7",\
"[32] LN2%PressureDewar",\
"[32] LN2%PressureManifold",\
"[32] LN2%LEDManifold",\
"[32] LN2%StatusFill",\
"[32] LN2%StatusError",\
"[32] LN2%NextFillTime",\
"[32] LN2%LED8",\
"[32] Default%CH 17",\
"[32] Default%CH 18",\
"[32] Default%CH 19",\
"[32] EBIT%MagPressure",\
"[32] EBIT%GunPressure",\
"[32] Default%rmon71",\
"[32] Default%CH 23",\
"[32] Default%CH 24",\
"[32] Default%CH 25",\
"[32] Default%CH 26",\
"[32] Default%CH 27",\
"[32] Default%CH 28",\
"[32] Default%CH 29",\
"[32] Default%CH 30",\
"[32] Default%CH 31",\
"[32] Default%CH 32",\
"[32] Default%CH 33",\
"Update Threshold Measured = FLOAT[34] :",\
"[0] 1",\
"[1] 1",\
"[2] 1",\
"[3] 1",\
"[4] 1",\
"[5] 1",\
"[6] 1",\
"[7] 1",\
"[8] 1",\
"[9] 1",\
"[10] 1",\
"[11] 1",\
"[12] 1",\
"[13] 1",\
"[14] 1",\
"[15] 1",\
"[16] 1",\
"[17] 1",\
"[18] 1",\
"[19] 1",\
"[20] 1",\
"[21] 1",\
"[22] 1",\
"[23] 1",\
"[24] 1",\
"[25] 1",\
"[26] 1",\
"[27] 1",\
"[28] 1",\
"[29] 1",\
"[30] 1",\
"[31] 1",\
"[32] 1",\
"[33] 1",\
"",\
NULL }

#endif

#ifndef EXCL_SCALER3302

#define SCALER3302_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} SCALER3302_COMMON;

#define SCALER3302_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 377",\
"Period = INT : 5000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxebit.triumf.ca",\
"Frontend name = STRING : [32] feSis",\
"Frontend file name = STRING : [256] fesis.c",\
"",\
NULL }

#endif

#ifndef EXCL_LABVIEW

#define LABVIEW_SETTINGS_DEFINED

typedef struct {
  DWORD     tcp_port;
} LABVIEW_SETTINGS;

#define LABVIEW_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"tcp_port = DWORD : 12021",\
"",\
NULL }

#define LABVIEW_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} LABVIEW_COMMON;

#define LABVIEW_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 6",\
"Trigger mask = WORD : 64",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 16777215",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 511",\
"Period = INT : 1",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] titan04.triumf.ca",\
"Frontend name = STRING : [32] felabview",\
"Frontend file name = STRING : [256] felabview.cxx",\
"",\
NULL }

#endif

