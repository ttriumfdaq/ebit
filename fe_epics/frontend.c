/********************************************************************\

  Name:         frontend.c
  Created by:   Stefan Ritt

  Contents:     Example Slow Control Frontend for equipment control
                through EPICS channel access.

  $Log: frontend.c,v $
  Revision 1.6  2000/03/02 21:54:48  midas
  Added comments concerning channel names and possibility to disable functions

  Revision 1.5  1999/12/21 09:37:00  midas
  Use new driver names

  Revision 1.3  1999/10/08 14:04:45  midas
  Set initial channel number to 10

  Revision 1.2  1999/09/22 12:01:09  midas
  Fixed compiler warning

  Revision 1.1  1999/09/22 09:19:25  midas
  Added sources

  Revision 1.3  1998/10/23 08:46:26  midas
  Added #include "null.h"

  Revision 1.2  1998/10/12 12:18:59  midas
  Added Log tag in header


\********************************************************************/

#include <stdio.h>
#include "midas.h"
#include "generic.h"
#include "epics_ca.h"

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "Epics";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms    */
INT display_period = 000;

/* maximum event size produced by this frontend */
INT max_event_size = 1000;

/* buffer size to hold events */
INT event_buffer_size = 10*1000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5*1024*1024;

/*-- Equipment list ------------------------------------------------*/

/* 
The following statement allocates 10 channels for the beamline
control through the epics channel access device driver. The 
EPICS channel names are stored under 
   
  /Equipment/Beamline/Settings/Devices/Beamline

while the channel names as the midas slow control sees them are
under 

  /Equipment/Bemaline/Settings/Names

An example set of channel names is saved in the triumf.odb file
in this directory and can be loaded into the ODB with the odbedit
command

  load triumf.odb

before the frontend is started. The CMD_SET_LABEL statement 
actually defines who determines the label name. If this flag is
set, the CMD_SET_LABEL command in the device driver is disabled,
therefore the label is taken from EPICS, otherwise the label is
taken from MIDAS and set in EPICS.

The same can be done with the demand values. If the command
CMD_SET_DEMAND is disabled, the demand value is always determied
by EPICS.
*/

/* device driver list */
DEVICE_DRIVER epics_driver[] = {
  { "Epics", epics_ca, 20, 0 }, /* , CMD_SET_LABEL }, /* disable CMD_SET_LABEL */
  { "" }
};
INT cd_gen(INT cmd, PEQUIPMENT pequipment);
INT cd_gen_read(char *pevent, int); // copied from generic.h

EQUIPMENT equipment[] = {

  { "Epics",           /* equipment name */
    { 5, 0,                 /* event ID, trigger mask */
      "SYSTEM",                   /* event buffer */
      EQ_SLOW,              /* equipment type */
      0,                    /* event source */
      "MIDAS",              /* format */
      TRUE,                 /* enabled */
      511,               /* Read at every possible opportunity */
      1000,                 /* read every 1 sec */
      0,                    /* stop run after this event limit */
      0,                    /* number of sub events */
      1,                    /* log history every event */
      "","",""}, /* extra braces added for fucking Midas 2.0.0 */
    cd_gen_read,          /* readout routine */
    cd_gen,               /* class driver main routine */
    epics_driver,         /* device driver list */
    NULL,                 /* init string */
  },

  { "" }
};

/*-- Dummy routines ------------------------------------------------*/

INT  poll_event(INT source[], INT count, BOOL test) {return 1;};
INT  interrupt_configure(INT cmd, INT source[], PTYPE adr) {return 1;};

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  return CM_SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  return CM_SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
  static HNDLE hDB, hDemand=0,hISCL=0;
  static DWORD watchdog_time=0;
  static float  dog=0.f;
  static HNDLE hODBPV[20];
  static DWORD odbpvTID[20];
  static DWORD odbpvInd[20];
  /* slow down frontend not to eat all CPU cycles */
  /* ss_sleep(50); */
  cm_yield(50); /* 15Feb05 */
   
  if (ss_time() - watchdog_time > 1)
  {
#if 1
    watchdog_time = ss_time();
    if (!hDemand)
    {
      /* Run this code first time through */
      int i,j,k,blah,tid;
      char odb2pv[100],cblah[100];
      char *pch;
      HNDLE hTEMP,hBLAH;
      printf("First time through\n");
      cm_get_experiment_database(&hDB, NULL);
      if (db_find_key(hDB, 0, "/equipment/epics/variables/demand", &hDemand) != DB_SUCCESS)
      {
       cm_msg(MERROR, "frontend_loop", "epics demand key not found");
       return FE_ERR_HW;
      }
      if (db_find_key(hDB,0,"/equipment/epics/settings/keys", &hTEMP) != DB_SUCCESS) {
        cm_msg(MERROR, "fe_epics", "keys for ODB2PV not found");
	cm_msg(MERROR, "fe_epics","You MUST define /equipment/epics/setting/keys[] first!");
        return FE_ERR_HW;
      }
      /* Fill the hODBPV array with keys from &hTEMP = /equipment/epics/settings/keys[] */
      for (i=0; i<20; i++) { /* Get handles for copying over variables --shouldn't be hardcoded, but oh well*/
	hODBPV[i]=0xdeaddead; /* Use this to indicate a key not to be used for anything */
        odbpvTID[i]=0; /* type of datum pointed to by key */
	blah = 100; /* I hope this works */
	/* FIrst get a string containing the name of a key to be tranferred */
	if ( db_get_data_index(hDB,hTEMP,odb2pv,&blah,i,TID_STRING)==DB_SUCCESS) {
          /* Now check that the entry in keys is not empty */
	  if (strlen(odb2pv)>0) /* Not null */ {
	    /* Now check if that string is actually a real key */
            if (db_find_key(hDB,0,odb2pv,&hBLAH) == DB_SUCCESS) {
              hODBPV[i]=hBLAH; /* replace 0xdeaddead with valid key locator */
	      /* Now check what type of key it is */
              if (db_get_key_info(hDB,hBLAH,cblah,100,&tid,&blah,&blah)== DB_SUCCESS) {
                odbpvTID[i]=tid;
                odbpvInd[i]=0; /* assume at first that you want the 0th index of that key (e.g. not an array) */
		pch = strchr(odb2pv,'['); /* Check if there is an array index for this key */
                if (pch!=NULL) {
                  sscanf(pch,"[%d]",&k);
                  odbpvInd[i]=k;
                }                
              }
            }
          } 
        }
	printf("Key %d for odb2pv xfer %s set to 0x%08x index %02d type %02d\n",
               i, odb2pv, hODBPV[i],odbpvInd[i],odbpvTID[i]);
      }
    }
    /* Block this set of code just for readibility and localization of variables */
    {
      int i,j; float f; int blah;
      int dbstat;
      f=-666.0; /* take out once debugged */
      dbstat = DB_SUCCESS+1; /* default bad */
      for (i=0; i<20; i++) {
	if (hODBPV[i]!=0xdeaddead) {
          HNDLE hBLAH;
          hBLAH=hODBPV[i];
	  switch (odbpvTID[i]) {
	    case TID_FLOAT: { /* Add more of these as necessary */
              float datum;
	      datum = 555.;
	      blah = sizeof(datum);
	      dbstat=db_get_data_index(hDB,hBLAH,&datum,&blah,odbpvInd[i],odbpvTID[i]);
	      // printf("datum = %f, blah = %d\n",datum,blah);
	      f = datum;
	      break;
	    }
	    default: {
	      DWORD datum; /* default to the easiest one */
	      datum=555;
	      blah=sizeof(datum); /* I hope this works ... */
	      dbstat=db_get_data_index(hDB,hBLAH,&datum,&blah,odbpvInd[i],odbpvTID[i]);
	      // printf("datum = %d, blah = %d\n",datum,blah);
	      f = (float)datum;
	    }
          }
          if (dbstat!=DB_SUCCESS) printf("Error reading i=%d, err=%d\n",i,dbstat);
          db_set_data_index(hDB,hDemand,&f,sizeof(f),i,TID_FLOAT);
        }
      }
    }
#endif
  }
  return CM_SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  return CM_SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
  printf("Epics - EOR \n");
  return CM_SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
  return CM_SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
  return CM_SUCCESS;
}

/*------------------------------------------------------------------*/
