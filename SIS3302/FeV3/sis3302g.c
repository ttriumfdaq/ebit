#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "sis3302g.h"

int sis3302_adc_data_buffer[8] = {
   SIS3302_ADC1_OFFSET,   SIS3302_ADC2_OFFSET,
   SIS3302_ADC3_OFFSET,   SIS3302_ADC4_OFFSET,
   SIS3302_ADC5_OFFSET,   SIS3302_ADC6_OFFSET,
   SIS3302_ADC7_OFFSET,   SIS3302_ADC8_OFFSET
};

//=========================================================================
//========================== Midas wrapper ================================
#include "mvmestd.h"

int vme_A32D32_read(int p, unsigned int module_addr, unsigned int *data);
int vme_A32D32_write(int p, unsigned int module_addr, unsigned int data);
int vme_A32MBLT64_read(int p, unsigned int addr, unsigned int *dest, unsigned int  dma_req_length, unsigned int *dma_length);

int vme_A32D32_read(int p, unsigned int module_addr, unsigned int *data) {
  MVME_INTERFACE *mvme;

  mvme = (MVME_INTERFACE *) p;
  mvme_set_am(mvme, MVME_AM_A32);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  *data = mvme_read_value(mvme, module_addr);
  return 0;
}
int vme_A32D32_write(int p, unsigned int module_addr, unsigned int data) {
  MVME_INTERFACE *mvme;

  mvme = (MVME_INTERFACE *) p;
  mvme_set_am(mvme, MVME_AM_A32);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  mvme_write_value(mvme, module_addr, data);
  return 0;
}
int vme_A32MBLT64_read(int p, unsigned int addr, unsigned *dest, unsigned dma_req_length, unsigned *dma_length)
{  
   int i; // originally this function didn't return anything due to 3 bugs!

   *dma_length = 0;
   for (i=0; i<dma_req_length; i++) {   
      vme_A32D32_read(p, addr, dest++); // vme_A32D32_read(p, addr, dest);
      addr +=4; (*dma_length)++;        // *dma_length++;
   }
   return 0;
}

///////////////////////////////////////////////////////////////////////////
///////////////////////      General Registers      ///////////////////////

int SIS3302_Reset(int myvme, int base_addr) /*  Reset SIS3302  */
{
   int addr = base_addr + SIS3302_KEY_RESET;
   vme_A32D32_write(myvme, addr, 0x0);
   return(0);
}
int SIS3302_Version(int myvme, int base_addr, int *data)
{
   int addr = base_addr + SIS3302_MODID;
   vme_A32D32_read(myvme, addr, (unsigned *)data);
   return(0);  
}
// start acquisition
int SIS3302_Start1(int myvme, int base_addr)
{
   int addr = base_addr + SIS3302_KEY_DISARM_AND_ARM_BANK1;  
   vme_A32D32_write(myvme, addr, 0x0);
   return(0);
}
int SIS3302_Start2(int myvme, int base_addr)
{
   int addr = base_addr + SIS3302_KEY_DISARM_AND_ARM_BANK2;  
   vme_A32D32_write(myvme, addr, 0x0);
   return(0);
}
int SIS3302_CsrWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_CONTROL_STATUS;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
int SIS3302_AcqCtrlWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_ACQUISITION_CONTROL;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
int SIS3302_AcqCtrlRead(int myvme, int base_addr, int *data)
{
   int addr = base_addr + SIS3302_ACQUISITION_CONTROL;
   vme_A32D32_read(myvme, addr, (unsigned *)data);
   return(0);
}
// Reset Sample Logic (should do this after changing parameters)
int SIS3302_SampLogReset(int myvme, int base_addr)
{
   int addr = base_addr + SIS3302_KEY_SAMPLE_LOGIC_RESET;
   vme_A32D32_write(myvme, addr, 0x0);
   return(0);
}
// address threshold flag
int SIS3302_AddrThrWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_END_ADDRESS_THRESHOLD_ALL_ADC;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
// Memory Page Register
int SIS3302_MemPageWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_ADC_MEMORY_PAGE_REGISTER;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
///////////////////////////////////////////////////////////////////////////
///////////////////////         IRQ Registers       ///////////////////////
int SIS3302_IrqConfWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_IRQ_CONFIG;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}

int SIS3302_IrqCtrlWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_IRQ_CONTROL;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}

int SIS3302_BltBcastWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_CBLT_BROADCAST_SETUP;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
///////////////////////////////////////////////////////////////////////////
/////////////////////       Address Registers       ///////////////////////
// 
int SIS3302_EndSampleAddress(int myvme, int base_addr, int *data, int chan)
{
   int addr = base_addr;
   switch(chan){
   case  1: addr += SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC1; break;
   case  2: addr += SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC2; break;
   case  3: addr += SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC3; break;
   case  4: addr += SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC4; break;
   case  5: addr += SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC5; break;
   case  6: addr += SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC6; break;
   case  7: addr += SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC7; break;
   default: addr += SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC8; break;
   }
   vme_A32D32_read(myvme, addr, (unsigned *)data);
   return(0);
}

///////////////////////////////////////////////////////////////////////////
/////////////////////       Trigger Registers       ///////////////////////
// 
int SIS3302_TrigDelayGatelenWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ALL_ADC;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
int SIS3302_FastFirTrigWrite(int myvme, int base_addr, int data, int chan)
{
   int addr = base_addr;
   switch(chan){
   case  1: addr += SIS3302_TRIGGER_SETUP_ADC1; break;
   case  2: addr += SIS3302_TRIGGER_SETUP_ADC2; break;
   case  3: addr += SIS3302_TRIGGER_SETUP_ADC3; break;
   case  4: addr += SIS3302_TRIGGER_SETUP_ADC4; break;
   case  5: addr += SIS3302_TRIGGER_SETUP_ADC5; break;
   case  6: addr += SIS3302_TRIGGER_SETUP_ADC6; break;
   case  7: addr += SIS3302_TRIGGER_SETUP_ADC7; break;
   default: addr += SIS3302_TRIGGER_SETUP_ADC8; break;
   }
   vme_A32D32_write(myvme, addr, data);
   return(0);
}

int SIS3302_TrigThreshWrite(int myvme, int base_addr, int data, int chan)
{
   int addr = base_addr;
   switch(chan){
   case  1: addr += SIS3302_TRIGGER_THRESHOLD_ADC1; break;
   case  2: addr += SIS3302_TRIGGER_THRESHOLD_ADC2; break;
   case  3: addr += SIS3302_TRIGGER_THRESHOLD_ADC3; break;
   case  4: addr += SIS3302_TRIGGER_THRESHOLD_ADC4; break;
   case  5: addr += SIS3302_TRIGGER_THRESHOLD_ADC5; break;
   case  6: addr += SIS3302_TRIGGER_THRESHOLD_ADC6; break;
   case  7: addr += SIS3302_TRIGGER_THRESHOLD_ADC7; break;
   default: addr += SIS3302_TRIGGER_THRESHOLD_ADC8; break;
   }
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
///////////////////////////////////////////////////////////////////////////
/////////////////////      E Filter Registers       ///////////////////////
// Energy Window
int SIS3302_EnergyGatelenWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_ENERGY_GATE_LENGTH_ALL_ADC;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
// Energy Window Gap, Peaking and Decimation
int SIS3302_EnergySetupWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_ENERGY_SETUP_GP_ALL_ADC;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
// raw data buffer configuration
int SIS3302_RawBufConfWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_RAW_DATA_BUFFER_CONFIG_ALL_ADC;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
// energy sample length
int SIS3302_ESampleWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_ENERGY_SAMPLE_LENGTH_ALL_ADC;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
// energy sample index 1-3
int SIS3302_ESampleIndexWrite(int myvme, int base_addr, int data, int num)
{
   int addr = base_addr;
   switch(num){
   case  1: addr += SIS3302_ENERGY_SAMPLE_START_INDEX1_ALL_ADC; break;
   case  2: addr += SIS3302_ENERGY_SAMPLE_START_INDEX2_ALL_ADC; break;
   default: addr += SIS3302_ENERGY_SAMPLE_START_INDEX3_ALL_ADC; break;
   }
   vme_A32D32_write(myvme, addr, data);
   return(0);
}

int SIS3302_TauWrite(int myvme, int base_addr, int data, int chan)
{
   int addr = base_addr;
   switch(chan){
   case  1: addr += SIS3302_ENERGY_TAU_FACTOR_ADC1; break;
   case  2: addr += SIS3302_ENERGY_TAU_FACTOR_ADC2; break;
   case  3: addr += SIS3302_ENERGY_TAU_FACTOR_ADC3; break;
   case  4: addr += SIS3302_ENERGY_TAU_FACTOR_ADC4; break;
   case  5: addr += SIS3302_ENERGY_TAU_FACTOR_ADC5; break;
   case  6: addr += SIS3302_ENERGY_TAU_FACTOR_ADC6; break;
   case  7: addr += SIS3302_ENERGY_TAU_FACTOR_ADC7; break;
   default: addr += SIS3302_ENERGY_TAU_FACTOR_ADC8; break;
   }
   vme_A32D32_write(myvme, addr, data);
   return(0);
}

int SIS3302_EvtConfWrite(int myvme, int base_addr, int data, int chan)
{
   int addr = base_addr;
   switch(chan){
   case  0: addr += SIS3302_EVENT_CONFIG_ADC12; break;
   case  1: addr += SIS3302_EVENT_CONFIG_ADC34; break;
   case  2: addr += SIS3302_EVENT_CONFIG_ADC56; break;
   default: addr += SIS3302_EVENT_CONFIG_ADC78; break;
   }
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
/////////////////////////////////////////////////////////////////////
//////////////////////     MCA  Settings     ////////////////////////
// 
int SIS3302_McaScanNHistWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_MCA_SCAN_NOF_HISTOGRAMS_PRESET;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
// MCA_SCAN_SETUP_PRESCALE_FACTOR
int SIS3302_McaScanPrescaleWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_MCA_SCAN_SETUP_PRESCALE_FACTOR;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
// MCA_SCAN_CONTROL
int SIS3302_McaScanCtrlWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_MCA_SCAN_CONTROL;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
// MCA_MULTISCAN_NOF_SCANS_PRESET
int SIS3302_McaNScanWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_MCA_MULTISCAN_NOF_SCANS_PRESET;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
// ENERGY2HISTOGRAM_PARAM
int SIS3302_E2HistWrite(int myvme, int base_addr, int data, int group)
{
   int addr = base_addr;
   switch(group){
   case  1: addr += SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC1357; break;
   default: addr += SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC2468; break;
   }
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
// MCA_HISTOGRAM_PARAM_ALL_ADC
int SIS3302_McaHistParWrite(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_MCA_HISTOGRAM_PARAM_ALL_ADC;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
#if(0)
// 
int SIS3302_Write(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_;
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
// 
int SIS3302_Write(int myvme, int base_addr, int data, int num)
{
   int addr = base_addr;
   switch(num){
   case  1: addr += SIS3302_; break;
   case  2: addr += SIS3302_; break;
   default: addr += SIS3302_; break;
   }
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
#endif
///////////////////////////////////////////////////////////////////////////
///////////////////////         DAC Registers       ///////////////////////
int SIS3302_DACCsrSET(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_DAC_CONTROL_STATUS;  
   vme_A32D32_write(myvme, addr, data);
   return(0);
}
int SIS3302_DACCsrRead(int myvme, int base_addr, int *data)
{
   int addr = base_addr + SIS3302_DAC_CONTROL_STATUS;  
   vme_A32D32_read(myvme, addr, (unsigned *)data);
   return(0);
}

int SIS3302_DACSET(int myvme, int base_addr, int data)
{
   int addr = base_addr + SIS3302_DAC_DATA;  
   vme_A32D32_write(myvme, addr, data);
   return(0);
}

int SIS3302_DACREAD(int myvme, int base_addr, int *data)
{
   int addr = base_addr + SIS3302_DAC_DATA;  
   vme_A32D32_read(myvme, addr, (unsigned *)data);
   return(0);
}
/* ----------------------------------------------------------
   offset Value_array		DAC offset value (16 bit)
---------------------------------------------------------- */
#define TIMEOUT_COUNT 5000
int sis3302_write_dac_offset(int myvme, unsigned module_addr, unsigned int *offset_value_array)
{
   unsigned i, data, count;
   for (i=0;i<8;i++) {	
      data =  offset_value_array[i] ;
      SIS3302_DACSET(myvme, module_addr, data);
      data =  1 + (i << 4); // write to DAC Register
      SIS3302_DACSET(myvme, module_addr, data);

      count = 0 ;
      do {
	 SIS3302_DACCsrRead(myvme, module_addr, (int *)&data);
         count++;
      } while ( ((data & 0x8000) == 0x8000) && (count < TIMEOUT_COUNT) );
      if( count >= TIMEOUT_COUNT ){ return -2; }

      data =  2 + (i << 4); // Load DACs 
      SIS3302_DACSET(myvme, module_addr, data);

      count = 0 ;
      do {
 	 SIS3302_DACCsrRead(myvme, module_addr, (int *)&data);
         count++;
      } while ( ((data & 0x8000) == 0x8000) && (count < TIMEOUT_COUNT) );
      if( count >= TIMEOUT_COUNT ){ return -3; }
   }
   return 0;	 
}
