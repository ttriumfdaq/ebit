/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Mon Feb 15 12:23:31 2010

\********************************************************************/

#ifndef EXCL_SIS3302G

#define SIS3302G_PARAM_DEFINED

typedef struct {
  INT       raw_samples;
  INT       corr_samples;
} SIS3302G_PARAM;

#define SIS3302G_PARAM_STR(_name) char *_name[] = {\
"[.]",\
"raw_samples = INT : 0",\
"corr_samples = INT : 0",\
"",\
NULL }

#endif

#ifndef EXCL_TRIGGER

#define TRIGGER_SETTINGS_DEFINED

typedef struct {
  struct {
    INT       internal_trigger;
    INT       negative_input;
    INT       eventsize_threshold;
    INT       pretrigger;
    INT       trigger_gate_length;
    INT       energy_gate_length;
    INT       decimation;
    INT       energy_gap;
    INT       energy_peaktime;
    INT       energy_tau;
    INT       rawbuf_start;
    INT       raw_samples;
    INT       energy_sample_length;
    INT       energy_sample_index1;
    INT       energy_sample_index2;
    INT       energy_sample_index3;
    INT       fastfir_peaktime;
    INT       fastfir_gap;
    INT       fastfir_triglength;
    INT       trigger_threshold;
    INT       dac_offset;
  } sis3302g;
} TRIGGER_SETTINGS;

#define TRIGGER_SETTINGS_STR(_name) char *_name[] = {\
"[sis3302g]",\
"internal trigger = INT : 3",\
"negative input = INT : 0",\
"eventsize threshold = INT : 10",\
"pretrigger = INT : 128",\
"trigger Gate Length = INT : 448",\
"Energy Gate Length = INT : 630",\
"Decimation = INT : 0",\
"Energy Gap = INT : 40",\
"Energy PeakTime = INT : 180",\
"Energy Tau = INT : 0",\
"RawBuf Start = INT : 0",\
"Raw Samples = INT : 256",\
"Energy Sample Length = INT : 256",\
"Energy Sample Index1 = INT : 0",\
"Energy Sample index2 = INT : 0",\
"Energy Sample index3 = INT : 0",\
"FastFir PeakTime = INT : 8",\
"FastFir Gap = INT : 12",\
"FastFir TrigLength = INT : 10",\
"Trigger Threshold = INT : 256",\
"DAC Offset = INT : 0",\
"",\
NULL }

#define TRIGGER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TRIGGER_COMMON;

#define TRIGGER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 2",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 500",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxdaq13.triumf.ca",\
"Frontend name = STRING : [32] feSis",\
"Frontend file name = STRING : [256] fesis.c",\
"",\
NULL }

#endif

#ifndef EXCL_SCALER

#define SCALER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} SCALER_COMMON;

#define SCALER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 377",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxdaq13.triumf.ca",\
"Frontend name = STRING : [32] feSis",\
"Frontend file name = STRING : [256] fesis.c",\
"",\
NULL }

#endif

