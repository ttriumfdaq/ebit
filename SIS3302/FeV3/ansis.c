#include <stdio.h>
#include <time.h>
#include <unistd.h> /* usleep */
#include <math.h>

#include "midas.h"
#include "experim.h"

#include <TH1F.h>
#include <TH2F.h>
#include <TTree.h>
#include <TDirectory.h>

/*-- Module declaration --------------------------------------------*/
int sis3302_event(EVENT_HEADER *, void *);
int sis3302_init(void);
int sis3302_bor(INT run_number);
int sis3302_eor(INT run_number);
static void odb_callback(INT hDB, INT hseq, void *info);

SIS3302G_PARAM_STR(sis3302g_param_str);
SIS3302G_PARAM sis3302g_param;

ANA_MODULE sis3302_module = {
   "sis3302",              /* module name           */
   "CP",                   /* author                */
   sis3302_event,          /* event routine         */
   sis3302_bor,            /* BOR routine           */
   sis3302_eor,            /* EOR routine           */
   sis3302_init,           /* init routine          */
   NULL,                   /* exit routine          */
   &sis3302g_param,         /* parameter structure   */
   sizeof(sis3302g_param),  /* structure size        */
   sis3302g_param_str,      /* initial parameters    */
};
/*-------------------------------------------------------------------*/

#define NUM_CHAN    8
#define NUM_MISC    2
#define ADC_CHAN    16384
#define MAX_SAMPLES 16384

static TH1F *hEnergy[NUM_CHAN], *hRaw[NUM_CHAN], *hRaw2[NUM_CHAN];

static TH1F     *h_misc[NUM_MISC];
static char misc_handle[NUM_MISC][16] = {"EHIT", "GDEHIT"};
static char misc_title [NUM_MISC][64] = {"E pattern" "Good Energy Pattern"};               
static int    misc_chan[NUM_MISC]     = { NUM_CHAN, NUM_CHAN};

int sis3302_unpack(int nitems, DWORD *pdata, int chan);

//---------------------------------------------------------------------

float spread(int val){ return( val + rand()/(1.0*RAND_MAX) ); }
int sis3302_eor(INT run_number){ return SUCCESS; }
int sis3302_bor(INT run_number){ return SUCCESS; }
void odb_callback(INT hDB, INT hseq, void *info){ }

int hist_init_roody()
{
   char name[256], title[256];
   int i;

   open_subfolder("Raw");                                        // Waveforms 
   for(i=0; i<NUM_CHAN; i++){    
      sprintf(name,  "WFD%d", i );  
      sprintf(title, "Wave form channel%d", i );  
      hRaw[i] = H1_BOOK(name, title, MAX_SAMPLES, 0, MAX_SAMPLES);
   }
   close_subfolder();
   open_subfolder("Raw2");                                        // Waveforms 
   for(i=0; i<NUM_CHAN; i++){    
      sprintf(name,  "WFDC%d", i );  
      sprintf(title, "Corrected Wave form channel%d", i );  
      hRaw2[i] = H1_BOOK(name, title, MAX_SAMPLES, 0, MAX_SAMPLES);
   }
   close_subfolder();
   open_subfolder("Energy");                                      // Energy  
   for(i=0; i<NUM_CHAN; i++){
     sprintf(name,  "ADC%d", i);  
     sprintf(title, "Energy chan%d", i );
     hEnergy[i] = H1_BOOK(name, title, ADC_CHAN, 0, ADC_CHAN );
   }
   close_subfolder();
   open_subfolder("Misc");                                        // Misc
   for(i=0; i<NUM_MISC; i++){
      h_misc[i] = H1_BOOK(misc_handle[i], misc_title[i], misc_chan[i], 0, misc_chan[i]);
   }
   close_subfolder();
   return(0);
}

int sis3302_init(void)
{
   char odb_str[128], errmsg[128];
   HNDLE hDB, hKey;

   sprintf(odb_str,"/Analyzer/Parameters/sis3302g");
   cm_get_experiment_database(&hDB, NULL);
   db_create_record( hDB, 0, odb_str, strcomb(sis3302g_param_str) );
   db_find_key( hDB, 0, odb_str, &hKey );
   if( db_open_record(hDB, hKey, &sis3302g_param, sizeof(sis3302g_param), MODE_READ, odb_callback, NULL) != DB_SUCCESS ){
       sprintf(errmsg, "Cannot open %s tree in ODB", odb_str);
       cm_msg(MERROR,"sis3302_init", errmsg);
   }
   odb_callback(hDB, hKey, NULL); /* initialise .. */
   hist_init_roody();
   return SUCCESS;
}

int sis3302_event(EVENT_HEADER *pheader, void *pevent)
{
   static char bars[] = "|/-\\";
   static int i_bar;
   char bank_name[5];
   int i, bank_len;
   DWORD *idata; 
 
   for(i=0; i<NUM_CHAN; i++){
      sprintf(bank_name,"SIS%d", i);
      if( (bank_len = bk_locate(pevent, bank_name, &idata)) == 0 ){ continue; }
      sis3302_unpack(bank_len, idata, i);
      printf("..%c - Serial:%d size:%d", bars[i_bar++ % 4],
          (int)SERIAL_NUMBER(pevent), bk_size(pevent) );
      if( ((SERIAL_NUMBER(pevent)+1) % 1000) == 0 ){
         printf("\n");
      } else {
         printf("\r");
      }
   }
   return(0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/* Sis data currently looks like ...                                                               */
/* Bank:SIS0 Length: 152(I*1)/38(I*4)/38(Type) Type:Unsigned Integer*4                             */
/*    1-> 0x49832000 0x969db6fe 0x5aee5abe 0x5b3b5b1d 0x5b965b75 0x5bf45bd0 0x5c565c26 0x5cc45c87  */
/*    9-> 0x5d2f5c1d 0x5d7c5d3b 0x5dc85d99 0x5de35e0d 0x5e135e25 0x5e325e1a 0x5e445e27 0x5e295e3a  */
/*   17-> 0x5e5a5e06 0x5e6a5df9 0x5e295e41 0x5e1b5e3a 0x5e005e2a 0x5db65dd8 0x5d785d8d 0x5cfe5d41  */
/*   25-> 0x5c9b5cd7 0x5c305c77 0x5be05c06 0x5b835baa 0x5b215b52 0x5ac15af1 0x5a615aa0 0x59fe5a2f  */
/*   33-> 0x59ab59d2 0x59425976 0x00011cfa 0x000000ed 0x01000001 0xdeadbeef                        */
/*   Timestamp[47:32] Evthead,ADCID                                          0x4983 2000           */
/*   Timestamp[32:00] Timestamp[32:00]                                       0x969d b6fe           */
/*   Sample2,         sample1          (0-65532)                             0x5aee ....           */
/*   MAWD Buffer (Signed int)          (0-510)                                                     */
/*   Emax                                                                    0x00011cfa            */
/*   Efirst                                                                  0x000000ed            */
/*   Flags:  Pileup,Retrig,N+1Trig,N-1Trig,FastTrgCount[4bit],23*0,TrigFlag  0x01000001            */
/*   DeadBeef                                                                                      */
/////////////////////////////////////////////////////////////////////////////////////////////////////

#define SIS_TRAILER          0xdeadbeef

static int timestamp[2], ev_head;

int sis3302_unpack(int nitems, DWORD *pdata, int channel)
{
   int i, chanflag, sample, flags, energy, ehigh, elow;
   short data;

   timestamp[0] = pdata[1]; timestamp[1] = pdata[0] >> 16;
   ev_head  = pdata[0] & 0xffff;
   pdata += 2; nitems -=2;

   chanflag = ev_head & 0xf;

   sample = 0;
   for(i=0; i<(sis3302g_param.raw_samples/2); i++){
      data = pdata[i] & 0xffff;
      hRaw[channel]->SetBinContent(sample++, data);
      data = pdata[i] >> 16;
      hRaw[channel]->SetBinContent(sample++, data);
   }
   pdata += i; nitems -= i;

   sample = 0;
   for(i=0; i<sis3302g_param.corr_samples; i++){
      hRaw2[channel]->SetBinContent(sample++, pdata[i]);
   }
   pdata += i; nitems -= i;

#define ENERGY_DIVISOR 256

   ehigh = pdata[0]; elow = pdata[1]; energy = ehigh - elow;
   hEnergy[channel]->Fill(energy/ENERGY_DIVISOR, 1);
   pdata += 2; nitems -=2;
   flags = pdata[0];

   if( pdata[1] != SIS_TRAILER ){
     fprintf(stderr,"error unpacking data\n");
   }
   return SUCCESS;
}
