/*  Filename: SIS3302_v1405.h                                              */

/* SIS3302 Standard */
#define SIS3302_CONTROL_STATUS                  0x0	 /* read/write; D32 */
#define SIS3302_MODID                           0x4	 /* read only;  D32 */
#define SIS3302_IRQ_CONFIG                      0x8      /* read/write; D32 */
#define SIS3302_IRQ_CONTROL                     0xC      /* read/write; D32 */
#define SIS3302_ACQUISITION_CONTROL            0x10      /* read/write; D32 */

#define SIS3302_CBLT_BROADCAST_SETUP           0x30      /* read/write; D32 */
#define SIS3302_ADC_MEMORY_PAGE_REGISTER       0x34      /* read/write; D32 */

#define SIS3302_DAC_CONTROL_STATUS             0x50      /* read/write; D32 */
#define SIS3302_DAC_DATA                       0x54      /* read/write; D32 */

/* Key Addresses  */
#define SIS3302_KEY_RESET                      0x400	 /* write only; D32 */
#define SIS3302_KEY_SAMPLE_LOGIC_RESET         0x410	 /* write only; D32 */
#define SIS3302_KEY_DISARM                     0x414	 /* write only; D32 */
#define SIS3302_KEY_TRIGGER                    0x418	 /* write only; D32 */
#define SIS3302_KEY_TIMESTAMP_CLEAR            0x41C	 /* write only; D32 */
#define SIS3302_KEY_DISARM_AND_ARM_BANK1       0x420	 /* write only; D32 */
#define SIS3302_KEY_DISARM_AND_ARM_BANK2       0x424	 /* write only; D32 */
#define SIS3302_KEY_RESET_DDR2_LOGIC           0x428	 /* write only; D32 */

/* all ADC FPGA groups (all but 1st:Gamma) */
#define SIS3302_EVENT_CONFIG_ALL_ADC                	    0x01000000	  
#define SIS3302_END_ADDRESS_THRESHOLD_ALL_ADC       	    0x01000004
#define SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ALL_ADC 0x01000008
#define SIS3302_RAW_DATA_BUFFER_CONFIG_ALL_ADC         	    0x0100000C

#define SIS3302_ENERGY_SETUP_GP_ALL_ADC   		  0x01000040
#define SIS3302_ENERGY_GATE_LENGTH_ALL_ADC		  0x01000044 
#define SIS3302_ENERGY_SAMPLE_LENGTH_ALL_ADC		  0x01000048
#define SIS3302_ENERGY_SAMPLE_START_INDEX1_ALL_ADC	  0x0100004C
#define SIS3302_ENERGY_SAMPLE_START_INDEX2_ALL_ADC	  0x01000050
#define SIS3302_ENERGY_SAMPLE_START_INDEX3_ALL_ADC	  0x01000054
#define SIS3302_ENERGY_TAU_FACTOR_ADC1357		  0x01000058
#define SIS3302_ENERGY_TAU_FACTOR_ADC2468		  0x0100005C

/* ADC12 FPGA group */
#define SIS3302_EVENT_CONFIG_ADC12                	  0x02000000 // Gamma
#define SIS3302_END_ADDRESS_THRESHOLD_ADC12      	  0x02000004 // Gamma
#define SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC12 0x02000008 // Gamma
#define SIS3302_RAW_DATA_BUFFER_CONFIG_ADC12       	  0x0200000C // Gamma
#define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC1          	  0x02000010
#define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC2          	  0x02000014
#define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC1   	  0x02000018
#define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC2   	  0x0200001C
#define SIS3302_ACTUAL_SAMPLE_VALUE_ADC12           	  0x02000020
#define SIS3302_DDR2_TEST_REGISTER_ADC12		  0x02000028
#define SIS3302_TRIGGER_SETUP_ADC1                     	  0x02000030
#define SIS3302_TRIGGER_THRESHOLD_ADC1              	  0x02000034
#define SIS3302_TRIGGER_SETUP_ADC2                  	  0x02000038
#define SIS3302_TRIGGER_THRESHOLD_ADC2              	  0x0200003C
#define SIS3302_ENERGY_SETUP_GP_ADC12   		  0x02000040 // Gamma
#define SIS3302_ENERGY_GATE_LENGTH_ADC12	          0x02000044 // Gamma
#define SIS3302_ENERGY_SAMPLE_LENGTH_ADC12   	          0x02000048 // Gamma
#define SIS3302_ENERGY_SAMPLE_START_INDEX1_ADC12          0x0200004C // Gamma
#define SIS3302_ENERGY_SAMPLE_START_INDEX2_ADC12          0x02000050 // Gamma
#define SIS3302_ENERGY_SAMPLE_START_INDEX3_ADC12	  0x02000054 // Gamma
#define SIS3302_ENERGY_TAU_FACTOR_ADC1			  0x02000058 // Gamma
#define SIS3302_ENERGY_TAU_FACTOR_ADC2			  0x0200005C // Gamma

/* ADC34 FPGA group */
#define SIS3302_EVENT_CONFIG_ADC34                	  0x02800000 // Gamma
#define SIS3302_END_ADDRESS_THRESHOLD_ADC34      	  0x02800004 // Gamma
#define SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC34 0x02800008 // Gamma
#define SIS3302_RAW_DATA_BUFFER_CONFIG_ADC34       	  0x0280000C // Gamma
#define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC3          	  0x02800010
#define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC4          	  0x02800014
#define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC3   	  0x02800018
#define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC4   	  0x0280001C
#define SIS3302_ACTUAL_SAMPLE_VALUE_ADC34           	  0x02800020
#define SIS3302_DDR2_TEST_REGISTER_ADC34		  0x02800028
#define SIS3302_TRIGGER_SETUP_ADC3                     	  0x02800030
#define SIS3302_TRIGGER_THRESHOLD_ADC3              	  0x02800034
#define SIS3302_TRIGGER_SETUP_ADC4                  	  0x02800038
#define SIS3302_TRIGGER_THRESHOLD_ADC4              	  0x0280003C
#define SIS3302_ENERGY_SETUP_GP_ADC34   		  0x02800040 // Gamma
#define SIS3302_ENERGY_GATE_LENGTH_ADC34	          0x02800044 // Gamma
#define SIS3302_ENERGY_SAMPLE_LENGTH_ADC34   	          0x02800048 // Gamma
#define SIS3302_ENERGY_SAMPLE_START_INDEX1_ADC34          0x0280004C // Gamma
#define SIS3302_ENERGY_SAMPLE_START_INDEX2_ADC34          0x02800050 // Gamma
#define SIS3302_ENERGY_SAMPLE_START_INDEX3_ADC34	  0x02800054 // Gamma
#define SIS3302_ENERGY_TAU_FACTOR_ADC3			  0x02800058 // Gamma
#define SIS3302_ENERGY_TAU_FACTOR_ADC4			  0x0280005C // Gamma

/* ADC56 FPGA group */
#define SIS3302_EVENT_CONFIG_ADC56                	  0x03000000 // Gamma
#define SIS3302_END_ADDRESS_THRESHOLD_ADC56      	  0x03000004 // Gamma
#define SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC56 0x03000008 // Gamma
#define SIS3302_RAW_DATA_BUFFER_CONFIG_ADC56       	  0x0300000C // Gamma
#define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC5          	  0x03000010
#define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC6          	  0x03000014
#define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC5   	  0x03000018
#define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC6   	  0x0300001C
#define SIS3302_ACTUAL_SAMPLE_VALUE_ADC56           	  0x03000020
#define SIS3302_DDR2_TEST_REGISTER_ADC56		  0x03000028
#define SIS3302_TRIGGER_SETUP_ADC5                    	  0x03000030
#define SIS3302_TRIGGER_THRESHOLD_ADC5              	  0x03000034
#define SIS3302_TRIGGER_SETUP_ADC6                  	  0x03000038
#define SIS3302_TRIGGER_THRESHOLD_ADC6              	  0x0300003C
#define SIS3302_ENERGY_SETUP_GP_ADC56   		  0x03000040 // Gamma
#define SIS3302_ENERGY_GATE_LENGTH_ADC56	          0x03000044 // Gamma
#define SIS3302_ENERGY_SAMPLE_LENGTH_ADC56   	          0x03000048 // Gamma
#define SIS3302_ENERGY_SAMPLE_START_INDEX1_ADC56          0x0300004C // Gamma
#define SIS3302_ENERGY_SAMPLE_START_INDEX2_ADC56          0x03000050 // Gamma
#define SIS3302_ENERGY_SAMPLE_START_INDEX3_ADC56	  0x03000054 // Gamma
#define SIS3302_ENERGY_TAU_FACTOR_ADC5			  0x03000058 // Gamma
#define SIS3302_ENERGY_TAU_FACTOR_ADC6			  0x0300005C // Gamma

/* ADC78 FPGA group */
#define SIS3302_EVENT_CONFIG_ADC78                	  0x03800000 // Gamma
#define SIS3302_END_ADDRESS_THRESHOLD_ADC78      	  0x03800004 // Gamma
#define SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC78 0x03800008 // Gamma
#define SIS3302_RAW_DATA_BUFFER_CONFIG_ADC78       	  0x0380000C // Gamma
#define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC7          	  0x03800010
#define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC8          	  0x03800014
#define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC7   	  0x03800018
#define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC8   	  0x0380001C
#define SIS3302_ACTUAL_SAMPLE_VALUE_ADC78           	  0x03800020
#define SIS3302_DDR2_TEST_REGISTER_ADC78		  0x03800028
#define SIS3302_TRIGGER_SETUP_ADC7                     	  0x03800030
#define SIS3302_TRIGGER_THRESHOLD_ADC7              	  0x03800034
#define SIS3302_TRIGGER_SETUP_ADC8                  	  0x03800038
#define SIS3302_TRIGGER_THRESHOLD_ADC8              	  0x0380003C
#define SIS3302_ENERGY_SETUP_GP_ADC78   		  0x03800040 // Gamma
#define SIS3302_ENERGY_GATE_LENGTH_ADC78	          0x03800044 // Gamma
#define SIS3302_ENERGY_SAMPLE_LENGTH_ADC78   	          0x03800048 // Gamma
#define SIS3302_ENERGY_SAMPLE_START_INDEX1_ADC78          0x0380004C // Gamma
#define SIS3302_ENERGY_SAMPLE_START_INDEX2_ADC78          0x03800050 // Gamma
#define SIS3302_ENERGY_SAMPLE_START_INDEX3_ADC78	  0x03800054 // Gamma
#define SIS3302_ENERGY_TAU_FACTOR_ADC7			  0x03800058 // Gamma
#define SIS3302_ENERGY_TAU_FACTOR_ADC8			  0x0380005C // Gamma

#define SIS3302_ADC1_OFFSET                         	  0x04000000	  
#define SIS3302_ADC2_OFFSET                         	  0x04800000	  
#define SIS3302_ADC3_OFFSET                         	  0x05000000	  
#define SIS3302_ADC4_OFFSET                         	  0x05800000	  
#define SIS3302_ADC5_OFFSET                         	  0x06000000	  
#define SIS3302_ADC6_OFFSET                         	  0x06800000	  
#define SIS3302_ADC7_OFFSET                         	  0x07000000	  
#define SIS3302_ADC8_OFFSET                         	  0x07800000	  

#define SIS3302_NEXT_ADC_OFFSET                     	  0x00800000	  

/* define sample clock */
#define SIS3302_ACQ_SET_CLOCK_TO_100MHZ               0x70000000 /* default */
#define SIS3302_ACQ_SET_CLOCK_TO_50MHZ                0x60001000
#define SIS3302_ACQ_SET_CLOCK_TO_25MHZ                0x50002000
#define SIS3302_ACQ_SET_CLOCK_TO_10MHZ                0x40003000
#define SIS3302_ACQ_SET_CLOCK_TO_1MHZ                 0x30004000
#define SIS3302_ACQ_SET_CLOCK_TO_LEMO_RANDOM_CLOCK_IN 0x20005000
#define SIS3302_ACQ_SET_CLOCK_TO_LEMO_CLOCK_IN        0x10006000
/*#define SIS3302_ACQ_SET_CLOCK_TO_P2_CLOCK_IN        0x00007000 */
#define SIS3302_ACQ_SET_CLOCK_TO_SECOND_100MHZ        0x00007000

#define SIS3302_ACQ_DISABLE_LEMO_TRIGGER       0x01000000 // GAMMA, 091207
#define SIS3302_ACQ_ENABLE_LEMO_TRIGGER        0x00000100 // GAMMA, 091207
#define SIS3302_ACQ_DISABLE_LEMO_TIMESTAMPCLR  0x02000000 // GAMMA, 091207
#define SIS3302_ACQ_ENABLE_LEMO_TIMESTAMPCLR   0x00000200 // GAMMA, 091207
/* new 16.3.2009 */
#define SIS3302_ACQ_DISABLE_EXTERNAL_LEMO_IN3  0x01000000 // GAMMA,V_1205+
#define SIS3302_ACQ_ENABLE_EXTERNAL_LEMO_IN3   0x00000100 // GAMMA,V_1205+
#define SIS3302_ACQ_DISABLE_EXTERNAL_LEMO_IN2  0x02000000 // GAMMA,V_1205+
#define SIS3302_ACQ_ENABLE_EXTERNAL_LEMO_IN2   0x00000200 // GAMMA,V_1205+
#define SIS3302_ACQ_DISABLE_EXTERNAL_LEMO_IN1  0x04000000 // GAMMA,V_1205+
#define SIS3302_ACQ_ENABLE_EXTERNAL_LEMO_IN1   0x00000400 // GAMMA,V_1205+

#define SIS3302_ACQ_SET_LEMO_IN_MODE0          0x00070000 // GAMMA,V_1205+
#define SIS3302_ACQ_SET_LEMO_IN_MODE1          0x00060001 // GAMMA,V_1205+
#define SIS3302_ACQ_SET_LEMO_IN_MODE2          0x00050002 // GAMMA,V_1205+
#define SIS3302_ACQ_SET_LEMO_IN_MODE3          0x00040003 // GAMMA,V_1205+
#define SIS3302_ACQ_SET_LEMO_IN_MODE4          0x00030004 // GAMMA,V_1205+
#define SIS3302_ACQ_SET_LEMO_IN_MODE5          0x00020005 // GAMMA,V_1205+
#define SIS3302_ACQ_SET_LEMO_IN_MODE6          0x00010006 // GAMMA,V_1205+
#define SIS3302_ACQ_SET_LEMO_IN_MODE7          0x00000007 // GAMMA,V_1205+
#define SIS3302_ACQ_SET_LEMO_IN_MODE_BIT_MASK  0x00000007 // GAMMA,V_1205+

#define SIS3302_ACQ_SET_LEMO_OUT_MODE0         0x00300000 // GAMMA,V_1205+
#define SIS3302_ACQ_SET_LEMO_OUT_MODE1         0x00200010 // GAMMA,V_1205+
#define SIS3302_ACQ_SET_LEMO_OUT_MODE2         0x00100020 // GAMMA,V_1205+
#define SIS3302_ACQ_SET_LEMO_OUT_MODE3         0x00000030 // GAMMA,V_1205+
#define SIS3302_ACQ_SET_LEMO_OUT_MODE_BIT_MASK 0x00000030 // GAMMA,V_1205+

#define SIS3302_ACQ_SET_FEEDBACK_INTERNAL_TRIGGER 0x00000040 // GAMMA,V_1205+
#define SIS3302_ACQ_CLR_FEEDBACK_INTERNAL_TRIGGER 0x00400000 // GAMMA,V_1205+

#define SIS3302_ACQ_SET_MCA_MODE	       0x00000008 // GAMMA,V_1205+
#define SIS3302_ACQ_CLR_MCA_MODE               0x00080000 // GAMMA,V_1205+

#define SIS3302_BROADCAST_MASTER_ENABLE        0x20	
#define SIS3302_BROADCAST_ENABLE               0x10	

/* gamma  */
#define EVENT_CONF_ADC2_EXTERN_GATE_ENABLE_BIT	   0x2000 // GAMMA,V_1205+
#define EVENT_CONF_ADC2_INTERN_GATE_ENABLE_BIT	   0x1000 // GAMMA,V_1205+
#define EVENT_CONF_ADC2_EXTERN_TRIGGER_ENABLE_BIT  0x800	   
#define EVENT_CONF_ADC2_INTERN_TRIGGER_ENABLE_BIT  0x400	
#define EVENT_CONF_ADC2_INPUT_INVERT_BIT	   0x100	  

#define EVENT_CONF_ADC1_EXTERN_GATE_ENABLE_BIT	   0x20 // GAMMA,V_1205+
#define EVENT_CONF_ADC1_INTERN_GATE_ENABLE_BIT	   0x10 // GAMMA,V_1205+
#define EVENT_CONF_ADC1_EXTERN_TRIGGER_ENABLE_BIT  0x8	  
#define EVENT_CONF_ADC1_INTERN_TRIGGER_ENABLE_BIT  0x4	 
#define EVENT_CONF_ADC1_INPUT_INVERT_BIT	   0x1	   

#define DECIMATION_DISABLE		     0x00000000
#define DECIMATION_2			     0x10000000
#define DECIMATION_4			     0x20000000
#define DECIMATION_8			     0x30000000

/* gamma  MCA */
#define SIS3302_MCA_SCAN_NOF_HISTOGRAMS_PRESET	0x80 // read/write; D32
#define SIS3302_MCA_SCAN_HISTOGRAM_COUNTER      0x84 // read only;  D32
#define SIS3302_MCA_SCAN_SETUP_PRESCALE_FACTOR  0x88 // read/write; D32
#define SIS3302_MCA_SCAN_CONTROL		0x8C // read/write; D32

#define SIS3302_MCA_MULTISCAN_NOF_SCANS_PRESET	0x90 // read/write; D32
#define SIS3302_MCA_MULTISCAN_SCAN_COUNTER	0x94 // read only;  D32
#define SIS3302_MCA_MULTISCAN_LAST_SCAN_HISTOGRAM_COUNTER  0x98	// rd only D32

#define SIS3302_KEY_MCA_SCAN_LNE_PULSE              0x410 // write only; D32
#define SIS3302_KEY_MCA_SCAN_ARM         	    0x414 // write only; D32
#define SIS3302_KEY_MCA_SCAN_START                  0x418 // write only; D32
#define SIS3302_KEY_MCA_SCAN_DISABLE           	    0x41C // write only; D32

#define SIS3302_KEY_MCA_MULTISCAN_START_RESET_PULSE 0x420 // write only; D32
#define SIS3302_KEY_MCA_MULTISCAN_ARM_SCAN_ARM      0x424 // write only; D32
#define SIS3302_KEY_MCA_MULTISCAN_ARM_SCAN_ENABLE   0x428 // write only; D32
#define SIS3302_KEY_MCA_MULTISCAN_DISABLE           0x42C // write only; D32

#define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC1357 0x01000060 // write only;D32
#define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC2468 0x01000064 // write only;D32
#define SIS3302_MCA_HISTOGRAM_PARAM_ALL_ADC    	   0x01000068 // write only;D32

#define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC1 0x02000060 // read/write; D32
#define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC2 0x02000064 // read/write; D32
#define SIS3302_MCA_HISTOGRAM_PARAM_ADC12    	0x02000068 // read/write; D32
#define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC3 0x02800060 // read/write; D32
#define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC4 0x02800064 // read/write; D32
#define SIS3302_MCA_HISTOGRAM_PARAM_ADC34    	0x02800068 // read/write; D32
#define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC5 0x03000060 // read/write; D32
#define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC6 0x03000064 // read/write; D32
#define SIS3302_MCA_HISTOGRAM_PARAM_ADC56    	0x03000068 // read/write; D32 
#define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC7 0x03800060 // read/write; D32
#define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC8 0x03800064 // read/write; D32
#define SIS3302_MCA_HISTOGRAM_PARAM_ADC78    	0x03800068 // read/write; D32

#define SIS3302_MCA_TRIGGER_START_COUNTER_ADC1 	0x02000080 // read only; D32
#define SIS3302_MCA_PILEUP_COUNTER_ADC1 	0x02000084 // read only; D32
#define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC1 	0x02000088 // read only; D32
#define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC1 	0x0200008C // read only; D32

#define SIS3302_MCA_TRIGGER_START_COUNTER_ADC2 	0x02000090 // read only; D32
#define SIS3302_MCA_PILEUP_COUNTER_ADC2 	0x02000094 // read only; D32
#define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC2    0x02000098 // read only; D32
#define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC2     0x0200009C // read only; D32

#define SIS3302_MCA_TRIGGER_START_COUNTER_ADC3 	0x02800080 // read only; D32
#define SIS3302_MCA_PILEUP_COUNTER_ADC3 	0x02800084 // read only; D32
#define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC3 	0x02800088 // read only; D32
#define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC3 	0x0280008C // read only; D32

#define SIS3302_MCA_TRIGGER_START_COUNTER_ADC4 	0x02800090 // read only; D32
#define SIS3302_MCA_PILEUP_COUNTER_ADC4 	0x02800094 // read only; D32
#define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC4 	0x02800098 // read only; D32
#define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC4 	0x0280009C // read only; D32

#define SIS3302_MCA_TRIGGER_START_COUNTER_ADC5 	0x03000080 // read only; D32
#define SIS3302_MCA_PILEUP_COUNTER_ADC5 	0x03000084 // read only; D32
#define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC5 	0x03000088 // read only; D32
#define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC5 	0x0300008C // read only; D32

#define SIS3302_MCA_TRIGGER_START_COUNTER_ADC6 	0x03000090 // read only; D32
#define SIS3302_MCA_PILEUP_COUNTER_ADC6 	0x03000094 // read only; D32
#define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC6 	0x03000098 // read only; D32
#define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC6 	0x0300009C // read only; D32

#define SIS3302_MCA_TRIGGER_START_COUNTER_ADC7 	0x03800080 // read only; D32
#define SIS3302_MCA_PILEUP_COUNTER_ADC7 	0x03800084 // read only; D32
#define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC7 	0x03800088 // read only; D32
#define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC7 	0x0380008C // read only; D32

#define SIS3302_MCA_TRIGGER_START_COUNTER_ADC8 	0x03800090 // read only; D32
#define SIS3302_MCA_PILEUP_COUNTER_ADC8 	0x03800094 // read only; D32
#define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC8 	0x03800098 // read only; D32
#define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC8 	0x0380009C // read only; D32

#define SIS3302_ACQ_SET_MCA_MODE 	0x00000008  /* GAMMA, up V_1205   */
#define SIS3302_ACQ_CLR_MCA_MODE 	0x00080000  /* GAMMA, up V_1205   */

int vme_A32D32_read(int p, unsigned int module_addr, unsigned int *data);
int vme_A32D32_write(int p, unsigned int module_addr, unsigned int data);
int vme_A32MBLT64_read(int p, unsigned int addr, unsigned int *dest, unsigned int  dma_req_length, unsigned int *dma_length);

///////////////////////      General Registers      ///////////////////////
int SIS3302_Reset       (int hVme, int base_addr);
int SIS3302_Version     (int hVme, int base_addr, int *data);
int SIS3302_Start1      (int hVme, int base_addr);
int SIS3302_Start2      (int hVme, int base_addr);
int SIS3302_CsrWrite    (int hVme, int base_addr, int data);
int SIS3302_AcqCtrlWrite(int hVme, int base_addr, int data);
int SIS3302_AcqCtrlRead (int hVme, int base_addr, int *data);
int SIS3302_SampLogReset(int hVme, int base_addr);
int SIS3302_AddrThrWrite(int hVme, int base_addr, int data);
int SIS3302_MemPageWrite(int hVme, int base_addr, int data);

///////////////////////         IRQ Registers       ///////////////////////
int SIS3302_IrqConfWrite (int hVme, int base_addr, int data);
int SIS3302_IrqCtrlWrite (int hVme, int base_addr, int data);
int SIS3302_BltBcastWrite(int hVme, int base_addr, int data);

/////////////////////       Address Registers       ///////////////////////
int SIS3302_EndSampleAddress(int hVme, int base_addr, int *data, int chan);

/////////////////////       Trigger Registers       ///////////////////////
int SIS3302_TrigDelayGatelenWrite(int hVme, int base_addr, int data);
int SIS3302_FastFirTrigWrite (int hVme, int base_addr, int data, int chan);
int SIS3302_TrigThreshWrite  (int hVme, int base_addr, int data, int chan);

/////////////////////      E Filter Registers       ///////////////////////
int SIS3302_EnergyGatelenWrite(int hVme, int base_addr, int data);
int SIS3302_EnergySetupWrite  (int hVme, int base_addr, int data);
int SIS3302_RawBufConfWrite   (int hVme, int base_addr, int data);
int SIS3302_ESampleWrite      (int hVme, int base_addr, int data);
int SIS3302_ESampleIndexWrite (int hVme, int base_addr, int data, int num);
int SIS3302_TauWrite          (int hVme, int base_addr, int data, int chan);
int SIS3302_EvtConfWrite      (int hVme, int base_addr, int data, int chan);

//////////////////////     MCA  Settings     ////////////////////////
int SIS3302_McaScanNHistWrite   (int hVme, int base_addr, int data);
int SIS3302_McaScanPrescaleWrite(int hVme, int base_addr, int data);
int SIS3302_McaScanCtrlWrite    (int hVme, int base_addr, int data);
int SIS3302_McaNScanWrite       (int hVme, int base_addr, int data);
int SIS3302_E2HistWrite         (int hVme, int base_addr, int data, int group);
int SIS3302_McaHistParWrite     (int hVme, int base_addr, int data);

///////////////////////         DAC Registers       ///////////////////////
int SIS3302_DACCsrSET    (int hVme, int base_addr, int data);
int SIS3302_DACCsrRead   (int hVme, int base_addr, int *data);
int SIS3302_DACSET       (int hVme, int base_addr, int data);
int SIS3302_DACREAD      (int hVme, int base_addr, int *data);
int sis3302_write_dac_offset(int hVme, unsigned module_addr, unsigned int *offset_value_array);
