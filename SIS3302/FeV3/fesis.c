#include <stdio.h>
#include <stdlib.h>
#include "midas.h"
#include "mvmestd.h"
#include "experim.h"
#include "vmicvme.h"

#include "sis3302g.h"

#define VME_DMA_BUFSIZE 0x200000  // 8MByte
#define SIS3302_NUMCHAN 8
#define SIS3302_BASE    0x20000000

typedef struct {
    INT       internal_trigger;
    INT       negative_input;
    INT       eventsize_threshold;
    INT       pretrigger;
    INT       trigger_gate_length;
    INT       energy_gate_length;
    INT       decimation;
    INT       energy_gap;
    INT       energy_peaktime;
    INT       energy_tau;
    INT       rawbuf_start;
    INT       raw_samples;
    INT       energy_sample_length;
    INT       energy_sample_index1;
    INT       energy_sample_index2;
    INT       energy_sample_index3;
    INT       fastfir_peaktime;
    INT       fastfir_gap;
    INT       fastfir_triglength;
    INT       trigger_threshold;
    INT       dac_offset;
} Sis3302g;

/*-- Golbal declarations -------------------------------------------*/
char *frontend_name      = "feSis";     /* Client name seen by MIDAS clients */
char *frontend_file_name = __FILE__;                    /* Don't change this */
BOOL frontend_call_loop  = FALSE; /* call frontend_loop periodically if TRUE */
INT display_period       = 0;   /* Frontend status page update interval [ms] */
INT max_event_size       = 524288;      /* max event size from this frontend */
INT max_event_size_frag  = 5 * 1024*1024; /* max event fragmented event size */
INT event_buffer_size = 4 * 800000;            /* buffer size to hold events */

const int module_addr = SIS3302_BASE; /* VME base address */
int   hVme;                           /* Vme handle */
extern HNDLE hDB;                     /* Odb Handle */
TRIGGER_SETTINGS ts;                  /* Odb settings */
unsigned *dma_buffer;
Sis3302g *sis;

/*-- Function declarations -----------------------------------------*/
INT frontend_init (             ), frontend_exit (             );
INT  begin_of_run ( INT, char * ), end_of_run    ( INT, char * );
INT     pause_run ( INT, char * ), resume_run    ( INT, char * );
INT frontend_loop (             );
INT read_trigger_event ( char *, INT );
INT  read_scaler_event ( char *, INT );
int config_sis3302 (int module_addr);

BANK_LIST trigger_bank_list[] = {
  {"SIS0", TID_DWORD, 128, NULL},
  {""},
};

/*-- Equipment list ------------------------------------------------*/
#undef USE_INT
EQUIPMENT equipment[] = {

   {"Trigger",               /* equipment name */
    {1, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
#ifdef USE_INT
     EQ_INTERRUPT,           /* equipment type */
#else
     EQ_POLLED, //PERIODIC, // POLLED,              /* equipment type */
#endif
     LAM_SOURCE(0, 0x0),     /* event source crate 0, all stations */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_RUNNING,          /* read only when running */
     500,                    /* poll for 500ms */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* don't log history */
     "", "", "",},
    read_trigger_event,      /* readout routine */
    NULL, NULL,
    trigger_bank_list,
    }
,
   {"Scaler",                /* equipment name */
    {2, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     EQ_PERIODIC ,           /* equipment type */
     0,                      /* event source */
     "MIDAS",                /* format */
     FALSE,                   /* enabled */
     RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */
     RO_ODB,                 /* and update ODB */
     10000,                  /* read every 10 sec */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* log history */
     "", "", "",},
    read_scaler_event,       /* readout routine */
    },
   {""}
};
//////////////////////////////////////////////////////////////////////
INT       frontend_exit (                               ){ return SUCCESS; }
INT          end_of_run (INT run_number, char *error    ){ return SUCCESS; }
INT           pause_run (INT run_number, char *error    ){ return SUCCESS; }
INT          resume_run (INT run_number, char *error    ){ return SUCCESS; }
INT       frontend_loop (                               ){ return SUCCESS; }
INT interrupt_configure ( INT cmd, INT source, PTYPE adr){ return SUCCESS; }

void seq_callback(INT hDB, INT hseq, void *info)
{
   printf("odb ... trigger settings touched\n");
}

INT frontend_init()
{
   MVME_INTERFACE *vme_handle;
   int size, status;
   char set_str[80];
   HNDLE hSet;

   size = sizeof(TRIGGER_SETTINGS);
   TRIGGER_SETTINGS_STR(trigger_settings_str);     /* Book Setting space */
   sprintf(set_str, "/Equipment/Trigger/Settings");
   status = db_create_record(hDB, 0, set_str, strcomb(trigger_settings_str));
   if( (status = db_find_key (hDB, 0, set_str, &hSet)) != DB_SUCCESS ){
      cm_msg(MINFO,"FE","Key %s not found", set_str);
   }
   if ((status = db_open_record(hDB, hSet, &ts, size, /* enable hot-link */
                     MODE_READ, seq_callback, NULL)) != DB_SUCCESS ){
      cm_msg(MINFO,"FE","Failed to enable ts hotlink", set_str);
   }
   sis = (Sis3302g *)&(ts.sis3302g);
   mvme_open(&vme_handle, 0);
   if( vme_handle == NULL ){
      printf("VME init failed\n");
      return -1;
   }
   hVme = (int)vme_handle;
   if( (dma_buffer=(unsigned*)malloc(VME_DMA_BUFSIZE*sizeof(int))) == NULL ){
      printf("VME_DMA_BUF malloc failed\n");
      return -1;
   }
   return SUCCESS;
}

INT begin_of_run(INT run_number, char *error)
{
   config_sis3302(module_addr);
   SIS3302_Start1(hVme, module_addr); // Start Readout Loop  */
   printf("End of BOR\n");
   return SUCCESS;
}

INT poll_event(INT source, INT count, BOOL test)
{
   //static int poll_counter = 0;
   int i, lam;
   for(i=0; i<count; i++){
      lam = 0;
      SIS3302_AcqCtrlRead(hVme, module_addr, &lam );
      //printf("Poll[%06d] ACQCTRL:0x%x ", poll_counter++, lam); 
      lam = (lam & 0x80000) >> 19;
      if( lam && !test ){ return lam; }
   }  
   return 0;
}

void dump_event(int *data, int size)
{
   int i;
   for(i=0; i<size; i++){
      printf(" 0x%08x", data[i] );
      if( ((i+1) % 8 ) == 0 ){ printf("\n    "); }
   }
   if( (i % 8) != 0 ){ printf("\n"); }
}

extern int sis3302_adc_data_buffer[8]; /* address of each ADCs data buffer */

INT read_trigger_event(char *pevent, INT off)
{
   int i, end_addr, vme_addr, *ev_start = NULL;
   static int i_bar, bank_flag = 1;
   static char bars[] = "|/-\\";
   char bank_name[5];
   unsigned dma_result;
   DWORD *pdata;

   bk_init32(pevent);

   // disarm currently active bank, arm other bank, and set read address
   if( bank_flag == 1 ){
      bank_flag = 0;
      SIS3302_Start2(hVme, module_addr);            /* bank 2 is now armed */
      SIS3302_MemPageWrite(hVme, module_addr, 0x0); /* read bank1 (page0)  */
   } else {
      bank_flag = 1;
      SIS3302_Start1(hVme, module_addr);            /* bank 1 is now armed */
      SIS3302_MemPageWrite(hVme, module_addr, 0x4); /* read bank2 (page4)  */
   }
   for(i=0; i<SIS3302_NUMCHAN; i++){
      SIS3302_EndSampleAddress(hVme, module_addr, &end_addr, i+1); /* 1-8 */
      end_addr &= 0xffffff;        // mask bank2 addr bit(bit 24)
      //if( end_addr > 0x3fffff ){   // more than 1 page memory buffer is used
      //   printf("warning: buffer > 8MByte , use only 8 MByte\n");
      //   end_addr = 0x3ffffc;  // dirty implementation!! max 8Mbyte (1 page)
      //}
      if( end_addr > 200000 ){ // approaching max event size
	printf("warning: truncated event (size:%d)\n", end_addr);
         end_addr = 200000;  
      }
      end_addr >>= 1; /* convert to lwords */
      if( end_addr == 0 ){ continue; }

      vme_addr = module_addr + sis3302_adc_data_buffer[i];
      vme_A32MBLT64_read(hVme, vme_addr, dma_buffer, end_addr, &dma_result);

      sprintf(bank_name, "SIS%d", i);
      bk_create(pevent, bank_name, TID_DWORD, &pdata);
      if( ev_start == NULL ){ ev_start = (int *)pdata; }
      memcpy(pdata, dma_buffer, dma_result*sizeof(int) );
      pdata += dma_result;
      bk_close(pevent, pdata);
   }
   printf("..%c - Serial:%d size:%d", bars[i_bar++ % 4],
	  (int)SERIAL_NUMBER(pevent), bk_size(pevent) );
   if( ((SERIAL_NUMBER(pevent)+1) % 1000) == 0 ){
      printf("\n");
   } else {
      printf("\r");
   }
   fflush(stdout);
   return bk_size(pevent);
}

INT read_scaler_event(char *pevent, INT off)
{
   DWORD *pdata;

   bk_init(pevent);                              /* init bank structure */
   bk_create(pevent, "SCLR", TID_DWORD, &pdata); /* create SCLR bank */
   ;                                             /* read scaler bank */
   bk_close(pevent, pdata);                      /* close scaler bank */
   return bk_size(pevent);
}

int config_sis3302(int module_addr)
{
  int i, data, pretrig, gatelen;

   SIS3302_Reset(hVme,module_addr);
   printf("SIS3302_Reset \n");

   SIS3302_CsrWrite(hVme, module_addr, 0);      // ControlStatus
   SIS3302_IrqConfWrite(hVme, module_addr, 0);  // InterruptConfiguration
   SIS3302_IrqCtrlWrite(hVme, module_addr, 0);  // InterruptControl
   SIS3302_BltBcastWrite(hVme, module_addr, 0); // BroadcastSetup

   data = SIS3302_ACQ_SET_CLOCK_TO_100MHZ;      // AcquisitionControl
   // + SIS3302_ACQ_ENABLE_LEMO_TIMESTAMPCLR + SIS3302_ACQ_ENABLE_LEMO_TRIGGER
   SIS3302_AcqCtrlWrite(hVme, module_addr, data);

   // Event Config (all adc-FPGAs)
   for(i=0; i<4; i++){
      data = (module_addr & 0xf8000000); // channel ID = Module Address 31:27
      if( sis->internal_trigger & 0x1 ){
         data += EVENT_CONF_ADC1_INTERN_TRIGGER_ENABLE_BIT;
      }
      if( sis->internal_trigger & 0x2 ){
         data += EVENT_CONF_ADC2_INTERN_TRIGGER_ENABLE_BIT;
      }
      if( sis->negative_input & 0x1 ){
         data += EVENT_CONF_ADC1_INPUT_INVERT_BIT;
      }
      if( sis->negative_input & 0x2 ){
         data += EVENT_CONF_ADC2_INPUT_INVERT_BIT;
      }
      SIS3302_EvtConfWrite(hVme, module_addr, data, i);
   }
   SIS3302_AddrThrWrite(hVme, module_addr, sis->eventsize_threshold );

   gatelen = sis->trigger_gate_length - 1;
   pretrig = sis->pretrigger+2;
   if( gatelen < 0 ){ gatelen = 0; }
   if( pretrig > 1021 ){ pretrig -= 1022; } // odd(see manual)
   data = (pretrig << 16) + gatelen;
   SIS3302_TrigDelayGatelenWrite(hVme, module_addr, data);

   SIS3302_EnergyGatelenWrite(hVme, module_addr, sis->energy_gate_length);

   data = (sis->decimation & 0x3) + ((sis->energy_gap & 0xff) << 8) +
                                     (sis->energy_peaktime & 0xff);
   SIS3302_EnergySetupWrite(hVme, module_addr, data);

   for(i=0; i<8; i++){
      SIS3302_TauWrite(hVme, module_addr, sis->energy_tau, i+1); // Tau Factors
   }
   data = (sis->rawbuf_start & 0xffe) +  ((sis->raw_samples & 0xffc) << 16); 
   SIS3302_RawBufConfWrite(hVme, module_addr, data);

   SIS3302_ESampleWrite     (hVme, module_addr, sis->energy_sample_length);  
   SIS3302_ESampleIndexWrite(hVme, module_addr, sis->energy_sample_index1, 1);
   SIS3302_ESampleIndexWrite(hVme, module_addr, sis->energy_sample_index2, 2);
   SIS3302_ESampleIndexWrite(hVme, module_addr, sis->energy_sample_index3, 3);

   for(i=0; i<8; i++){ 
      data = ((sis->fastfir_triglength & 0x3f) << 16) + ((sis->fastfir_gap & 0x1f) << 8) +
             (sis->fastfir_peaktime & 0x1f); 
      SIS3302_FastFirTrigWrite(hVme, module_addr, data, i+1);
   }
   for(i=0; i<8; i++){ 
      data = 0x2000000 +/* Enable(GT is set)*/ 0x0010000 + /* zero threshold */
         sis->trigger_threshold;
      SIS3302_TrigThreshWrite(hVme, module_addr, data, i+1);
   }
   //data = 38144;
   //sis3302_write_dac_offset(hVme, module_addr, &data );
   /* this value is not applied! */
 
   SIS3302_McaScanNHistWrite   (hVme, module_addr, 0 );
   SIS3302_McaScanPrescaleWrite(hVme, module_addr, 0 );
   SIS3302_McaScanCtrlWrite    (hVme, module_addr, 0 );
   SIS3302_McaNScanWrite       (hVme, module_addr, 0 );
   SIS3302_E2HistWrite         (hVme, module_addr, 0, 0); /* ADC1357 */
   SIS3302_E2HistWrite         (hVme, module_addr, 0, 1); /* ADC2468 */
   SIS3302_McaHistParWrite     (hVme, module_addr, 0 );

   SIS3302_SampLogReset(hVme, module_addr);

   // if version-reg & 0x00000040 != 0x0 (4 channel flag) => 4 channels not 8! 
   SIS3302_Version(hVme, module_addr+SIS3302_MODID, &data );
   printf("SIS3302 Version: 0x%08x\n\n", data );
   return(0);
}
