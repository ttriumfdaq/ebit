#include <stdio.h>
#include <string.h>
#include <time.h>

#include "midas.h"
#include "experim.h"

/* The analyzer name (client name) as seen by other MIDAS clients   */
char *analyzer_name = "Analyzer";

/* analyzer_loop is called with this interval in ms (0 to disable)  */
INT analyzer_loop_period = 0;

/* default ODB size */
INT odb_size = DEFAULT_ODB_SIZE;

/*-- Module declarations -------------------------------------------*/
extern ANA_MODULE sis3302_module;

ANA_MODULE *trigger_module[] = {
   &sis3302_module,
   NULL
};

/*-- Bank definitions ----------------------------------------------*/
BANK_LIST ana_trigger_bank_list[] = {
  {"SIS0", TID_DWORD, 256, NULL},
  {""} ,
};
/*-- Event request list --------------------------------------------*/

ANALYZE_REQUEST analyze_request[] = {
   {"Trigger",                  /* equipment name */
    {1,                         /* event ID */
     TRIGGER_ALL,               /* trigger mask */
     GET_SOME,                  /* get some events */
     "SYSTEM",                  /* event buffer */
     TRUE,                      /* enabled */
     "", "",}
    ,
    NULL,                       /* analyzer routine */
    trigger_module,             /* module list */
    ana_trigger_bank_list,      /* bank list */
    1000,                       /* RWNT buffer size */
    TRUE,                       /* Use tests for this event */
   },
   {""},
};

/*--=================-------------------------------------------------*/

INT analyzer_exit(){ return CM_SUCCESS; }
INT ana_begin_of_run(INT run_number, char *error){ return CM_SUCCESS; }
INT ana_pause_run(INT run_number, char *error){ return CM_SUCCESS; }
INT ana_resume_run(INT run_number, char *error){ return CM_SUCCESS; }
INT analyzer_loop(){ return CM_SUCCESS; }
INT ana_end_of_run(INT run_number, char *error){ return CM_SUCCESS; }
INT analyzer_init(){ return SUCCESS; }
