#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include "midas.h"
#include "mvmestd.h"
#include "experim.h"
#include "vmicvme.h"

#include "sis3302g.h"

#define use_scaler

typedef TRIGGER_SETTINGS Sis3302g; // get it out of experim.h

/*-- Golbal declarations -------------------------------------------*/
char *frontend_name      = "feSis";     /* Client name seen by MIDAS clients */
char *frontend_file_name = __FILE__;                    /* Don't change this */
BOOL frontend_call_loop  = FALSE; /* call frontend_loop periodically if TRUE */
INT display_period       = 0;   /* Frontend status page update interval [ms] */
INT max_event_size       = 524288;      /* max event size from this frontend */
INT max_event_size_frag  = 5 * 1024*1024; /* max event fragmented event size */
INT event_buffer_size = 4 * 800000;            /* buffer size to hold events */

#define VME_DMA_BUFSIZE 0x400000  // 4MByte
#define EVT_BUFSIZE     0x100000  // 1MByte
#define SIS3302_NUMCHAN 8
#define SIS3302_BASE    0x40000000
#define NSCALERSPERCHAN 12
#define NSCALERSGLOBAL 16
/*
  // per channel:
  0:events, 1:retrigger, 2:pileup, 3: #bytes(?) in data
 */
const int module_addr = SIS3302_BASE; /* VME base address */
int   hVme;                           /* Vme handle */
extern HNDLE hDB;                     /* Odb Handle */
TRIGGER_SETTINGS ts;                  /* Odb settings */
unsigned *dma_buffer, dma_bufpos, dma_bufend;
unsigned *evt_buffer, evt_bufpos;
Sis3302g *sis;
int ierror;
int scalerperchan[SIS3302_NUMCHAN][NSCALERSPERCHAN];
int scalerglobal[NSCALERSGLOBAL];

/*-- Function declarations -----------------------------------------*/
INT frontend_init (             ), frontend_exit (             );
INT  begin_of_run ( INT, char * ), end_of_run    ( INT, char * );
INT     pause_run ( INT, char * ), resume_run    ( INT, char * );
INT frontend_loop (             );
INT read_trigger_event ( char *, INT );
INT  read_scaler_event ( char *, INT );
int config_sis3302 (int module_addr);

BANK_LIST trigger_bank_list[] = {
  {"SIS0", TID_DWORD, 128, NULL},
  {""},
};

/*-- Equipment list ------------------------------------------------*/
#undef USE_INT
EQUIPMENT equipment[] = {

  {"Trigger",               /* equipment name */
   {1, 0,                   /* event ID, trigger mask */
    "SYSTEM",               /* event buffer */
#ifdef USE_INT
    EQ_INTERRUPT,           /* equipment type */
#else
    EQ_POLLED, //PERIODIC, // POLLED,              /* equipment type */
#endif
    LAM_SOURCE(0, 0x0),     /* event source crate 0, all stations */
    "MIDAS",                /* format */
    TRUE,                   /* enabled */
    RO_RUNNING,          /* read only when running */
    500,                    /* poll for 500ms */
    0,                      /* stop run after this event limit */
    0,                      /* number of sub events */
    0,                      /* don't log history */
    "", "", "",},
   read_trigger_event,      /* readout routine */
   NULL, NULL,
   trigger_bank_list,
  }

#ifdef use_scaler
  ,
  {"Scaler3302",                /* equipment name */
   {2, 0,                   /* event ID, trigger mask */
    "SYSTEM",               /* event buffer */
    EQ_PERIODIC ,           /* equipment type */
    0,                      /* event source */
    "MIDAS",                /* format */
    FALSE,                   /* enabled */
    RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */
    RO_ODB,                 /* and update ODB */
    5000,                  /* read every 5 sec */
    0,                      /* stop run after this event limit */
    0,                      /* number of sub events */
    0,                      /* log history */
    "", "", "",},
   read_scaler_event,       /* readout routine */
  },
  {""}
#endif

};

//////////////////////////////////////////////////////////////////////
INT       frontend_exit (                               ){ return SUCCESS; }
//INT          end_of_run (INT run_number, char *error    ){ return SUCCESS; }
INT           pause_run (INT run_number, char *error    ){ return SUCCESS; }
INT          resume_run (INT run_number, char *error    ){ return SUCCESS; }
INT       frontend_loop (                               ){ return SUCCESS; }
INT interrupt_configure ( INT cmd, INT source, PTYPE adr){ return SUCCESS; }

void seq_callback(INT hDB, INT hseq, void *info)
{
  printf("odb ... trigger settings touched\n");
}

INT frontend_init()
{
  MVME_INTERFACE *vme_handle;
  int size, status;
  char set_str[80];
  HNDLE hSet;

  size = sizeof(TRIGGER_SETTINGS);
  TRIGGER_SETTINGS_STR(trigger_settings_str);     /* Book Setting space */
  sprintf(set_str, "/Equipment/Trigger/Settings");
  status = db_create_record(hDB, 0, set_str, strcomb(trigger_settings_str));
  if( (status = db_find_key (hDB, 0, set_str, &hSet)) != DB_SUCCESS ){
    cm_msg(MINFO,"FE","Key %s not found", set_str);
  }
  if ((status = db_open_record(hDB, hSet, &ts, size, /* enable hot-link */
			       MODE_READ, seq_callback, NULL)) != DB_SUCCESS ){
    cm_msg(MINFO,"FE","Failed to enable ts hotlink", set_str);
  }
  sis = (Sis3302g *)&(ts.sis3302g);
  mvme_open(&vme_handle, 0);
  if( vme_handle == NULL ){
    printf("VME init failed\n");
    return -1;
  }
  hVme = (int)vme_handle;
  if( (dma_buffer=(unsigned*)malloc(VME_DMA_BUFSIZE*sizeof(int))) == NULL ){
    printf("VME_DMA_BUF malloc failed\n");
    return -1;
  }
  if( (evt_buffer=(unsigned*)malloc(EVT_BUFSIZE*sizeof(int))) == NULL ){
    printf("EVT_BUF malloc failed\n");
    return -1;
  }
  return SUCCESS;
}


INT poll_event(INT source, INT count, BOOL test)
{
  //static int poll_counter = 0;
  int i, lam;
  for(i=0; i<count; i++){
    // dma_bufpos is steadily increasing here, buf_end a constant
    if( dma_bufpos ){ lam=1; } // still data in buffer
    //if( dma_bufend ){ printf("DBE %x\n",dma_bufend); lam=1; } // still data in buffer
    else {
      lam = 0;
      SIS3302_AcqCtrlRead(hVme, module_addr, &lam );
      //printf("Poll[%06d] ACQCTRL:0x%x ", poll_counter++, lam); 
      //printf("lam %x",lam);
      lam = (lam & 0x80000) >> 19;
    }
    if( lam && !test ){ return lam; }
  }  
  return 0;
}

void dump_event(int *data, int size)
{
  int i;
  for(i=0; i<size; i++){
    printf(" 0x%08x", data[i] );
    if( ((i+1) % 8 ) == 0 ){ printf("\n    "); }
  }
  if( (i % 8) != 0 ){ printf("\n"); }
}

extern int sis3302_adc_data_buffer[8]; /* address of each ADCs data buffer */

/* each read from module - read all available data                    */
/* may be many events, and not guaranteed to be complete events?      */
/* anyway read through resulting dma buffer copying event to evtbuf   */
/* when end of event seen - return single complete event to midas     */
/* (and reset evt_buffer), when end of dmabuf reached - reread module */

/* instead of having to check that bufpos<bufend && bufend>0  */
/* for buffer non-empty, just reset bufend to zero when empty */
/* and check for this                                         */
int read_event_data(int chan);
INT read_event_from_buf(char *pevent, INT off, int current_chan)
{
  //static char bars[] = "|/-\\";
  //static int i_bar;
  char bank_name[5];
  DWORD *pdata;
  //if (dma_bufpos)
    //printf("read_event_from_buf %i indices pos %i end %i\n",current_chan,dma_bufpos,dma_bufend); /* TEMP: : this is alreaady called 4 times for same  event */
  if( dma_bufend == 0 ){ return(0); }
  while( dma_bufpos < dma_bufend ){
    if( dma_buffer[dma_bufpos] != 0xdeadbeef ){ /* not end of event yet */
      evt_buffer[evt_bufpos++] = dma_buffer[dma_bufpos++];
      continue;
    }
    evt_buffer[evt_bufpos++] = dma_buffer[dma_bufpos++]; /* copy endofevent */
    bk_init32(pevent); /* now have full event in evt buffer */
    sprintf(bank_name, "SIS%d", current_chan );
    bk_create(pevent, bank_name, TID_DWORD, &pdata);
    memcpy(pdata, evt_buffer, evt_bufpos*sizeof(int) );
    pdata += evt_bufpos;
    bk_close(pevent, pdata);

    // some accounting
    scalerperchan[current_chan][0]+=1;
    scalerglobal[0]+=1;
    scalerperchan[current_chan][3]+=evt_bufpos*sizeof(int);
    scalerglobal[3]+=evt_bufpos*sizeof(int);
    // should we look at flags here? requires unpacking.
    printf("trailer %x",evt_buffer[evt_bufpos]);
    printf("flags %x",evt_buffer[evt_bufpos-1]);
    scalerperchan[current_chan][1]+= (evt_buffer[evt_bufpos-1] >>30) && 0x1; //retrigger
    scalerperchan[current_chan][2]+= (evt_buffer[evt_bufpos-1] >>31) && 0x1; //pile-up



    //printf("%i\n",evt_bufpos); // evt_bufpos is usually 6 (probably correct, minimum size of event in words) 
    evt_bufpos = 0;
    //printf("dma_bufpos %x dma_bufend %x\n",dma_bufpos,dma_bufend);
    /*
      printf("..%c - Serial:%d size:%d", bars[i_bar++ % 4],
      (int)SERIAL_NUMBER(pevent), bk_size(pevent) );
      if( ((SERIAL_NUMBER(pevent)+1) % 1000) == 0 ){
      printf("\n");
      } else {
      printf("\r");
      }
      fflush(stdout);
    */
    //printf("%x\n",evt_buffer[1]); /* TEMP : this is alreaady called 4 times per event */
    return bk_size(pevent);
  }
  /* reached end of dmabuf without having full event */
  /* wait for module to have more data and then read again */
  //printf("read_event_from_buf done\n");
  dma_bufpos = dma_bufend = 0;
  return(0);
}
int last_event_truncated;
static int current_chan=0, bank1_flag = 1, bank_done=1;
static int readout_counter=0;
INT read_trigger_event(char *pevent, INT off)
{
  int i;
  //printf("dma_bufend %i bank %i chan %i\n",dma_bufend,bank_flag,current_chan);
  // this means more events for the current channel
  if( dma_bufend ){ return(read_event_from_buf(pevent, off, current_chan)); }

  /* BIG MISTAKE HERE: banks are switched before ALL CHANNELS are read out */
  /* ->introduce flag to only switch after last channel was done */

  if( bank_done ) {
    //printf("----- bank switch %i\n", bank1_flag);
    bank_done=0; current_chan=-1;
    // disarm currently active bank, arm other bank, and set read address
    if( bank1_flag == 1 ){
      bank1_flag = 0;
      ierror=SIS3302_Start2(hVme, module_addr);            /* bank 2 is now armed */
      //if (ierror) printf("bad2 %i\n",ierror);
      //else printf("good2\n");
      SIS3302_MemPageWrite(hVme, module_addr, 0x0); /* read bank1 (page0)  */
    } else {
      bank1_flag = 1;
      ierror=SIS3302_Start1(hVme, module_addr);            /* bank 1 is now armed */
      //if (ierror) printf("bad1 %i\n",ierror);
      //else printf("good1\n");
      SIS3302_MemPageWrite(hVme, module_addr, 0x4); /* read bank2 (page4)  */
    }
    // poll for bank switch to be finished
    vme_A32D32_read(hVme, module_addr+SIS3302_ACQUISITION_CONTROL, &ierror);
    while ( (ierror & 0x30000) == 0 ) {
      vme_A32D32_read(hVme, module_addr+SIS3302_ACQUISITION_CONTROL, &ierror);
    }
  } // end bank_done

  /* if we have partial event in buffer - need to read same channel     */
  /* (this may never happen if module never provides incomplete events) */
  if( evt_bufpos ){
    printf ("warning: incomplete event? evt_bufpos %x chan %i\n",evt_bufpos, current_chan);
    read_event_data(current_chan);
  } else { /* otherwise try reading all channels for data */

    //if (current_chan==7) current_chan=0;
    for(i=current_chan+1; i<SIS3302_NUMCHAN; i++){
      //printf("testing ch %i bufend %x ",i,dma_bufend);
      if (i==7) bank_done=1;
      if( read_event_data(i) == 0 ){
	//printf("nothin\n");
	continue;
      }
      current_chan = i;
      //printf("data!\n");
      break;
    }
  }
  //return (0);
  readout_counter+=1;
  //printf("rc %i chan %i\n",readout_counter,current_chan); 
  return(read_event_from_buf(pevent, off, current_chan));
}

int read_event_data(int chan)
{
  unsigned int end_addr, vme_addr;
  /* dma_bufend should be zero (=> buffer empty) before calling this fn */
  if( dma_bufend ){ printf("warning: discarding data ...\n"); }
  /* bufpos should have been reset to zero elsewhere ... */
  if( dma_bufpos ){ printf("warning: dma_bufpos non-zero ...\n"); }

  // SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADCx
  SIS3302_EndSampleAddress(hVme, module_addr, &end_addr, chan+1); /* 1-8 */

  /* STRUCK:
  addr = gl_uint_ModAddrRun[module_index] + previous_bank_sample_address_reg_offset[channel_index] ;
  if ((error = sub_vme_A32D32_read(addr,&gl_uint_end_sample_address[channel_index] )) != 0) {
    sisVME_ErrorHandling (error, addr, "sub_vme_A32D32_read");
    return -1;
  }
  */

  // ??????????????????????????????????????????????????????????????????????????????????????
  // DOES THIS NEED TO BE IN? POLLING FOR BANK READY?
  //read ACR to be sure
  //vme_A32D32_read(hVme, module_addr+SIS3302_ACQUISITION_CONTROL, &ierror); // REMOVED 6.2.12
  //while ( (ierror & 0x30000) == 0 ) {
  //  vme_A32D32_read(hVme, module_addr+SIS3302_ACQUISITION_CONTROL, &ierror);
  //}
  //printf("ACR %x\n",ierror);
  // ??????????????????????????????????????????????????????????????????????????????????????

  //usleep (2000);
  if (( (end_addr >> 24) & 0x1 ) != bank1_flag) {
    printf("CONFUSION: bank flag messed up. Not sure what to do now %i %x\n",chan,end_addr) ;
    // 53200 and 63200 ok, 43200 not?
  }


  end_addr &= 0xffffff;        // mask bank2 addr bit(bit 24)
  if( end_addr > 0x3fffff ){   // more than 1 page memory buffer is used
    printf("warning: buffer > 8MByte , use only 8 MByte\n");
    end_addr = 0x3ffffc;  // dirty implementation!! max 8Mbyte (1 page)
  }
  if( end_addr > VME_DMA_BUFSIZE ){ // approaching max event size
    //printf("warning: reading partial event (size:%d)\n", end_addr);
    printf("warning: Event data truncated (size:%d)\n", end_addr);
    /* this should not be a problem as long as we do next read */
    /* from same place - channel and bank */
    last_event_truncated = 1;
    end_addr = VME_DMA_BUFSIZE;  
  } else {
    //printf("trung %i\n",chan);
    last_event_truncated = 0;
  }

  //end_addr >>= 1; /* convert to lwords */ // REMOVED 6.2.12 Dont change original variable
  //printf("read_event_data, ch %i -- %x --\n",chan,end_addr);
  if( end_addr == 0 ){ return(0); }

  vme_addr = module_addr + sis3302_adc_data_buffer[chan];
  vme_A32MBLT64_read(hVme, vme_addr, dma_buffer, end_addr >>1, &dma_bufend);

  //printf(" dma bufend %i end_ addr %i\n",dma_bufend,end_addr >>1);
  //int * crash = NULL;
  //*crash = 1;

  return(dma_bufend);
  /* TEMP: this returns 72 words with eventsize threshold = 100 */
}

#ifdef use_scaler
INT read_scaler_event(char *pevent, INT off)
{
  DWORD *pdata, *pdata2;
  char bname[256];
  int i,j;

  bk_init(pevent);                              /* init bank structure */
  bk_create(pevent, "SCRG", TID_DWORD, &pdata); /* create SCLR bank */

  // fill bank
  for (i=0; i<NSCALERSGLOBAL;i++) {
    pdata[i]=100+i;
  }
  pdata+=NSCALERSGLOBAL;
  bk_close(pevent, pdata);                      /* close scaler bank */
  //return bk_size(pevent);

  //---------------------
  for (i=0; i<SIS3302_NUMCHAN; i++) {
    sprintf(bname,  "SCR%d", i );
    bk_create(pevent, bname, TID_DWORD, &pdata2); /* create SCLR bank */
    for (j=0; j<NSCALERSPERCHAN; j++) {
      pdata2[j]=1000*j+i;
    }
    pdata2+=NSCALERSPERCHAN;
    bk_close(pevent, pdata2);                      /* close scaler bank */
  }

  return bk_size(pevent);
}
#endif

int config_sis3302(int module_addr)
{
  unsigned int i, data, pretrig, gatelen, data2;

  SIS3302_Reset(hVme,module_addr);
  printf("SIS3302_Reset\n");

  SIS3302_CsrWrite(hVme, module_addr, 0);      // ControlStatus
  SIS3302_IrqConfWrite(hVme, module_addr, 0);  // InterruptConfiguration
  SIS3302_IrqCtrlWrite(hVme, module_addr, 0);  // InterruptControl
  SIS3302_BltBcastWrite(hVme, module_addr, 0); // BroadcastSetup

  data = SIS3302_ACQ_SET_CLOCK_TO_10MHZ;      // AcquisitionControl
  //data = SIS3302_ACQ_SET_CLOCK_TO_50MHZ;      // AcquisitionControl
  data += SIS3302_ACQ_ENABLE_LEMO_TIMESTAMPCLR;
  // + SIS3302_ACQ_ENABLE_LEMO_TIMESTAMPCLR + SIS3302_ACQ_ENABLE_LEMO_TRIGGER
  SIS3302_AcqCtrlWrite(hVme, module_addr, data);


  //--------- Event Config
  //printf("%x\n",sis->sis3302g.internal_trigger);
  for(i=0; i<4; i++){
    //printf ("--- %i -------",i);
    data = (module_addr & 0xf8000000); // channel ID = Module Address 31:27
    if( sis->sis3302g.internal_trigger & (int)(pow(2,(i*2+0))) ){
      data += EVENT_CONF_ADC1_INTERN_TRIGGER_ENABLE_BIT;
    }
    if( sis->sis3302g.internal_trigger &  (int)(pow(2,(i*2+1)))){
      data += EVENT_CONF_ADC2_INTERN_TRIGGER_ENABLE_BIT;
    }
    if( sis->sis3302g.negative_input & (int)(pow(2,(i*2+0))) ){
      data += EVENT_CONF_ADC1_INPUT_INVERT_BIT;
    }
    if( sis->sis3302g.negative_input & (int)(pow(2,(i*2+1))) ){
      data += EVENT_CONF_ADC2_INPUT_INVERT_BIT;
    }
    //printf("%x %x --- ",(i*2+0x1) ,(i*2+0x2) );
    printf("event config %i %x\n",i,(data & 0xffff));
    SIS3302_EvtConfWrite(hVme, module_addr, data, i); // SIS3302_EVENT_CONFIG_ADCxy (52, 4.20)
  }



  // 4.21 event extended config - not needed

  SIS3302_AddrThrWrite(hVme, module_addr, (sis->sis3302g.eventsize_threshold & (0xfffffc)) ); // SIS3302_END_ADDRESS_THRESHOLD_ALL_ADC (54, 4.22)


  gatelen = sis->sis3302g.trigger_gate_length;
  pretrig = sis->sis3302g.pretrigger_delay;
  if( gatelen < 0 ){ gatelen = 0; }
  if( pretrig > 1021 ){ pretrig -= 1022; } // odd(see manual)
  data = (pretrig << 16) + gatelen;
  SIS3302_TrigDelayGatelenWrite(hVme, module_addr, data); // SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ALL_ADC (55, 4.23)

  //--------- waveform+trapezoid storage
  //data = (sis->sis3302g.rawbuf_start & 0xffe) +  ((sis->sis3302g.raw_samples & 0xffc) << 16); 

  data = (sis->sis3302g.rawbuf_start) +  ((sis->sis3302g.raw_samples) << 16); 
  printf("Raw data buffer config %x\n",data);
  SIS3302_RawBufConfWrite(hVme, module_addr, data); // SIS3302_RAW_DATA_BUFFER_CONFIG_ALL_ADC (56, 4.24)
  // 4.25 to 4.27 addresses for data


  //--------- trigger
  for(i=0; i<8; i++){ 
    data = (sis->sis3302g.triggersettings.peakingtime[i] & 0xff) +
      ((sis->sis3302g.triggersettings.gaptime[i] & 0xff) << 8) +
      ((sis->sis3302g.triggersettings.pulse_length & 0xff) << 16) + 
      ((sis->sis3302g.triggersettings.internal_gate_length & 0x3f) << 24);
    SIS3302_FastFirTrigWrite(hVme, module_addr, data, i+1); // SIS3302_TRIGGER_SETUP_ADCx (59, 4.28)
    //printf("SIS3302_TRIGGER_SETUP_ADC%i %x\n",i,data);
    //vme_A32D32_read(hVme,module_addr+SIS3302_TRIGGER_SETUP_ADC1,&data);
    //printf("   SIS3302_TRIGGER_SETUP_ADC%i %x\n",i,data);
    

    data = (sis->sis3302g.triggersettings.peakingtime[i] & 0x300) +
      ((sis->sis3302g.triggersettings.gaptime[i] & 0x300) << 8);
    data += ((sis->sis3302g.triggersettings.decimation[i] & 0x3) << 16);
    data += ((sis->sis3302g.triggersettings.internal_delay & 0x1f) << 24);

    // struck does this differently:
    data2 = ((sis->sis3302g.triggersettings.internal_delay & 0xff) << 24);
    data2+= (sis->sis3302g.triggersettings.peakingtime[i] & 0x3000000) >>8;
    data2+=      ((sis->sis3302g.triggersettings.gaptime[i] & 0xf00));
    data2+= (sis->sis3302g.triggersettings.peakingtime[i] & 0xf00) >>8;

    SIS3302_FastFirTrigWriteExt(hVme, module_addr, data, i+1); // SIS3302_TRIGGER_EXTENDED_SETUP_ADCx (60, 4.29)

    data  = 0x2000000; /* Enable(GT is set)*/ 
    data += 0x0010000; /* zero threshold */
    // data += 0x800000; // enable extended threshold
    data += (sis->sis3302g.triggersettings.threshold[i] & 0xff);
    SIS3302_TrigThreshWrite(hVme, module_addr, data, i+1); // SIS3302_TRIGGER_THRESHOLD_ADCx (62, 4.30)
  }

  // 4.31 is extended threshold? used nowhere? address not even in header file? wtf?
   
  //--------- energy sampling
  for (i=0; i<4; i++) {
    data = ((sis->sis3302g.energysampling.decimation[i] & 0x3) << 28) + 
      ((sis->sis3302g.energysampling.gaptime[i] & 0xff) << 8) +
      ((sis->sis3302g.energysampling.peakingtime[i] & 0x300) << 16) + 
      (sis->sis3302g.energysampling.peakingtime[i] & 0xff);
    SIS3302_EnergySetupWriteChan(hVme, module_addr, data, i); // SIS3302_ENERGY_SETUP_GP_ADCxy (67, 4.32)
  }
  //1324
  SIS3302_EnergyGatelenWrite(hVme, module_addr, sis->sis3302g.energy_gate_length); // SIS3302_ENERGY_GATE_LENGTH_ALL_ADC (68, 4.33)
  // max 510 and EVEN number
  if (sis->sis3302g.energy_sample_length>510) {
    printf("SIS3302: WARNING: energy_sample_length>510\n");
    sis->sis3302g.energy_sample_length=510;
  };
  SIS3302_ESampleWrite      (hVme, module_addr, sis->sis3302g.energy_sample_length & 0xfffe); // SIS3302_ENERGY_SAMPLE_LENGTH_ALL_ADC (69, 4.34)
  SIS3302_ESampleIndexWrite (hVme, module_addr, sis->sis3302g.energy_sample_index1, 1);
  SIS3302_ESampleIndexWrite (hVme, module_addr, sis->sis3302g.energy_sample_index2, 2);
  SIS3302_ESampleIndexWrite (hVme, module_addr, sis->sis3302g.energy_sample_index3, 3);
  printf("esample %i %i %i %i\n",sis->sis3302g.energy_sample_length,sis->sis3302g.energy_sample_index1,
	 sis->sis3302g.energy_sample_index2,sis->sis3302g.energy_sample_index3);
  for(i=0; i<8; i++){
    SIS3302_TauWrite(hVme, module_addr, sis->sis3302g.energysampling.tau[i], i+1); // SIS3302_ENERGY_TAU_FACTOR_ADCx (70, 4.35)
  }
  //1324
   

  //--------- dac offsets
  //unsigned int ttt[8];
  //for (i=0; i<8; i++) ttt[i]=sis->sis3302g.dac[i];
  //ierror=sis3302_write_dac_offset(hVme, module_addr, ttt );

  ierror=sis3302_write_dac_offset(hVme, module_addr, sis->sis3302g.dac );
  //if (ierror) printf("SIS3302 error %i writing DAC\n",ierror);
  //else printf("SIS3302 applied DAC %i (ch1) to %i (ch8)\n",sis->sis3302g.dac[0],sis->sis3302g.dac[7]);

  SIS3302_McaScanNHistWrite   (hVme, module_addr, 0 );
  SIS3302_McaScanPrescaleWrite(hVme, module_addr, 0 );
  SIS3302_McaScanCtrlWrite    (hVme, module_addr, 0 );
  SIS3302_McaNScanWrite       (hVme, module_addr, 0 );
  SIS3302_E2HistWrite         (hVme, module_addr, 0, 0); /* ADC1357 */
  SIS3302_E2HistWrite         (hVme, module_addr, 0, 1); /* ADC2468 */
  SIS3302_McaHistParWrite     (hVme, module_addr, 0 );


  ierror=SIS3302_SampLogReset(hVme, module_addr);
  //if (ierror) printf("ERROR %i in sampling logic reset\n",ierror);

  ierror=SIS3302_Version(hVme, module_addr, &data );
  printf("SIS3302 Version: 0x%08x  error %i\n", data, ierror );

  //ierror=vme_A32D32_write(hVme, module_addr+SIS3302_KEY_DISARM_AND_ARM_BANK1, 0x1);
  //if (ierror) printf("ERROR %i in bank1\n",ierror);

  // clear timestamp once via software (49, 4.18)
  ierror=vme_A32D32_write(hVme, module_addr+SIS3302_KEY_TIMESTAMP_CLEAR, 0x1);
  //if (ierror) printf("ERROR %i in timestamp reset\n",ierror);

  vme_A32D32_read(hVme, module_addr+SIS3302_ACQUISITION_CONTROL, &ierror);
  printf("ACR %x\n",ierror);

  // read back DAC
  sis3302_read_dac_offset(hVme, module_addr,sis->sis3302g.dac);

  return(0);
}

INT end_of_run(INT run_number, char *error)
{
  //ierror=vme_A32D32_write(hVme, module_addr+SIS3302_KEY_SAMPLE_LOGIC_RESET, 0x1);
  //ierror=vme_A32D32_write(hVme, module_addr+SIS3302_KEY_0x404_SAMPLE_LOGIC_RESET, 0x1);
  ierror=SIS3302_SampLogReset(hVme, module_addr);
  ierror=SIS3302_SampLogDisarm(hVme, module_addr);

  current_chan=0, bank1_flag = 1, bank_done=1;
  evt_bufpos = dma_bufpos = dma_bufend = 0;

  printf("End of EOR\n");
  return SUCCESS;
}

INT begin_of_run(INT run_number, char *error) {
  int i,j;

  config_sis3302(module_addr);
  ierror=SIS3302_Start1(hVme, module_addr);
  // if (ierror) printf("ERROR %i in BOR's Start1\n",ierror);
  vme_A32D32_read(hVme, module_addr+SIS3302_ACQUISITION_CONTROL, &ierror);
  printf("ACR %x\n",ierror);

  current_chan=0, bank1_flag = 1, bank_done=1;
  evt_bufpos = dma_bufpos = dma_bufend = 0;

  // reset scalers
  for (i=0; i<NSCALERSPERCHAN; i++)
    for (j=0; j<SIS3302_NUMCHAN; j++)
      scalerperchan[j][i]=0;

  for (i=0; i<NSCALERSGLOBAL; i++)
    scalerglobal[i]=0;

  printf("End of BOR\n");
  return SUCCESS;
}
