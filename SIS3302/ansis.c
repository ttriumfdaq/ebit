#include <stdio.h>
#include <iostream>
#include <time.h>
#include <unistd.h> /* usleep */
#include <math.h>

#include "midas.h"
#include "experim.h"

#include <TH1F.h>
#include <TH2F.h>
#include <TTree.h>
#include <TDirectory.h>

/*-- Module declaration --------------------------------------------*/
int sis3302_event(EVENT_HEADER *, void *);
int sis3302_init(void);
int sis3302_bor(INT run_number);
int sis3302_eor(INT run_number);
static void odb_callback(INT hDB, INT hseq, void *info);


//SIS3302G_PARAM_STR(sis3302g_param_str);
//SIS3302G_PARAM sis3302g_param;

/* AG 04.03.10 */
TRIGGER_SETTINGS_STR(trigger_settings_str);
TRIGGER_SETTINGS trigger_settings;

TITAN_ACQ_PPG_CYCLE_MODE_1E_STR(ppg_settings_str);
TITAN_ACQ_PPG_CYCLE_MODE_1E ppg_settings;

ANA_MODULE sis3302_module = {
   "sis3302",              /* module name           */
   "CP",                   /* author                */
   sis3302_event,          /* event routine         */
   sis3302_bor,            /* BOR routine           */
   sis3302_eor,            /* EOR routine           */
   sis3302_init,           /* init routine          */
   NULL,                   /* exit routine          */
   //&sis3302g_param,         /* parameter structure   */
   //sizeof(sis3302g_param),  /* structure size        */
   //sis3302g_param_str,      /* initial parameters    */
   &trigger_settings,
   sizeof(trigger_settings),
   trigger_settings_str,
};
/*-------------------------------------------------------------------*/

#define NUM_CHAN    8
#define NUM_MISC    2
// #define ADC_CHAN    16384
#define ADC_CHAN    32768

// #define MAX_SAMPLES 16384
#define WAVE_SAMPLES 800 // 16000
#define ADC_CLOCK 10000000. // 10 MHz hardcoded

static TH1F *hEnergy[NUM_CHAN], *hRaw[NUM_CHAN], *hRaw2[NUM_CHAN];
static TH1F *hEnergyMinMax[NUM_CHAN], *hEnergy64k[NUM_CHAN];;
static TH1F *hEnergyRt[NUM_CHAN], *hRawRt[NUM_CHAN], *hRaw2Rt[NUM_CHAN];
static TH1F *hEnergyPu[NUM_CHAN], *hRawPu[NUM_CHAN], *hRaw2Pu[NUM_CHAN];

static TH1F *hTime[NUM_CHAN],*hEnergyc0[NUM_CHAN],*hEnergyc1[NUM_CHAN],*hEnergyc2[NUM_CHAN];
static TH2F *hEvsTfilled[NUM_CHAN];
static TH1F *hCh, *hFreq, *hDeltaT[NUM_CHAN];

static TTree *tIndEvents;
static int i_ch, i_emax, i_ezero;
static uint64_t i_time48, tlast, tlastlast, tlast3, lasttime[NUM_CHAN]={0}; //4294967295;
static int soc[NUM_CHAN]={0}; // start of cycle, first event of new cycle

static TH1F     *h_misc[NUM_MISC];
static char misc_handle[NUM_MISC][16] = {"EHIT", "GDEHIT"};
static char misc_title [NUM_MISC][64] = {"E pattern" "Good Energy Pattern"};               
static int    misc_chan[NUM_MISC]     = { NUM_CHAN, NUM_CHAN};
static double tin, tout;

int sis3302_unpack(int nitems, DWORD *pdata, int chan);

//---------------------------------------------------------------------

//float spread(int val){ return( val + rand()/(1.0*RAND_MAX) ); }
int sis3302_eor(INT run_number){ return SUCCESS; }
int sis3302_bor(INT run_number){ return SUCCESS; }
void odb_callback(INT hDB, INT hseq, void *info){ }

int hist_init_roody()
{
   char name[256], title[256];
   int i;

   open_subfolder("Raw");                                        // Waveforms 
   for(i=0; i<NUM_CHAN; i++){    
      sprintf(name,  "WFD%d", i );  
      sprintf(title, "Wave form channel%d", i );  
      //hRaw[i] = H1_BOOK(name, title, WAVE_SAMPLES, 0, WAVE_SAMPLES);
      hRaw[i] = H1_BOOK(name, title, trigger_settings.sis3302g.raw_samples, 0, trigger_settings.sis3302g.raw_samples);
   }
   close_subfolder();
   open_subfolder("Raw2");                                        // Waveforms 
   for(i=0; i<NUM_CHAN; i++){    
      sprintf(name,  "WFDC%d", i );  
      sprintf(title, "Corrected Wave form channel%d", i );  
      //hRaw2[i] = H1_BOOK(name, title, WAVE_SAMPLES, 0, WAVE_SAMPLES);
      hRaw2[i] = H1_BOOK(name, title, trigger_settings.sis3302g.energy_sample_length, 0, trigger_settings.sis3302g.energy_sample_length);
   }
   close_subfolder();
   open_subfolder("Energy");                                      // Energy  
   for(i=0; i<NUM_CHAN; i++){
     sprintf(name,  "ADC%d", i);  
     sprintf(title, "Energy chan%d", i );
     hEnergy[i] = H1_BOOK(name, title, ADC_CHAN, 0, ADC_CHAN );
     sprintf(name,  "ADCrt%d", i);  
     hEnergyRt[i] = H1_BOOK(name, name, ADC_CHAN, 0, ADC_CHAN );
     sprintf(name,  "ADCpu%d", i);  
     hEnergyPu[i] = H1_BOOK(name, name, ADC_CHAN, 0, ADC_CHAN );
     sprintf(name,  "ADCminmax%d", i);  
     hEnergyMinMax[i] = H1_BOOK(name, name, ADC_CHAN, 0, ADC_CHAN );
     sprintf(name,  "ADC64k%d", i);  
     hEnergy64k[i] = H1_BOOK(name, name, 65536, 0, 65536 );

     sprintf(name,  "ADCc0%d", i);  
     sprintf(title, "Energy chanc0%d", i );
     hEnergyc0[i] = H1_BOOK(name, title, ADC_CHAN, 0, ADC_CHAN );

     sprintf(name,  "ADCc1%d", i);  
     sprintf(title, "Energy chanc1%d", i );
     hEnergyc1[i] = H1_BOOK(name, title, ADC_CHAN, 0, ADC_CHAN );

     sprintf(name,  "ADCc2%d", i);  
     sprintf(title, "Energy chanc2%d", i );
     hEnergyc2[i] = H1_BOOK(name, title, ADC_CHAN, 0, ADC_CHAN );

     sprintf(name,  "ADCt%d", i);  
     sprintf(title, "t vs Energy chan %d", i );
     hEvsTfilled[i] = H2_BOOK(name, title, 100, 0, ADC_CHAN ,25, tin, tout);
     hEvsTfilled[i]->SetOption("COLZ");
   }
   for(i=0; i<NUM_CHAN; i++){
     sprintf(name,  "time %d", i);  
     sprintf(title, "time chan %d", i );
     hTime[i] = H1_BOOK(name, title, 50000, 0, pow(2,48) );
   }
   close_subfolder();
   open_subfolder("Misc");                                        // Misc
   for(i=0; i<NUM_MISC; i++){
      h_misc[i] = H1_BOOK(misc_handle[i], misc_title[i], misc_chan[i], 0, misc_chan[i]);
   }
   hCh=H1_BOOK("Channels","Channels",8,1,9);
   hFreq=H1_BOOK("Frequency","Frequency",8,1,9);

   for(i=0; i<NUM_CHAN; i++){
     sprintf(name,  "DeltaT_%d", i+1);  
     hDeltaT[i] = H1_BOOK(name, name, 10000, 0000, 500000 );
   }

   close_subfolder();

   tIndEvents=new TTree("T","all signals");
   tIndEvents->Branch("channel",&i_ch,"i_ch/I");
   tIndEvents->Branch("time48",&i_time48,"i_time48/l");
   tIndEvents->Branch("emax",&i_emax,"i_emax/I");
   tIndEvents->Branch("ezero",&i_ezero,"i_ezero/I");
   gManaHistosFolder->Add(tIndEvents);

   return(0);
}

int sis3302_init(void)
{
   char odb_str[128], errmsg[128];
   HNDLE hDB, hKey;

   sprintf(odb_str,"/Analyzer/Parameters/sis3302g");
   cm_get_experiment_database(&hDB, NULL);
   db_create_record( hDB, 0, odb_str, strcomb(trigger_settings_str) );
   db_find_key( hDB, 0, odb_str, &hKey );
   if( db_open_record(hDB, hKey, &trigger_settings, sizeof(trigger_settings), MODE_READ, odb_callback, NULL) != DB_SUCCESS ){
       sprintf(errmsg, "Cannot open %s tree in ODB", odb_str);
       cm_msg(MERROR,"sis3302_init", errmsg);
   }
   odb_callback(hDB, hKey, NULL); /* initialise .. */
   //hist_init_roody();
   
   /*AG 04.03.10
   	Fix to get updated sample lengths.*/
   sprintf(odb_str,"/Equipment/Trigger/Settings/sis3302g");
   cm_get_experiment_database(&hDB, NULL);
   db_create_record( hDB, 0, odb_str, strcomb(trigger_settings_str) );
   db_find_key( hDB, 0, odb_str, &hKey );
   if( db_open_record(hDB, hKey, &trigger_settings, sizeof(trigger_settings), MODE_READ, odb_callback, NULL) != DB_SUCCESS ){
       sprintf(errmsg, "Cannot open %s tree in ODB", odb_str);
       cm_msg(MERROR,"sis3302_init", errmsg);
   }


   sprintf(odb_str,"/Equipment/TITAN_ACQ/Settings/ppg_cycle_mode_1e");
   sprintf(odb_str,"/Alias/ppg_blocks&");
   cm_get_experiment_database(&hDB, NULL);
   db_create_record( hDB, 0, odb_str, strcomb(ppg_settings_str) );
   db_find_key( hDB, 0, odb_str, &hKey );
   if( db_open_record(hDB, hKey, &ppg_settings, sizeof(ppg_settings), MODE_READ, odb_callback, NULL) != DB_SUCCESS ){
       sprintf(errmsg, "Cannot open %s tree in ODB", odb_str);
       cm_msg(MERROR,"ppg_init", errmsg);
   }

   tin=ppg_settings.pulse9.time_offset__ms_;
   tout=ppg_settings.pulse13.time_offset__ms_;
   std::cout << "pulse9 " << ppg_settings.pulse9.time_offset__ms_ << std::endl;
   std::cout << "pulse9 " << ppg_settings.pulse9.pulse_width__ms_ << std::endl;
   std::cout << "pulse13 " << ppg_settings.pulse13.time_offset__ms_ << std::endl;
   std::cout << "pulse13 " << ppg_settings.pulse13.pulse_width__ms_ << std::endl;
   //std::cout << "pulse9 " << ppg_settings.names.names[1] << std::endl;

   db_close_record(hDB,hKey);

   hist_init_roody();
   return SUCCESS;
}

//--------------------------------------------------------------------
int sis3302_event(EVENT_HEADER *pheader, void *pevent) {
//--------------------------------------------------------------------

   static char bars[] = "|/-\\";
   static int i_bar;
   char bank_name[5];
   int i, bank_len;
   DWORD *idata; 
   
   for(i=0; i<NUM_CHAN; i++){
     sprintf(bank_name,"SIS%d", i);
     if( (bank_len = bk_locate(pevent, bank_name, &idata)) == 0 ){ continue; }
     // std::cout << " data size " << bank_len << " " << bk_size(pevent) << std::endl;
     sis3302_unpack(bank_len, idata, i);
     //     printf("..%c - Serial:%d size:%d", bars[i_bar++ % 4],
     // 	    (int)SERIAL_NUMBER(pevent), bk_size(pevent) );
     //      if( ((SERIAL_NUMBER(pevent)+1) % 1000) == 0 ){ 
	//        printf("\n");
/*      } else { */
/*        printf("\r"); */
     //} 
   }
   return(0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/* Sis data currently looks like ...                                                               */
/* Bank:SIS0 Length: 152(I*1)/38(I*4)/38(Type) Type:Unsigned Integer*4                             */
/*    1-> 0x49832000 0x969db6fe 0x5aee5abe 0x5b3b5b1d 0x5b965b75 0x5bf45bd0 0x5c565c26 0x5cc45c87  */
/*    9-> 0x5d2f5c1d 0x5d7c5d3b 0x5dc85d99 0x5de35e0d 0x5e135e25 0x5e325e1a 0x5e445e27 0x5e295e3a  */
/*   17-> 0x5e5a5e06 0x5e6a5df9 0x5e295e41 0x5e1b5e3a 0x5e005e2a 0x5db65dd8 0x5d785d8d 0x5cfe5d41  */
/*   25-> 0x5c9b5cd7 0x5c305c77 0x5be05c06 0x5b835baa 0x5b215b52 0x5ac15af1 0x5a615aa0 0x59fe5a2f  */
/*   33-> 0x59ab59d2 0x59425976 0x00011cfa 0x000000ed 0x01000001 0xdeadbeef                        */
/*   Timestamp[47:32] Evthead,ADCID                                          0x4983 2000           */
/*   Timestamp[32:00] Timestamp[32:00]                                       0x969d b6fe           */
/*   Sample2,         sample1          (0-65532)                             0x5aee ....           */
/*   MAWD Buffer (Signed int)          (0-510)                                                     */
/*   Emax                                                                    0x00011cfa            */
/*   Efirst                                                                  0x000000ed            */
/*   Flags:  Pileup,Retrig,N+1Trig,N-1Trig,FastTrgCount[4bit],23*0,TrigFlag  0x01000001            */
/*   DeadBeef                                                                                      */
/////////////////////////////////////////////////////////////////////////////////////////////////////

#define SIS_TRAILER          0xdeadbeef

static unsigned int timestamp[2], ev_head;

int sis3302_unpack(int nitems, DWORD *pdata, int channel)
{
  int i, chanflag, sample;
  int energy, ehigh, elow;
  int data;
  unsigned int ftc;
  unsigned int flags, puflag, rtflag;
  unsigned int uint_energy;
  //std::cout << " 0 " << nitems << std::endl;

  timestamp[0] = pdata[1]; timestamp[1] = pdata[0] & 0xffff0000;
  timestamp[1] >>=16;
  i_time48=timestamp[1]; i_time48<<=32; 
  i_time48+=timestamp[0];
  
  ev_head  = pdata[0] & 0xffff;
  pdata += 2; nitems -=2;
  
  chanflag = ev_head & 0xf;

  signed short int raw_samples[trigger_settings.sis3302g.raw_samples];
  sample = -1;
  for(i=0; i<(trigger_settings.sis3302g.raw_samples/2); i++){
    data = pdata[i] & 0xffff;
    raw_samples[sample++]=data;
    data = (pdata[i] >> 16) & 0xffff;
    raw_samples[sample++]=data;
  }
  //std::cout << trigger_settings.sis3302g.raw_samples << " " << trigger_settings.sis3302g.energy_sample_length << std::endl;
  //std::cout << "raw sample " << i << std::endl;
  pdata += i; nitems -= i;
  //std::cout << " 1 " << nitems << std::endl;

  if (trigger_settings.sis3302g.energy_sample_length>510) trigger_settings.sis3302g.energy_sample_length=510;
  //trigger_settings.sis3302g.energy_sample_length&=0xfffe; // even number
  int energy_samples[trigger_settings.sis3302g.energy_sample_length];
  i=0;
  
  for(i=0;i<trigger_settings.sis3302g.energy_sample_length;i++){
    data = pdata[i] & 0xffffffff;
    energy_samples[i]=data;
  }
  pdata += i; nitems -= i;
  //std::cout << " 2 " << nitems << std::endl;

  ehigh = pdata[0]; elow = pdata[1]; energy = ehigh; // - elow;
  uint_energy= (unsigned int) energy; //thats what the STRUCK code does
  pdata += 2; nitems -=2;
  flags = pdata[0];
  ftc=(flags & 0xf000000) >> 24;
  nitems -=2; // flags and trailer

  rtflag=flags & 0x40000000;
  puflag=flags & 0x80000000;
  /*
  if (ftc==1 && rtflag!=0) {
    std::cerr << "WARNING: retrigger: " << channel << " " << i_time48 << " " << elow << " " <<  ehigh << " " << flags << std::endl; 
    return 0;
  }
  */
  /*
  if (flags & 0x40000000) {
    std::cerr << "WARNING: retrigger: " << channel << " " << i_time48 << " " << elow << " " <<  ehigh << std::endl; 
    return 0;
  }
  if (flags & 0x80000000) {
    std::cerr << "WARNING: pileup: " << channel << " " << i_time48 << " " << elow << " " << ehigh << std::endl; 
    return 0;
  }
  */

  if (chanflag!=channel) {
    std::cerr << "ERROR: channel confusion: " << chanflag << " vs " << channel << std::endl; 
    return -1;
  }
  if( pdata[1] != SIS_TRAILER || nitems!=0) {
    fprintf(stderr,"Error unpacking data channel %i: trailer -1- %x -0- %x -2- %x items left %i\n",channel,pdata[1], pdata[0], pdata[2],nitems);
    return -1;
  }
  //
  //---- upacking done

  hCh->Fill(channel+1);

  int64_t d=i_time48-lasttime[channel];
  lasttime[channel]=i_time48;
  hDeltaT[channel]->Fill(d);
  hFreq->SetBinContent(channel+1,1./(d/10e6));

  int ENERGY_DIVISOR = 128; //trigger_settings.sis3302g.energy_gate_length;
  //if (!(rtflag | puflag)) 
  if (ftc==1 && rtflag==0) {
    hEnergy[channel]->Fill(uint_energy/ENERGY_DIVISOR, 1);
    hEnergyMinMax[channel]->Fill((ehigh-elow)/ENERGY_DIVISOR, 1);
    hEnergy64k[channel]->Fill(uint_energy, 1);
  }
  if (rtflag) hEnergyRt[channel]->Fill(uint_energy/ENERGY_DIVISOR, 1);
  if (puflag) hEnergyPu[channel]->Fill(uint_energy/ENERGY_DIVISOR, 1);

  // pulse shape
  for(i=0; i<(trigger_settings.sis3302g.raw_samples); i++){
    hRaw[channel]->SetBinContent(i+1, raw_samples[i]);
  }
  // trapzoid shape
  for(i=0; i<(trigger_settings.sis3302g.energy_sample_length); i++){
    hRaw2[channel]->SetBinContent(i+1, energy_samples[i]);
  }

  // tree
  i_ch=channel;
  i_emax=ehigh;
  i_ezero=elow;
  //tIndEvents->Fill();   

  if (channel>=0) {
    std::cout << "ch " << channel << " t" << i_time48 << " Es " << elow << " " << ehigh << " flags " <<std::hex<< flags<< std::dec
	      << " e/div " << ehigh/ENERGY_DIVISOR 
	      << " ftc " << ftc
	      << " uenergy " << uint_energy
	      << std::endl;
  }
  return SUCCESS;


#ifdef no

  int64_t d=i_time48-lasttime[channel];
  hDeltaT[channel]->Fill(d);
  if (channel==2) { //(d<=1 || d>50e3) {
    std::cout << "d wrong " << channel << " " << d << " ---- ";
    std::cout << i_time48 << " // " << lasttime[channel] << " // " << std::endl;
  }
  if (d!=0) {
    hFreq->SetBinContent(channel+1,1./(d/10e6));
    lasttime[channel]=i_time48;
  }
  //  return 0;
  if (d<-10000) {
    //std::cout << d << " new cycle " << i_time48 << " === " << lasttime[channel] << " === " << channel << " .." << time(NULL) << "             "<< std::endl;
     lasttime[channel]=0;
     soc[channel]=1;
  }
  else {
    lasttime[channel]=i_time48;
    soc[channel]=0;
  }

   hTime[channel]->Fill(i_time48,1);
   //int raw_samples[trigger_settings.sis3302g.raw_samples];

   sample = 0;
   //std::cerr << "R_Sample Length = " << trigger_settings.sis3302g.raw_samples << std::endl;
   //std::cerr << "E_Sample Length = " << sis3302g_param.corr_samples << std::endl;
   //for(i=0; i<(sis3302g_param.raw_samples/2); i++){
   //for(i=0; i<(300); i++){
   for(i=0; i<(trigger_settings.sis3302g.raw_samples/2); i++){
      data = pdata[i] & 0xffff;
      //raw_samples[sample]=data;
      hRaw[channel]->SetBinContent(sample++, data);
      data = (pdata[i] >> 16) & 0xffff;
      //raw_samples[sample]=data;
      hRaw[channel]->SetBinContent(sample++, data);
      //std::cout << "ASDF ASDFAWEF AWEF WAEF AWEF" << std::endl;
   }
   pdata += i; nitems -= i;

   sample = 0;
   //for(i=0; i<sis3302g_param.corr_samples; i++){
   //for(i=0; i<510; i++){
   //if (trigger_settings.sis3302g.energy_sample_length>510) trigger_settings.sis3302g.energy_sample_length=510;
   //trigger_settings.sis3302g.energy_sample_length&=0xfffe; // even number
   //int energy_samples[trigger_settings.sis3302g.energy_sample_length];
   //int fff;
   for(i=0;i<;i++){
      //hRaw2[channel]->SetBinContent(sample++, pdata[i] );
      data = pdata[i] & 0xffffffff;
      //energy_samples[i]=data;
      hRaw2[channel]->SetBinContent(sample++, data );
      //std::cout << "2222ASDF ASDFAWEF AWEF WAEF AWEF" << std::endl;
   }
   pdata += i; nitems -= i;

   //#define ENERGY_DIVISOR 256
   int ENERGY_DIVISOR = 512; //trigger_settings.sis3302g.energy_gate_length;
   ehigh = pdata[0]; elow = pdata[1]; energy = ehigh; // - elow;


   hEnergy[channel]->Fill(energy/ENERGY_DIVISOR, 1);
   hEnergyc0[channel]->Fill(energy/ENERGY_DIVISOR, 1); //current
   if (soc[channel]) {
     for (i=0; i<ADC_CHAN; i++) {
       hEnergyc2[channel]->SetBinContent(i+1,hEnergyc0[channel]->GetBinContent(i+1)-hEnergyc1[channel]->GetBinContent(i+1));
       hEnergyc1[channel]->SetBinContent(i+1,hEnergyc0[channel]->GetBinContent(i+1));
       hEnergyc0[channel]->SetBinContent(i+1,0);
     }
   }
   hEvsTfilled[channel]->Fill(energy/ENERGY_DIVISOR,i_time48/ADC_CLOCK);
   pdata += 2; nitems -=2;
   flags = pdata[0];
#endif
#ifdef nono
   //if (i_time48<tlast) std::cout << "BUUUUUUUUUUUUUUUUUUUUUUUUH" << std::endl;
   if (i_time48==tlast3){
     std::cout << "------------WARNING LAST TIME 3" << std::endl;
     std::cout << "ch" << channel << " t" << i_time48 << " Es " << elow << " " << ehigh << " flags " <<std::hex<< flags<< std::dec << std::endl;
   }
   tlast3=tlastlast;
   if (i_time48==tlastlast) {
     std::cout << "WARNING LAST TIME 2" << std::endl;
     std::cout << "ch" << channel << " t" << i_time48 << " Es " << elow << " " << ehigh << " flags " <<std::hex<< flags<< std::dec << std::endl;
   }
   tlastlast=tlast;
   if (i_time48==tlast) { std::cout <<"WARNING LAST TIME 1" << std::endl;    std::cout << "ch" << channel << " t" << i_time48 << " Es " << elow << " " << ehigh << " flags " <<std::hex<< flags<< std::dec << std::endl;
   }
   tlast=i_time48;

#endif
   i_ch=channel;
   i_emax=ehigh;
   i_ezero=elow;
   //std::cout << i_ch << " " << i_emax << " " << i_time48 << " " << std::endl;
   //tIndEvents->Fill();   

   if( pdata[1] != SIS_TRAILER ){
     fprintf(stderr,"error unpacking data\n");
   }
   return SUCCESS;
}
