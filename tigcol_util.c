/*#######################################################################*/
/*##################          Event Readout           ###################*/
/*#######################################################################*/

//***** to improve performance need to eliminate copying of data from buffer to buffer
//***** and just use pointer copying
//***** (do after current readout is properly working and tested)

#include <stdio.h>
#include <stdlib.h> /* free/malloc */
#include <string.h> /* memset */
#include <unistd.h> /* usleep */
#include "midas.h" /* TID_DWORD, TID_WORD */

extern int  TIGCOL_NFrameRead(int collector);
extern int  TIGCOL_vme_fifo_addr(int Collector_id);
extern void TIGCOL_read_port_parameter(int collector, int port, int chan);
extern int  TIGCOL_NextFrame(int Collector_id);

int readback_parameters(int collector, int port, int chan)
{
   TIGCOL_read_port_parameter(collector, port, chan );
   return(0);
}

#define MAX_COLLECTORS 8
/* separate buffers for each collector would simplify readout */
#define                            EV_BUF_SIZE    (1<<20) /* allow 1Mbyte readout buffer */
unsigned int evbuf[MAX_COLLECTORS][EV_BUF_SIZE], evbufpos[MAX_COLLECTORS];
/* Also use tmp evbuf of above size for dma readout */

#define ASSEMBLE_EVENTS   /* turn on event assembly in frontend */
#define MAX_SAMPLES  16384 /* no longer any real reason for this check */
#ifndef NUM_CHAN
#define NUM_CHAN       720
#endif
#ifndef NUM_DEBUG_CHAN
#define NUM_DEBUG_CHAN 600
#endif

#define DYNAMIC_EVBUF
#ifndef DYNAMIC_EVBUF
   #define NUM_EVBUF 128
   #define num_evbuf 128
#endif

#define TRIG_HIST_SIZE  0xffff /* below requires this be (power of 2)-1 */
int trigger_history[(TRIG_HIST_SIZE+1)];

//#define ASM_BUF_SIZE    (1<<15) /*  32 kbyte event buffer */
//#define ASM_BUF_SIZE    (1<<17) /*  128 kbyte event buffer */
#define ASM_BUF_SIZE    (1<<18) /* 256 kbyte event buffer */
/* each tig10_event struct used to have storage for 600*(7+512/2) ~ 1.5Mbytes */ 
/* still allow this for if zero-supn is disabled (not all chans have 512 samples) */
typedef struct tig10_event_struct {
  int  data_len;
  int  trigger_num;       /* pass trigger num and data consecutively */
  char data[ASM_BUF_SIZE];
} Tig10_event;

#ifndef DYNAMIC_EVBUF
Tig10_event assembled, tig10_event[NUM_EVBUF];
#else
Tig10_event assembled, *tig10_event;
static int num_evbuf;
#endif


//int   event_id[NUM_EVBUF][NUM_CHAN];      /* store midas event numbers for debugging */
//int event_save[NUM_EVBUF][NUM_CHAN][128]; /* store midas events for debugging        */

/* *** Need to test that debug_chan_offset+num_debug_chan <= NUM_CHAN */
int print_chan_offset = 0, num_print_chan = NUM_DEBUG_CHAN;

int         assemble_init ( int, int, int );
int    store_tigcol_event ( char *pevent, int debug_interval );
int      debug_dump_event ( unsigned int *buf, int len, int full, int port );
int assemble_tigcol_event ( int, unsigned int *evntbuf, int evntbuflen,
                            int frontend_event_assembly, int port_offset );
INT pio_read_tigcol_event ( char *pevent, int collector, int frontend_event_assembly,
                            int debug_interval, int suppress_error_messages);
INT dma_read_tigcol_event ( char *pevent, int collector, int frontend_event_assembly,
                            int debug_interval, int suppress_error_messages);

extern int *vme_blt_read(int vme_addr, int size);

/* have array of partially assembled events: "tig10_event[NUM_EVBUF]"            */
/* and space for an event that comes completely assembled this time "assembled"  */
/* assembly is based on trigger numbers (present in both header and trailer)     */
/* there are NUM_EVBUF spaces for partial events, so bits of one event can have  */
/* at most NUM_EVBUF new triggers between them                                   */
/* as new trigger numbers are seen, new slots are needed for this event,         */
/* to make room, the least recently modified slot is copied out (to "assembled") */
/* the static array "order" keeps track of the age of each slot:                 */
/* on each write into tig10_event[N], each element of "order" is increased by 1  */
/* and element "N" is set to zero                                                */
/* *** if NUM_EVBUF is too small to fully assemble events, they will be returned */
/*     with ports missing, and multiple partial events with the same trigger No. */
/*     will be seen                                                              */
#ifndef DYNAMIC_EVBUF
static int evbuf_order[NUM_EVBUF], evbuf_inuse;
#else
static int *evbuf_order, evbuf_inuse;
#endif
int assemble_init(int debug_chan, int debug_offset, int requested_evbuf)
{
   static int prev_evbuf;
   int i;

#ifdef DYNAMIC_EVBUF
   if( (num_evbuf = requested_evbuf) != prev_evbuf ){ /* realloc buffers */
      if( tig10_event != NULL ){ free(tig10_event); tig10_event = NULL; }
      if( evbuf_order != NULL ){ free(evbuf_order); evbuf_order = NULL; }
      if( (evbuf_order = (int *)malloc(num_evbuf*sizeof(int))) == NULL ){
	 fprintf(stderr,"failed to alloc %d event buffers\n", num_evbuf);
         return(-1);
      }
      if( (tig10_event = (Tig10_event *)malloc(num_evbuf*sizeof(Tig10_event))) == NULL ){
	 fprintf(stderr,"failed to alloc %d event buffers\n", num_evbuf);
         free(evbuf_order); evbuf_order = NULL;
         return(-1);
      }
      prev_evbuf = num_evbuf;
   }
#endif
   evbuf_inuse = 0;
   memset(evbuf_order, 0, num_evbuf*sizeof(int));
   memset(trigger_history, 0, TRIG_HIST_SIZE*sizeof(int));
   for(i=0; i<num_evbuf; i++){
      tig10_event[i].trigger_num = -1; tig10_event[i].data_len = 0;
   }
   num_print_chan = debug_chan;
   if( (print_chan_offset = debug_offset) > NUM_CHAN ){ print_chan_offset = NUM_CHAN; }
   if( num_print_chan + print_chan_offset > NUM_CHAN ){ num_print_chan = NUM_CHAN - print_chan_offset; }
   return(0);
}
int skip_assembly(int trigger_num, unsigned int *evntbuf, int evntbuflen)
{
   int evbytes;
   assembled.trigger_num = trigger_num;
   evbytes = (evntbuflen-2)*sizeof(int);
   assembled.data_len = evbytes;
   memcpy( assembled.data, evntbuf+1, evbytes );
   return( 1 ); /* was event_avail - which is currently zero - error? */
}
int assemble_tigcol_event(int collector, unsigned int *evntbuf, int evntbuflen, int frontend_event_assembly, int port_offset)
{
   static int current; // remember current slot, and keep writing to it until trigger number changes
   int i, j, evbytes, max, max_slot, trigger_num;
   Tig10_event *ptr = tig10_event+current;
   int event_avail = 0;

   /* currently dma transfers sometimes give a few leading zeroes ? */
   for(i=0; i<evntbuflen; i++){ if( evntbuf[i] ){ break; } }
   if( i > 0 ){ evntbuf += i; evntbuflen -= i; }

   trigger_num = evntbuf[ 0 ] & 0x0ffffff;
   evntbuf[port_offset] |= collector << 8; /* ADD collector to port num */

   /* The following little bit of code seems to be archaic, or possible just plain wrong. */
   /* I shall skip it. */
   /*wtf */
#if (0)
   if( evntbuf[port_offset] == 0xc00005ff ){ /* do not assemble events from this channel - P-Type Ge */
      return( skip_assembly(trigger_num, evntbuf, evntbuflen) ); 
   }
   return( skip_assembly(trigger_num, evntbuf, evntbuflen) );
#endif
   if( ptr->trigger_num != trigger_num ){ /* new trigger - switch to new event slot */
      for(i=0; i<num_evbuf; i++){ /* first check if this trigger already seen in other event slot */
         if( tig10_event[i].trigger_num == trigger_num ){
            for(j=0; j<num_evbuf; j++){ ++evbuf_order[j]; }
            evbuf_order[i] = 0; current = i; break;
         }
      }
      if( i == num_evbuf ){ /* this trigger not found - need new slot */
         for(i=0; i<num_evbuf; i++){ /* check for unused event slots */
            if( tig10_event[i].trigger_num == -1 ){
               for(j=0; j<num_evbuf; j++){ ++evbuf_order[j]; }
               evbuf_order[i]=0; current=i; ++evbuf_inuse; break;
            }
         }
         if( i == num_evbuf ){ /* no unused slots - take place of oldest used slot  */
            max = ++evbuf_order[0]; max_slot = 0;
            for(j=1; j<num_evbuf; j++){ if( ++evbuf_order[j] > max ){ max = evbuf_order[j]; max_slot = j; } }
            evbuf_order[max_slot] = 0; current = max_slot;
            assembled.trigger_num = tig10_event[current].trigger_num;
            assembled.data_len = tig10_event[current].data_len;
            memcpy( assembled.data, tig10_event[current].data, tig10_event[current].data_len );
            //tig10_event[current].trigger = -1; set just below
            tig10_event[current].data_len = 0;
/*PROGRAMMABLE DELAY */ if( frontend_event_assembly > 1 ){ usleep(frontend_event_assembly); }
            event_avail = 1;
         }
      }
      ptr = &tig10_event[current];
      ptr->trigger_num  = trigger_num;
   }
   // ** Should preserve this check, which was in previous assembly
   //if( ptr->port_hitpattern[(int)port/32] & (1<<(port%32)) ){
   //   if( ! quiet ){ fprintf(stderr,"Event 0x%x: port %d already seen - discarding\n",
   //			     trigger_num, port );
   //   fprintf(stderr,"          port[%s]\n", binstring( ptr->port_hitpattern, NUM_CHAN ) );
   //   debug_dump_event(evntbuf, evntbuflen, 0, 0); } return( event_avail );
   // }

   /* copy event, skipping header and trailer (first+last words) */
   evbytes = (evntbuflen - 2) * sizeof(int);
   if( ptr->data_len + evbytes >= ASM_BUF_SIZE ){
     /* due to corrupt data, or ASM_BUF_SIZE too small for current events */
     /* (curr event size depends on waveform lengths, num chans, and amount of zero-supn) */
     /*     could write out current partial event, and restart new event with this data */
     /*     but makes more complex (and still wrong), better to fix buffer size  */
      fprintf(stderr,"******Event assembly: event too large - data discarded ...\n");
   } else {
      memcpy( ptr->data+ptr->data_len, evntbuf+1, evbytes );
      ptr->data_len += evbytes;
   }
   return( event_avail ); 
}

int flush_event_available(){ return(evbuf_inuse); }
// find oldest buffer, copy to assembled, and mark as unused
// zero oldest slot, (already incremented all others)
int flush_one_event(char *pevent, int debug_interval, int suppress_error_messages)
{
    int j, current, max, max_slot;
    if( evbuf_inuse == 0 ){ return(0); } // no data available
    max = ++evbuf_order[0]; max_slot = 0;   
    for(j=1; j<num_evbuf; j++){ if( evbuf_order[j] > max ){ max = ++evbuf_order[j]; max_slot = j; } }
    if( tig10_event[max_slot].trigger_num == -1 ){ // no data available???
       for(j=0; j<num_evbuf; j++){ --evbuf_order[j]; } return(0);
    }
    evbuf_order[max_slot] = 0; current = max_slot;
    assembled.trigger_num = tig10_event[current].trigger_num;
    assembled.data_len = tig10_event[current].data_len;
    memcpy( assembled.data, tig10_event[current].data, tig10_event[current].data_len );
    tig10_event[current].trigger_num = -1; /* mark this slot as unused .. */
    tig10_event[current].data_len = 0;
    --evbuf_inuse;
    return( store_tigcol_event(pevent, debug_interval) );
}

/*  frames read into evbuf[col] (each filled in parallel)              */
/*  as soon as 1 contains a trailer (i.e. has 1 whole event)           */
/*  this is copied to event (or assembly buffer) and bufpos reset to 0 */

/* dma: have extra dmabuf[col] (plus len+POS), copied as above to evbuf[], till trail */
/* if dmabuf contains data, that is used, otherwise a new dma is done                 */
/*   could add this check to poll - so bufs can be emptied even if no more events     */
/*      could copy directly to evbuf, and each call, check back from end for trail    */
/*      (if none, copy remains to start + new dma)                                    */

/* new assembly - need to check event */

static int check_tigcol_event(int collector, unsigned int *evntbuf, int length, int quiet );
extern void *reconstruct_tigcol_event( unsigned int *evntbuf, int evntbuflen, int quiet );

int store_tigcol_event(char *pevent, int debug_interval)
{
   static int count;
   int *ddata;

   if( debug_interval && ((++count) % debug_interval) == 0 ){
      reconstruct_tigcol_event( (unsigned int *)&assembled.trigger_num,
                                 1+assembled.data_len/sizeof(int), 1 );
   }
   trigger_history[assembled.trigger_num & TRIG_HIST_SIZE] = assembled.trigger_num;
   bk_init(pevent);
   bk_create(pevent, "TIG0", TID_DWORD, &ddata);
   *(ddata++) = assembled.trigger_num;
   memcpy((char *)ddata, (char *)assembled.data, assembled.data_len );
   bk_close(pevent, ddata+(int)(assembled.data_len/sizeof(int)) );
   return( bk_size(pevent) );
}

#define HEADER_MASK 0xF0000000
 
/* Data Header definitions */
#define  TIGCOL_HEADER               (DWORD) (0x80000000)
#define  TIGCOL_TIME_STAMP           (DWORD) (0xA0000000)
#define  TIGCOL_CHANNEL              (DWORD) (0xC0000000)
#define  TIGCOL_DATA                 (DWORD) (0x00000000)
#define  TIGCOL_FEATURE_TIME         (DWORD) (0x40000000)
#define  TIGCOL_FEATURE_CHARGE       (DWORD) (0x50000000)
#define  TIGCOL_FEATURE_MASK         (DWORD) (0x70000000)
#define  TIGCOL_TRAILER              (DWORD) (0xE0000000)
#define  TIGCOL_OUT_OF_SYNC          (DWORD) (0x88000000)
#define  TIGCOL_TIMEOUT              (DWORD) (0x10000000)

/*-- Event readout -------------------------------------------------*/
/* start new event on header, but end old on trailer - can miss words in between */
INT pio_read_tigcol_event(char *pevent, int collector, int frontend_event_assembly, int debug_interval, int suppress_error_messages)
{
   int *evntbuf = evbuf[collector], *evntbufpos = &evbufpos[collector];
   int i, port_offset, nframe, frame=0, *pdata;
   if( ! frontend_event_assembly ){ bk_init(pevent); }

   if( (nframe=TIGCOL_NFrameRead( collector ) ) <= 0 ){ return(0); }
   for(i=0; i<nframe; i++){
      if( (frame = TIGCOL_NextFrame( collector ) ) == 0x10000bad ){ continue; }
      evntbuf[(*evntbufpos)++] = frame;
      if( *evntbufpos >= EV_BUF_SIZE ){
         fprintf(stderr,"event buffer overrun: data discarded\n");
         *evntbufpos = 0;
      }
      if( ! (frame & 0x80000000) ){ continue; }
      switch( frame & HEADER_MASK ){ /* frame has header bit set */
      case TIGCOL_HEADER:     //  printf("header found 0x%lx\n", frame);
	//evtcomplete = FALSE;
         //evbufpos = 0;
         //  if (frame & (TIGCOL_OUT_OF_SYNC | TIGCOL_TIMEOUT))
	 //  cm_msg(MINFO,"readout","Header info 0x%lx", frame);
         //evbuf[evbufpos++] = frame;
         break;
      case TIGCOL_TRAILER:
         if( frame & TIGCOL_TIMEOUT ){
	    cm_msg(MINFO,"readout","Trailer info 0x%lx", frame);
         }
         if( frontend_event_assembly ){
 	    port_offset = check_tigcol_event(collector, (unsigned int *)evntbuf, *evntbufpos, suppress_error_messages);
	    if( port_offset == -1 ){ *evntbufpos = 0; return(0); }
            if( assemble_tigcol_event(collector, evntbuf, *evntbufpos, frontend_event_assembly, port_offset) ){
               *evntbufpos = 0; return( store_tigcol_event(pevent, debug_interval) );
            } else {
  	       *evntbufpos = 0; return(0);
            }
         } else {
	    bk_create(pevent, "WFDN", TID_DWORD, &pdata);
    	    memcpy((char *) pdata, (char *)evntbuf, *evntbufpos*sizeof(DWORD));
            check_tigcol_event(collector, (unsigned int *)evntbuf, *evntbufpos, suppress_error_messages);
            pdata += *evntbufpos; *evntbufpos = 0;
	    bk_close(pevent, pdata);
	    return bk_size(pevent);
         }
      }
   }
   return 0;
}

extern int *vme_blt_read(int vme_addr, int size);
//int tmp_buf[EV_BUF_SIZE], tmp_bufpos;
int tmp_buf[MAX_COLLECTORS][EV_BUF_SIZE], tmp_bufpos[MAX_COLLECTORS], tmp_bufwords[MAX_COLLECTORS];
int *dma_buf/*, dma_bufpos, dma_buf_words*/;

INT dma_read_tigcol_event(char *pevent, int collector, int frontend_event_assembly, int debug_interval, int suppress_error_messages)
{
   int *dbuf = tmp_buf[collector], *dbufpos = &tmp_bufpos[collector], *dbuflen = &tmp_bufwords[collector];
   int frame=0, *pdata, port_offset, nframe, vme_addr=TIGCOL_vme_fifo_addr( collector );
   int *evntbuf = evbuf[collector], *evntbufpos = &evbufpos[collector];
#ifndef OLDVMEDRV
   fprintf(stderr,"DMA readout not yet implemented here\n");
   return(0);
#else
   if( ! frontend_event_assembly ){ bk_init(pevent); }
   if( *dbufpos >  *dbuflen ){ fprintf(stderr,"dma readout buffer read beyond end\n"); }
   if( *dbufpos >= *dbuflen ){
      if( (nframe=TIGCOL_NFrameRead( collector )) <= 0 ){ return(0); }

      nframe -= nframe%2;

      /* do NOT read any odd 32bit part *EXCEPT* for very last frame - then read pad as well */
      //if( (dma_buf = (int *)vme_blt_read(vme_addr, (nframe-(nframe%2) + 2*(nframe==1)) *4)) == NULL ){
      if( (dma_buf = (int *)vme_blt_read(vme_addr, (nframe-(nframe%2)) *4)) == NULL ){
	/* ??? */         return(0);
      }
      //if( (nframe % 2) != 0 ){   /* 32bit read of any remaining 32bits */
      // 	 dma_buf[nframe-(nframe%2)] = TIGCOL_NextFrame( collector ); 
      //      }
      memcpy( dbuf, dma_buf, nframe*sizeof(int) ); /* dma buffer gets reused - need to copy */
      *dbufpos = 0; *dbuflen = nframe;
   }
   while( *dbufpos < *dbuflen ){
     frame = dbuf[(*dbufpos)++];
     //if( frame == 0x10000bad || frame == 0xdeaddead || frame == 0x00000000 ){ continue; }
      if( frame == 0xdeaddead ){ continue; }
      evntbuf[(*evntbufpos)++] = frame;
      if( *evntbufpos >= EV_BUF_SIZE ){
         fprintf(stderr,"event buffer overrun: data discarded\n");
         *evntbufpos = 0;
      }
      if( !(frame & 0x80000000) ){ continue; }
      /* frame has header bit set */
      switch( frame & HEADER_MASK ){
      case TIGCOL_HEADER: break;
      case TIGCOL_TRAILER:
         if( frame & TIGCOL_TIMEOUT ){
	    cm_msg(MINFO,"readout","Trailer info 0x%lx", frame);
         }
         if( frontend_event_assembly ){
 	    port_offset = check_tigcol_event(collector, (unsigned int *)evntbuf, *evntbufpos, suppress_error_messages);
	    if( port_offset == -1 ){ *evntbufpos = 0; return(0); }
            if( assemble_tigcol_event(collector, evntbuf, *evntbufpos, frontend_event_assembly, port_offset ) ){
               *evntbufpos = 0; return( store_tigcol_event(pevent, debug_interval) );
            } else {
  	       *evntbufpos = 0; return(0);
            }
         } else {
	    bk_create(pevent, "WFDN", TID_DWORD, &pdata);
    	    memcpy((char *) pdata, (char *)evntbuf, *evntbufpos*sizeof(DWORD));
            check_tigcol_event(collector, (unsigned int *)evntbuf, *evntbufpos, suppress_error_messages);
            pdata += *evntbufpos; *evntbufpos = 0;
	    bk_close(pevent, pdata);
	    return bk_size(pevent);
         }
      }
   }
   return 0;
#endif
}
/*===================================================================================================================================*/
int debug_dump_event(unsigned int *buf, int len, int full, int port)
{
   int i;
   if( ! full && len > 16 ){
      fprintf(stderr,"0x%8x 0x%8x 0x%8x 0x%8x 0x%8x 0x%8x 0x%8x 0x%8x\n ... ",
         buf[0], buf[1], buf[2], buf[3], buf[4], buf[5], buf[6], buf[7] );
      fprintf(stderr,"skip %3d items ... %19s 0x%8x 0x%8x 0x%8x 0x%8x\n\n",
         len-12, "", buf[len-4], buf[len-3], buf[len-2], buf[len-1] );
      return(0);
   }
   fprintf(stderr,"Event 0x%x: port:%3d ...\n   ", buf[0], port );
   for(i=0; i<len; i++){
      if( (i+1)%8 == 0 ){ fprintf(stderr,"\n   "); }
      fprintf(stderr," 0x%8x ", buf[i] );
   }
   if( i%8 != 0 ){ fprintf(stderr,"\n"); }
   fprintf(stderr,"\n");
   return(0);
}

#define MAX_PORT_OFFSET 7
static short check_wave_len[NUM_CHAN];
int check_tigcol_event(int collector, unsigned int *evntbuf, int evntbuflen, int quiet)
{
   int i, trigger_num, port, port_offset, wave_len;
   int check_wave_data = 1;

   /* currently dma transfers sometimes give a few leading zeroes ? */
   for(i=0; i<evntbuflen; i++){ if( evntbuf[i] ){ break; } }
   if( i > 0 ){
      evntbuf += i; evntbuflen -= i;
      if( ! quiet ){ fprintf(stderr,"Event 0x%x: %d leading zeroes\n", evntbuf[0], i ); }
   }

   // find port first, so can include full channel info (+collector) in following error messages
   for(i=1; i<MAX_PORT_OFFSET; i++){
      if( (evntbuf[i] & 0xf0000000) == 0xc0000000 ){ port_offset = i; break; }
   }
   if( i == MAX_PORT_OFFSET ){
      if( ! quiet ){ fprintf(stderr,"Event 0x%x: chan/port header item missing - discarding\n", evntbuf[0]);
      debug_dump_event(evntbuf, evntbuflen, 0, 0); }
      return(-1);
   }
   if( (evntbuf[port_offset] & 0xffffff00) != 0xc0000000 ){ /* need to write col_id here */
      if( ! quiet ){ fprintf(stderr,"Event 0x%x: (col=%d) chan/port contains extra data - discarding\n",
         evntbuf[0], collector);
         debug_dump_event(evntbuf, evntbuflen, 0, 0); }
      //return(-1);
   }
   evntbuf[port_offset] |= collector << 8; /* add the collector# to the channel number */
   if( (evntbuf[           0] & 0xf0000000) != 0x80000000 ||
       (evntbuf[evntbuflen-1] & 0xf0000000) != 0xE0000000 ){
      if( ! quiet ){ fprintf(stderr,"Event 0x%x: start/end problem - discarding\n", evntbuf[0]);
      debug_dump_event(evntbuf, evntbuflen, 0, 0); }
      return(-1);
   }
   if( (trigger_num = evntbuf[0] & 0x0ffffff) != (evntbuf[evntbuflen-1] & 0x0ffffff) ){
      if( ! quiet ){ fprintf(stderr,"Event 0x%x: head/trail mismatch - discarding\n", evntbuf[0]);
      debug_dump_event(evntbuf, evntbuflen, 0, 0); }
      return(-1);
   }
   if( trigger_history[trigger_num & TRIG_HIST_SIZE] == trigger_num ){
     if( ! quiet ){ fprintf(stderr,"Event 0x%x: Assembly failure - event# already written - increase number of buffers\n", evntbuf[0]); }
   }
   //if( (evntbuf[           1] & 0xff000000) != 0xa0000000 &&
   //    (evntbuf[           2] & 0xff000000) != 0xa0000000 ){
   //   if( ! quiet ){ fprintf(stderr,"Event 0x%x: timestamp problem - discarding\n", evntbuf[0]);
   //   debug_dump_event(evntbuf, evntbuflen, 0, 0); }
   //   return(-1);
   // }
   if( (evntbuf[evntbuflen-2] & 0xf0000000) != 0x40000000 ||
       (evntbuf[evntbuflen-3] & 0xf0000000) != 0x50000000 ){
      if( ! quiet ){ fprintf(stderr,"Event 0x%x: charge/cfd problem - discarding\n", evntbuf[0]);
      debug_dump_event(evntbuf, evntbuflen, 0, 0); }
      return(-1);
   }
   port  = evntbuf[port_offset] & 0xff;
   port -= 32*(port >= 0x80); /* for some reason there is a 2 card gap */
   port -=  6*(int)(port/16); /* tig10 only use 10 channels, but each cards port is 16 above prev */ 
   if( collector > 0 ){ port += 120*(collector-1); } /* if multi-card system, adjust port# for collector id */

   if( port < 0 || port >= NUM_CHAN ){
      if( ! quiet ){ fprintf(stderr,"Event 0x%x: port:%d out of range - discarding\n",
         trigger_num, port);
      debug_dump_event(evntbuf, evntbuflen, 0, 0); } return(-1);
   }
   if( check_wave_data ){
      wave_len = 0;
      for(i=port_offset+1; i<evntbuflen-3; i++){ /* loop over wave data */
         if( (evntbuf[i] & 0xf0000000) == 0 ){ wave_len+=2; continue; }
         //if( ! quiet ){ fprintf(stderr,"Event 0x%x: got unknown item type:%d\n",
	 //  	        trigger_num, evntbuf[i] >> 28); }
      }
   } else {
      wave_len = 2*( evntbuflen-3 - (port_offset+1) );
   }
   if( wave_len >= MAX_SAMPLES ){
      if( ! quiet ){ fprintf(stderr,"Event 0x%x: wavelength exceeds max samples\n", trigger_num );
      debug_dump_event(evntbuf, evntbuflen, 0, 0); } wave_len = 0;
   }
   if( check_wave_len[port] == 0 ){
      check_wave_len[port] = wave_len;
   } else
   if( check_wave_len[port] != wave_len ){
      if( ! quiet ){ fprintf(stderr,"Event 0x%x: port%3d Waveform length changed from %d to %d\n",
         trigger_num, port, check_wave_len[port], wave_len);
      }
      check_wave_len[port] = 0;
   }
   return( port_offset ); 
}
