

/* BLOCKS appears under "ppg cycle" in odb
 */
// Standard Width Pulse Block
typedef struct {
  double time_offset__ms_; /* length of offset from reference  */
  char time_reference[20]; /* "T0" = from PPG start */
  char ppg_signal_name[16]; // PPG output name
} STD_PULSE_BLOCK;

#define STD_PULSE_BLOCK_STR "\
time offset (ms) = DOUBLE : \n\
time reference = STRING : [20] \n\
PPG signal name =  STRING : [16] \n\
"
typedef struct {
  STD_PULSE_BLOCK std_pulse_block;
  double pulse_width_ms;
} STD_PULSE_INFO;


// Variable Width Pulse Block
typedef struct {
  double time_offset__ms_; /* length of offset from reference  */
  char time_reference[20]; /* 0 = from PPG start */
  double pulse_width__ms_;
  char ppg_signal_name[16]; // PPG output name
} PULSE_BLOCK;

#define PULSE_BLOCK_STR "\
time offset (ms) = DOUBLE : 0\n\
time reference = STRING : [20] t0\n\
pulse width (ms) = DOUBLE : 0\n\
PPG signal name =  STRING : [16] unknown\n\
"
typedef struct {
  PULSE_BLOCK pulse_block;
  // any common info goes here
} PULSE_INFO;


// Transition Block
typedef struct {
  double time_offset__ms_; /* length of offset from reference  */
  char time_reference[20]; /* e.g. "T0" = from PPG start */
  char ppg_signal_name[16]; // PPG output name
} TRANS_BLOCK;

#define TRANS_BLOCK_STR "\
time offset (ms) = DOUBLE : 0\n\
time reference = STRING : [20] t0\n\
PPG signal name =  STRING : [16] unknown\n\
"
typedef struct {
  TRANS_BLOCK trans_block;
  // add here any common params
} TRANS_INFO;







// BIT PATTERN Transition Block ; operate on more than one bit
typedef struct {
  double time_offset__ms_; /* length of offset from reference  */
  char time_reference[20]; /* e.g. "T0" = from PPG start */
  DWORD bit_pattern; // PPG bit pattern
} PATTERN_BLOCK;

#define PATTERN_BLOCK_STR "\
time offset (ms) = DOUBLE : 0\n\
time reference = STRING : [20] t0\n\
bit pattern = DWORD : 0\n\
"
typedef struct {
  PATTERN_BLOCK pattern_block;
  // add here any common params
} PATTERN_INFO;


/* may not need DELAY .... added internally as spacer */

// Delay  Block ; no change to bit pattern
typedef struct {
  double time_offset__ms_; /* length of offset from reference  */
  char time_reference[20]; /* 0 = from PPG start */
} DELAY_BLOCK;

#define DELAY_BLOCK_STR "\
time offset (ms) = DOUBLE : 0\n\
time reference = STRING : [20] t0\n\
"
typedef struct {
  DELAY_BLOCK delay_block;
  // add here any common params
} DELAY_INFO;




// BEGIN LOOP
typedef struct {
  double time_offset__ms_; /* length of offset from reference  */
  char time_reference[20]; /* e.g. T0 = from PPG start */
  INT loop_count; // loop counter
  /* Loop will automatically define time references at begin and end of all loops 
      _TBEGIN_<loopname>  _TEND_<loopname> */

} BEGIN_LOOP_BLOCK;

#define BEGIN_LOOP_BLOCK_STR "\
time offset (ms) = DOUBLE : 0\n\
time reference = STRING : [20] t0\n\
loop count = INT : 0\n\
"
typedef struct {
  BEGIN_LOOP_BLOCK begin_loop_block;
} BEGIN_LOOP_INFO;

/* automatically defines references L_loop1, E_loop1 where loop1 is the loopname */ 

// END LOOP
typedef struct {
  double time_offset__ms_; /* length of offset from reference  */
  char time_reference[20]; /* expect e.g. _TBEGIN_<loopname> = from loop reference */
} END_LOOP_BLOCK;
#define END_LOOP_BLOCK_STR "\
time offset (ms) = DOUBLE : 0\n\
time reference = STRING : [20] t0\n\
"
typedef struct {
  END_LOOP_BLOCK end_loop_block;
} END_LOOP_INFO;







// EV_STEP ELECTRODE  electrode voltage step  or AWG step
typedef struct {
  double time_offset__ms_; /* length of offset from reference  */
  char time_reference[20]; /* 0 = from PPG start */
  INT      awg_unit;  // unit number (default is unit 0)
  char     start_value[128]; 
  char     end_value[128];
  char     nsteps_at_start_ramp[128];
  char     nsteps_at_end_ramp[128];
} EV_STEP_BLOCK;

#define EV_STEP_BLOCK_STR "\
time offset (ms) = DOUBLE : 0\n\
time reference = STRING : [20] t0\n\
AWG unit = INT : 1\n\
start value = STRING : [128]\n\
end value = STRING : [128]\n\
Nsteps at start ramp = STRING : [128]\n\
Nsteps at end ramp = STRING : [128]\n\
"
typedef struct {
  EV_STEP_BLOCK ev_step_block;
  // add common parameters here
} EV_STEP_INFO;





/* EV_SET ELECTRODE  electrode voltage settings
   ... expected to do this prior to starting PPG sequence, therefore time offset not included */
typedef struct {
  double time_offset__ms_; /* length of offset from reference  */
  char time_reference[20]; /* 0 = from PPG start */
  INT      awg_unit;  // unit number (default is unit 0)
  char     set_values__volts_[128];  // input as (1,5),(3,-9)
} EV_SET_BLOCK;

#define EV_SET_BLOCK_STR "\
time offset (ms) = DOUBLE : 0\n\
time reference = STRING : [20] t0\n\
AWG unit = INT : 1\n\
Set Values (Volts) = STRING : [128]\n\
"
typedef struct {
  EV_SET_BLOCK ev_set_block;
  // add common parameters here
} EV_SET_INFO;







/* TIME REFERENCE
      define a time reference
*/
typedef struct {
  double time_offset__ms_; /* length of offset from reference below  */
  char time_reference[60]; /* e.g. "t0"  from PPG start */
} TIME_REF_BLOCK;

#define  TIME_REF_BLOCK_STR "\
time offset (ms) = DOUBLE : 0\n\
time reference = STRING : [60] t0\n\
"

typedef struct {
  TIME_REF_BLOCK time_ref_block;
  //add common params here
} TIME_REF_INFO;


// RF SWEEP
typedef struct {
  char generator[20];
  DWORD start_frequency__khz_;
  DWORD stop_frequency__khz_;
  DWORD duration__ms_;
  DWORD amplitude__mv_;
} RF_SWEEP_BLOCK;

#define RF_SWEEP_BLOCK_STR "\
generator = STRING : [20] \n\
start frequency (kHz) = DWORD : 0\n\
stop frequency (kHz) = DWORD : 0\n\
duration (ms) = DWORD : 0\n\
amplitude (mv) = DWORD : 0\n\
"
typedef struct {
  RF_SWEEP_BLOCK rf_sweep_block;
  double param; // add any common parameter here
} RF_SWEEP_INFO;


// RF FM SWEEP  (counted as an RF_SWEEP block)
//  Mode: internal FM single form externally triggered
typedef struct {
  char generator[20];
  DWORD start_frequency__khz_;
  DWORD stop_frequency__khz_;
  //DWORD duration__ms_;  // determined by cycle duration
  DWORD amplitude__mv_;
} RF_FM_SWEEP_BLOCK;

#define RF_FM_SWEEP_BLOCK_STR "\
generator = STRING : [20] \n\
start frequency (kHz) = DWORD : 0\n\
stop frequency (kHz) = DWORD : 0\n\
amplitude (mv) = DWORD : 0\n\
"
typedef struct {
  RF_FM_SWEEP_BLOCK rf_fm_sweep_block;
  //DOUBLE param; any future parameter
} RF_FM_SWEEP_INFO;



// RF BURST   (counted as an RF_SWEEP block)
//  Mode: externally triggered burst
typedef struct {
  char generator[20];
  DWORD frequency__khz_;
  DWORD number_of_cycles;
  DWORD amplitude__mv_;
} RF_BURST_BLOCK;

#define RF_BURST_BLOCK_STR "\
generator = STRING : [20] \n\
frequency (kHz) = DWORD : 0\n\
number of cycles = DWORD : 0\n\
amplitude (mv) = DWORD : 0\n\
"
typedef struct {
  RF_BURST_BLOCK rf_burst_block;
  //double param; // any future parameter
} RF_BURST_INFO;




// RF SWIFT AWG    (counted as an RF_SWEEP block)
//  Mode: externally triggered AWG burst
typedef struct {
  char generator[20];
  DWORD start_frequency_interval__khz_;
  DWORD stop_frequency_interval__khz_;
  DWORD notch_frequency__khz_;  
  DWORD amplitude__mv_;
  DWORD number_of_frequencies;
  DWORD number_of_bursts;
} RF_SWIFT_AWG_BLOCK;

#define RF_SWIFT_AWG_BLOCK_STR "\
generator = STRING : [20] \n\
start frequency interval (kHz) = DWORD : 0\n\
stop frequency interval (kHz) = DWORD : 0\n\
notch frequency (kHz) = DWORD : )\n\
amplitude (mv) = DWORD : 0\n\
number of frequencies= DWORD : 0\n\
number of bursts = DWORD : 0\n\
"
typedef struct {
  RF_SWIFT_AWG_BLOCK rf_swift_awg_block;
  //double param; any future parameter
} RF_SWIFT_AWG_INFO;
