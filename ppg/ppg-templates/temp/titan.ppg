// Header section

Clock Frequency = 10 Mhz;
ISA Card Address = 8000;   // VME address of the pulse programmer board (in hex)
Number of Flags = 24;      // Number of output bits

//************************************************************************************************************************************//
//************************************************************************************************************************************//

// !! USER SECTION !! //

  // Delay declarations
  
  d_msnp=0;
  d_dac_service=0;
  d_count=0;
  d_pulsew=0;
  d_rfon=0; 
  d_tof=0;
  d_inj=0;
  d_rfgate=0;
  
// !! END OF USER SECTION !! //

//************************************************************************************************************************************//
// Bit patterns ATTENTION!!: The bits are adressed from high to low, the position in the sum gives the number of the bit affected.

f_rf0_on       = 1,1;   // bit   1
f_rf0_off      = 0,1;
f_rf90_on      = 1,1;   // bit   2
f_rf90_off     = 0,1;
f_trapt_on     = 1,1;   // bit   3
f_trapt_off    = 0,1;
f_inj_on     = 1,1;   // bit   4
f_inj_off    = 0,1;
f_rfgate_on    = 1,1;   // bit   5
f_rfgate_off   = 0,1;

f_rf_on        = 1,1;   // bit   6
f_rf_off       = 0,1;

f_msnp_hi  = 1,1;   // bit   7 next dwelltime multiscaler pulse
f_msnp_lo  = 0,1;
f_counter_e    = 1,1;   // bit   8 multiscaler counter-gate
f_counter_d    = 0,1;

f_userbit1_on  = 1,1;   // bit   9 -> Userbit1
f_userbit1_off = 0,1;

f_userbit2_on  = 1,1;   // bit  10 -> Userbit2
f_userbit2_off = 0,1;

f_freqp_hi     = 1,1;   // bit  11 next FSC freq. ext. strobe
f_freqp_lo     = 0,1;

f_udctrl_u     = 1,1;   // bit  12 Strobe up down control
f_udctrl_d     = 0,1;

f_dacservp_hi  = 1,1;   // bit  13 Dac service pulse
f_dacservp_lo  = 0,1;

f_beam_on      = 1,1;   // bit 14
f_beam_off     = 0,1;

f_pol_pos      = 1,1;   // bit 15
f_pol_neg      = 0,1;

f_fsc_0        = 0,9;
f_fsc_1        = 1,9;

f_fsc_dummy  = 0,9;   // mask for channel 16 - 24 FSC memory select channels

//************************************************************************************************************************************//

// Program //--------------------------------------------------------------------------------------------------------------// Program //
// Simplified script for scanning freq, rf always on, Syd Kreitzman, April 24/2001;   d_count + d_mnsp is the dwell time were counting is enabled

// delay before injection trigger
 d_inj         f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_inj_off + f_trapt_off + f_rf90_off + f_rf0_off;

// trigger the injection
 d_pulsew         f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_inj_on + f_trapt_off + f_rf90_off + f_rf0_off;

// delay before the RF comes on
 d_rfon         f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_inj_off + f_trapt_off + f_rf90_off + f_rf0_off;

// trigger the RF and open gate
 d_msnp         f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_on + f_rfgate_on + f_inj_off + f_trapt_off + f_rf90_off + f_rf0_off;

// keep RF gate open
 d_rfgate         f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_on + f_inj_off + f_trapt_off + f_rf90_off + f_rf0_off;
 

// delay before TOF measurements
 d_tof         f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_inj_off + f_trapt_off + f_rf90_off + f_rf0_off;


// trigger trap 
Loop trap n_trap_less_two;
// First MCS next pulse with Trap trigger pulse
 d_msnp         f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_hi + f_rf_off + f_rfgate_off + f_inj_off + f_trapt_on + f_rf90_off + f_rf0_off;
 d_count        f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_inj_off + f_trapt_off + f_rf90_off + f_rf0_off;

Loop main n_mcs_less_three;
// MCS next pulses without Trap trigger pulse
 d_msnp         f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_hi + f_rf_off + f_rfgate_off + f_inj_off + f_trapt_off + f_rf90_off + f_rf0_off;
 d_count        f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_inj_off + f_trapt_off + f_rf90_off + f_rf0_off;
 
End loop main;
// Last MCS next pulse without Trap trigger pulse (end loop statements cannot be consecutive)
 d_msnp         f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_hi + f_rf_off + f_rfgate_off + f_inj_off + f_trapt_off + f_rf90_off + f_rf0_off;
 d_count        f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_inj_off + f_trapt_off + f_rf90_off + f_rf0_off;
 

End loop trap;

// the last trap trigger... last mcs pulse has DAQ service time set high.

 d_msnp         f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_hi + f_rf_off + f_rfgate_off + f_inj_off + f_trapt_on + f_rf90_off + f_rf0_on;
 d_count        f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_inj_off + f_trapt_off + f_rf90_off + f_rf0_off;


Loop last n_mcs_less_two;

 d_msnp         f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_hi + f_rf_off + f_rfgate_off + f_inj_off + f_trapt_off + f_rf90_off + f_rf0_off;
 d_count        f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_inj_off + f_trapt_off + f_rf90_off + f_rf0_off;

End loop last;

 // dac service pulse with the N + 1st msnp
 
 d_msnp         f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_hi + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_inj_off + f_trapt_off + f_rf90_off + f_rf0_off;
 d_dac_service  f_fsc_0 + f_pol_pos + f_beam_on  + f_dacservp_hi + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_inj_off + f_trapt_off + f_rf90_off + f_rf0_off;

