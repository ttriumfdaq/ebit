/* ev_structure.h
   Structures for collecting ev blocks and voltages
*/

# define MAX_V 20

  typedef struct
  {
    double t0_offset_ms;
    INT awg_unit;
    INT ptrans_index;
    INT code;
  }EVSET;
  
  typedef  struct
  {
    EVSET evset;
    BOOL step; // true if EV_STEP; false if EV_SET
    INT loop_index;
    double t0_start_loop_ms;
    double t0_end_all_loops_ms;
    INT loop_level; // 1 = outermost loop 
  }EV;

  typedef struct
  {
    EV ev[MAX_EV_BLOCKS];
    INT num_blocks; // number of blocks for this unit
  }EV_UNIT;
EV_UNIT ev_unit[2];




typedef struct
{
  INT   code[MAX_V]; // 0=set, >0 end_step (code=number of steps)
  double Vset[MAX_V];
}CHANV;

typedef struct
{
  INT npts;
  BOOL limit;
  CHANV channel[8];
} SETP;

SETP setp[2]; // one for each unit

/* prototypes */
void print_setpoints(void);
void print_stored_setp(FILE *fout);
void store_setp(INT r, INT my_index);
void init_setp(void);
