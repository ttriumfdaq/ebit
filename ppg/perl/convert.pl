#!/usr/bin/perl -w
use warnings;
use strict;
sub cleanup ($);
sub testsub (\%);
sub loops($$ \%); # in make_loops.pl
use lib "/home/ebit/mpet/online/ppg/ppg/perl";
require "make_loops.pl";

#our ( $file, $nplot, $max_time, $lc, $outfile, $rn ) = @ARGV;# input parameters
#our $nparam=6; # need 6 input params
# make sure we can open this output file

my $infile = "/home/ebit/mpet/online/ppg/ppg/ppgload/ppgload.dat";
my $outfile = "/home/ebit/mpet/online/ppg/ppg/ppgload/ppgplot.dat";
my $line;
my $bitpat=0;
my @fields;
my $max_inputs=24;
my $total_time =0.0;
my $clock_cycle = 1/100000; # in ms
my $total_clock =0;
my $counter=0;
my $time=0;
my $nloops=0;
my $pattern;
my ($s, $string, $lc, $instruction, $bl, $clock, $tmp);
my ($who, $gotit);
our (@cleaned);
our @used;
my $len_used;
my $instruction_num;
my @loopbits;
my ($index,$len,$input);

my @names=qw(AWGHV  EGUN     RFTRIG1  GATE1    RFTRIG2 GATE2  RFTRIG3  GATE3 RFTRIG4  GATE4 
             AWGAM  HV       TDCGATE  TDCBLOCK unused  unused unused  unused unused  TOFGATE 
             unused EBITTRIG );
my @loopnames=qw(SCAN RAMP LOOP3 LOOP4 LOOP5); # expect two loops only 

my $maxloops = $#loopnames ; #max number of loops defined -1
my %loopinfo; # store the loop information in a hash of hashes
my $got_it=0;

use lib "/home/ebit/mpet/online/ppg/ppg/perl";
require "make_loops.pl";

# go through file once to find loop information and determine the bits that ARE used


unless (-e $infile)
{
    die " no such file as $infile\n";
}

open (IN,$infile) or die $!;
while (<IN>)
{
    chomp $_;
    if(/\#/)
    {
	print "skipping line: $_\n";
	next;
    }
    if(/Num/)
    {
	print "skipping line: $_\n";
	next;
    }
    unless(/\d/)
    {
	print "blank line\n";
	next;
    }

    print "working on line $_\n";
    @fields=split;
    $instruction_num = $fields[0] + 0;
    $pattern = hex $fields[1];
    $pattern +=0;
#    print "pattern: $pattern\n";    
    $bitpat = $bitpat | $pattern;  # remember which bits are used
    $instruction = hex $fields[4];
    $tmp = $instruction & 0x300000;
    
    if ($tmp ==  0x200000)  # begin loop
    {

       if($nloops > $maxloops) { die "Too many loops defined"; }

       $lc = $instruction & 0xFFFFF;
       # $string = " # begin loop; loopcount = $lc ";
       $loopinfo { $loopnames[$nloops] } = { 
	   start => $instruction_num,
	   count => $lc
	   };
       push @loopbits,0;
       $nloops++;
    }

    elsif ($tmp == 0x300000)  # end loop
    {   # end loop
	$bl = $instruction & 0xFFFFF; # instruction number of begin loop
	$got_it=0;
        for $who (keys %loopinfo) {
            print("Comparing start value ", $loopinfo{$who}->{start}, " with $bl\n");
	    if ( $loopinfo{$who}->{start} +0  ==  $bl+0 )
	    {
		print "found end of loop $who at instruction number $instruction_num \n";
		$loopinfo{$who}->{end} = $instruction_num;
		$gotit=1;
		#print "end: ",$loopinfo{$who}->{end},"\n";
		#print "count: ",$loopinfo{$who}->{count},"\n";
	    }
	}
	unless ($gotit) 
	{ die "found endloop with no beginloop at instruction line $instruction_num"};
    }
}
close IN; 


print "Loops found : $nloops\n";
for $who (keys %loopinfo) {
    print "\n$who\n";
    print "start: ",$loopinfo{$who}->{start},"\n";
    print "end: ",$loopinfo{$who}->{end},"\n";
    print "count: ",$loopinfo{$who}->{count},"\n";
}


# TEMP
print "calling testsub \n";
testsub (%loopinfo);


printf ("bitpat= 0x%x\n", $bitpat);


$s = join(' ',split('',sprintf ("%24.24b",$bitpat)));
print "$s\n";
@used =  split(" ",$s);
$len_used =$#used + 1; # maximum number of inputs defined
print "len_used= $len_used\n";
printf ("%24.24b \n",$bitpat);
print "used bits: $s\n";

# Open output file and write the names of the Inputs and Loops
print "opening $outfile\n";
open (OUT,">$outfile") or ($tmp= $!);
if ($tmp)
{ 
      die "FAILURE cannot open file \"$outfile\"; $tmp"  ;
}

# print first line of file
print OUT "# ";

# Add the names of the loops we have found
for $who (keys %loopinfo) {
    print OUT $who,"(lc",$loopinfo{$who}->{count},") " ;
}
$index=0;  $input=$len_used;
while ( $index < $len_used)
{
    if ($used[$index] == 0) 
    { #print "Input $input is NOT used\n";
    }
    else  
    { 
	#print "Input $input IS used\n";
	print  "$names[$input-1](ch$input) ";
        print OUT "$names[$input-1](ch$input) ";
    }
    $index++; $input--;
}
print OUT "\n";

# go through file again, this time writing the output file
# ------------  reopening file -------------------
open (IN,$infile) or die $!;  # reopen IN
my $endflag;
$counter=0;
while (<IN>)
{

    chomp $_;
    if(/\#/)
    {
	print "skipping line: $_\n";
	next;
    }
    if(/Num/)
    {
	print "skipping line: $_\n";
	next;
    }
    unless(/\d/)
    {
	print "blank line\n";
	next;
    }

    print "working on line $_\n";
    @fields=split;
    
    #   print hex $fields[1];
    $pattern = hex $fields[1];
    printf ("Calling cleanup with pattern  0x%x\n", $pattern);
    @cleaned=qw(); # make sure cleaned is empty
    cleanup($pattern);  # removes bits that aren't used
    printf "After cleanup, clean is @cleaned\n";

    $clock = hex $fields[3] ; # 3 clock cycles per instruction
    $clock += 3;
    $instruction = hex $fields[4];
    $tmp = $instruction & 0x300000;
    $endflag=0;
    if ( $tmp == 0x100000 ) 
    {  
	$string = " Continue";  
    }
    elsif ($tmp == 0x300000) 
    {  # end loop
	$bl = $instruction & 0xFFFFF;
	$string = " end loop; go to line $bl ";
	$endflag=1;

    }
    elsif ($tmp ==  0x200000) 
    {   # begin_loop
	$lc = $instruction & 0xFFFFF;
	$string = " begin loop ; count = $lc ";
        shift  @loopbits; # remove an element from bottom
        push  @loopbits,1; # add one to top
    }

    else 
    { 
	$string = " # halt ";
    }
    

    $pattern+=0;
#print "$s\n";
#    printf ("%d %f   0x%8.8x %f %s  %s\n\n",$total_clock, $total_time, $clock, $time, $s, $string);
#    printf OUT ("%12.5f  %s  %s\n",$total_time, $s, $string);
    print "loopbits : @loopbits\n";
    print "cleaned : @cleaned\n";

    printf  ("%12.5f ",$total_time);
    print  "@loopbits @cleaned # $counter $string";
    printf (" (pattern: 0x%x) \n",$pattern);
    
    printf OUT ("%12.5f  ",$total_time);
    print  OUT "@loopbits @cleaned # ";
    printf  OUT ("%3.3d %s",$counter, $string);
    printf OUT (" (pattern: 0x%x) \n",$pattern);    
    if($endflag)
    {   # end of loop
	unshift @loopbits,0; # add an element to bottom
	pop @loopbits; # remove from top
    }
    $time = $clock * $clock_cycle;
    $total_clock += $clock;
    $total_time +=  $time;
    $counter++; # program line counter
#    if ($counter > 3){ exit;}
}
close IN;
close OUT;
print "output file : $outfile\n";
my $ret=0;
print "calling subroutine loops\n";
$ret = loops($outfile, "RAMP", %loopinfo);
print "ret=$ret\n";




sub cleanup ($)
{
    my $pattern = shift;
    my @fields;

    my ($s,$e);
    my $tmp;

    my $index;
    my $input;
    
    print "used   : @used \n";
    $len = $#used + 1;
    if($len < 1 )
    { 
	die "problem with length of used array  ($len)\n" ;
	return;
    }
    
    $s = join(' ',split('',sprintf ("%24.24b",$pattern)));
    print "pattern: $s\n";
    @fields =  split(" ",$s);
    my $len1 =$#fields + 1; # maximum number of inputs defined
    #print "len1= $len1\n";
    if($len1 != $len){ die "arrays  fields and used should have the same length (24.24b)"; } 
    # test @fields= qw (1 2 9 3 9 5 6 7 8 4 9 9 9 1 9 5 9 9 9 9 9 1 9 3);
    print "fields : @fields\n";
    $index=0;$input=24;
    while ( $index < $len)
    {
	if (($used[$index] ) == 0) 
	{ 
	    # print "Input $input is NOT used\n";
	    $e = shift @fields; # remove the element
          #  if($e > 0) {die "popping used element  at index $index when bitpat says unused";}
	  #  printf("removing element $e at index $index from fields\n");	    
	}
	else  
	{ 
	   # print "Input $input IS used\n";	
            $e = shift @fields;
	   # print "moving element $e at index $index to cleaned\n";
	    push @cleaned, $e;
	}
	$index++; $input--;
    }
    print "cleaned: @cleaned\n";
    return;
}


sub testsub (\%)
{
    my $href = shift;
    if (ref($href) ne 'HASH') { die "Expected a hash reference, not $href\n"; }
   
    my %h = %$href; 
    my $w;
    my $nloops=0;
    

    for $w (keys %h) {
	print "\n testsub: $w \n";
	print "start: ",$h{$w}->{start},"\n";
	print "end: ",$h{$w}->{end},"\n";
	print "count: ",$h{$w}->{count},"\n";
        $nloops++;
    }
    print "testsub:  Loops found : $nloops \n";
}



