#!/usr/bin/perl -w
#
# Run this file e.g.
# /home/ebit/online/ppg/perl/gnuplot.pl /home/ebit/online/ppg/perl /home/ebit/online/ppg/ppgload/ebit.dat 5 1490.010000 0 /data/ebit/info/runXX_ebit.png 500 loopname
#
######################################################################
#                                                                        
# gnuplot.pl 
#
#  Input Parameters:
#     Either
#         ONE (inc_dir) i.e. no plot parameters. Parameters will be read from file plotparams.dat (produced by loops.pl which is called by do_plots.pl)
#     Or parameters are:  
#         inc_dir  i.e. .../ppg/perl   
#         filename
#         number of inputs to plot
#         min time range (x axis) usually 0 except for code=3
#         max time range (x axis)
#         name of output file (run$rn_ebit.png)
#         run number
#         mode (may be "none")
#         total number of loops defined (inluding expanded loop)
#         OPTIONAL one or more pairs of
#                       loop count for expanded loop 
#                       expanded loop name (optional) with count 
#
#
#  e.g. gnuplot.pl      - parameters will be read from file  plotparams.dat
#       gnuplot.pl /home/ebit/mpet/online/ppg/ppg/ppgplot/ppgplottimes.dat 8 0 0.0047 rn cpet 1a 2 LOOP1 2 LOOP2 5
#  do_plots.pl produces plot automatically from ppgload.dat
#
#examples
#  do_plots.pl    do not expand a loop
#  do_plots.pl    RAMP 5  expand RAMP with loopcount of 5  ( note max loopcount for expansion is 250)
#  do_plots.pl    RAMP  SCAN 2  expand RAMP with loopcount in file and SCAN loop with loopcount of 2  ( note max Combined loopcount for expansion is 250)
#
#                   PARAMETERS                                            |   optional - supply if loop is expanded
#              inc_dir                          input file          #inputs xmin xmax    rn   mode nloops |   Loopname1  Loopcount1  Loopname2 Loopcount2
# ./gnuplot.pl /home/mpet/online/ppg/perl  ../ppgload/ppgplotloop.dat   14   0   1101.2  500   1a    2    |      RAMP     20            SCAN      2
#
#  if no parameters supplied, get parameters from file plotparams.dat (fixed name for this file written by loops.pl)
use warnings;
use strict;

# The output files are ../ppgload/gnuplot.png  (use display gnuplot.png to view)
#                  and ../ppgload/gnuplot.txt  
#                              (use gnuplot>load "gnuplot.txt" to display, then zoom with right mouse button (RH mouse), u to unzoom )
my $num_params =$#ARGV +1; # number of input parameters
my $inc_dir =shift @ARGV;
print "inc_dir=$inc_dir\n";

my $debug=0;

our $path;
our $expt;
unless ($inc_dir) { die "ppg_plot: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/params.pm"; # params.pm   $path, $expt from environment variable



my ( $file, $nplot, $min_time, $max_time, $rn, $mode, $nloops );
my (@loopnames,@loopcounts);
my $count;
my $nparam=7; # Excluding inc_path, need 7 input params if NOT using an input file for params : path filename num_inputs xmin xmax rn nloops  (others are optional)
              #                     if using the file plotparams.dat for the input params, file must contain 8 input params                     
my $ppgpath = $path;
my $plotpath = $ppgpath."ppgplot/";
my $gnufile = "gnuplot";
my $paramfile = $plotpath . "plotparams.dat"; 
my ( $line,  $index,$i,$j,$k,$l,$len,$nl);
my (@names,@array);
my $name="gnuplot.pl";
my $title;
my ($my_term,$my_term_x11);
my $factor;
my $code;
my (@fields);
##################### G L O B A L S ####################################
#our $ODB_SUCCESS=0;   # status = 0 is success for odb
#our $EXPERIMENT="ebit";
#our $ANSWER=" ";      # reply from odb_cmd
#our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
# for odb  msg cmd:
#our $MERROR=1; # error
#our $MINFO=2;  # info
#our $MTALK=32; # talk

#######################################################################
#use lib "$path/perl";
$|=1; # flush output buffers

print ("path is $path\n"); 

$count =$#ARGV +1;
print "count: $count  num input parameters : $num_params\n";
print "\n\ngnuplot.pl: starting with num arguments = $num_params\n";
print "gnuplot.pl: ppgpath is $ppgpath\n";
#$loopnames[0]="";
#$loopcounts[0]=0;
$code=0;

if ($count > 0)
{ # use parameters from input line
    print "using parameters from input line ...\n";
( $file, $nplot, $min_time, $max_time, $rn, $mode, $nloops, @array ) = @ARGV;# input parameters
}
else
{
    print "using parameters from file $paramfile ...\n";
    open (IN,$paramfile) or die $!;
    while (<IN>)
    {
	chomp $_;
	if(/\#/)
	{
	    print "skipping line: $_\n";
	    next;
	}
	@fields = split;
	$count = @fields;
	( $file, $nplot, $min_time, $max_time, $rn, $mode, $nloops, @array ) = @fields;
	print "array: @array\n";
    }
    close IN;
}

$i=0;$j=0;

$len = $#array + 1;
print "len $len\n";
print "loop parameters array is: @array\n";
while ($j < $len)
{
    $loopnames[$i] = $array[$j];
    $j++;
    $loopcounts[$i] = $array[$j];

    $j++;
    $i++;
}
$nl=$i; # number of loops to be expanded
	
print "loopnames: \"@loopnames\"\n";
print "loopcounts: \"@loopcounts\"\n";

if($count < $nparam)
{
#    send_message($name,$MINFO,"FAILURE: not enough parameters supplied ($count); expect $nparam " ) ;
    print "FAILURE: not enough parameters supplied ($nparam); expect $count \n";
    print "Parameters: filename; number of plots; min_time; max_time;   run number; mode\n ";
    die "              number of loops defined ;  and optional expanded loop loopname(s) and count(s)\n";
}

print   "Starting with parameters:  \n";
print    "  filename = $file; min_time = $min_time;  max_time = $max_time; number of plots=$nplot \n";
print    "  run number = $rn; mode=$mode; nloops=$nloops \n";
print    "count= $count, nparam = $nparam \n";
$code=0;

my $Exp=uc($expt); # upper case

if ($nl == 0) 
{ 
    print "No loops are expanded\n"; 
    $title = "\"$Exp $mode run $rn : one PPG cycle (loop count=1 for all loops);\"";
} 
else
{ 
    $code =1;
    print "number of loops to be expanded: $nl\n";
    $i = join "\\\",\\\"", @loopnames;
    $j = join ",", @loopcounts;
    $k = " respectively"; 
    $l = "(s)";

    if ($nl ==1){$k=$l="";}
    $title =  "\"$Exp $mode run $rn : one PPG cycle (loop$l \\\"$i\\\" expanded to $j loops$k)\"";

 }
print "nl=$nl\n";
print "title: $title\n";




# Output files
my $outfile =  $plotpath.$gnufile.".png";
my $plotfile = $plotpath.$gnufile.".txt"; 
my $plotfile2 = $plotpath.$gnufile.".tmp"; 

my ($xmax,$xmin,$xoffset,$yoffset);
# calculate x axis offsets of 5% to get plots away from y axes
my $time_range =  ($max_time - $min_time);
$xoffset = 0.015 * $time_range;


  # xmin, xmax for offsetting the axes
 #   $xmin =( 0.05 * $time_range) + $min_time;   
 #   $xmin *=-1;
 #   $xmax = $time_range +( 0.05 * $time_range) ;
  # these offsets are for the plot labels e.g. TDCBLOCK(ch14)
 #   $xoffset = $xmin/2;
  #$xoffset = -0.03 * $time_range;
   
   # $yoffset = 0.4;
$my_term = "png large   size 1000,800   xffffff x000000 x404040 xff0000 xffa500 x66cdaa xcdb5cd xadd8e6 x0000ff xdda0dd x9500d3 x008000 x800000 x000080 x00ff00 x800080 x008080 xff00ff x808080";

$yoffset = 0.4;

unless (-e $file)
{
#  send_message($name,$MINFO,"FAILURE file \"$file\" does not exist" ) ;
  print "FAILURE input file \"$file\" does not exist\n"  ;
  die "gnuplot.pl: no such file as $file\n";
}
print "Opening input file $file\n";
open (RFC,$file) or die $!;
$line=<RFC>;
if($debug) { print $line; }
$line =~tr/ / /s;
chomp $line;
@names = split (/ /,$line);
shift @names; # remove #

# make sure the file contains at least one PPG input
$i=@names;
unless ($i)
{
#    send_message($name,$MINFO,"FAILURE file \"$file\" first line does not contain any PPG signal names" ) ;
    print "FAILURE file \"$file\" first line does not contain any PPG signal names\n"  ;
    die "gnuplot.pl: file \"$file\" first line does not contain any PPG signal names  \n";
}
print "\nLoop(s) and input names from file $file:\n";
foreach $index (@names)
{ 
    print "$index\n";  
}
my $ytics="set ytics (";
$j=2;$k=0;$i=0;
foreach $index (@names)
{  
    unless($k)	
    { # first valid time 
	$array[$i]="\  '$file\' using 1:(\$$j+$k) with steps notitle "; # notitle-> no key
        $ytics = $ytics . "\"$index\" $k";

    }
    else
    {
	$array[$i]=", \'$file\' using 1:(\$$j+$k) with steps notitle"; # notitle-> no key
        $ytics = $ytics . ", \"$index\" $k ";
    }

    $j++;$i++;$k+=2;
  
}
$ytics = $ytics.")";
$nplot*=2; # double because plots are separated


print "title: $title\n";
print("plot: \n");
$i=0;
foreach $index (@array) {
    print "$index\n"; 
}


# Send these commands to gnuplot to make the output file gnuplot.png
print("sending commands to gnuplot...\n");

#open (OUT,"|gnuplot") or die $!;
#print OUT<<eot;

# Open output file 
my $tmp;
print "opening $plotfile2\n";
open (OUT,">$plotfile2") or ($tmp= $!);
if ($tmp)
{ 
    die "FAILURE cannot open file \"$plotfile2\"; $tmp"  ;
}
print OUT<<eot;
set xlabel "time (ms)"
set ylabel "Loops /  PPG Outputs"
set title $title  font "large"
set time
$ytics
set yrange [-1:$nplot + 1]
set xrange [$min_time:$max_time]
set offset $xoffset,$xoffset,$yoffset,$yoffset
set term $my_term 
set out "$outfile"
plot  @array 

eot
close OUT;
system "gnuplot $plotfile2";
# note that the lines "term png ... " and "set out $outfile" cause gnuplot to produce the output png file $outfile
unless (-e $outfile)
{
   die "gnuplot.pl:  problem producing output png file $outfile" ;
}

print "gnuplot.pl:  file $outfile has been made to be used with command \"display\" \n";


# Now produce gnuplot.txt to be used interactively with gnuplot
# using the same commands as above *** until the line with "term" is encountered ***
# (default term is Xterm )
# then add other commands for crosshairs etc.
print "\nopening output file $plotfile to be used interactively with gnuplot\n";
# Open output file 
open (OUT,">$plotfile") or ($tmp= $!);
if ($tmp)
{ 
    die "FAILURE cannot open file \"$plotfile\"; $tmp"  ;
}
print "\nRe-opening temporary file $plotfile2 as input\n";
open (IN,$plotfile2) or die $!;
while (<IN>)
{
        if (/term/i)
           { last; } # look for line containing term png ...
        print OUT $_; # copy line to output file
}
close IN; 


# now add extra lines for using gnuplot interactively. 
# gnuplot will use default term (Xterm)
print OUT "bind c \"set offset 0,0,0.4,0.4 ; replot\"\n";  # define "c" cmd to remove offsets
print OUT "plot  @array \n"; 
print OUT "pause -1 \"Press Return to finish\"\n";
close OUT;
print "closed $plotfile\n";

# convert png to gif for gnuplot web page
# print "plotpath $plotpath\n";
system "convert $plotpath/gnuplot.png $plotpath/gnuplot.gif";
print "Output files are $outfile, $plotfile2, $plotfile, gnuplot.png, gnuplot.gif\n";
print "gnuplot.pl ending\n\n";


