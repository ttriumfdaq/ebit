#!/usr/bin/perl -w

my $name="gnuplot_ins.txt";

my $expt =  $ENV{MIDAS_EXPT_NAME};
print "Looking for \"$name\" to kill plot window for $expt\n";
my $username;
my $process_id;
my $killed;
open PS, "ps -efw |";
while (<PS>)
{
    print "$_";
    if(/([a-z]+)[\s]+([\d]+) [\s].*$name/) # look for character string (i.e. username) followed by
#                                              spaces, then a space +1 or more digits
#                                              first set of brackets  ->backreference $1 (username)
#                                              second set of brackets ->backreference $2 (pid)
    {
        $username = $1;
        $process_id = $2;
#       print "$_";
#        print "found $name $port_msg running  with PID $process_id and username $username\n" ;
        print "found $name  running  with PID $process_id and username $username\n" ;
        if( $username eq $expt)
        {
            print "killing $process_id\n";
            open KI," kill $process_id |";
            while (<KI>)
            {
                print "$_";
            }
            close (KI);
            $killed=1;
        }
        else
        {
            print "Warning: username $username not same as $expt\n";
            print "\n *** Another user ($username) has put up a plot file ... \n";
            print " *** that user must kill process $process_id  \n\n"; # but we can still start out plot
        }
    }
}
close(PS) ;
unless ($killed)
{
    print "\"$name\" not found; plot program not running for $expt\n";
}
exit;
