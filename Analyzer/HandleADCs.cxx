//
// ROOT analyzer
//
// ADCs V792 handling
//
// $Id$
//

/// \mainpage
///
/// \section intro_sec Introduction
///
///
/// \section features_sec Features
///  
///   state = gOdb->odbReadInt("/runinfo/State");


#include <stdio.h>
#include <sys/time.h>
#include <iostream>
#include <assert.h>
#include <signal.h>

#include "ADCs.h"

//extern TFolder* gManaHistosFolder;
//extern VirtualOdb* gOdb;

static TH1D* samplePlot[32];
static int BOR_done=0;
//------------------------------------------------------------
void HandleADCs(TMidasEvent& event, void* ptr, int nitems)
{

  // ----- ADC0 bank ----
  uint32_t *samples = (uint32_t*) ptr;
  int data, channel;
  int slotn = 0, debug0=1;

  printf("Number of Items:%d SerNr:%d \n", nitems, event.GetSerialNumber());

  if (!BOR_done) {
    HandleBOR_ADCs(0,0);
    BOR_done = 1;
  }

  if (nitems > 2) {
    for (int i=0; i<nitems; i++) {
      /* Unpack data and increment raw histogram */
      switch (samples[i] & 0xFF000000) {
      case 0xFA000000:   // Event Header
	//        nentry = ((samples[i] >> 8 ) & 0x3F);
        break;
      case 0xF8000000:   // Event Data
        /* Need to code the slot number in the channel# */
        channel = (slotn * 32) + ((samples[i] >> 16) & 0x3F);
        data = (samples[i] & 0xFFF);
        if (channel < 32) {
	  if (data > 0) 
	    samplePlot[channel]->Fill((float)data, 1.);
          if (debug0)
            printf("Incr -> data A0   :%x ch:%d, data:%d\n"
                   , samples[i], channel, data);
        }
	break;
      case 0xFC000000:   // Event Trailer
        slotn++;
        break;
      case 0xFE000000:   // No valid data
        printf("unvalid data !\n");
        break;
      } // switch
    } // for loop
  } // nitems
}

//------------------------------------------------------------
void HandleBOR_ADCs(int run, int time)
{
  printf(" in ADCs BOR... Booking\n");

  if (gOutputFile)
    {
      gOutputFile->cd();
      TDirectory* subdir = gDirectory->mkdir("subdir");
      subdir->cd();
    }

  char name[32];
  // Booking Histos
  samplePlot[0] = (TH1D*) gDirectory->Get("channel0");
  if (samplePlot[0] == 0) {
    for (int i=0;i<32;i++) {
      sprintf(name, "channel%d", i);
      samplePlot[i] = new TH1D(name, name, 4096, 0, 4095);
    }
  }
  BOR_done = 1;
}

//------------------------------------------------------------
void HandleEOR_ADCs(int run, int time)
{
  printf(" in ADCs EOR\n");
}
