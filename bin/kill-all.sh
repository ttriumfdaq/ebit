#!/bin/sh

# Host based
if [ $HOST == "lxebit.triumf.ca" ] ; then
 echo "run kill-all from titan04 only "
else
  killall mserver
  killall mlogger
  killall mhttpd
  killall feebit_ppg
  killall analyzer
  sleep 1
fi
#end file
