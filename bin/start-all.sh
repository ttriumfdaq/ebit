#!/bin/sh
# Host based
# make sure this is being run from $DAQ_HOST (defined in .cshrc)
# 
if [ -z "$DAQ_HOST" ]; then
    echo "Environment variable DAQ_HOST must be set in .cshrc for data acquisition"
    exit 1
fi
echo  $HOST > ~/temp
grep --silent $DAQ_HOST ~/temp
if [ "$?" != "0" ] ; then 
   echo "You must be logged onto DAQ_HOST ($DAQ_HOST) to run this file (NOT $HOST)"
   exit 2;
fi
rm ~/temp

if [ -z "$MIDASSYS" ]; then
    echo "Environment variable MIDASSYS must be set in .cshrc for data acquistion"
    exit 1
fi


odbedit -c clean

ps -ef > ~/temp
grep --silent "mserver -p 7075" ~/temp
if [ "$?" != "0" ];  then
    echo "Starting mserver on port 7075 (ebit) "
    $MIDASSYS/linux/bin/mserver  -p 7075 -D
else
 echo "mserver for ebit is already running"
fi
rm ~/temp


# Start the http midas server
ps -ef > ~/temp
grep --silent "mhttpd -p 8089" ~/temp
if [ "$?" != "0" ] ; then
    echo "Starting mhttpd"
     $MIDASSYS/linux/bin/mhttpd -p 8089 -D
  else
 echo mhttpd is already running
fi
rm ~/temp

sleep 2
#xterm -e ./frontend &
#xterm -e ./analyzer &

# The  midas logger
# Start the logger
$MIDASSYS/linux/bin/odbedit -c scl | grep --silent  Logger
if [ "$?" != "0" ] ; then
    echo "Starting mlogger"
    $MIDASSYS/linux/bin/mlogger  -D
  else
 echo mlogger is already running
fi


# start the feebit program via remote login on lxebit
ssh lxebit ~/online/bin/start_feebit


echo Please point your web browser to http://localhost:8089
#echo Or run: mozilla http://localhost:8089 &
#echo To look at live histograms, run: roody -Hlocalhost

#end file
