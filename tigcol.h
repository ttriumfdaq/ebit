/*********************************************************************

  Name:         TIGCOL.h
  Created by:   Pierre-Andre Amaudruz / Jean-Pierre Martin

  Contents:     For Tigress Collector Flash ADC / 105msps from J.-P. Martin
                
  $Log: TIGCOL.h,v $
*********************************************************************/
#
#include <stdio.h>
#include <string.h>
 
#ifndef  __TIGCOL_INCLUDE_H__
#define  __TIGCOL_INCLUDE_H__
 
#define  TIGCOL_SUCCESS              (int)   (1)
#define  TIGCOL_ERR_HW               (int)   (603)

#define  TIGCOL_NFRAME_MASK          0xFFFF

/* Registers Offsets */
#define  TIGCOL_CSR_REG_RW           (WORD) (0)
/* CSR Register bit  assignment
   0 : R / Run active
   1 : W / Trigger
   ? : R / FE Full
   ? : R / Collector Full 
*/
#define  TIGCOL_CSR                        (WORD) (0x0008>>1)
#define  TIGCOL_SSET_CSR                   (WORD) (0x0010>>1)
#define  TIGCOL_SCLEAR_CSR                 (WORD) (0x0018>>1)
#define  TIGCOL_TEST_REG_RW                (WORD) (0x0028>>1)
#define  TIGCOL_FIRMWARE_R                 (WORD) (0x0030>>1) // !! mv to param
#define  TIGCOL_GENERAL_RESET              (WORD) (0x0080>>1) // !! to be implemented
#define  TIGCOL_NFRAME_R                   (WORD) (0x01e0>>1)
#define  TIGCOL_PARAM_DATA_RW              (WORD) (0x0050>>1)
#define  TIGCOL_PARAM_ID_R                 (WORD) (0x0060>>1)
//#define  TIGCOL_TRIGGER_WIDTH          (WORD) (0x0070>>1)
#define  TIGCOL_TRIGGER_DISABLE             (WORD) (0x0120>>1)
#define  TIGCOL_TRIGGER_GLOBAL_MULTIPLICITY (WORD) (0x0130>>1)
#define  TIGCOL_TRIGGER_CLOVER_MULTIPLICITY (WORD) (0x0138>>1)
#define  TIGCOL_TRIGGER_ENABLE_CURRENT     (WORD) (0x0140>>1)
#define  TIGCOL_TRIGGER_ENABLE_ANY         (WORD) (0x0148>>1)
#define  TIGCOL_TRIGGER_ENABLE_CLOVER      (WORD) (0x0150>>1)
#define  TIGCOL_TRIGGER_REQUIRE            (WORD) (0x0128>>1)
#define  TIGCOL_TRIGGER_GENERATE_LUT       (WORD) (0x0158>>1)
/* DT is for 3 registers: duration, delay, deadtime */
#define  TIGCOL_DT_PORT_SELECT             (WORD) (0x0100>>1)
#define  TIGCOL_DT_DEADTIME                (WORD) (0x0118>>1)
#define  TIGCOL_TRIGGER_DURATION           (WORD) (0x0108>>1)
#define  TIGCOL_TRIGGER_DELAY              (WORD) (0x0110>>1)

#define  TIGCOL_DATA_FIFO_R                (WORD) (0x0200>>1)

/* Parameters ID 
          1: PED                             X0000
          2: HitDetThresh                    X0020  
          3: ClipDelay                       X0028          -> CfdDelay
          4: PRETRIG                         X0014
          5: SEGMENT_SIZE                      100
          6: K (Integration time)            X0190 (400)
          7: L (Window size)                 X0200 (512)
          8: M (PZ constant)                 X1000 (4k)
          9: Featuredelay_A                  X0001 Obsolete -> Simln
         10: Modebits                        X0000
         11: FeatureDelay_B                  X0005 Obsolete -> ClipDelay
         12: LATENCY                         X00f0 750ns
         13: FIRMWAREID                      X0300
         14: ATTENUATOR                      X007b (64)
         15: TriggerThreshold                X0082 
*/
/* Parameters 
parmID: 3.. 0 : Param# in Tig10 channel
        5.. 4 : Size of param (0:16, 1:32, 2:48, 3:64bits)
            6 : Param ID Extension bit (0:16 ID, 1:32 ID)
            7 : Destination Read bit (0: Write, 1: Read)
W   :  11.. 8 : Channel# in TIG10
Y-Z :  15..12 : Port# on TIGCOL slave (0:top black[1], 11:bottom)
   
  Optional if bit 6 ON second word below is required
X   :    3.. 0 : Port# on TIGCOL master
        15.. 4 : N/A    
   
  Parameter Data: Least significant 16-bit word first
*/
#define  TIGCOL_PEDESTAL             (WORD) (1) 
#define  TIGCOL_HIT_THRESHOLD        (WORD) (2) // 
#define  TIGCOL_CFD_DELAY            (WORD) (3) // Global 40
#define  TIGCOL_PRE_TRIGGER          (WORD) (4) // Global   20
#define  TIGCOL_SAMPLE_WINDOW_SIZE   (WORD) (5) // Global  100
#define  TIGCOL_PARAM_K              (WORD) (6) // Global  400
#define  TIGCOL_PARAM_L              (WORD) (7) // Global  600
#define  TIGCOL_PARAM_M              (WORD) (8) // Delta  4096 
#define  TIGCOL_MOD_BITS             (WORD) (10)// Global    0 
/*       MOD_BITS:  (X2010)
  default: 0       0 : Enable simulation       
           0       1 : Suppress Raw Data
           0       2 : select deconvoluted raw data
           0       3 : Positive input
           ---------
          01     4-5 : Baseline Restore speed (1,8,64,256)
           0       6 : Freeze blr
           0       7 : Disable Trigger Request (was select clipped mode)
           ---------
           0       8 : Disable ADC
          00    9-10 : DC Offset (00:-veInput,11:+ve,01:central(bipolar)
           0      11 : Low Gain
           ---------
        0010   12-15 : Board Revision

-ve:2610 +ve:2018 +veLowGain:2818
(ELOG:2aug06:923am gives correct mode settings)
*/
#define  TIGCOL_HITDET_CLIP_DELAY    (WORD) (11)// Global   30
#define  TIGCOL_LATENCY              (WORD) (12) // 0
//#define  TIGCOL_TEST_REG             (WORD) (13) // 76 FirmwareID
#define  TIGCOL_ATTEN_FAC            (WORD) (14) // 400
#define  TIGCOL_TRIG_THRESHOLD       (WORD) (15)   

/* CSR bit asignment */
#define  TIGCOL_CSR_RUNNING          (WORD) (0x00000001)
#define  TIGCOL_CSR_PARAM_READY      (WORD) (0x00000004)
#define  TIGCOL_CSR_EVT_EMPTY        (WORD) (0x00000000) // !!
#define  TIGCOL_CSR_FE_FULL          (WORD) (0x00000000) // !!
#define  TIGCOL_CSR_SELECT_64BIT     (WORD) (0x00000008) // !!

#endif

