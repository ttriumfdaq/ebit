/* standard includes */
#include <stdio.h>
#include <time.h>
#include <unistd.h> /* usleep */
#include <stdlib.h>

/* midas includes */
#include "midas.h"
#include "experim.h"
#include "analyzer.h"

//#include "TIG10.h"
#define  TIG10_HEADER               (DWORD) (0x80000000)
#define  TIG10_TIME_STAMP           (DWORD) (0xA0000000)
#define  TIG10_CHANNEL              (DWORD) (0xC0000000)
#define  TIG10_FEATURE_TIME         (DWORD) (0x40000000)
#define  TIG10_FEATURE_CHARGE       (DWORD) (0x50000000)
#define  TIG10_FEATURE_MASK         (DWORD) (0x70000000)
#define  TIG10_TRAILER              (DWORD) (0xE0000000)

#define  TIGCOL_DATA                 (DWORD) (0x00000000)
#define  TIGCOL_OUT_OF_SYNC          (DWORD) (0x88000000)
#define  TIGCOL_TIMEOUT              (DWORD) (0x10000000)


/* root includes */
#include <TH1F.h>
#include <TH2F.h>
#include <TTree.h>
#include <TDirectory.h>

#define DATA_MASK       0x00003FFF
#define DATA_SHIFT      14
#define FEATURE_MASK    0x0FFFFFFF
#define TIME_SHIFT      12

//#define CHARGE_SCALE   1
const float CHARGE_SCALE=0.5;
#define NUM_GEFV      24
#define NUM_GEDET      8
#define MAX_SAMPLES 4096
#define ADC_CHAN   65536
#define TDC_CHAN    8192
#define NUM_CHAN      10
#define NUM_GECHAN   480
#define NUM_TIMES     80 /* 12*4:Ge/BGO-Si 8*2:Ge-BGO 4*4:Ge1-Ge2 */
#define CHAN_PER_TIG  10


CATHODE_PARAM cathode_param;
extern EXP_PARAM exp_param;
extern RUNINFO runinfo;

/*-- Module declaration --------------------------------------------*/

int cathode(EVENT_HEADER *, void *);
int cathode_init(void);
int cathode_bor(INT run_number);
int cathode_eor(INT run_number);

CATHODE_PARAM_STR(cathode_param_str);

ANA_MODULE cathode_module = {
   "Cathode",              /* module name           */
   "PAA",                  /* author                */
   cathode,                /* event routine         */
   cathode_bor,            /* BOR routine           */
   cathode_eor,            /* EOR routine           */
   cathode_init,           /* init routine          */
   NULL,                   /* exit routine          */
   &cathode_param,         /* parameter structure   */
   sizeof(cathode_param),  /* structure size        */
   cathode_param_str,      /* initial parameters    */
};

/*-- module-local variables ----------------------------------------*/

static TH1F *hCharge[NUM_CHAN], *hTime[NUM_CHAN];
static TH1F *hRaw[NUM_CHAN];

#define MISC_CHAN 128
#define NUM_MISC 25
static TH1F *h_misc[NUM_MISC];
static char misc_handle[NUM_MISC][16]={
   "Trig", "DTrig", "PORTHIT", "PUPHIT", "EHIT", "CFDHIT", "THIT", "UPTHIT", "LTHIT",
   "TRQHIT", "TACHIT", "GDEHIT", "GDTHIT", "EFOLD", "CFOLD", "LTIME", "DTIME", "LT_AGE", "T_REQ", "T_ACC", "T_FRAC", "T_RATER", "T_RATEA", "RF_AMP", "DeltaT"};
static char misc_title [NUM_MISC][64]={
  "Trigger", "Delta Trigger", "Port pattern","Pileup pattern","E pattern","CFD pattern",  /* 6 */
   "TimeStamp Hit","TimeStamp upper Hit", "Livetime Hit", "Trig Req Hit", "Trig Acc Hit", /* 5 */
   "Good Charge Pattern",  "Good Cfd Pattern", "E Fold", "CFD Fold",                      /* 4 */
    "Live Percent", "Dead Percent", "Livetime Age", "Triggers Requested", "Triggers Accepted", /* 5 */
    "Triggers Accept Percent", "Trigger Rate Req", "Trigger Rate Acc", "RF Amplitude", "Time Difference" };               
static int    misc_chan[NUM_MISC]    ={
   65536, 8192, MISC_CHAN, MISC_CHAN, MISC_CHAN, MISC_CHAN,
   MISC_CHAN, MISC_CHAN, MISC_CHAN, MISC_CHAN, MISC_CHAN,
   MISC_CHAN, MISC_CHAN, MISC_CHAN, MISC_CHAN, MISC_CHAN,
   MISC_CHAN, MISC_CHAN, MISC_CHAN, MISC_CHAN, MISC_CHAN, MISC_CHAN, MISC_CHAN, 16384, 8192 };

int cathode_eor(INT run_number){ return SUCCESS; }
static float charge_dispersion;

int hist_init_roody()
{
   char name[256], title[256];
   int i;
   open_subfolder("Raw");                                        // Waveforms 
   for(i=0; i<NUM_CHAN; i++){    
     sprintf(name,  "WFD%d_%d", i/CHAN_PER_TIG, i%CHAN_PER_TIG);  
     sprintf(title, "Wave form card%d ch%d", i/CHAN_PER_TIG, i%CHAN_PER_TIG);  
     hRaw[i] = H1_BOOK(name, title, MAX_SAMPLES, 0, MAX_SAMPLES);
   }
   close_subfolder();
   open_subfolder("Charge");                                      // Charge  
   for(i=0; i<NUM_CHAN; i++){
     sprintf(name,  "ADC%d_%d", i/CHAN_PER_TIG, i%CHAN_PER_TIG);  
     sprintf(title, "Charge card%d ch%d", i/CHAN_PER_TIG, i%CHAN_PER_TIG);
     hCharge[i] = H1_BOOK(name, title, ADC_CHAN, 0, ADC_CHAN);
   }
   close_subfolder();
   open_subfolder("Misc");             // Misc
   for(i=0; i<NUM_MISC; i++){    
     h_misc[i] = H1_BOOK(misc_handle[i], misc_title[i], misc_chan[i], 0, misc_chan[i]);
   }
   close_subfolder();
   return(0);
}

static void odb_callback(INT hDB, INT hseq, void *info);
int cathode_init(void)
{
   char odb_str[128], errmsg[128];
   HNDLE hDB, hKey;

   sprintf(odb_str,"/Analyzer/Parameters/Cathode");
   cm_get_experiment_database(&hDB, NULL);
   db_create_record( hDB, 0, odb_str, strcomb(cathode_param_str) );
   db_find_key( hDB, 0, odb_str, &hKey );
   if( db_open_record(hDB, hKey, &cathode_param, sizeof(cathode_param), MODE_READ, odb_callback, NULL) != DB_SUCCESS ){
       sprintf(errmsg, "Cannot open %s tree in ODB", odb_str);
       cm_msg(MERROR,"cathode_init", errmsg);
   }
   odb_callback(hDB, hKey, NULL); /* initialise .. */
   charge_dispersion = cathode_param.charge_dispersion; /* can only set this before spectra created */
   
   #ifdef USE_ROODY
      hist_init_roody();
   #else
      //hist_init();
   #endif

   return SUCCESS;
}
int cathode_raw(int nitems, DWORD *pdata)
{
   int i, channel=-1, sample=0;
   float feature;
   short  data;
  
   for (i = 0; i < nitems; i++) {
      if (pdata[i] & 0x80000000) {
         switch (pdata[i] & 0xE0000000) {
         case TIG10_HEADER:      
	   sample = 0;
	   break;
         case TIG10_TRAILER:
	   break;
         case TIG10_TIME_STAMP:
	   break;
         case TIG10_CHANNEL:
	    channel = (pdata[i] & 0xFF) - 0x80;
            channel -= 6*(int)(channel/16);
	    if( channel < 0 || channel >= NUM_CHAN ){
	       printf("channel %d out of range, set to %d\n", channel, NUM_CHAN-1);
	       channel = NUM_CHAN-1;
	    }
	    break;
         default: printf("unrecognised Header: %lu\n", pdata[i] );
         }
      } else if ((pdata[i] & TIG10_FEATURE_MASK) == TIG10_FEATURE_TIME) {  // time
      feature = (float) (pdata[i] & FEATURE_MASK);
      if( channel >= 0 ){ hTime[channel]->Fill(feature, 1); } else { fprintf(stderr,"feature without channel ...\n"); }
    } else if ((pdata[i] & TIG10_FEATURE_MASK) == TIG10_FEATURE_CHARGE) {  // Charge
      feature = (float) (pdata[i] & FEATURE_MASK);
      feature /= CHARGE_SCALE;
      if( channel >= 0 ){ hCharge[channel]->Fill((feature), 1);} else { fprintf(stderr,"feature without channel ...\n"); }
    } else {
      if( channel >= 0 ){
         data = pdata[i] & DATA_MASK;
         if( data & (1<<13) ){ data |= 0x3 << 14; }
         hRaw[channel]->SetBinContent(sample++, data + 8192);
         data = (pdata[i] >> DATA_SHIFT) & DATA_MASK;
         if( data & (1<<13) ){ data |= 0x3 << 14; }
         hRaw[channel]->SetBinContent(sample++, data + 8192);
      }
    }
  }
  return SUCCESS;
}

float spread(int val){ return( val + rand()/(1.0*RAND_MAX) ); }
/* --*********************************-- */
/* --** v copied from ev_check.c******-- */
/* --*********************************-- */
//#define MAX_SAMPLES     512
#define NUM_CHAN         10
#define NUM_EVBUF       128
#define MAX_PORT_OFFSET   7
#define MAX_COLLECTORS    6
#define HITPAT_SIZE ( (int)((NUM_CHAN+31)/32) )
 /* need to zero the trigger number, 8 hitpatterns and wavefm_lengths */
#define CLEAR_SIZE ( 1 + 9*HITPAT_SIZE + (NUM_CHAN+1)/2 )
typedef struct tig10_event_struct {
   int          trigger_num;               int         port_hitpattern[HITPAT_SIZE];
   int    charge_hitpattern[HITPAT_SIZE];  int          cfd_hitpattern[HITPAT_SIZE];
   int timestamp_hitpattern[HITPAT_SIZE];  int timestamp_up_hitpattern[HITPAT_SIZE];
   int  trig_req_hitpattern[HITPAT_SIZE];  int     trig_acc_hitpattern[HITPAT_SIZE];
   int    pileup_hitpattern[HITPAT_SIZE];  int     livetime_hitpattern[HITPAT_SIZE];
   short       waveform_length[NUM_CHAN];  int                     charge[NUM_CHAN];
   int                     cfd[NUM_CHAN];  int                  timestamp[NUM_CHAN];
   int            timestamp_up[NUM_CHAN];  int                   trig_req[NUM_CHAN];
   int                trig_acc[NUM_CHAN];  int                   livetime[NUM_CHAN];  
} Tig10_event;
int                     energy[NUM_CHAN]; // energy after online gainmatching
static Tig10_event tig10_event;
static Tig10_event *ptr = &tig10_event;
static short waveform[NUM_CHAN][MAX_SAMPLES];
static int process_waveforms = 1;
static int charge_threshold = 0;
static int correlation_report_interval = 1000;
static int delta_t_energy_threshold_ge = 0;
static int delta_t_energy_threshold_si = 0;

Tig10_event *reconstruct_tigcol_event( unsigned int *evntbuf, int evntbuflen, int quiet )
{
   int type, subtype, value, trigger_num, port, collector;
   unsigned int *evntbufend = evntbuf+evntbuflen;
   short *wave_len = NULL;

   //memset(ptr, 0, CLEAR_SIZE*sizeof(int) );
   memset(ptr, 0, sizeof(Tig10_event) );
   trigger_num = *(evntbuf++);
   while( evntbuf < evntbufend ){
      if( *evntbuf == 0xdeaddead ){ ++evntbuf; continue; }
      type = *evntbuf >> 28;  subtype = (*evntbuf & 0x0f000000) >> 24; value = *(evntbuf++) & 0x0fffffff;
      switch( type ){
      case 0x0: if( wave_len == NULL ){ fprintf(stderr,"Reconstruction error 0\n"); return(NULL); }
	 if( process_waveforms ){
   	    waveform[port][*(wave_len)  ] =   value                    & 0x3fff;
	    waveform[port][*(wave_len)+1] = ((value & 0x0fffffff)>>14) & 0x3fff;
	 }
         (*wave_len) += 2; break;           /* waveform data */
      case 0x1:            break;           /*  trapeze data */
      case 0x4:                             /*      CFD Time */
         ptr->cfd[port] = value & 0x00ffffff;
         ptr->cfd_hitpattern[(int)port/32] |= (1<<(port%32));
         break;
      case 0x5:                             /*        Charge */
         if( ptr->charge_hitpattern[(int)port/32] & (1<<(port%32)) ){
            fprintf(stderr,"Event 0x%x: port %d, charge already seen - discarding\n", trigger_num, port );
            /* debug_dump_event(evntbuf, evntbuflen, 0, 0); */ return(NULL);
         }
         ptr->charge_hitpattern[(int)port/32] |= (1<<(port%32));
         ptr->charge[port] = value & 0xffff;
         if( value & 0x10000 ){      /* pileup bit of charge */
            ptr->pileup_hitpattern[(int)port/32] |= (1<<(port%32));
         }
         break;
      case 0x8: break;                      /*  Event header */
      case 0xa:
         if( subtype == 0 ){                            /*    Time Stamp */
            ptr->timestamp[port]    = value & 0x00ffffff;
	    ptr->timestamp_hitpattern[(int)port/32] |= (1<<(port%32));
         } else if( subtype == 1 ){                    /* timestamp upper bits */
	    ptr->timestamp_up[port] = value & 0x00ffffff;
	    ptr->timestamp_up_hitpattern[(int)port/32] |= (1<<(port%32));
         } else if( subtype == 2 ){                    /* livetime */
            ptr->livetime[port]     = value & 0x00ffffff;
            ptr->livetime_hitpattern[(int)port/32] |= (1<<(port%32));
         } else if( subtype == 4 ){                    /* triggers requested */
            ptr->trig_req[port]     = value & 0x00ffffff;
            ptr->trig_req_hitpattern[(int)port/32] |= (1<<(port%32));
         } else if( subtype == 8 ){                    /* accepted triggers */
            ptr->trig_acc[port]     = value & 0x00ffffff;
            ptr->trig_acc_hitpattern[(int)port/32] |= (1<<(port%32));
         }
         break;
      case 0xb: break;                     /*   Trigger Pattern */
      case 0xc:                            /*   New Channel */
         port  =  value & 0xff;  collector = (value & 0xFF00) >> 8;
         port -= 32*(port >= 0x80); /* for some reason there is a 2 card gap */
         port -=  6*(int)(port/16); /* tig10 only use 10 channels, but each cards port is 16 above prev */ 
	 port -=  60; /* start at #60 for single-tig10 system */
         if( collector > 0 ){ port += 120*(collector-1); }
         if( port < 0 || port >= NUM_CHAN ){ fprintf(stderr,"Reconstruction error 2\n"); return(NULL); }
         if( ptr->port_hitpattern[(int)port/32] & (1<<(port%32)) ){
            fprintf(stderr,"Event 0x%x: port %d already seen - discarding\n", trigger_num, port );
            /*debug_dump_event(evntbuf, evntbuflen, 0, 0);*/ return(NULL);
         }
         ptr->port_hitpattern[(int)port/32] |= (1<<(port%32));
         wave_len  = &ptr->waveform_length[port]; /* should be zero st this point */
 	 break;
      case 0xe: break;                                                       /* Event Trailer */
      case 0xf: fprintf(stderr,"Reconstruction error 3\n"); return(NULL); /* EventBuilder Timeout*/
      default:  fprintf(stderr,"Reconstruction error 4\n"); return(NULL);
      }
   }
   return(ptr); 
}

void odb_callback(INT hDB, INT hseq, void *info)
{
    charge_threshold            = cathode_param.charge_spectrum_threshold;
    process_waveforms           = cathode_param.process_waveforms;
    correlation_report_interval = cathode_param.correlation_report_interval;
}

int cathode_bor(INT run_number)
{
    charge_threshold = cathode_param.charge_spectrum_threshold;
    process_waveforms = cathode_param.process_waveforms;
    correlation_report_interval = cathode_param.correlation_report_interval;
    delta_t_energy_threshold_ge = cathode_param.delta_t_energy_threshold_ge;
    delta_t_energy_threshold_si = cathode_param.delta_t_energy_threshold_si;
    //correlation_report_reset();
    return SUCCESS;
}

extern Tig10_event *reconstruct_tigcol_event( unsigned int *evntbuf, int evntbuflen, int quiet );
static int cathode_assembled(Tig10_event *ptr);

int cathode(EVENT_HEADER *pheader, void *pevent)
{
   Tig10_event *ptr;
   DWORD *idata; 
   int bank_len;

   if ((bank_len = bk_locate(pevent, "WFDN", &idata)) > 0 ){
      return( cathode_raw(bank_len, idata) );
   } else if ((bank_len = bk_locate(pevent, "TIG0", &idata)) > 0 ){
      if( (ptr = reconstruct_tigcol_event( (unsigned int *)idata, bank_len, 1 ) )){
         return( cathode_assembled(ptr) );
      } else { return(SUCCESS); }
   } else {
      return(0);
   }
}
static int update_livetimes(int chan);
int cathode_assembled(Tig10_event *ptr)
{
   int i, j, charge_fold, cfd_fold, deltaT;
   static int prev_trigger;
   short sample;


   h_misc[0]->Fill(ptr->trigger_num, 1);
   h_misc[1]->Fill(ptr->trigger_num-prev_trigger, 1);
   prev_trigger = ptr->trigger_num;

   charge_fold = cfd_fold = 0;
   for(i=0; i<NUM_CHAN; i++){
      if( ptr->port_hitpattern        [(int)i/32] & (1<<(i%32)) ){ h_misc[2]->Fill(i, 1); }
      if( ptr->pileup_hitpattern      [(int)i/32] & (1<<(i%32)) ){ h_misc[3]->Fill(i, 1); }
      if( ptr->charge_hitpattern      [(int)i/32] & (1<<(i%32)) ){ h_misc[4]->Fill(i, 1); } else { ptr->charge[i] = -1; } /* AG: 110609 */
      if( ptr->cfd_hitpattern         [(int)i/32] & (1<<(i%32)) ){ h_misc[5]->Fill(i, 1); } else { ptr->cfd   [i] = -1; } /* AG: 110609 */
      if( ptr->timestamp_hitpattern   [(int)i/32] & (1<<(i%32)) ){ h_misc[6]->Fill(i, 1); }
      if( ptr->timestamp_up_hitpattern[(int)i/32] & (1<<(i%32)) ){ h_misc[7]->Fill(i, 1); }
      if( ptr->livetime_hitpattern    [(int)i/32] & (1<<(i%32)) ){ h_misc[8]->Fill(i, 1); update_livetimes(i); }
      if( ptr->trig_req_hitpattern    [(int)i/32] & (1<<(i%32)) ){ h_misc[9]->Fill(i, 1); }
      if( ptr->trig_acc_hitpattern    [(int)i/32] & (1<<(i%32)) ){h_misc[10]->Fill(i, 1); }
      /* hCharge[i]->Fill( ptr->charge[i]/charge_dispersion, 1 ); */ /* AG: 110609 */
      if( ptr->charge[i] > 0 ){
         hCharge[i]->Fill( ptr->charge[i]/charge_dispersion, 1 );
         h_misc[11]->Fill(i, 1); ++charge_fold;
      }
      if( ptr->cfd[i] != 0 ){ h_misc[12]->Fill(i, 1); ++cfd_fold; }
   }
   h_misc[13]->Fill(charge_fold, 1);
   h_misc[14]->Fill(cfd_fold,    1);

//    if( ptr->cfd[8] != 0 && ptr->cfd[9] != 0 ){
//       deltaT = ptr->cfd[9] - ptr->cfd[8] + 4096;
//       h_misc[24]->Fill(deltaT, 1);
//    }
   
   if( ptr->charge[0] > 0 && ptr->charge[3] > 0 )
   {
   	deltaT = ptr->cfd[0] - ptr->cfd[3] + 4096;
	h_misc[24]->Fill(deltaT,1);
   }

   if( process_waveforms ){
      for(j=0; j<NUM_CHAN; j++){
  	 if( ptr->waveform_length[j] == 0 ){ continue; }
         hRaw[j]->Reset();
         for(i=0; i<ptr->waveform_length[j] && i<MAX_SAMPLES; i++){/* crap - should be able to memcpy */
            sample = waveform[j][i];
            if( sample & (1<<13) ){ sample |= 0x3 << 14; } /* charge bit extension 14 to 16 bits */
            hRaw[j]->SetBinContent(i, (float)(sample+8192));
         }
      }
   }
   return SUCCESS;
}

int update_livetimes(int chan)
{
   static int prev_req[NUM_CHAN], prev_acc[NUM_CHAN], prev_time[NUM_CHAN];
   float fraction, rate;
   int time;

   if( prev_time[chan] == 0 ){ prev_time[chan] = ptr->timestamp[chan]; return(0); } /* need previous time for rates */
   if( (time = ptr->timestamp[chan] - prev_time[chan]) < 0 ){ time += (1<<24); } /* 24bit timestamp overflowed */
   prev_time[chan] = ptr->timestamp[chan];
   /* for now assume livetimes always come with trigreq/acc */
   /* This simplifies test, as zero is not a valid livetime, but is a valid trigger count */
   if( ptr->livetime[chan] != 0 ){
      if( ptr->livetime_hitpattern[(int)chan/32] & (1<<(chan%32)) ){
         fraction = (float)(ptr->livetime[chan]) / (float)((1<<24)-1);
         h_misc[15]->SetBinContent(chan, 100.0*     fraction  );
         h_misc[16]->SetBinContent(chan, 100.0*(1.0-fraction) );
         h_misc[17]->SetBinContent(chan,                   0  );
      } else {
         h_misc[17]->SetBinContent(chan, (h_misc[17]->GetBinContent(chan) + 1) );
      }
   }
   if( ptr->trig_req_hitpattern[(int)chan/32] & (1<<(chan%32)) ){
       h_misc[18]->SetBinContent(chan, ptr->trig_req[chan] ); 
       rate = (ptr->trig_req[chan] - prev_req[chan]) / (time/1.0e8);
       prev_req[chan] = ptr->trig_req[chan];
       h_misc[21]->SetBinContent(chan, rate );
   }
   if( ptr->trig_acc_hitpattern[(int)chan/32] & (1<<(chan%32)) ){
       h_misc[19]->SetBinContent(chan, ptr->trig_acc[chan] ); 
       rate = (ptr->trig_acc[chan] - prev_acc[chan]) / (time/1.0e8);
       prev_acc[chan] = ptr->trig_acc[chan];
       h_misc[22]->SetBinContent(chan, rate );
   }
   if( ptr->trig_req_hitpattern[(int)chan/32] & (1<<(chan%32)) && 
       ptr->trig_acc_hitpattern[(int)chan/32] & (1<<(chan%32)) ){
      if( ptr->trig_req[chan] != 0 ){
         fraction = (float)(ptr->trig_acc[chan]) / (float)(ptr->trig_req[chan]);
         h_misc[20]->SetBinContent(chan, 100.0*fraction);
      }
   }
   return(0);
}
