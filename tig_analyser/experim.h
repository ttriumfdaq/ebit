/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Fri Oct 31 13:23:31 2008

\********************************************************************/

#define EXP_PARAM_DEFINED

typedef struct {
  char      comment[128];
} EXP_PARAM;

#define EXP_PARAM_STR(_name) char *_name[] = {\
"[.]",\
"comment = STRING : [128] gammas, again",\
"",\
NULL }

#define EXP_EDIT_DEFINED

typedef struct {
  BOOL      write_data;
} EXP_EDIT;

#define EXP_EDIT_STR(_name) char *_name[] = {\
"[.]",\
"Write Data = LINK : [19] /Logger/Write data",\
"number of scans = LINK : [53] /Equipment/TITAN_ACQ/ppg cycle/begin_scan/loop count",\
"",\
NULL }

#ifndef EXCL_CATHODE

#define CATHODE_PARAM_DEFINED

typedef struct {
  INT       offset;
  INT       charge_spectrum_threshold;
  INT       process_waveforms;
  INT       correlation_report_interval;
  float     charge_dispersion;
  INT       delta_t_energy_threshold_si;
  INT       delta_t_energy_threshold_ge;
} CATHODE_PARAM;

#define CATHODE_PARAM_STR(_name) char *_name[] = {\
"[.]",\
"offset = INT : 1",\
"Charge Spectrum Threshold = INT : 10",\
"process_waveforms = INT : 1",\
"correlation_report_interval = INT : 8000",\
"Charge Dispersion = FLOAT : 1",\
"Delta T Energy Threshold Si = INT : 1000",\
"Delta T Energy Threshold Ge = INT : 300",\
"",\
NULL }

#endif

#ifndef EXCL_TIGCOL

#define TIGCOL_SETTINGS_DEFINED

typedef struct {
  INT       command;
  BOOL      block_start_stop;
  INT       use_dma_transfer;
  INT       frontend_event_assembly;
  INT       debug_event_interval;
  INT       debug_event_channels;
  INT       debug_channel_offset;
  BOOL      frontend_quiet;
  INT       trigger_mode;
  INT       prescale_factor;
  BOOL      disable_waveforms;
  BOOL      zerosuppress_ge;
  BOOL      reinitialise;
  BOOL      hardware_gainmatch;
  struct {
    INT       collector_mask;
    INT       disabled_port_mask;
    struct {
      INT       channel_enable_mask;
      INT       channel_trigger_mask;
      INT       polarity_mask;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_0;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_1;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_8;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_9;
    } card_0;
  } master;
  struct {
    INT       collector_mask;
    INT       disabled_port_mask;
    struct {
      INT       channel_enable_mask;
      INT       channel_trigger_mask;
      INT       polarity_mask;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_0;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_1;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_2;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_3;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_4;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_5;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_6;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_7;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_8;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_9;
    } card_0;
  } slave1;
} TIGCOL_SETTINGS;

#define TIGCOL_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Command = INT : 0",\
"block start stop = BOOL : n",\
"use_dma_transfer = INT : 0",\
"frontend_event_assembly = INT : 1",\
"debug_event_interval = INT : 200",\
"debug event channels = INT : 10",\
"debug channel offset = INT : 60",\
"frontend_quiet = BOOL : n",\
"Trigger Mode = INT : 2",\
"Prescale Factor = INT : 1",\
"disable_waveforms = BOOL : y",\
"zerosuppress_ge = BOOL : y",\
"Reinitialise = BOOL : n",\
"Hardware Gainmatch = BOOL : n",\
"",\
"[Master]",\
"Collector Mask = INT : 3",\
"Disabled Port Mask = INT : 65471",\
"",\
"[Master/Card_0]",\
"Channel Enable Mask = INT : 771",\
"channel trigger mask = INT : 768",\
"Polarity mask = INT : 1",\
"",\
"[Master/Card_0/Chan_0]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[Master/Card_0/Chan_1]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[Master/Card_0/Chan_8]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[Master/Card_0/Chan_9]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1]",\
"Collector Mask = INT : 0",\
"Disabled Port Mask = INT : 65471",\
"",\
"[slave1/Card_0]",\
"Channel Enable Mask = INT : 771",\
"channel trigger mask = INT : 512",\
"Polarity mask = INT : 1",\
"",\
"[slave1/Card_0/Chan_0]",\
"Hit Threshold = INT : 200",\
"Trigger Threshold = INT : 200",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 64",\
"Sample window size = INT : 256",\
"K_param = INT : 100",\
"L_param = INT : 150",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 100",\
"",\
"[slave1/Card_0/Chan_1]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/chan_2]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/chan_3]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/chan_4]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/chan_5]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/chan_6]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/chan_7]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/Chan_8]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/Chan_9]",\
"Hit Threshold = INT : 110",\
"Trigger Threshold = INT : 110",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 64",\
"Sample window size = INT : 256",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 15000",\
"Mode = INT : 12328",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 40",\
"",\
NULL }

#define TIGCOL_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TIGCOL_COMMON;

#define TIGCOL_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 2",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 50",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxebit.triumf.ca",\
"Frontend name = STRING : [32] feEbit",\
"Frontend file name = STRING : [256] feebit.c",\
"",\
NULL }

#endif

#ifndef EXCL_TITAN_ACQ

#define TITAN_ACQ_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      char      experiment_name[32];
      char      ppgload_path[50];
      char      ppg_path[50];
      char      ppg_perl_path[50];
      float     daq_service_time__ms_;
      float     standard_pulse_width__ms_;
      float     awg_clock_width__ms_;
      float     tdcblock_width__ms_;
      float     ppg_clock__mhz_;
      DWORD     polarity;
      BOOL      external_trigger;
    } input;
    struct {
      char      compiled_file_time[32];
      DWORD     compiled_file_time__binary_;
      float     time_slice__ms_;
      float     ppg_nominal_frequency__mhz_;
      float     minimal_delay__ms_;
      float     ppg_freq_conversion_factor;
      struct {
        INT       num_loops;
        double    start_loop[10];
        double    one_loop_duration[10];
        double    all_loops_end[10];
        char      loop_names[10][20];
      } loops;
    } output;
  } ppg;
} TITAN_ACQ_SETTINGS;

#define TITAN_ACQ_SETTINGS_STR(_name) char *_name[] = {\
"[ppg/input]",\
"Experiment name = STRING : [32] ebit",\
"PPGLOAD path = STRING : [50] /home/ebit/online/ppg/ppgload",\
"PPG path = STRING : [50] /home/ebit/online/ppg",\
"PPG perl path = STRING : [50] /home/ebit/online/ppg/perl",\
"DAQ service time (ms) = FLOAT : 0",\
"standard pulse width (ms) = FLOAT : 0.001",\
"AWG clock width (ms) = FLOAT : 0.001",\
"TDCBLOCK width (ms) = FLOAT : 0.001",\
"PPG clock (MHz) = FLOAT : 100",\
"Polarity = DWORD : 512",\
"external trigger = BOOL : n",\
"",\
"[ppg/output]",\
"compiled file time = STRING : [32] Fri Oct 31 13:03:46 2008",\
"compiled file time (binary) = DWORD : 1225483426",\
"Time Slice (ms) = FLOAT : 1e-05",\
"PPG nominal frequency (MHz) = FLOAT : 10",\
"Minimal Delay (ms) = FLOAT : 5e-05",\
"PPG freq conversion factor = FLOAT : 10",\
"",\
"[ppg/output/loops]",\
"num_loops = INT : 3",\
"start_loop = DOUBLE[10] :",\
"[0] 5e-05",\
"[1] 1.0001",\
"[2] 8.211649999998738",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"one_loop_duration = DOUBLE[10] :",\
"[0] 214.3116999999987",\
"[1] 7.111500000000001",\
"[2] 10",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"all_loops_end = DOUBLE[10] :",\
"[0] 2143117.000049987",\
"[1] 8.111649999998738",\
"[2] 208.2116499999987",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"loop_names = STRING[10] :",\
"[20] SCAN",\
"[20] EXTR",\
"[20] SCLR",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"",\
NULL }

#define TITAN_ACQ_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TITAN_ACQ_COMMON;

#define TITAN_ACQ_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 10",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 377",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxebit.triumf.ca",\
"Frontend name = STRING : [32] feEbit",\
"Frontend file name = STRING : [256] feebit.c",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1A_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans1;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    dummy;
  } skip1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    undummy;
  } unskip1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans3;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse10;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } time_ep11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_scan;
  struct {
    double    dummy;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
  } image;
} TITAN_ACQ_PPG_CYCLE_MODE_1A;

#define TITAN_ACQ_PPG_CYCLE_MODE_1A_STR(_name) char *_name[] = {\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 5e-05",\
"loop count = INT : 10000",\
"",\
"[trans1]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] ISACBM",\
"",\
"[begin_extr]",\
"time offset (ms) = DOUBLE : 1",\
"loop count = INT : 1",\
"time reference = STRING : [20] _TTRANS1",\
"",\
"[pulse1]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 2",\
"ppg signal name = STRING : [16] TIBG1",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 3",\
"ppg signal name = STRING : [16] TIBG2",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 4",\
"time reference = STRING : [20] _TEND_PULSE2",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] RFQEXT",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TEND_PULSE3",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CAP1",\
"",\
"[skip1]",\
"dummy = DOUBLE : 1",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] TRFER1",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 3",\
"ppg signal name = STRING : [16] TRFER2",\
"",\
"[unskip1]",\
"undummy = DOUBLE : 2",\
"",\
"[end_extr]",\
"time offset (ms) = DOUBLE : 0.0005",\
"time reference = STRING : [20] _TEND_PULSE4",\
"",\
"[trans2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] ISACBM",\
"",\
"[trans3]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] TIG10",\
"",\
"[begin_sclr]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 20",\
"time reference = STRING : [20] ",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] SCALER1",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] SCALER2",\
"",\
"[end_sclr]",\
"time offset (ms) = DOUBLE : 10",\
"time reference = STRING : [20] _TBEGSCLR",\
"",\
"[trans4]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] TIG10",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] DUMP",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] KICK1",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TSTART_PULSE10",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] KICK2",\
"",\
"[time_ep11]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] _TTIME_EP11",\
"",\
"[skip]",\
"dummy = DOUBLE : 1",\
"",\
"[image]",\
"name = STRING : [32] ebit_pc_skip_trans.gif",\
"mode = STRING : [8] 1a",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1B_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans1;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_extr;
  struct {
    double    dummy;
  } skip1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans3;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse10;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } time_ep11;
  struct {
    double    undummy;
  } unskip1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_scan;
  struct {
    double    dummy;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
  } image;
} TITAN_ACQ_PPG_CYCLE_MODE_1B;

#define TITAN_ACQ_PPG_CYCLE_MODE_1B_STR(_name) char *_name[] = {\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 5e-05",\
"loop count = INT : 10000",\
"",\
"[trans1]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] ISACBM",\
"",\
"[begin_extr]",\
"time offset (ms) = DOUBLE : 1",\
"loop count = INT : 1",\
"time reference = STRING : [20] _TTRANS1",\
"",\
"[pulse1]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 2",\
"ppg signal name = STRING : [16] TIBG1",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 3",\
"ppg signal name = STRING : [16] TIBG2",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 4",\
"time reference = STRING : [20] _TEND_PULSE2",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] RFQEXT",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TEND_PULSE3",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CAP1",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] TRFER1",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 3",\
"ppg signal name = STRING : [16] TRFER2",\
"",\
"[end_extr]",\
"time offset (ms) = DOUBLE : 0.0005",\
"time reference = STRING : [20] _TEND_PULSE6",\
"",\
"[skip1]",\
"dummy = DOUBLE : 1",\
"",\
"[trans2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] ISACBM",\
"",\
"[trans3]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] TIG10",\
"",\
"[begin_sclr]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 20",\
"time reference = STRING : [20] ",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] SCALER1",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] SCALER2",\
"",\
"[end_sclr]",\
"time offset (ms) = DOUBLE : 10",\
"time reference = STRING : [20] _TBEGSCLR",\
"",\
"[trans4]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] TIG10",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] DUMP",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] KICK1",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TSTART_PULSE10",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] KICK2",\
"",\
"[time_ep11]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"",\
"[unskip1]",\
"undummy = DOUBLE : 2",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"",\
"[skip]",\
"dummy = DOUBLE : 1",\
"",\
"[image]",\
"name = STRING : [32] ebit_pc_end_after_extr.gif",\
"mode = STRING : [8] 1b",\
"",\
NULL }

#define TITAN_ACQ_MODE_NAME_DEFINED

typedef struct {
} TITAN_ACQ_MODE_NAME;

#define TITAN_ACQ_MODE_NAME_STR(_name) char *_name[] = {\
"mode_name = STRING : [8] 1a",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1C_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans1;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans3;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse10;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } time_ep11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_scan;
  struct {
    double    dummy;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
  } image;
} TITAN_ACQ_PPG_CYCLE_MODE_1C;

#define TITAN_ACQ_PPG_CYCLE_MODE_1C_STR(_name) char *_name[] = {\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 5e-05",\
"loop count = INT : 10000",\
"",\
"[trans1]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] ISACBM",\
"",\
"[begin_extr]",\
"time offset (ms) = DOUBLE : 1",\
"loop count = INT : 1",\
"time reference = STRING : [20] _TTRANS1",\
"",\
"[pulse1]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 2",\
"ppg signal name = STRING : [16] TIBG1",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 3",\
"ppg signal name = STRING : [16] TIBG2",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 4",\
"time reference = STRING : [20] _TEND_PULSE2",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] RFQEXT",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TEND_PULSE3",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CAP1",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] TRFER1",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 3",\
"ppg signal name = STRING : [16] TRFER2",\
"",\
"[end_extr]",\
"time offset (ms) = DOUBLE : 0.0005",\
"time reference = STRING : [20] _TEND_PULSE4",\
"",\
"[trans2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] ISACBM",\
"",\
"[trans3]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] TIG10",\
"",\
"[begin_sclr]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 20",\
"time reference = STRING : [20] ",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] SCALER1",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] SCALER2",\
"",\
"[end_sclr]",\
"time offset (ms) = DOUBLE : 10",\
"time reference = STRING : [20] _TBEGSCLR",\
"",\
"[trans4]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] TIG10",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] DUMP",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] KICK1",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TSTART_PULSE10",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] KICK2",\
"",\
"[time_ep11]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] _TTIME_EP11",\
"",\
"[skip]",\
"dummy = DOUBLE : 1",\
"",\
"[image]",\
"name = STRING : [32] ebit_pc.gif",\
"mode = STRING : [8] 1c",\
"",\
NULL }

#endif

#ifndef EXCL_POLL_PPG

#define POLL_PPG_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} POLL_PPG_COMMON;

#define POLL_PPG_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 1",\
"Period = INT : 500",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxebit.triumf.ca",\
"Frontend name = STRING : [32] feEbit",\
"Frontend file name = STRING : [256] feebit.c",\
"",\
NULL }

#endif

#ifndef EXCL_SCALER

#define SCALER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} SCALER_COMMON;

#define SCALER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 377",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxebit.triumf.ca",\
"Frontend name = STRING : [32] feEbit",\
"Frontend file name = STRING : [256] feebit.c",\
"",\
NULL }

#endif

#ifndef EXCL_TRIGGER

#define TRIGGER_SETTINGS_DEFINED

typedef struct {
  INT       command;
  BOOL      block_start_stop;
  INT       use_dma_transfer;
  INT       frontend_event_assembly;
  INT       debug_event_interval;
  INT       debug_event_channels;
  INT       debug_channel_offset;
  BOOL      frontend_quiet;
  INT       trigger_mode;
  INT       prescale_factor;
  BOOL      disable_waveforms;
  BOOL      zerosuppress_ge;
  BOOL      reinitialise;
  BOOL      hardware_gainmatch;
  struct {
    INT       collector_mask;
    INT       disabled_port_mask;
    struct {
      INT       channel_enable_mask;
      INT       channel_trigger_mask;
      INT       polarity_mask;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_0;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_1;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_8;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_9;
    } card_0;
  } master;
  struct {
    INT       collector_mask;
    INT       disabled_port_mask;
    struct {
      INT       channel_enable_mask;
      INT       channel_trigger_mask;
      INT       polarity_mask;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_0;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_1;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_8;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_9;
    } card_0;
  } slave1;
} TRIGGER_SETTINGS;

#define TRIGGER_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Command = INT : 0",\
"block start stop = BOOL : n",\
"use_dma_transfer = INT : 0",\
"frontend_event_assembly = INT : 1",\
"debug_event_interval = INT : 200",\
"debug event channels = INT : 10",\
"debug channel offset = INT : 60",\
"frontend_quiet = BOOL : n",\
"Trigger Mode = INT : 2",\
"Prescale Factor = INT : 1",\
"disable_waveforms = BOOL : y",\
"zerosuppress_ge = BOOL : y",\
"Reinitialise = BOOL : n",\
"Hardware Gainmatch = BOOL : n",\
"",\
"[Master]",\
"Collector Mask = INT : 3",\
"Disabled Port Mask = INT : 65471",\
"",\
"[Master/Card_0]",\
"Channel Enable Mask = INT : 0",\
"channel trigger mask = INT : 768",\
"Polarity mask = INT : 0",\
"",\
"[Master/Card_0/Chan_0]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[Master/Card_0/Chan_1]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[Master/Card_0/Chan_8]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[Master/Card_0/Chan_9]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1]",\
"Collector Mask = INT : 0",\
"Disabled Port Mask = INT : 65471",\
"",\
"[slave1/Card_0]",\
"Channel Enable Mask = INT : 0",\
"channel trigger mask = INT : 768",\
"Polarity mask = INT : 0",\
"",\
"[slave1/Card_0/Chan_0]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/Chan_1]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/Chan_8]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/Chan_9]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
NULL }

#define TRIGGER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TRIGGER_COMMON;

#define TRIGGER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 2",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 50",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxebit.triumf.ca",\
"Frontend name = STRING : [32] FeTigCol",\
"Frontend file name = STRING : [256] fetigcol.c",\
"",\
NULL }

#endif

#ifndef EXCL_PERIODIC

#define PERIODIC_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} PERIODIC_COMMON;

#define PERIODIC_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 3",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 33",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 511",\
"Period = INT : 1000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxebit.triumf.ca",\
"Frontend name = STRING : [32] FeTigCol",\
"Frontend file name = STRING : [256] fetigcol.c",\
"",\
NULL }

#endif

