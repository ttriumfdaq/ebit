
#define EXP_PARAM_DEFINED

typedef struct {
  char      comment[128];
} EXP_PARAM;

#define EXP_PARAM_STR(_name) char *_name[] = {\
"[.]",\
"comment = STRING : [128] gammas, again",\
"",\
NULL }

#define EXP_EDIT_DEFINED

typedef struct {
  BOOL      write_data;
  INT       loop_count;
} EXP_EDIT;

#define EXP_EDIT_STR(_name) char *_name[] = {\
"[.]",\
"Write Data = LINK : [19] /Logger/Write data",\
"number of scans = LINK : [53] /Equipment/TITAN_ACQ/ppg cycle/begin_scan/loop count",\
"",\
NULL }

#ifndef EXCL_CATHODE

#define CATHODE_PARAM_DEFINED

typedef struct {
  INT       offset;
  INT       charge_spectrum_threshold;
  INT       process_waveforms;
  INT       correlation_report_interval;
  float     charge_dispersion;
  INT       delta_t_energy_threshold_si;
  INT       delta_t_energy_threshold_ge;
} CATHODE_PARAM;

#define CATHODE_PARAM_STR(_name) char *_name[] = {\
"[.]",\
"offset = INT : 1",\
"Charge Spectrum Threshold = INT : 10",\
"process_waveforms = INT : 1",\
"correlation_report_interval = INT : 8000",\
"Charge Dispersion = FLOAT : 1",\
"Delta T Energy Threshold Si = INT : 1000",\
"Delta T Energy Threshold Ge = INT : 300",\
"",\
NULL }

#endif

#ifndef EXCL_TIGCOL

#define TIGCOL_SETTINGS_DEFINED

typedef struct {
  INT       command;
  BOOL      block_start_stop;
  INT       use_dma_transfer;
  INT       frontend_event_assembly;
  INT       debug_event_interval;
  INT       debug_event_channels;
  INT       debug_channel_offset;
  BOOL      frontend_quiet;
  INT       trigger_mode;
  INT       prescale_factor;
  BOOL      disable_waveforms;
  BOOL      zerosuppress_ge;
  BOOL      reinitialise;
  BOOL      hardware_gainmatch;
  struct {
    INT       collector_mask;
    INT       disabled_port_mask;
    struct {
      INT       channel_enable_mask;
      INT       channel_trigger_mask;
      INT       polarity_mask;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_0;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_1;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_8;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_9;
    } card_0;
  } master;
  struct {
    INT       collector_mask;
    INT       disabled_port_mask;
    struct {
      INT       channel_enable_mask;
      INT       channel_trigger_mask;
      INT       polarity_mask;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_0;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_1;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_2;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_3;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_4;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_5;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_6;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_7;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_8;
      struct {
        INT       hit_threshold;
        INT       trigger_threshold;
        INT       hit_detector_clip_delay;
        INT       pre_trigger;
        INT       sample_window_size;
        INT       k_param;
        INT       l_param;
        INT       m_param;
        INT       mode;
        INT       latency;
        INT       cfd_delay;
        INT       attenuation_factor;
      } chan_9;
    } card_0;
  } slave1;
} TIGCOL_SETTINGS;

#define TIGCOL_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Command = INT : 0",\
"block start stop = BOOL : n",\
"use_dma_transfer = INT : 0",\
"frontend_event_assembly = INT : 1",\
"debug_event_interval = INT : 200",\
"debug event channels = INT : 10",\
"debug channel offset = INT : 60",\
"frontend_quiet = BOOL : n",\
"Trigger Mode = INT : 2",\
"Prescale Factor = INT : 1",\
"disable_waveforms = BOOL : y",\
"zerosuppress_ge = BOOL : y",\
"Reinitialise = BOOL : n",\
"Hardware Gainmatch = BOOL : n",\
"",\
"[Master]",\
"Collector Mask = INT : 3",\
"Disabled Port Mask = INT : 65471",\
"",\
"[Master/Card_0]",\
"Channel Enable Mask = INT : 771",\
"channel trigger mask = INT : 768",\
"Polarity mask = INT : 1",\
"",\
"[Master/Card_0/Chan_0]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[Master/Card_0/Chan_1]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[Master/Card_0/Chan_8]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[Master/Card_0/Chan_9]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 140",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1]",\
"Collector Mask = INT : 0",\
"Disabled Port Mask = INT : 65471",\
"",\
"[slave1/Card_0]",\
"Channel Enable Mask = INT : 771",\
"channel trigger mask = INT : 512",\
"Polarity mask = INT : 1",\
"",\
"[slave1/Card_0/Chan_0]",\
"Hit Threshold = INT : 200",\
"Trigger Threshold = INT : 200",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 64",\
"Sample window size = INT : 256",\
"K_param = INT : 100",\
"L_param = INT : 150",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 100",\
"",\
"[slave1/Card_0/Chan_1]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/chan_2]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/chan_3]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/chan_4]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/chan_5]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/chan_6]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/chan_7]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/Chan_8]",\
"Hit Threshold = INT : 50",\
"Trigger Threshold = INT : 50",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 164",\
"Sample window size = INT : 128",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 0",\
"Mode = INT : 14392",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 5",\
"",\
"[slave1/Card_0/Chan_9]",\
"Hit Threshold = INT : 110",\
"Trigger Threshold = INT : 110",\
"Hit Detector Clip Delay = INT : 30",\
"Pre-trigger = INT : 64",\
"Sample window size = INT : 256",\
"K_param = INT : 200",\
"L_param = INT : 280",\
"M_Param = INT : 15000",\
"Mode = INT : 12328",\
"Latency = INT : 240",\
"CFD Delay = INT : 10",\
"Attenuation factor = INT : 40",\
"",\
NULL }

#define TIGCOL_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TIGCOL_COMMON;

#define TIGCOL_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 2",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 50",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxebit.triumf.ca",\
"Frontend name = STRING : [32] feEbit",\
"Frontend file name = STRING : [256] feebit.c",\
"",\
NULL }

#endif

