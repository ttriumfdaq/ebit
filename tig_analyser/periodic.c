#include <stdio.h>
#include <string.h>
#include <time.h>

#include "midas.h"
#include "experim.h"
#include "analyzer.h"

INT periodic_event(EVENT_HEADER *, void *);
INT periodic_bor(INT run_number);
INT periodic_eor(INT run_number);

ANA_MODULE periodic_module = {
   "Periodic",                  /* module name           */
   "Urmom",                     /* author                */
   periodic_event,              /* event routine         */
   periodic_bor,                /* BOR routine           */
   periodic_eor,                /* EOR routine           */
   NULL,                        /* init routine          */
   NULL,                        /* exit routine          */
   NULL,                        /* parameter structure   */
   0,                           /* structure size        */
   NULL,                        /* initial parameters    */
};

INT periodic_bor(INT run_number){ return SUCCESS; }
INT periodic_eor(INT run_number){ return SUCCESS; }
INT periodic_event(EVENT_HEADER * pheader, void *pevent)
{
   DWORD *psclr;
   int i, size;

   if( (size = bk_locate(pevent, "SCLR", &psclr)) ){
   }
 
   return SUCCESS;
}
