#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
# Check that the keys in the default load files exist
# 
# Run the check like this:
#     check_file.pl <expt>  <filename>
#     e.g. check_file.pl bnmr 1e_defaults.odb
#
#  see AAA_README in this directory for more information
#
# original EBIT version - checks load files for mode defaults
#
#
#
my $name="check_file";
my @cmd_array;

our ($file ) = @ARGV;
my $cmd;
my $debug=0;

unless ($file)
{
    print "FAILURE: too few parameters supplied\n"; 
    die  "e.g  check_file.pl defaults.odb \n";
}

my $pwd = $ENV{"PWD"}; # get the present directory
my $exp = $ENV{"MIDAS_EXPT_NAME"};
if($debug){ print "exp=$exp and pwd=$pwd\n"; }
my $filepath = $pwd."/".$file;

if($debug){ print "$filepath\n";}
unless (-e $filepath)
{
    die "FAILURE: non-existent file \"$filepath\"\n"; 
}

$cmd="/home/ebit/online/ppg/perl/mode_check.pl /home/ebit/online/ppg/perl $exp $pwd/$file $exp";

if($debug){ print "$name cmd=\"$cmd\"\n";}

unless (open (MFC,"$cmd |"))
{
 print "$name: cannot open filehandle MFC for command: $cmd ($!)\n";
 die "$name: cannot execute $cmd";
}

@cmd_array=<MFC>;       # get the output from the command
close (MFC);
print @cmd_array;
exit;
