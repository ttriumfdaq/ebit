/********************************************************************\ 
 
  Name:         fempet.c 
  Created by:   PAA 
 
  Contents:     MCP, VT2 
 
  $Id: fempet.c 83 2007-11-06 21:15:40Z midas $ 
 
\********************************************************************/ 
#undef    VMEIO_CODE 
#undef      VT2_CODE 
#undef    DA816_CODE 
#define      PPG_CODE 
#undef     VMCP_CODE 
#undef  SOFTDAC_CODE 
#undef   SCALER_CODE 
 
#undef VF48_CODE 
#undef VERBOSE 

#undef MPET
#define EBIT
 
#include <stdio.h> 
#include <stdlib.h> 
#include <sys/stat.h> // time 
#include "midas.h" 
#include "mvmestd.h" 
#include "vmicvme.h" 
#ifdef VMEIO_CODE 
#include "vmeio.h" 
#endif 


#ifdef PPG_CODE 
#include "vppg.h" 
#include "unistd.h" // for sleep 
FILE *ppginput; 
#endif 
 
#include "experim.h" 
 
/* Interrupt vector */ 
int trig_level =  0; 
#define TRIG_LEVEL  (int) 1 
#define INT_LEVEL   (int) 3 
#define INT_VECTOR  (int) 0x16 
extern INT_INFO int_info; 
int myinfo = VME_INTERRUPT_SIGEVENT; 
INT read_titan_event(char *pevent, INT off); 
 
/* Globals */ 
extern INT run_state;  
extern char exp_name[NAME_LENGTH]; 
 
/* make frontend functions callable from the C framework */ 
#ifdef __cplusplus 
extern "C" { 
#endif 
 
/*-- Globals -------------------------------------------------------*/ 
 
/* The frontend name (client name) as seen by other MIDAS clients   */ 
char *frontend_name = "feppg"; 
/* The frontend file name, don't change it */ 
char *frontend_file_name = __FILE__; 
 
/* frontend_loop is called periodically if this variable is TRUE    */ 
BOOL frontend_call_loop = TRUE; 
 
/* a frontend status page is displayed with this frequency in ms */ 
INT display_period = 000; 
 
/* maximum event size produced by this frontend */ 
INT max_event_size = 60000; 
 
/* maximum event size for fragmented events (EQ_FRAGMENTED) */ 
INT max_event_size_frag = 5 * 1024 * 1024; 
 
/* buffer size to hold events */ 
INT event_buffer_size = 10 * 20000; 
 
/* Hardware */ 
MVME_INTERFACE *myvme; 
 
 
/* VME base address */ 
DWORD VMEIO_BASE   = 0x780000; 
DWORD VT2_BASE     = 0xE00000; 
DWORD VMCP_BASE    = 0x790000; 
DWORD VF48_BASE    = 0xA00000; 
DWORD PPG_BASE     = 0x008000; 
DWORD LRS1151_BASE = 0x7A0000; 
 
/* Globals */ 
extern HNDLE hDB; 
HNDLE hSet, hTASet; 
TRIGGER_SETTINGS ts; 
BOOL  end_of_cycle = FALSE; 
BOOL  transition_PS_requested= FALSE; 
INT   gbl_cycle; 
BOOL   debug=0; 
 
#ifdef PPG_CODE 
  // PPG 
char cmd[128]; 
char ppgfile[128]; 
BOOL ppg_running; 
char PPGpath[80]; 
char plot_cmd[132]; 
char copy_cmd[132]; 
#endif 
 
/*-- Function declarations -----------------------------------------*/ 
INT frontend_init(); 
INT frontend_exit(); 
INT begin_of_run(INT run_number, char *error); 
INT end_of_run(INT run_number, char *error); 
INT pause_run(INT run_number, char *error); 
INT resume_run(INT run_number, char *error); 
INT frontend_loop(); 
extern void interrupt_routine(void); 
 
INT read_trigger_event(char *pevent, INT off); 
INT read_scaler_event(char *pevent, INT off); 
 
#ifdef PPG_CODE 
  INT ppg_load(char *ppgfile); 
  INT tr_checkppg(INT run_number, char *error); 
  INT tr_poststart(INT run_number, char *error); 
#endif 
 
BANK_LIST trigger_bank_list[] = { 
 
   /* online banks */ 
   {"MPET", TID_DWORD,  100, NULL} , 
   {"MCPP", TID_DWORD,  100, NULL} , 
   {"FWAV", TID_DWORD, 2000, NULL} , 
   {""} 
   , 
}; 
 
/*-- Equipment list ------------------------------------------------*/ 
 
#undef USE_INT 
 
EQUIPMENT equipment[] = { 
 
   {"Trigger",               /* equipment name */ 
    {1, 0,                  /* event ID, trigger mask */ 
     "SYSTEM",               /* event buffer */ 
#ifdef USE_INT 
     EQ_INTERRUPT,           /* equipment type */ 
#else 
     EQ_PERIODIC, //POLLED,     /* equipment type */ 
#endif 
     LAM_SOURCE(0, 0x0),     /* event source crate 0, all stations */ 
     "MIDAS",                /* format */ 
     FALSE,                   /* disabled */ 
     RO_RUNNING,             /* read only when running */ 
     500,                    /* poll for 500ms */ 
     0,                      /* stop run after this event limit */ 
     0,                      /* number of sub events */ 
     0,                      /* don't log history */ 
     "", "", "",}, 
    read_trigger_event,      /* readout routine */ 
    NULL, NULL, 
    trigger_bank_list, 
    } 
   , 
 
   {"Scaler",                /* equipment name */ 
    {2, 0,                   /* event ID, trigger mask */ 
     "SYSTEM",               /* event buffer */ 
     EQ_PERIODIC ,           /* equipment type */ 
     0,                      /* event source */ 
     "MIDAS",                /* format */ 
     FALSE,                   /* enabled */ 
     RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */ 
     RO_ODB,                 /* and update ODB */ 
     10000,                  /* read every 10 sec */ 
     0,                      /* stop run after this event limit */ 
     0,                      /* number of sub events */ 
     0,                      /* log history */ 
     "", "", "",}, 
    read_scaler_event,       /* readout routine */ 
    }, 
 
  {"Titan_acq",                /* equipment name */ 
    {10, 0,                   /* event ID, trigger mask */ 
     "SYSTEM",               /* event buffer */ 
     EQ_PERIODIC ,           /* equipment type */ 
     0,                      /* event source */ 
     "MIDAS",                /* format */ 
     FALSE,                   /* enabled */ 
     RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */ 
     RO_ODB,                 /* and update ODB */ 
     10000,                  /* read every 10 sec */ 
     0,                      /* stop run after this event limit */ 
     0,                      /* number of sub events */ 
     0,                      /* log history */ 
     "", "", "",}, 
    read_titan_event,       /* readout routine */ 
    }, 
 
   {""} 
}; 
 
#ifdef __cplusplus 
} 
#endif 
 
/********************************************************************\ 
              Callback routines for system transitions 
 
  These routines are called whenever a system transition like start/ 
  stop of a run occurs. The routines are called on the following 
  occations: 
 
  frontend_init:  When the frontend program is started. This routine 
                  should initialize the hardware. 
 
  frontend_exit:  When the frontend program is shut down. Can be used 
                  to releas any locked resources like memory, commu- 
                  nications ports etc. 
 
  begin_of_run:   When a new run is started. Clear scalers, open 
                  rungates, etc. 
 
  end_of_run:     Called on a request to stop a run. Can send 
                  end-of-run event and close run gates. 
 
  pause_run:      When a run is paused. Should disable trigger events. 
 
  resume_run:     When a run is resumed. Should enable trigger events. 
\********************************************************************/ 
 
/********************************************************************/ 
 
/*-- Sequencer callback info  --------------------------------------*/ 
void seq_callback(INT hDB, INT hseq, void *info) 
{ 
  printf("odb ... trigger settings touched\n"); 
} 
 
 
/*-- Deferred transition -------------------------------------------*/ 
/* will be called repeatively  while transition is reuqested until transition is issued */ 
BOOL wait_end_cycle(int transition, BOOL first) 
{ 
  if (first) { 
    /* Will go through here the first time wait_end_cycle() is called */ 
    /* setup user flags */ 
    transition_PS_requested = TRUE; 
    /* return false as long as the requested transition should be postponed */ 
    return FALSE; 
  } 
 
  /* Check the user flags for issuing the requested transition */ 
  if (end_of_cycle)  { 
    transition_PS_requested = FALSE; 
    end_of_cycle = FALSE; 
    /* Tell system to issue transition */ 
    return TRUE; 
  } 
  else 
    /* in any other case don't do anything ==> no transition */ 
    return FALSE; 
} 
 
/*-- Frontend Init -------------------------------------------------*/ 
INT frontend_init() 
{ 
  int size, status; 
  char set_str[80]; 
  char path[80]; 
 
  /* do not register for deferred transition */ 
  //  cm_register_deferred_transition(TR_STOP, wait_end_cycle); 
  // cm_register_deferred_transition(TR_PAUSE, wait_end_cycle); 
 
  // Open VME interface 
  status = mvme_open(&myvme, 0); 
 
#ifdef PPG_CODE 
  status = cm_register_transition(TR_START, tr_checkppg, 350); 
  if(status != CM_SUCCESS) 
    { 
      cm_msg(MERROR, "frontend_init", "cannot register to transition for tr_checkppg"); 
      return status; 
    } 
  status = cm_register_transition(TR_START, tr_poststart, 600); 
  if(status != CM_SUCCESS) 
    { 
      cm_msg(MERROR, "frontend_init", "cannot register to transition for tr_poststart"); 
      return status; 
    } 
 

 
  /* Book Setting space */ 
  TITAN_ACQ_SETTINGS_STR(titan_acq_settings_str); 
 
  /* Map /equipment/Titan_acq/settings for the sequencer */ 
  sprintf(set_str, "/Equipment/TITAN_ACQ/Settings"); 
  status = db_create_record(hDB, 0, set_str, strcomb(titan_acq_settings_str)); 
  status = db_find_key (hDB, 0, set_str, &hTASet); 
  if (status != DB_SUCCESS) 
    cm_msg(MINFO,"FE","Key %s not found", set_str); 
 
  /* find path for bytecode.dat */ 
  size = sizeof(path); 
  sprintf(set_str,"ppg/input/PPGLOAD path"); 
  status = db_get_value(hDB, hTASet, set_str, path, &size, TID_STRING, FALSE); 
  if(status != DB_SUCCESS) 
  { 
    cm_msg(MERROR,"frontend_init","cannot get path at %s (%d)",set_str,status); 
    return FE_ERR_ODB; 
  } 
  printf("PPG path:%s\n",path); 
  sprintf(ppgfile,"%s/bytecode.dat",path); 
  printf("ppgfile:%s\n",ppgfile); 
 
 
  /* find path for tri_config */ 
  size = sizeof(PPGpath); 
  sprintf(set_str,"ppg/input/PPG path"); 
  status = db_get_value(hDB, hTASet, set_str, PPGpath, &size, TID_STRING, TRUE); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"frontend_init","cannot get PPG_path at %s (%d)",set_str,status); 
      return FE_ERR_ODB; 
    } 
 
  printf("PPGpath:%s  Exp name = %s \n",PPGpath, exp_name); 
  sprintf(cmd,"%s/tri_config  -e %s -s -d",PPGpath,exp_name); 
  printf("cmd:%s\n",cmd); 
 
#ifdef PLOT 
  // also set up command for gnuplots 
  sprintf(plot_cmd,"%s/perl/plot_png.pl &",PPGpath); 
#endif 
 
 
   // see if we can read something from the PPG 
  { 
    BYTE data; 
    //    DWORD pol; 
    // PPG script must control these outputs 
    VPPGPolzCtlPPG( myvme, PPG_BASE); 
    VPPGBeamCtlPPG( myvme, PPG_BASE); 
    // reverse the polarity 
    //    pol = VPPGPolmskWrite( myvme, PPG_BASE, 0xFFFF); 
    //pol = VPPGPolmskWrite( myvme, PPG_BASE, 0x0000); 
    //printf("polarity mask =  0x%x\n",pol); 
 
    VPPGStatusRead(myvme,PPG_BASE); 

    data = VPPGExtTrigRegRead(myvme, PPG_BASE); 
  } 
#endif 
  /* print message and return FE_ERR_HW if frontend should not be started */ 
  return SUCCESS; 
} 
 
/*-- Frontend Exit -------------------------------------------------*/ 
 
INT frontend_exit() 
{ 
#ifdef PPG_CODE 
 
  /*Disable the PPG module just in case */ 
  VPPGStopSequencer(myvme, PPG_BASE); 
 
#endif 
   return SUCCESS; 
} 
 
/*-- Begin of Run --------------------------------------------------*/ 
 
INT begin_of_run(INT run_number, char *error) 
{ 
  int status, size; 
  DWORD pol,rpol,external_trig; 
  char set_str[80]; 
  BYTE data;

  /* put here clear scalers etc. */ 
 
  /* read Triggger settings */ 
    /*  size = sizeof(TRIGGER_SETTINGS); 
  if ((status = db_get_record (hDB, hSet, &ts, &size, 0)) != DB_SUCCESS) 
    return status; 
    */

#ifdef PPG_CODE 
  // make sure PPG controls these outputs 
    VPPGPolzCtlPPG( myvme, PPG_BASE); 
    VPPGBeamCtlPPG( myvme, PPG_BASE);

#ifdef EBIT
  /* read the ppg polarity from settings/ppg/input  */ 
  size = sizeof(pol); 
  sprintf(set_str,"ppg/input/polarity"); 
  status = db_get_value(hDB, hTASet, set_str, &pol, &size, TID_DWORD, FALSE); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"begin_of_run","cannot read Polarity at %s (%d)",set_str,status); 
      return FE_ERR_ODB; 
    } 

  /* read the ppg trigger setting from settings/ppg/input  */ 
  size = sizeof(pol); 
  sprintf(set_str,"ppg/input/external trigger"); 
  status = db_get_value(hDB, hTASet, set_str, &external_trig, &size, TID_BOOL, FALSE); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"begin_of_run","cannot read External Trigger setting at %s (%d)",set_str,status); 
      return FE_ERR_ODB; 
    } 
  printf("external trigger: %d\n",external_trig);
  if(external_trig)
    data = VPPGEnableExtTrig( myvme, PPG_BASE);
  else
    data = VPPGDisableExtTrig ( myvme, PPG_BASE);
 
    printf("trigger control reg : 0x%x\n",data);
#else  /* MPET */
  pol =  VPPG_DEFAULT_PPG_POL_MSK;  /* use default polarity */
#endif  // EBIT
 
  /* VPPGStartSequencer used to call
  VPPGRegWrite(mvme, base_adr, VPPG_PPG_START_TRIGGER  , 0);

  Since EBIT may want to write a different polarity, 
  write the polarity prior to starting sequencer
  */
   
  printf("begin_of_run: Writing PPG polarity mask: 0x%x\n",pol);

  rpol = VPPGPolmskWrite( myvme, PPG_BASE, pol); 
  printf("                Read back polarity mask: 0x%x\n",rpol); 
 

  if(ppg_load(ppgfile) != SUCCESS) 
    return FE_ERR_HW; 
 
  /* start the PPG  */ 
  printf("Starting PPG\n"); 
  VPPGStartSequencer(myvme, PPG_BASE); 
  ppg_running = TRUE; 
#endif 
 
  printf("End of BOR\n"); 
  return SUCCESS; 
} 
 
/*-- End of Run ----------------------------------------------------*/ 
INT end_of_run(INT run_number, char *error) 
{ 
 
#ifdef PPG_CODE 
  VPPGStopSequencer(myvme,PPG_BASE); /* stops sequencer; later disable ext. trigger if used */ 
#endif 
 
#ifdef VMCP_CODE 
  // Disable the module 
  lrs1190_Disable(myvme, VMCP_BASE); 
#endif 
 
  return SUCCESS; 
} 
 
/*-- Pause Run -----------------------------------------------------*/ 
INT pause_run(INT run_number, char *error) 
{ 
  return SUCCESS; 
} 
 
/*-- Resume Run ----------------------------------------------------*/ 
INT resume_run(INT run_number, char *error) 
{ 
  /* reset flag */ 
  end_of_cycle = FALSE; 
  return SUCCESS; 
} 
 
/*-- Frontend Loop -------------------------------------------------*/ 
INT frontend_loop() 
{ 
  char str[80]; 
  BYTE value; 
  INT status; 
 
#ifdef PPG_CODE 
  if (ppg_running) 
    { 
      if (run_state == STATE_RUNNING) 
	{ 
	  value = VPPGRegRead(myvme, PPG_BASE, VPPG_VME_READ_STAT_REG ); 
	  if  (value & 2) 
	    { 
	      if (debug) 
		printf("pulse blaster IS running\n"); 
	    } 
	  else 
	    { 
	      printf("pulse blaster is not running; stopping run\n"); 
	      ppg_running = FALSE; 
	      //        if (cm_transition(TR_STOP, 0, str, sizeof(str), SYNC, 0) != CM_SUCCESS) 
	      status = cm_transition(TR_STOP, 0, str, sizeof(str), SYNC, 0); 
	      if((status !=  CM_SUCCESS) && (status != CM_DEFERRED_TRANSITION)) 
		cm_msg(MERROR, "FEloop", "cannot stop run immediately: %s (%d)", str,status); 
	    } 
	} 
    } 
  if (debug) 
    { 
      value = VPPGRegRead(myvme, PPG_BASE, VPPG_VME_READ_STAT_REG ); 
      if  ((value & 2)==0) 
	printf("pulse blaster IS NOT running\n"); 
    } 
#endif 
  return SUCCESS; 
} 
 
/*------------------------------------------------------------------*/ 
/********************************************************************\ 
 
  Readout routines for different events 
 
\********************************************************************/ 
/*-- Trigger event routines ----------------------------------------*/ 
INT poll_event(INT source, INT count, BOOL test) 
     /* Polling routine for events. Returns TRUE if event 
  is available. If test equals TRUE, don't return. The test 
  flag is used to time the polling */ 
    //    if (vmeio_CsrRead(myvme, VMEIO_BASE)) 
    //    if (lam > 10) 
{ 

#ifdef VMEIO 
  int i; 
  int lam = 0; 
  for (i = 0; i < count; i++, lam++) { 
    lam = vmeio_CsrRead(myvme, VMEIO_BASE); 
    if (lam) 
      if (!test) 
  return lam; 
  } 
#endif 
  return 0; 
} 
 
/*-- Interrupt configuration ---------------------------------------*/ 
INT interrupt_configure(INT cmd, INT source, PTYPE adr) 
{ 
  switch (cmd) { 
  case CMD_INTERRUPT_ENABLE: 
    break; 
  case CMD_INTERRUPT_DISABLE: 
    break; 
  case CMD_INTERRUPT_ATTACH: 
    break; 
  case CMD_INTERRUPT_DETACH: 
    break; 
  } 
  return SUCCESS; 
} 
 
/*-- Event readout -------------------------------------------------*/ 
INT read_trigger_event(char *pevent, INT off) 
{
  if (transition_PS_requested) {
    /* transition requested: What do we do?
       set end_of_cycle = TRUE will issue the postponed transition
       keep end_of_cycle = FALSE will deferre the transition to later */
    printf("%d Could postpone the transition now!\n", gbl_cycle++);
    if (gbl_cycle >= 1)
      end_of_cycle = TRUE;
  }
  return 0;
} 
/*-- Dummy Titan Event --------------------------------------------*/ 
INT read_titan_event(char *pevent, INT off) 
{ 
  return 0; 
} 
 
/*-- Scaler event --------------------------------------------------*/ 
INT read_scaler_event(char *pevent, INT off) 
{ 
  return 0; 
} 
 
 
#ifdef PPG_CODE 
 
INT tr_checkppg(INT run_number, char *error) 
{ 
  struct stat stbuf; 
  int status; 
  time_t timbuf; 
  char timbuf_ascii[30]; 
  int elapsed_time; 
 
 
  //  status = execl("/bin/sh", "sh", "-c", cmd, NULL); 
  //  printf("tr_checkppg: %s sent through execl (status:%d\n",cmd, status); 
 
  printf("tr_checkppg: sending system command:\"%s\" \n",cmd); 
  status =  system(cmd); 
 
  stat (ppgfile,&stbuf); 
  printf("tr_checkppg: file %s  size %d\n",ppgfile, (int)stbuf.st_size); 
 
  if(stbuf.st_size < 0) 
    { 
      cm_msg(MERROR,"tr_checkppg","PPG load file %s cannot be found",ppgfile); 
      return DB_FILE_ERROR ; 
    } 
 
  strcpy(timbuf_ascii, (char *) (ctime(&stbuf.st_mtime)) ); 
  printf ("tr_checkppg: PPG loadfile last modified:  %s or (binary) %d \n",timbuf_ascii,(int)stbuf.st_size); 
 
 
  // get present time 
  time (&timbuf); 
 
  strcpy(timbuf_ascii, (char *) (ctime(&timbuf)) ); 
  printf ("tr_checkppg: Present time:  %s or (binary) %d \n",timbuf_ascii,(int)timbuf); 
 
  elapsed_time= (int) ( timbuf - stbuf.st_mtime ); 
 
  printf("tr_precheck: time since ppg loadfile last updated: %d seconds \n",elapsed_time); 
  if(elapsed_time > 3) 
    { 
      cm_msg(MERROR,"tr_checkppg","PPG load file %s is too old",ppgfile); 
      return DB_FILE_ERROR ; 
    } 
  return SUCCESS; 
} 
 
 
INT ppg_load(char *ppgfile) 
{ 
  /* download ppg file bytecode.dat 
   */ 
 
 
  /* Stop the PPG sequencer  */ 
  VPPGStopSequencer(myvme, PPG_BASE); 
 
  /* tr_precheck checked that tri_config has run recently */ 
  if(VPPGLoad(myvme, PPG_BASE, ppgfile) != SUCCESS) 
    { 
      cm_msg(MERROR,"ppg_load","failure loading ppg with file %s",ppgfile); 
      return FE_ERR_HW; 
    } 
  printf("ppg_load: PPG file %s successfully loaded",ppgfile); 
  //  cm_msg(MINFO,"ppg_load","PPG file %s successfully loaded",ppgfile); 
  return SUCCESS; 
} 
 
 
INT tr_poststart(INT run_number, char *error) 
{ 
  INT status; 
  char copy_cmd[132]; 
  char my_exp[10];
 
  if(run_state == STATE_RUNNING) 
    { 
#ifdef PLOT 
      // cm_msg(MINFO,"tr_poststart","plotting PPG data"); 
      //printf("tr_poststart: sending system command:\"%s\" \n",plot_cmd); 
      //status =  system(plot_cmd); 
      //printf("tr_poststart: status after system command=%d\n",status); 
#endif 
#ifdef EBIT
  sprintf(my_exp,"ebit");
#else
 sprintf(my_exp,"mpet");
#endif
      sprintf(copy_cmd,"cp %s/ppgload/bytecode.dat /data/%s/info/run%d_bytecode.dat &", 
        PPGpath,my_exp,run_number); 
      status =  system(copy_cmd); 
      printf("tr_poststart: status after system command \"%s\" is %d\n",copy_cmd, status); 
 
      sprintf(copy_cmd,"cp %s/ppgload/%s.ppg /data/%s/info/run%d_%s.ppg &",PPGpath,my_exp,my_exp,run_number,my_exp); 
      status =  system(copy_cmd); 
      printf("tr_poststart: status after system command \"%s\" is %d\n",copy_cmd, status); 
 
 
    } 
  return SUCCESS; 
} 
 
#endif 
 
/* emacs 
 * Local Variables: 
 * mode:C 
 * mode:font-lock 
 * tab-width: 8 
 * c-basic-offset: 2 
 * End: 
 */ 
 
 
