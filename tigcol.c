#include <stdio.h>
#include <string.h>
#include <unistd.h>  /* for usleep */
#include "vmicvme.h"
#include "tigcol.h"

extern mvme_addr_t vmic_mapcheck (MVME_INTERFACE *mvme, mvme_addr_t vme_addr, mvme_size_t n_bytes);

#define MAX_COLLECTORS       8 /* also defined in fetigcol.c */
#define TIGCOL_ID          0x0
#define TIGCOL_BASE        (DWORD) (0xA00000+(0x10000*TIGCOL_ID))
#define MASTER_TIGCOL 0
#define RF_TIGCOL (2)          /* special */
#define SI_TIGCOL (5)          /* special */

void  TIGCOL_ParamWrite(int X, int Y, int Z, int W, int parmID, int parm);
int    TIGCOL_ParamRead(int X, int Y, int Z, int W, int parmID);
int   TIGCOL_NFrameRead(int collector);

MVME_INTERFACE *myvme;
static short *pTColReg[MAX_COLLECTORS];
static int     *pAData[MAX_COLLECTORS];

MVME_INTERFACE *TIGCOL_init()
{
   int i, window_num, region_size;
   void *map_addr;

   mvme_open(&myvme, 0);
   mvme_set_am(myvme, MVME_AM_A24_ND);
   mvme_set_dmode(myvme, MVME_DMODE_D32);
   region_size= 0x10000 * MAX_COLLECTORS; /* Map whole TIGCOL region */
   window_num = 0;
   map_addr = (void *)vmic_mapcheck(myvme, TIGCOL_BASE, region_size);
   printf("Base pointer for TIGCOL 0x%x: 0x%8x of slot %d\n",
      TIGCOL_BASE, map_addr, window_num );
   for(i=0; i<MAX_COLLECTORS; i++){    /* Setup Local Base pointers */      
      pTColReg[i] = (short *)map_addr + i*(0x10000 >> 1);
        pAData[i] = (int *)(pTColReg[i] + TIGCOL_DATA_FIFO_R);
      printf("pT10Reg[%2d]: %p - pAData1: %p\n", i, pTColReg[i], pAData[i] );
   }
   return(myvme);
}

int TIGCOL_vme_fifo_addr(int Collector_id)
{
  return( TIGCOL_BASE + (0x10000*Collector_id) + (TIGCOL_DATA_FIFO_R<<1) );
}
void TIGCOL_AcqStart()
{
   pTColReg[MASTER_TIGCOL][TIGCOL_SSET_CSR] = TIGCOL_CSR_RUNNING; return;
}
void TIGCOL_AcqStop()
{
   pTColReg[MASTER_TIGCOL][TIGCOL_SCLEAR_CSR] = TIGCOL_CSR_RUNNING; return;
}
void TIGCOL_stop_and_reset(int Collector_id)
{
   if( Collector_id == 0 ){ TIGCOL_AcqStop(); }
   pTColReg[Collector_id][TIGCOL_GENERAL_RESET] = 0;
}
#define TIMEOUT_COUNT 2000000
void TIGCOL_FlushEvents(int collector_mask)
{
   int i, j, count;
   fprintf(stdout,"flushing data ...");
   for(i=0; i<MAX_COLLECTORS; i++){
      if( ! (collector_mask & (1<<i)) ){ continue; }
      if( ! (collector_mask & 1) ){ i = 0; } /* special case: slave1 is #0 in single card setup */
      count = 0; fprintf(stdout,"%d ", i);
      while( TIGCOL_NFrameRead(i) ){
	if( ++count > TIMEOUT_COUNT ){ fprintf(stdout,"Timeout "); break; }
         j = *pAData[i];
      }
      if( ! (collector_mask & 1) ){ break; } /* special case */
   }
   fprintf(stdout,"done\n");
}
int TIGCOL_NFrameRead(int Collector_id)
{
  return (int)(pTColReg[Collector_id][TIGCOL_NFRAME_R] & TIGCOL_NFRAME_MASK);
}
int TIGCOL_NextFrame(int Collector_id){ return( *pAData[Collector_id] ); }

void TIGCOL_set_64bit_transfer(int Collector_id, int set_64bit_mode)
{
   if( set_64bit_mode ){
      pTColReg[Collector_id][TIGCOL_SSET_CSR  ] = TIGCOL_CSR_SELECT_64BIT;
   } else {
      pTColReg[Collector_id][TIGCOL_SCLEAR_CSR] = TIGCOL_CSR_SELECT_64BIT;
   }
}

void TIGCOL_set_new_trigger(int Collector_id, int final_trigger_mode, int disable_mask, int prescale_factor )
{
   int master_pattern;
   if       ( final_trigger_mode ==   0 ){ master_pattern = 0x000; /* all boxes, all triggers    */
   } else if( final_trigger_mode ==   1 ){ master_pattern = 0xffe; /* box1       all singles     */
   } else if( final_trigger_mode ==   2 ){ master_pattern = 0xffd; /* box2       singles Ge      */
   } else if( final_trigger_mode ==   3 ){ master_pattern = 0xffb; /* box3       Si anySingles   */
   } else if( final_trigger_mode ==   4 ){ master_pattern = 0xff7; /* box4       Si-Ge Coinc (was P/S Ds Si Singl) */
   } else if( final_trigger_mode ==   5 ){ master_pattern = 0xfef; /* box5  ***  Si Singles      */
   } else if( final_trigger_mode ==   6 ){ master_pattern = 0xfdf; /* box6       Si-Si Coinc     */
   } else if( final_trigger_mode ==   7 ){ master_pattern = 0xfbf; /* box7       BGO Singles     */
   } else if( final_trigger_mode ==   8 ){ master_pattern = 0xfd7; /* NOGOOD box4|6***  Si-Si|PS-Ds-Si */
   } else if( final_trigger_mode ==   9 ){ master_pattern = 0xfeb; /* box5|3***  Si Singles + pulser */
   } else if( final_trigger_mode ==  10 ){ master_pattern = 0xff9; /* box2|3     singles Ge + pulser */
   } else if( final_trigger_mode ==  11 ){ master_pattern = 0xfed; /* box2|5     singles {Ge|Si} */
   } else {
     fprintf(stdout,"Unknown Trigger Mode\n");
   }
   switch( Collector_id ){
   case 0: /* master trigger settings */ /* Updated by GH for July 2008 config */
      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     0; /* box 0: Port Disable mask */
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = disable_mask;

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     1; /* box 1 - All Singles */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    10; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0x03f; /* enable ports 7-12 */ //was 83f
      pTColReg[Collector_id][(WORD) (0x0148>>1)]  = 0xcff; /* trigtype: ports 7,8,9,12:4, port 11:1  -- was 0x33f*/

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     2; /* box 2 - Ge Singles */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    10; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0x43f; /* for July 2008 config x43f -- gh -- was a3f */
      //pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0x47f; /* TEMP CHANGE TO EXCLUDE PORT7 */

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     3; /* box 3 - Si Singles (no ring-sector-coinc) */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    10;
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xbff; /* port 11 was 0xdff        */
      pTColReg[Collector_id][(WORD) (0x0148>>1)]  = 0x300; /* trigger type 4 port 11 -- was 0x0c0 */
      //pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xfbf; /* TEMP CHANGE TO PORT7 */
      //pTColReg[Collector_id][(WORD) (0x0148>>1)]  = 0x003; /* TEMP CHANGE TO PORT7 trigger type 4 */

      //pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     4; /* box 4 - P/S Downstrm Si Singles */
      //pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    10; 
      //pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xbff; /* port 11 -- was 0xdff */
      //pTColReg[Collector_id][(WORD) (0x0148>>1)]  = 0x100; /* trigger type 2 port 11 -- was 0x040 */
      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     4; /* box 4 - Si-Ge Coincidence */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    10; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0x83f; /* ports 7,8,9,10,11 */
      pTColReg[Collector_id][(WORD) (0x0128>>1)]  = 0x400; /* require port 11 (si) */
      pTColReg[Collector_id][(WORD) (0x0130>>1)]  = 0x002; /* Multiplicity 2 */

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     5; /* box 5 - Si Singles */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    10; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xbff; /* port 11 -- was 0xdff */
      //pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xfbf; /* TEMP CHANGE TO PORT7 */

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     6; /* box 6 - Si-Si Coinc */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    10; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xbff; /* port 11 -- was 0xdff */
      pTColReg[Collector_id][(WORD) (0x0148>>1)]  = 0x200; /* trigger type 3 port 11 -- was 0x080*/

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     7; /* box 7 - BGO Singles */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    10; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xc3f; /* crate 1-4  was 0xe3f*/
      pTColReg[Collector_id][(WORD) (0x0148>>1)]  = 0x0aa; /* trigger type 3 ports 7-10 was 0x02a */

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =    16; /* box 16 .. */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    50; 
      pTColReg[Collector_id][(0x0120>>1)]         = master_pattern;

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =    17; /* box 17 .. */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    50; 
      pTColReg[Collector_id][(0x0120>>1)]         = master_pattern;

      break;
   case 5:/* slave ??? trigger settings */
      // Changes here to reflect July/August 2008 settings
      // see https://elog.triumf.ca/Tigress/TIGRESS+Experimental+Facility/1622
      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =            0; /* box 0:Port Disable mask */
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = disable_mask;
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =         0x70; /* correct pattern duration */
      pTColReg[Collector_id][(WORD) (0x0110>>1)]  =         0x30; /* correct pattern delay    */

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     1; /* box 1 S3 Silicon Sectors 1-32 */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    20; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0x0ff; /* ports 9-12 ;was cards 1-4 (ff0) */

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     2; /* box 2 S3 Silicon Rings 1-24   */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    20; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xf1f; /* ports 6-8; was cards 5-7 (f8f) */

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     3; /* box 3 S2 Silicon Sectors2 1-16 */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    20; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xfe7; /* ports 4-5; was cards  8-9 (e7f)*/

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     4; /* box 4 S2 Silicon Rings2 1-24 */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    20; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xff8; /* ports 1-3, was cards 10-12 (1ff) */

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     5; /* box 5 Any Silicon Sectors 1-32/1-16 */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    20; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0x0e7; /* was 0x078 (4-5,9-12)*/

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     6; /* box 6 Any Silicon Rings 1-24/1-24 */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    20; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xf18; /* was 0x017 (1-3,6-8)  */

      //pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     7; /* box 7 Single Silicon Card */
      //pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    20; 
      //pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xffe; /* port 1  */

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =    16; /* Any Si ring-sector coinc */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    50; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xfcf; /* 5-6:fcf */
      pTColReg[Collector_id][(WORD) (0x0130>>1)]  = 0x002; /* Multiplicity 2 */
      pTColReg[Collector_id][(WORD) (0x0138>>1)]  = prescale_factor;

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =    17; /* Dwnstrm (cd2) Si ring-sector Coinc */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    50; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xff3; /* 3-4:ff3 */
      pTColReg[Collector_id][(WORD) (0x0130>>1)]  = 0x002; /* Multiplicity 2 */
      pTColReg[Collector_id][(WORD) (0x0138>>1)]  = prescale_factor;

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =    18; /* box 18 - Sicd1-sicd2 coincidence  */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    50; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xff0; /* 1-4 ff0 */
      pTColReg[Collector_id][(WORD) (0x0130>>1)]  = 0x004; /* Multiplicity 4 */

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =    19; /* box 18 - Si Singles  */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    50; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xff0; /* 1-4 ff0 */
      //pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xfbf; /* TEMP CHANGE TO box7 */

      break;
   case 1: case 2: case 3: case 4: case 6: case 7:/* Ge/BGO slave trigger settings  wXrg XbWx RGxB */
      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =            0; /* box 0: Port Disable mask */
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = disable_mask;
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =         0x70; /* correct pattern duration */
      pTColReg[Collector_id][(WORD) (0x0110>>1)]  =         0x30; /* correct pattern delay    */

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     1; /* box 1 - Ge1 Singles */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    10; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xfd2; /* 4 Ge1 Cards */
    //pTColReg[Collector_id][(WORD) (0x0158>>1)]  = 0x012; /* read bgo1 on Ge2 trigger */
 
      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     2; /* box 2 - Ge2 Singles */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    10; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0x4bf; /* 4 Ge2 Cards */
    //pTColReg[Collector_id][(WORD) (0x0158>>1)]  = 0x480; /* read bgo2 on Ge2 trigger */

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =     3; /* box 3 - BGO Singles */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    10; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xb6d; /* 4 BGO Cards */

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =    16; /* box 16: Trigger 1 (Ge singles) */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    50; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xffc; /* 1:ffe  2:ffd  1|2:ffc  3:ffb */

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =    17; /* box 17: Trigger 2 Ge-Ge */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    50; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xffc; /* 1:ffe  2:ffd  1|2:ffc  3:ffb */
      pTColReg[Collector_id][(WORD) (0x0130>>1)]  = 0x002; /* Multiplicity 2 */

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =    18; /* box 18: BGO Singles */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    50; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xffb;

      pTColReg[Collector_id][(WORD) (0x0100>>1)]  =    19; /* box 19: BGO|Ge Singles */
      pTColReg[Collector_id][(WORD) (0x0108>>1)]  =    50; 
      pTColReg[Collector_id][(WORD) (0x0120>>1)]  = 0xff8; /* 1|2|3:ff8              */

      break;
   }
}
/* WARNING - requires odb parameter ordering to match ordering below */
/* ** port argument in natural format here (0-11) **/
void TIGCOL_setup_port_parameter(int Collector_id, int port, int chan, int *settings, int trig, int polarity, int disable_wavefm, int zerosuppress)
{
   int colr, clov, t10colchan, mode_bits, offset;
   int attenuation, tmp_mode;
   static int lastport = -1;

   colr = Collector_id; clov = 0; t10colchan = 0xe;
   if( port > 5 ){ port -= 6; clov = 1; }
   if( port+6*clov != lastport ){ /* starting new tig10 - set default */
      TIGCOL_ParamWrite(colr,clov,port,0xf, TIGCOL_MOD_BITS, 0xb100 );
      lastport = port+6*clov;
   }
   TIGCOL_ParamWrite(colr,clov,port,chan, TIGCOL_HIT_THRESHOLD,      settings[ 0] );
   TIGCOL_ParamWrite(colr,clov,port,chan, TIGCOL_TRIG_THRESHOLD,     settings[ 1] );
   TIGCOL_ParamWrite(colr,clov,port,chan, TIGCOL_HITDET_CLIP_DELAY,  settings[ 2] );
   if( ! disable_wavefm ){
      TIGCOL_ParamWrite(colr,clov,port,chan, TIGCOL_PRE_TRIGGER,        settings[ 3] );
      TIGCOL_ParamWrite(colr,clov,port,chan, TIGCOL_SAMPLE_WINDOW_SIZE, settings[ 4] );
   } else {
      TIGCOL_ParamWrite(colr,clov,port,chan, TIGCOL_PRE_TRIGGER,        0 );
      TIGCOL_ParamWrite(colr,clov,port,chan, TIGCOL_SAMPLE_WINDOW_SIZE, 4 );
   }
   TIGCOL_ParamWrite(colr,clov,port,chan, TIGCOL_PARAM_K,            settings[ 5] );
   TIGCOL_ParamWrite(colr,clov,port,chan, TIGCOL_PARAM_L,            settings[ 6] );
   TIGCOL_ParamWrite(colr,clov,port,chan, TIGCOL_PARAM_M,            settings[ 7] );
   mode_bits                                                       = settings[ 8];
   if( disable_wavefm ){ mode_bits |= 0x2; }
   tmp_mode = mode_bits & 0xff77; /* clear polarity and trigger bits */
   if( trig     ){ tmp_mode |= 0x80; }
   if( polarity ){ tmp_mode |= 0x08; }
   TIGCOL_ParamWrite(colr,clov,port,chan, TIGCOL_MOD_BITS, tmp_mode );
   TIGCOL_ParamWrite(colr,clov,port,chan, TIGCOL_LATENCY,            settings[ 9] );
   TIGCOL_ParamWrite(colr,clov,port,chan, TIGCOL_CFD_DELAY,     settings[10] );
   //TIGCOL_ParamWrite(colr,clov,port,chan, TIGCOL_ATTEN_FAC,     settings[11] );
   attenuation                                                     = settings[11];
   if( (attenuation *= 64) > 65535 ){ attenuation = 65535;
      fprintf(stdout,"Attenuation [%d] out of range col,clov,port,chan=[%d,%d,%d,%d]\n",
          attenuation, colr,clov,port,chan);
   }
   TIGCOL_ParamWrite(colr,clov,port,chan, TIGCOL_ATTEN_FAC, attenuation );
   if( ! zerosuppress ){
      TIGCOL_ParamWrite(colr,clov,port,t10colchan,  0,     1 ); /* stb#1 */
      TIGCOL_ParamWrite(colr,clov,port,t10colchan, 11, 0x3ff );
   }
   fprintf(stdout,"   Port %d chan %d ...\n", port+6*clov, chan );
   fprintf(stdout,"      HitThresh: %4d",   settings[ 0] );
   fprintf(stdout,"     TrigThresh: %4d",   settings[ 1] );
   fprintf(stdout," ClpDelay/Integ: %4d\n",   settings[ 2] );
   fprintf(stdout,"        PreTrig: %4d",   settings[ 3] );
   fprintf(stdout,"        Samples: %4d",   settings[ 4] );
   fprintf(stdout,"              K: %4d\n",   settings[ 5] );
   fprintf(stdout,"              L: %4d",   settings[ 6] );
   fprintf(stdout,"              M: %4d",   settings[ 7] );
   fprintf(stdout,"           Mode: %04x\n",     tmp_mode  );
   fprintf(stdout,"        latency: %4d",   settings[ 9] );
   fprintf(stdout," Cfd/IntegDelay: %4d",   settings[10] );
   fprintf(stdout,"    Attenuation: %4d\n\n", settings[11] );
}

void TIGCOL_read_port_parameter(int collector, int port, int chan)
{
   int clov=0, ped, htr, ttr, cpd, ptg, swn, pmk, pml, mod, lcy, atn;

   if( port > 5 ){ port -= 6; clov = 1; }
   ped = TIGCOL_ParamRead(collector, clov, port, chan, TIGCOL_PEDESTAL );
   htr = TIGCOL_ParamRead(collector, clov, port, chan, TIGCOL_HIT_THRESHOLD  );
   ttr = TIGCOL_ParamRead(collector, clov, port, chan, TIGCOL_TRIG_THRESHOLD  );
   cpd = TIGCOL_ParamRead(collector, clov, port, chan, TIGCOL_HITDET_CLIP_DELAY  );
   ptg = TIGCOL_ParamRead(collector, clov, port, chan, TIGCOL_PRE_TRIGGER  );
   swn = TIGCOL_ParamRead(collector, clov, port, chan, TIGCOL_SAMPLE_WINDOW_SIZE  );
   pmk = TIGCOL_ParamRead(collector, clov, port, chan, TIGCOL_PARAM_K  );
   pml = TIGCOL_ParamRead(collector, clov, port, chan, TIGCOL_PARAM_L  );
   mod = TIGCOL_ParamRead(collector, clov, port, chan, TIGCOL_MOD_BITS  );
   lcy = TIGCOL_ParamRead(collector, clov, port, chan, TIGCOL_LATENCY  );
   atn = TIGCOL_ParamRead(collector, clov, port, chan, TIGCOL_ATTEN_FAC  );
   fprintf(stderr,"Pedastal[port:%d][chan:%d]:%5d(0x%4x)\n", port, chan, ped, ped);
   fprintf(stderr,"HitThres[port:%d][chan:%d]:%5d(0x%4x)\n", port, chan, htr, htr);
   fprintf(stderr,"TrgThres[port:%d][chan:%d]:%5d(0x%4x)\n", port, chan, ttr, ttr);
   fprintf(stderr,"HDClpDly[port:%d][chan:%d]:%5d(0x%4x)\n", port, chan, cpd, cpd);
   fprintf(stderr,"PreTrigr[port:%d][chan:%d]:%5d(0x%4x)\n", port, chan, ptg, ptg);
   fprintf(stderr,"SamplWin[port:%d][chan:%d]:%5d(0x%4x)\n", port, chan, swn, swn);
   fprintf(stderr,"Param__k[port:%d][chan:%d]:%5d(0x%4x)\n", port, chan, pmk, pmk);
   fprintf(stderr,"Param__l[port:%d][chan:%d]:%5d(0x%4x)\n", port, chan, pml, pml);
   fprintf(stderr,"ModeBits[port:%d][chan:%d]:%5d(0x%4x)\n", port, chan, mod, mod);
   fprintf(stderr,"Latency [port:%d][chan:%d]:%5d(0x%4x)\n", port, chan, lcy, lcy);
   fprintf(stderr,"Atten   [port:%d][chan:%d]:%5d(0x%4x)\n", port, chan, atn, atn);
}

void TIGCOL_ParamWrite(int collector, int clover, int port, int chan, int parmID, int parm)
{
   int i, res, param_col_id = 0;
   if( param_col_id == 0 ){  // Single Collector card
      pTColReg[collector][TIGCOL_PARAM_ID_R] = (clover<<15 | port<<12 | chan<<8 | parmID);
      pTColReg[collector][TIGCOL_PARAM_DATA_RW] = parm;
   } else {
      pTColReg[collector][TIGCOL_PARAM_ID_R] = (clover<<15 | port<<12 | chan<<8 | 0x40 | parmID);
      pTColReg[collector][TIGCOL_PARAM_ID_R] = param_col_id;
      pTColReg[collector][TIGCOL_PARAM_DATA_RW] = parm;
   }
   // read back parameter ...
   if( port==7 ){ return; } // no readback for broadcast
   if( chan == 0xf ){
      for(i=0; i<10; i++){
	if( (res = TIGCOL_ParamRead(collector, clover, port, i, parmID )) == parm ){ continue; }
	fprintf(stderr,"ParamWriteFail: , col:%d clov:%d port:%d chan:%d parID:%2d  wrote %5d, read back %5d\n",
                collector, clover, port, i, parmID, parm, res );
      }
   } else if( (res = TIGCOL_ParamRead(collector, clover, port, chan, parmID )) != parm ) {
      fprintf(stderr,"ParamWriteFail: col:%d clov:%d port:%d chan:%d parID:%2d  wrote %5d, read back %5d\n",
              collector, clover, port, chan, parmID, parm, res );
   }
}

int TIGCOL_ParamRead(int collector, int clover, int port, int chan, int parmID)
{
   int count, ready, result;
   int param_col_id = 0;
   unsigned short val;
   if( chan == 0xf ){ printf("disabled\n"); return(0); }
   if( param_col_id==0 ){  // Single Collector card
     pTColReg[collector][TIGCOL_PARAM_ID_R] = (clover<<15 | port<<12 | chan<<8 | 0x80 | parmID);
     pTColReg[collector][TIGCOL_PARAM_DATA_RW] = 0;   // Trigger the cmd
     count=0;
     while( ! (ready = (pTColReg[collector][TIGCOL_CSR] & TIGCOL_CSR_PARAM_READY)) ){
       if( ++count > 1000 ){ printf("Param_read: Timeout\n"); break; }
     }
     val = pTColReg[collector][TIGCOL_PARAM_DATA_RW];
     result = val;
     //printf("Param_read: count=%d result=%d\n", count, result);
     return result;
   } else {
     pTColReg[collector][TIGCOL_PARAM_ID_R] = (clover<<15 | port<<12 | chan<<8 | 0xC0 | parmID);
     pTColReg[collector][TIGCOL_PARAM_ID_R] = param_col_id;
     pTColReg[collector][TIGCOL_PARAM_DATA_RW] = 0;   // Trigger the cmd
     count=0;
     while( ! (ready = (pTColReg[collector][TIGCOL_CSR] & TIGCOL_CSR_PARAM_READY)) ){
       if( ++count > 1000 ){ printf("Param_read: Timeout\n"); break; }
     } 
     val = pTColReg[collector][TIGCOL_PARAM_DATA_RW];
     result = val;
     //printf("Param_read: count=%d result=%d\n", count, result);
     return result;
   }
}














#if(0)
void TIGCOL_check_port_parameter(WORD *ptr, int ped, int ht, int tt, int cd, int pt, int ws, int pk, int pl, int mod, int lat, int atn)
{
   int col, clov, chan, offset;
   col = 0; clov = 1;
   for(chan=0; chan<10; chan++){
      if( (val = TIGCOL_ParamRead(ptr,col,clov,port,chan, TIGCOL_PEDESTAL )) != ped ){
	;
      }
      if( (val = TIGCOL_ParamRead(ptr,col,clov,port,chan, TIGCOL_HIT_THRESHOLD )) != ht ){
	;
      }
      if( (val = TIGCOL_ParamRead(ptr,col,clov,port,chan, TIGCOL_TRIG_THRESHOLD )) != tt ){
	;
      }
      if( (val = TIGCOL_ParamRead(ptr,col,clov,port,chan, TIGCOL_HITDET_CLIP_DELAY )) != cd ){
	;
      }
      if( (val = TIGCOL_ParamRead(ptr,col,clov,port,chan, TIGCOL_PRE_TRIGGER )) != pt ){
	;
      }
      if( (val = TIGCOL_ParamRead(ptr,col,clov,port,chan, TIGCOL_SAMPLE_WINDOW_SIZE )) != ws ){
	;
      }
      if( (val = TIGCOL_ParamRead(ptr,col,clov,port,chan, TIGCOL_PARAM_K )) != pk ){
	;
      }
      if( (val = TIGCOL_ParamRead(ptr,col,clov,port,chan, TIGCOL_PARAM_L )) != pl ){
	;
      }
      if( (val = TIGCOL_ParamRead(ptr,col,clov,port,chan, TIGCOL_MOD_BITS )) != mod ){
	;
      }
      if( (val = TIGCOL_ParamRead(ptr,col,clov,port,chan, TIGCOL_LATENCY )) != lat ){
	;
      }
      if( (val = TIGCOL_ParamRead(ptr,col,clov,port,chan, TIGCOL_ATTEN_FAC )) != atn ){
	;
      }
   }
}
#endif
