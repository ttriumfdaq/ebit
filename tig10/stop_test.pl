#!/usr/bin/perl -w
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
#  stop_now.pl
# 
# invoke this perlscript with cmd 
#                          param     
# stop_test.pl               200
#
#
# version for ebit
# 1 parameter : value for $param_path
#
# Stops run, sets parameter, restarts run
#
#
# $Log: stop_test.pl,v $
#
#
#
#
use strict; 
sub print_out($$$); # prototype with 3 arguments

#my $ppg_cycle_path = "Equipment/TITAN_ACQ/ppg cycle/";
#my $param_path = "end_sclr/time offset (ms)";

my $ppg_cycle_path = "Equipment/Tigcol/settings/slave1/Card_0/Chan_9/";
my $ppg_cycle_path2 = "Equipment/Tigcol/settings/slave1/Card_0/Chan_3/";
my $param_pathK = "K_param";
my $param_pathL = "L_param";
my $param_pathM = "M_param";
my $param_attn  = "Attenuation factor";

######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_PAUSED=2;  # Run state is paused
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#######################################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
# init_check uses $inc_dir, $expt, $beamline from the input parameters
#our($value) =@ARGV;

our $value;
#our($filename) =@ARGV;

our $outname = $ARGV[1];
open(DAT,$ARGV[0]) || die("Could not open file!!");
my @raw_data=<DAT>;
close(DAT); 

#our $len =  $#ARGV; # array length
our $name = "stop_test"; # same as filename
#our $nparam = 1;
our $outfile = "stop_now.txt";
our $parameter_msg = "value for \'$param_pathK\"";
our $suppress=$FALSE; # do not suppress message from open_output_file
our $valuea;
our $valueb;
our $valuec;
#########################################################
my $status;
my ($old_run, $eqp_name);
my $run_number;
my ($transition, $run_state);
my $path;
my $key;
my $count=0;
my $line;
#########################################################

$|=1; # flush output buffers


require "odb_simple.pl"; 

open(DAT,">>$outname") || die("Cannot Open Output File.");

foreach $line (@raw_data)
{
	
	chomp($line);
	($valuea,$valueb,$valuec)=split(/\ /,$line);
	
	#print  "$name:number of params: $len\n";
	#unless ( ($len+1) == $nparam) { die " supply $nparam parameter(s): $parameter_msg";}
	#print  "$name: Arguments supplied:  @ARGV\n";
	print  "$name starting with parameters: $valuea $valueb $valuec \n";
	
	
	
	unless ($valuea) 
	{
	    print_out ($name, "FAILURE: No value supplied ",$DIE); 
	}
	unless ($valueb)
	{
	    print_out ($name, "FALIURE: No value supplied ",$DIE);
	}
	unless ($valuec)
	{
	    print_out ($name, "FALIURE: No value supplied ",$DIE);
	}
	$EXPERIMENT = "test";
	
	($status, $path, $key) = odb_cmd ( "ls","/runinfo","/run number" ) ;
	unless ($status) 
	{ 
	    print_out ($name,"Error reading run number ",$DIE);
	} 
	
	$run_number = get_int ( $key);
	
	#print  "$name: Current run number = $run_number\n";
	
	# see if the run is stopped
	($run_state,$transition) = get_run_state();
	if($DEBUG) { print "After get_run_state, run_state=$run_state, trans=$transition\n"; } 
	if ($run_state == $STATE_STOPPED)
	{   # Run is stopped
	    print("$name: Run $run_number is already stopped\n");
	}
	else
	{
	#   stop the run
	    print  "Stopping run $run_number... \n";
	
	    ($status) = odb_cmd ( "stop" ) ;
	    unless ($status)
	    {
	    	print "Something strange happened.  Sending MIDAS stop.\n";
		($status) = odb_cmd ( "stop" ) ;
		exit_with_message($name);
	    }
	
	    #print_out ($name,"Stop  command has been sent",$CONT);
	
	    while ($run_state != $STATE_STOPPED)
	    {
	       $count++;
	       sleep(3);
	
	       ($run_state,$transition) = get_run_state();
	       get_run_state($transition,$run_state);
	       if($transition) 
	       { 
	           print "Run is in transition ($transition) \n"; 
		   print "Waiting for run to stop...  count= $count\n";       
	       }
	       else
	       {
		   #print "Run is not in transition; count = $count, run_state = $run_state\n";
	           last; # not in transition; not worth waiting any longer 
	       }
	
	       last if($count > 10);
	          
	   } # while
	
	    if($run_state != $STATE_STOPPED ) 
	    {
		print_out($name," Timeout: Can't stop the run ",$DIE);
	    }
	    else
	    { print_out ($name," Run $run_number is now stopped ",$CONT);}
	}
	
	# set the parameter value...
	print ("setting \"$ppg_cycle_path$param_pathK\" to $valuea...  \n");
	$value = $valuea;
	($status, $path, $key) = odb_cmd ( "set","$ppg_cycle_path","$param_pathK","$value" ) ;
	($status, $path, $key) = odb_cmd ( "set","$ppg_cycle_path","$param_attn","$value" ) ;
	unless ($status) 
	{ 
	    print_out ($name,"Error setting \'$ppg_cycle_path $param_pathK\' to $value",$DIE);
	}
	 
	print ("setting \"$ppg_cycle_path$param_pathL\" to $valueb...  \n");
	$value = $valueb;
	($status, $path, $key) = odb_cmd ( "set","$ppg_cycle_path","$param_pathL","$value" ) ;
	unless ($status) 
	{ 
	    print_out ($name,"Error setting \'$ppg_cycle_path $param_pathL\' to $value",$DIE);
	}
	
	print ("setting \"$ppg_cycle_path$param_pathM\" to $valuec...  \n");
	$value = $valuec;
	($status, $path, $key) = odb_cmd ( "set","$ppg_cycle_path","$param_pathM","$value" ) ;
	unless ($status) 
	{ 
	    print_out ($name,"Error setting \'$ppg_cycle_path $param_pathM\' to $value",$DIE);
	}
	
	print ("setting \"$ppg_cycle_path2$param_pathK\" to $valuea...  \n");
	$value = $valuea;
	($status, $path, $key) = odb_cmd ( "set","$ppg_cycle_path2","$param_pathK","$value" ) ;
	($status, $path, $key) = odb_cmd ( "set","$ppg_cycle_path2","$param_attn","$value" ) ;
	unless ($status) 
	{ 
	    print_out ($name,"Error setting \'$ppg_cycle_path2 $param_pathK\' to $value",$DIE);
	}
	 
	print ("setting \"$ppg_cycle_path2$param_pathL\" to $valueb...  \n");
	$value = $valueb;
	($status, $path, $key) = odb_cmd ( "set","$ppg_cycle_path2","$param_pathL","$value" ) ;
	unless ($status) 
	{ 
	    print_out ($name,"Error setting \'$ppg_cycle_path2 $param_pathL\' to $value",$DIE);
	}
	
	print ("setting \"$ppg_cycle_path2$param_pathM\" to $valuec...  \n");
	$value = $valuec;
	($status, $path, $key) = odb_cmd ( "set","$ppg_cycle_path2","$param_pathM","$value" ) ;
	unless ($status) 
	{ 
	    print_out ($name,"Error setting \'$ppg_cycle_path2 $param_pathM\' to $value",$DIE);
	}
	# now restart the run
	# print "Successfully set value to $value \n";
	
	
	#   start the run
	$run_number++;
	print  "Now starting run $run_number   (run_state=$run_state) \n";
	
	($status) = odb_cmd ( "start" ) ;
	unless ($status) { exit_with_message($name); }
	($run_state,$transition) = get_run_state();
	get_run_state($transition,$run_state);
	if($transition) 
	{ 
	   print "Run is in transition ($transition).. waiting for run to start \n"; 
	   sleep(5);
	}
	if($run_state != $STATE_RUNNING ) 
	{
	    print_out($name," FAILURE: Can't start the run ",$DIE);
	}
	
	print DAT "run $run_number $valuea $valueb $valuec \n";
	sleep(600); # Take a spectrum for some length of time

}

($status, $path, $key) = odb_cmd ( "ls","/runinfo","/run number" ) ;
	unless ($status) 
	{ 
	    print_out ($name,"Error reading run number ",$DIE);
	} 
	
	$run_number = get_int ( $key);
	
	#print  "$name: Current run number = $run_number\n";
	
	# see if the run is stopped
	($run_state,$transition) = get_run_state();
	if($DEBUG) { print "After get_run_state, run_state=$run_state, trans=$transition\n"; } 
	if ($run_state == $STATE_STOPPED)
	{   # Run is stopped
	    print("$name: Run $run_number is already stopped\n");
	}
	else
	{
	#   stop the run
	    print  "Stopping run $run_number... \n";
	
	    ($status) = odb_cmd ( "stop" ) ;
	    unless ($status)
	    {
	    	print "Something strange happened.  Sending MIDAS stop.\n";
		($status) = odb_cmd ( "stop" ) ;
		exit_with_message($name);
	    }
	
	    #print_out ($name,"Stop  command has been sent",$CONT);
	
	    while ($run_state != $STATE_STOPPED)
	    {
	       $count++;
	       sleep(3);
	
	       ($run_state,$transition) = get_run_state();
	       get_run_state($transition,$run_state);
	       if($transition) 
	       { 
	           print "Run is in transition ($transition) \n"; 
		   print "Waiting for run to stop...  count= $count\n";       
	       }
	       else
	       {
		   #print "Run is not in transition; count = $count, run_state = $run_state\n";
	           last; # not in transition; not worth waiting any longer 
	       }
	
	       last if($count > 10);
	          
	   } # while
	
	    if($run_state != $STATE_STOPPED ) 
	    {
		print_out($name," Timeout: Can't stop the run ",$DIE);
	    }
	    else
	    { print_out ($name," Run $run_number is now stopped ",$CONT);}
	}
exit;

