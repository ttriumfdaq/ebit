#ifndef PPGMODFILE_INCLUDE_H_
#define  PPGMODFILE_INCLUDE_H_

typedef struct
{
  char            name[80]; // name of variable, e.g. "dwell time"
  int             index; // instruction number in the file
  unsigned long   value;     // value to set instruction to
  char            data_type[15] ; // data type (one of "delay" "loopcount" ...)
} FILE_VAR;  // filled by reading mod file

// prototypes
int ppg_modify_file(char * infile, char * outfile, char *modfile);
int write_modifications (char *modfilename, char *outfile, DWORD array[], char *message);
int get_mod(char * line, FILE_VAR *mods);
void load_mod(char *mline, int *index, unsigned int *count, int *type);
int  get_next_line(char *line);
void close_files(void);
void  lineWrite (COMMAND *data_struct);
void get_message(char *string,INT len);
#endif

