/********************************************************************\ 
 
  Name:         feebit.c 
  Created by:   PAA 
  
  $Id
 
\********************************************************************/ 
 
#include <stdio.h> 
#include <stdlib.h> 
#include <sys/stat.h> // time 
#include "midas.h"

#ifndef OLDVMEDRV
#include "mvmestd.h" 
#include "vmicvme.h" 
#endif
#include "experim.h" 
 
/* Interrupt vector */ 
int trig_level =  0; 
#define TRIG_LEVEL  (int) 1 
#define INT_LEVEL   (int) 3 
#define INT_VECTOR  (int) 0x16 
#ifndef OLDVMEDRV
extern INT_INFO int_info; 
int myinfo = VME_INTERRUPT_SIGEVENT; 
#endif

/* Globals */ 
extern INT run_state;  
extern char exp_name[NAME_LENGTH]; 
 
/* make frontend functions callable from the C framework */ 
#ifdef __cplusplus 
extern "C" { 
#endif 
 
/*-- Globals -------------------------------------------------------*/ 
 
/* The frontend name (client name) as seen by other MIDAS clients   */ 
char *frontend_name = "feEbit_tig10"; 
/* The frontend file name, don't change it */ 
char *frontend_file_name = __FILE__; 
 
/* frontend_loop is called periodically if this variable is TRUE    */ 
BOOL frontend_call_loop = TRUE; 
 
/* a frontend status page is displayed with this frequency in ms */ 
INT display_period = 000; 
 
/* maximum event size produced by this frontend */
INT max_event_size = 60000;  /* CPJ tigress uses 524000 */
 
/* maximum event size for fragmented events (EQ_FRAGMENTED) */ 
INT max_event_size_frag = 5 * 1024 * 1024; 
 
/* buffer size to hold events */ 
INT event_buffer_size = 10 * 20000;  /* CPJ tigress uses 10* 524000 */
 
/* Hardware */ 
#ifndef OLDVMEDRV
MVME_INTERFACE *myvme; 
#endif

/* Globals */ 
extern HNDLE hDB; 
HNDLE hSet, hTASet; 
TIGCOL_SETTINGS ts; 
BOOL  end_of_cycle = FALSE; 
BOOL  transition_PS_requested= FALSE; 
INT   gbl_cycle; 
BOOL   debug=0; 
  
/*-- Function declarations -----------------------------------------*/ 
INT frontend_init(); 
INT frontend_exit(); 
INT begin_of_run(INT run_number, char *error); 
INT end_of_run(INT run_number, char *error); 
INT pause_run(INT run_number, char *error); 
INT resume_run(INT run_number, char *error); 
INT frontend_loop(); 
extern void interrupt_routine(void); 
 
INT read_tigcol_event(char *pevent, INT off);

extern void  TIGCOL_ParamWrite(int X, int Y, int Z, int W, int parmID, int parm);
extern int    TIGCOL_ParamRead(int X, int Y, int Z, int W, int parmID);
extern int   TIGCOL_NFrameRead(int collector);
extern int TIGCOL_init();
extern void TIGCOL_AcqStart();
extern void TIGCOL_stop_and_reset(int Collector_id);
extern void TIGCOL_set_new_trigger(int Collector_id, int final_trigger_mode, int disable_mask, int prescale_factor );
extern void TIGCOL_setup_port_parameter(int Collector_id, int port, int chan, int *settings, int trig, int polarity, int disable_wavefm, int zerosuppress);
extern void TIGCOL_set_64bit_transfer(int Collector_id, int set_64bit_mode);

BANK_LIST tigcol_bank_list[] = {
    {""},
};
 
/*-- Equipment list ------------------------------------------------*/ 
 
#undef USE_INT 
 
EQUIPMENT equipment[] = { 
 
    {"Tigcol",                /* equipment name */
     {1, 0,                   /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_POLLED,              /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING,             /* read only when running */
      50,                     /* poll for 50ms */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",}
     ,
     read_tigcol_event,      /* readout routine */
     NULL, NULL,
     tigcol_bank_list,       /* bank list */
    }
   ,  
   {""} 
}; 
 
#ifdef __cplusplus 
} 
#endif 
 
/********************************************************************\ 
              Callback routines for system transitions 
 
  These routines are called whenever a system transition like start/ 
  stop of a run occurs. The routines are called on the following 
  occations: 
 
  frontend_init:  When the frontend program is started. This routine 
                  should initialize the hardware. 
 
  frontend_exit:  When the frontend program is shut down. Can be used 
                  to releas any locked resources like memory, commu- 
                  nications ports etc. 
 
  begin_of_run:   When a new run is started. Clear scalers, open 
                  rungates, etc. 
 
  end_of_run:     Called on a request to stop a run. Can send 
                  end-of-run event and close run gates. 
 
  pause_run:      When a run is paused. Should disable trigger events. 
 
  resume_run:     When a run is resumed. Should enable trigger events. 
\********************************************************************/ 
 
/********************************************************************/ 

TIGCOL_SETTINGS ts;              /* Tigress odb Settings */
HNDLE   hSet, hKey, hGE, hBGO, hKeyPosition, hEPM, hEPD;
extern HNDLE hDB;

static int collector_mask; /* referenced in event polling, only updated from odb on start of run */

#define MAX_COLLECTOR 8 /* allow up to 6 collector cards per crate *//* also defined in tigcol.c */
#define NO_RESET 0
#define RESET    1

//extern void assemble_init(int, int);
extern void assemble_init(int, int, int);
extern int dma_read_tigcol_event(char *pevent, int collector, int event_assembly,
                                 int debug_interval, int suppress_error_messages);
extern int pio_read_tigcol_event(char *pevent, int collector, int event_assembly,
                                 int debug_interval, int suppress_error_messages);

int TIGCOL_FlushEvents(int collector_mask );


/* need to change the odb setting structures by hand if odb entries renamed/removed */
#define TSGE (int *)&ts.master.ge
#define TSLV (int *)&ts.master
#define TS   (int *)&ts
#define NUM_SLAVE  1
#define NUM_CARDS  1 /* Max per slave, not total */
#define NUM_PORT  12
#define NUM_DET    2
#define NUM_CHAN 120

static int   *slave_address[NUM_SLAVE+1] = {(int *)&ts.master, (int *)&ts.slave1};
static int card_offset = (int *)&ts.master.card_0 - (int *)&ts.master.card_0; /* only 1 card at the moment */
static int chan_offset = (int *)&ts.master.card_0.chan_1 - (int *)&ts.master.card_0.chan_0;
/*-- Sequencer callback info  --------------------------------------*/ 
void seq_callback(INT hDB, INT hseq, void *info) 
{ 
  printf("odb ... trigger settings touched\n"); 
} 

/*-- Frontend Init -------------------------------------------------*/
#ifndef OLDVMEDRV
int mvme_init  (void);/* vmicvme.c */
#endif
INT frontend_init() 
{ 
  int size, status; 
  char set_str[80]; 
  char path[80]; 
 
   printf ("frontend_init: starting\n");
   TIGCOL_SETTINGS_STR(tigcol_settings_str);
   sprintf(set_str, "/Equipment/Tigcol/settings");
   status = db_create_record(hDB, 0, set_str, strcomb(tigcol_settings_str));
   status = db_find_key (hDB, 0, set_str, &hSet);
   if (status != DB_SUCCESS){ cm_msg(MINFO,"FE","Key %s not found", set_str); }
#ifndef OLDVMEDRV
   if( (myvme = TIGCOL_init()) != NULL ){
#else
   mvme_init();
   if( TIGCOL_init() >= 0 ){
#endif
      TIGCOL_stop_and_reset( 0 );
   } else {
      cm_msg(MERROR, "frontend_init", "can't initialse tigcol"); 
      return(-1);
   }
   return SUCCESS; 
} 
 
/*-- Frontend Exit -------------------------------------------------*/ 
 
INT frontend_exit() 
{ 
   return SUCCESS; 
} 
 
/*-- Begin of Run --------------------------------------------------*/ 
 
INT begin_of_run(INT run_number, char *error) 
{ 
   int status;
   status = begin_of_run_tigcol(run_number, error);
   return status;
}

/* WARNING - the following uses slave1 and trigger1 ordering for all slaves and triggers  */
/* WARNING - this relies on identical odb entries in each slave/trigger                   */
/* WARNING - if any keys are deleted/reordered strange and incorrect behaviour will occur */
static int block_start_stop, use_dma_transfer, frontend_event_assembly,
   debug_event_interval, suppress_error_messages, hardware_gainmatch;
INT begin_of_run_tigcol(INT run_number, char *error)
{
   int i, slave_offset, trigger_mode, disable_waveforms, zerosuppress;
   int prescale_factor, port, chan, size, status, trig, polarity;
   int *chan_mask_ptr, *trig_mask_ptr, *polarity_mask_ptr;
   int *odb_address, *disabled_port_mask_ptr;
   int current_card_offset, current_chan_offset;

   size = sizeof(TIGCOL_SETTINGS); /* read Triggger settings */
   if( (status = db_get_record (hDB, hSet, &ts, &size, 0)) != DB_SUCCESS ){ return status; }
   use_dma_transfer          = 0;
   frontend_event_assembly   = ts.frontend_event_assembly;
   debug_event_interval      = ts.debug_event_interval;
   suppress_error_messages   = ts.frontend_quiet;
   trigger_mode              = ts.trigger_mode;
   prescale_factor           = ts.prescale_factor;
   disable_waveforms         = ts.disable_waveforms;
   collector_mask            = ts.master.collector_mask;
   zerosuppress              = ts.zerosuppress_ge;

   //assemble_init( ts.debug_event_channels, ts.debug_channel_offset );
   assemble_init( ts.debug_event_channels, ts.debug_channel_offset, 16 );
   disabled_port_mask_ptr =  (int *)&ts.master.disabled_port_mask;
   chan_mask_ptr          =  (int *)&ts.master.card_0.channel_enable_mask;
   trig_mask_ptr          =  (int *)&ts.master.card_0.channel_trigger_mask;
   polarity_mask_ptr      =  (int *)&ts.master.card_0.polarity_mask;
   for(i=0; i<=NUM_SLAVE; i++){
      if( ! (collector_mask & (1<<i)) ){ continue; }
      if( i ){ printf("Writing Slave %d Parameters ...\n", i ); }
      else   { printf("Writing Master Parameters ...\n");       }
      slave_offset = slave_address[i] - slave_address[0];
      TIGCOL_set_new_trigger(i, trigger_mode, disabled_port_mask_ptr[slave_offset], prescale_factor );
      if( i == 0 ){ continue; }
      current_card_offset = 0;
      for(port=0; port<12; port++){ /* only works with 1 port at moment - "card0" is used */
         if( disabled_port_mask_ptr[slave_offset] & (1<<port) ){ continue; }
         current_chan_offset = 0;
         for(chan=0; chan<10; chan++){
  	    current_chan_offset = chan*chan_offset;
	    if( ! (chan_mask_ptr[slave_offset + current_card_offset] & (1<<chan)) ){ continue; }
            trig     = ((    trig_mask_ptr[slave_offset + current_card_offset] & (1<<chan)) == 0 ); /* is trig enable */
            polarity = ((polarity_mask_ptr[slave_offset + current_card_offset] & (1<<chan)) > 0 );
            odb_address = (int *)&ts.master.card_0.chan_0.hit_threshold + slave_offset +
                                             current_card_offset + current_chan_offset;
 	    TIGCOL_setup_port_parameter(i, port, chan, odb_address, trig, polarity, disable_waveforms, zerosuppress );
	 }
         current_card_offset += card_offset;
      }
      TIGCOL_set_64bit_transfer(i, use_dma_transfer );
   }
   printf("done ... ");
   TIGCOL_AcqStart(); /* always to board #0 */
   printf("running\n");

   return( FE_SUCCESS );
}

/*-- End of Run ----------------------------------------------------*/ 
INT end_of_run(INT run_number, char *error) 
{ 
   TIGCOL_AcqStop();/* can disable start/stop for scan */
   TIGCOL_FlushEvents( collector_mask );

  return SUCCESS; 
} 
 
/*-- Pause Run -----------------------------------------------------*/ 
INT pause_run(INT run_number, char *error) 
{ 
  return SUCCESS; 
} 
 
/*-- Resume Run ----------------------------------------------------*/ 
INT resume_run(INT run_number, char *error) 
{ 
  /* reset flag */ 
  end_of_cycle = FALSE; 
  return SUCCESS; 
} 
 
/*-- Frontend Loop -------------------------------------------------*/ 
INT frontend_loop() 
{ 
  return SUCCESS; 
} 
 
/*------------------------------------------------------------------*/ 
/********************************************************************\ 
 
  Readout routines for different events 
 
\********************************************************************/ 
/*-- Trigger event routines ----------------------------------------*/ 
#define MINIMUM_EVENT_LENGTH 8 // Head,Pat,chan,Ts1,Ts2,E,T,Trail  was 13 for some reason?
static int poll_words, poll_collector, nodata_count, call_flush_event;
INT poll_event(INT source, INT count, BOOL test) 
{ 
   int i, j=0, nevt=0;
   for(i = 0; i < count; i++){
     if( !(collector_mask & 1) ){ /* single card system */
	 nevt = TIGCOL_NFrameRead(0);
      } else {
         for(j=1; j<MAX_COLLECTOR; j++){
            if( ! (collector_mask & (1<<j)) ){ continue; }
            if( (nevt = TIGCOL_NFrameRead(j)) >= MINIMUM_EVENT_LENGTH ){ break; }
         }
      }
      if( nevt >= MINIMUM_EVENT_LENGTH && !test ){
         call_flush_event = 0; nodata_count = 0; poll_words = nevt; poll_collector = j;
         return(1);
      }
   }
   if( !test ){
     if( ++nodata_count < 10 ){ return(0); } // no data at least 10 times before flushing
     //if( flush_event_available() ){ call_flush_event = 1; return(1); }
   }
   return(0);
} 
 
/*-- Interrupt configuration ---------------------------------------*/ 
INT interrupt_configure(INT cmd, INT source, PTYPE adr) 
{ 
  switch (cmd) { 
  case CMD_INTERRUPT_ENABLE: 
    break; 
  case CMD_INTERRUPT_DISABLE: 
    break; 
  case CMD_INTERRUPT_ATTACH: 
    break; 
  case CMD_INTERRUPT_DETACH: 
    break; 
  } 
  return SUCCESS; 
} 
 
/*-- Event readout -------------------------------------------------*/ 

INT read_tigcol_event(char *pevent, INT off)
{
   static int next_collector = 1;
   int i, collector, count[MAX_COLLECTOR];
   for(i=0; i<MAX_COLLECTOR; i++){ count[i] = -2; }

   if( call_flush_event ){ // special case for low data rates, when electronics empty ...
      return( flush_one_event(pevent, debug_event_interval, suppress_error_messages) );
   }

   //fprintf(stdout,"starting readout at port %d ... ", next_collector);
   //next_collector = 1;
   for(i=0; i<MAX_COLLECTOR; i++){
      if( (collector = i+next_collector) >= MAX_COLLECTOR ){ collector -= MAX_COLLECTOR; }
      if( collector == 0 ){ continue; }
      if( ! (collector_mask & (1<<collector)) ){ continue; }
      if( !(collector_mask & 1) ){ collector = 0; i = MAX_COLLECTOR; } /* single card system */
      if( (count[collector]=TIGCOL_NFrameRead(collector)) < MINIMUM_EVENT_LENGTH ){ continue; }
      if( (next_collector = collector+1) >= MAX_COLLECTOR){ next_collector = 0; }
      //fprintf(stdout,"port %4d : %8d words\n", collector, count);
      if( use_dma_transfer ){
         return( dma_read_tigcol_event(pevent,collector,frontend_event_assembly,
                                      debug_event_interval,suppress_error_messages) );
      } else {
         return( pio_read_tigcol_event(pevent,collector,frontend_event_assembly,
				  debug_event_interval,suppress_error_messages) );
      }
   }
   fprintf(stdout,"no event! poll showed %d words in collector %d, here showed %d words\n",
           poll_words, poll_collector, count[poll_collector] );
   return(0);
}
/* emacs 
 * Local Variables: 
 * mode:C 
 * mode:font-lock 
 * tab-width: 8 
 * c-basic-offset: 2 
 * End: 
 */ 
 
 
