#include <stdio.h>
#include <string.h>  /* memset */

#define MAX_SAMPLES     512
#define NUM_CHAN        600
#define NUM_EVBUF       128
#define MAX_PORT_OFFSET   7
#define MAX_COLLECTORS    6
#define HITPAT_SIZE ( (int)((NUM_CHAN+31)/32) )
#define CLEAR_SIZE ( 1 + 9*HITPAT_SIZE ) /* need to zero the 8 hitpatterns and trigger number */

typedef struct tig10_event_struct {
   int          trigger_num;               int         port_hitpattern[HITPAT_SIZE];
   int    charge_hitpattern[HITPAT_SIZE];  int          cfd_hitpattern[HITPAT_SIZE];
   int timestamp_hitpattern[HITPAT_SIZE];  int timestamp_up_hitpattern[HITPAT_SIZE];
   int  trig_req_hitpattern[HITPAT_SIZE];  int     trig_acc_hitpattern[HITPAT_SIZE];
   int    pileup_hitpattern[HITPAT_SIZE];  int     livetime_hitpattern[HITPAT_SIZE];
   int                  charge[NUM_CHAN];  int                        cfd[NUM_CHAN];
   int               timestamp[NUM_CHAN];  int               timestamp_up[NUM_CHAN];
   int                trig_req[NUM_CHAN];  int                   trig_acc[NUM_CHAN];
   int                livetime[NUM_CHAN];  short          waveform_length[NUM_CHAN];
} Tig10_event;
static Tig10_event tig10_event;
static Tig10_event *ptr = &tig10_event;

extern int print_chan_offset, num_print_chan;

int print_reconstructed_event(Tig10_event *ptr, int, int);

/* most errors in recontruction should already have been caught in event check */
/* so don't need to retain full error messages here                            */
Tig10_event *reconstruct_tigcol_event( unsigned int *evntbuf, int evntbuflen, int quiet )
{
   unsigned int *evntbufend = evntbuf+evntbuflen;
   int trigger_num, type, subtype, value, port, collector;
   short *wave_len = NULL;

   memset(ptr, 0, CLEAR_SIZE*sizeof(int) );
   memset(ptr->waveform_length, 0, NUM_CHAN*sizeof(short) );
   ptr->trigger_num = trigger_num = *(evntbuf++);
   while( evntbuf < evntbufend ){
      if( *evntbuf == 0xdeaddead ){ ++evntbuf; continue; }
      type = *evntbuf >> 28;  subtype = (*evntbuf & 0xf000000) >> 24; value = *(evntbuf++) & 0xffffff;
      switch( type ){
      case 0x0: if( wave_len == NULL ){ fprintf(stderr,"Reconstruction error 0\n"); return(NULL); }
         (*wave_len) += 2; break;           /* waveform data */
      case 0x1:            break;           /*  trapeze data */
      case 0x4:                             /*      CFD Time */
         ptr->cfd[port] = value & 0x00ffffff;
         ptr->cfd_hitpattern[(int)port/32] |= (1<<(port%32));
         break;
      case 0x5:                             /*        Charge */
         if( ptr->charge_hitpattern[(int)port/32] & (1<<(port%32)) ){
            fprintf(stderr,"Event 0x%x: port %d, charge already seen - discarding\n", trigger_num, port );
            /* debug_dump_event(evntbuf, evntbuflen, 0, 0); */ return(NULL);
         }
         ptr->charge_hitpattern[(int)port/32] |= (1<<(port%32));
         ptr->charge[port] = value & 0xffff;
         if( value & 0x10000 ){      /* pileup bit of charge */
            ptr->pileup_hitpattern[(int)port/32] |= (1<<(port%32));
         }
         break;
      case 0x8: break;                      /*  Event header */
      case 0xa:
         if( subtype == 0 ){                            /*    Time Stamp */
            ptr->timestamp[port]    = value & 0x00ffffff;
	    ptr->timestamp_hitpattern[(int)port/32] |= (1<<(port%32));
         } else if( subtype == 1 ){                    /* timestamp upper bits */
	    ptr->timestamp_up[port] = value & 0x00ffffff;
	    ptr->timestamp_up_hitpattern[(int)port/32] |= (1<<(port%32));
         } else if( subtype == 2 ){                    /* livetime */
            ptr->livetime[port]     = value & 0x0fffffff;
            ptr->livetime_hitpattern[(int)port/32] |= (1<<(port%32));
         } else if( subtype == 4 ){                    /* triggers requested */
            ptr->trig_req[port]     = value & 0x0fffffff;
            ptr->trig_req_hitpattern[(int)port/32] |= (1<<(port%32));
         } else if( subtype == 8 ){                    /* accepted triggers */
            ptr->trig_acc[port]     = value & 0x0fffffff;
            ptr->trig_acc_hitpattern[(int)port/32] |= (1<<(port%32));
         }
         break;
      case 0xb: break;                     /*   Trig Pattern */
      case 0xc:                            /*   New Channel */
         port  =  value & 0xff;  collector = (value & 0xFF00) >> 8;
         port -= 32*(port >= 0x80); /* for some reason there is a 2 card gap */
         port -=  6*(int)(port/16); /* tig10 only use 10 channels, but each cards port is 16 above prev */ 
         if( collector > 0 ){ port += 120*(collector-1); }
         if( port < 0 || port >= NUM_CHAN ){
            fprintf(stderr,"Reconstruction error 2\n"); return(NULL);
         }
         if( ptr->port_hitpattern[(int)port/32] & (1<<(port%32)) ){
            fprintf(stderr,"Event 0x%x: port %d already seen - discarding\n", trigger_num, port );
            /*debug_dump_event(evntbuf, evntbuflen, 0, 0);*/ return(NULL);
         }
         ptr->port_hitpattern[(int)port/32] |= (1<<(port%32));
         wave_len  = &ptr->waveform_length[port]; /* should be zero at this point */
 	 break;
      case 0xe: break;                                                       /* Event Trailer */
      case 0xf: fprintf(stderr,"Reconstruction error 3\n"); return(NULL); /* EventBuilder Timeout*/
      default:  fprintf(stderr,"Reconstruction error 4\n"); return(NULL);
      }
   }
   print_reconstructed_event(ptr, num_print_chan, print_chan_offset);
   return(ptr); 
}

#define CHECK_COLUMNS 30

char *binstring ( int *data, int prec, int offs );
char *separator(int chan);

int print_reconstructed_event(Tig10_event *ptr, int num_print_chan, int print_chan_offset )
{
   char val[16];
   int i, j, k;
   fprintf(stdout,"Trigger:%12d 0x%8x\n", ptr->trigger_num, ptr->trigger_num );
   fprintf(stdout,"          port[%s]\n", binstring( ptr->port_hitpattern,         num_print_chan, print_chan_offset ) );
   fprintf(stdout,"        pileup[%s]\n", binstring( ptr->pileup_hitpattern,       num_print_chan, print_chan_offset ) );
   fprintf(stdout,"        charge[%s]\n", binstring( ptr->charge_hitpattern,       num_print_chan, print_chan_offset ) );
   fprintf(stdout,"           cfd[%s]\n", binstring( ptr->cfd_hitpattern,          num_print_chan, print_chan_offset ) );
   fprintf(stdout,"     timestamp[%s]\n", binstring( ptr->timestamp_hitpattern,    num_print_chan, print_chan_offset ) );
   fprintf(stdout,"  timestamp hi[%s]\n", binstring( ptr->timestamp_up_hitpattern, num_print_chan, print_chan_offset ) );
   fprintf(stdout,"      livetime[%s]\n", binstring( ptr->livetime_hitpattern,     num_print_chan, print_chan_offset ) );
   fprintf(stdout,"      trig_req[%s]\n", binstring( ptr->trig_req_hitpattern,     num_print_chan, print_chan_offset ) );
   fprintf(stdout,"      trig_acc[%s]\n", binstring( ptr->trig_acc_hitpattern,     num_print_chan, print_chan_offset ) );
   fprintf(stdout,"       \033[30mCharges ...%s", separator(0) );
   for(i=0; i<num_print_chan; i++){ j = i + print_chan_offset; k = ptr->charge_hitpattern[(int)j/32] & (1<<(j%32));
      if( k ){ sprintf(val,"%8d", ptr->charge[j]); } else { sprintf(val,"    .   "); }
      fprintf(stdout," %8s%s", val, separator(i+1) );
   }
   if( (i%CHECK_COLUMNS) != 0 ){ fprintf(stdout,"\n"); }
   fprintf(stdout,"       \033[30mCFD Times ...%s", separator(0) );
   for(i=0; i<num_print_chan; i++){ j = i + print_chan_offset; k = ptr->cfd_hitpattern[(int)j/32] & (1<<(j%32));
      if( k ){ sprintf(val,"%8x", ptr->cfd[j]); } else { sprintf(val,"    .   "); }
      fprintf(stdout," %8s%s", val, separator(i+1) );
   }
   if( (i%CHECK_COLUMNS) != 0 ){ fprintf(stdout,"\n"); }
   fprintf(stdout,"       \033[30mTimestamps ...%s", separator(0) );
   for(i=0; i<num_print_chan; i++){ j = i + print_chan_offset; k = ptr->timestamp_hitpattern[(int)j/32] & (1<<(j%32));
      if( k ){ sprintf(val,"%8x", ptr->timestamp[j]); } else { sprintf(val,"    .   "); }
      fprintf(stdout," %8s%s", val, separator(i+1) );
   }
   if( (i%CHECK_COLUMNS) != 0 ){ fprintf(stdout,"\n"); }
   /*   fprintf(stdout,"       \033[30mTimestamp upper bits ...%s", separator(0) );
   for(i=0; i<num_print_chan; i++){
      fprintf(stdout," %8x%s", ptr->timestamp_up[i+print_chan_offset], separator(i+1) );
   }
   if( (i%CHECK_COLUMNS) != 0 ){ fprintf(stdout,"\n"); }*//*
   fprintf(stdout,"       \033[30mLivetimes ...%s", separator(0) );
   for(i=0; i<num_print_chan; i++){ j = i + print_chan_offset; k = ptr->livetime_hitpattern[(int)j/32] & (1<<(j%32));
      if( k ){ sprintf(val,"%8d", ptr->livetime[j]); } else { sprintf(val,"    .   "); }
      fprintf(stdout," %8s%s", val, separator(i+1) );
   }
   *//* if( (i%CHECK_COLUMNS) != 0 ){ fprintf(stdout,"\n"); }
   fprintf(stdout,"       \033[30mTrigger Requested ...%s", separator(0) );
   for(i=0; i<num_print_chan; i++){
      fprintf(stdout," %8x%s", ptr->trig_req[i+print_chan_offset], separator(i+1) );
   }
   if( (i%CHECK_COLUMNS) != 0 ){ fprintf(stdout,"\n"); }
   fprintf(stdout,"       \033[30mTrigger accepted ...%s", separator(0) );
   for(i=0; i<num_print_chan; i++){
      fprintf(stdout," %8x%s", ptr->trig_acc[i+print_chan_offset], separator(i+1) );
   }
   if( (i%CHECK_COLUMNS) != 0 ){ fprintf(stdout,"\n"); }
   */fprintf(stdout,"       \033[30mWave Lengths ...%s", separator(0) );
   for(i=0; i<num_print_chan; i++){ j = i + print_chan_offset; k = ptr->waveform_length[j];
      if( k ){ sprintf(val,"%8d", k); } else { sprintf(val,"    .   "); }
      fprintf(stdout," %8s%s", val, separator(i+1) );
   }
   if( (i%CHECK_COLUMNS) != 0 ){ fprintf(stdout,"\n"); }
   fprintf(stdout,"\033[30m------------------------------------------\n");
   return(0);
}

char *separator(int chan)
{
   static char result[32];
   if( (chan%10) != 0 ){ return(""); }
   switch((int)(chan/10)){
   case 0: case  6: case 12: case  18: case 24: case 30: sprintf(result,"\n         \033[34m"); break;
   case 1: case  7: case 13: case  19: case 25: case 31: sprintf(result,"\n         \033[33m"); break;
   case 2: case  8: case 14: case  20: case 26: case 32: sprintf(result,"\n         \033[32m"); break;
   case 3: case  9: case 15: case  21: case 27: case 33: sprintf(result,"\n         \033[31m"); break;
   case 4: case 10: case 16: case  22: case 28: case 34: sprintf(result,"\n         \033[33m"); break;
   case 5: case 11: case 17: case  23: case 29: case 35: sprintf(result,"\n         \033[30m"); break;
   default:                                              sprintf(result,"\n         \033[35m"); break;
   }
   if( (chan%CHECK_COLUMNS) == 0 ){ return( result ); }
   return( result+1 ); /* skip newline */
}

/* i:  0(0x00000000) ~i:  -1(0xffffffff), i:256(0x00000100) ~i:-257(0xfffffeff)*/
/* set bit N: |= (1<<n) clear bit N: &= ~(1<<N)                                */
#define MAX_BINSTR_PREC 2048
#define NROWS 2
char *binstring(int *data, int prec, int offset)
{
   static char result[MAX_BINSTR_PREC];
   int i, j, k, m;

   k = 0; memcpy(&result[k], "\033[34m", 5); k+=5;
   for(m=0; m<NROWS; m++){
      if( m > 0 ){ memcpy(&result[k], "\033[30m]\n              [\033[34m", 27); k+=27;}
      for(i=0; i<prec && k<MAX_BINSTR_PREC; i+=NROWS){
         j = i+m+offset; result[k++] = ( data[j/32] & (1<<(j%32)) ) ? '1' : '.';
         if( (((i/NROWS)+1)%5) != 0 ){ continue; }
    	 result[k++] = ( (i/NROWS+1) % 30 ) ? ' ' : '|'; /* print extra space + change colour */
	 switch((int)(((i/NROWS)+1)/5)){
         case 0: case  6: case 12: case  18: case 24: case 30: memcpy(&result[k], "\033[34m", 5); k+=5; break;
         case 1: case  7: case 13: case  19: case 25: case 31: memcpy(&result[k], "\033[33m", 5); k+=5; break;
         case 2: case  8: case 14: case  20: case 26: case 32: memcpy(&result[k], "\033[32m", 5); k+=5; break;
         case 3: case  9: case 15: case  21: case 27: case 33: memcpy(&result[k], "\033[31m", 5); k+=5; break;
         case 4: case 10: case 16: case  22: case 28: case 34: memcpy(&result[k], "\033[33m", 5); k+=5; break;
         case 5: case 11: case 17: case  23: case 29: case 35: memcpy(&result[k], "\033[30m", 5); k+=5; break;
         default:         memcpy(&result[k], "\033[35m", 5); k+=5; break;
         }
      }
   }
   memcpy(&result[k], "\033[30m\0", 5);
   return( result );
}
